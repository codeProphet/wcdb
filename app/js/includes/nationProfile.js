    
    //declaration of global variable
      //var nation = sessionStorage.getItem("nation"); 
      
      function getid(){
                  var str=window.location.href;
                  var idArr= str.split("/");
                  var l=idArr.length;
                  return idArr[l-1];
                }
      var nation=getid();

      $(function(){

        $('#nationHome').html(`
          <a  class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
          <span><img src="https://www.countryflags.io/${nation}/flat/16.png"></span>
                 ${nation}  <span class=" fa fa-angle-down"></span>
                  
                </a>
                <ul class="dropdown-menu dropdown-usermenu pull-right" role="menu">
                  <li>
                    <a onclick="routie('/${nation}')"><i class="icon-chart pull-right"></i>
                      <span>Dashboard</span>
                    </a>
                  </li>
                  <li>
                    <a onclick="routie('/${nation}/disciples')"><i class="icon-users pull-right"></i>
                      <span>Disciples</span>
                    </a>
                  </li>
                  <li>
                    <a onclick="routie('/${nation}/provinces')"><i class="icon-grid pull-right"></i> Provinces</a>                    
                    </a>
                  </li> 
                  <li>
                    <a onclick="routie('/${nation}/localities')"><i class="icon-pin pull-right"></i> Localities</a>                    
                    </a>
                  </li>    
                  <li>
                    <a onclick="routie('/${nation}/assemblies')"><i class="icon-home pull-right"></i> House Churches</a>                    
                    </a>
                  </li> 
                  <li>
                    <a onclick="routie('/${nation}/events')"><i class="icon-calendar pull-right"></i>
                      <span> Events</span> 
                    </a>
                  </li>
                  <li>
                    <a onclick="routie('/${nation}/settings')"><i class="icon-settings pull-right"></i> Settings</a>                    
                    </a>
                  </li> 
              
                </ul>
          `);
      });

      
    
        //---------------------------------------------------------------//
        //---------                DASHBOARD              ---------------//
        //---------------------------------------------------------------//

        $.getJSON('api/funcs.php?fn=fetchEvents&n='+nation, function(data) {
          var template = $('#eventstpl').html();
          var html = Mustache.to_html(template, data);
          $('#topNationalEvents').html(html);
        }); //getEvents
        
        $.getJSON('api/funcs.php?fn=fetchOnlineUsers&n='+nation, function(data) {
          var template = $('#usersOnlinestpl').html();
          var html = Mustache.to_html(template, data);
          $('#onlineNationalUsers').html(html);
        }); //getTopUsers
        
       
        //initialise select inputs
        $("#leadersDropDown").select2({ //create house church leaders drop down
          maximumSelectionLength: 4,
          placeholder: "Max 4",
          allowClear: true
        });
        

        refreshMemberCombo(nation); //initialise leaders combo
        initialiseRegionsCombo(); //initialise regions combo
        initialiseFamiliesCombo(nation); //initialise families combo
        initialiseProvincesCombo(nation); //initialise provinces combo
        initialiseNationTotals(nation); //initialise nation totals
        initialiseNationDetails(nation); //initialise nation details

        //--- DRAW CHARTS
         var myChart = echarts.init(document.getElementById('echart_bar_horizontal'), theme);

      $.ajax({ url: "api/api.php/records/vw_nationstats?filter=nation,eq,"+nation,
              method: "GET",
              success: function(data) {
                var date = [];
                var disciples = [];
                var jsonArr = data.records ;//jQuery.parseJSON(data);

                for(var i in jsonArr) {
                  disciples.push(jsonArr[i].members);
                  date.push(jsonArr[i].date);
                }
                //console.log(province);
                //console.log(db_disciples);
                //console.log(disciples);
                  myChart.setOption({
                    title: {
                      text: 'Disciples',
                      subtext: 'nation disciples'
                    },
                    tooltip: {
                      trigger: 'axis'
                    },
                    legend: {
                      data: ['Disciples']
                    },
                    /*toolbox: {
                      show: true,
                      feature: {
                        saveAsImage: {
                          show: true
                        }
                      }
                    },*/
                    toolbox: {
                        show: true,
                        feature: {
                          magicType: {
                            show: true,
                            type: ['line', 'bar', 'stack', 'tiled']
                          },
                          restore: {
                            show: true
                          },
                          saveAsImage: {
                            show: true
                          }
                        }
                      },
                    calculable: true,
                    yAxis: [{
                      type: 'value',
                      boundaryGap: [0, 0.01]
                    }],
                    xAxis: [{
                      type: 'category',
                      data: date
                    }],
                    series: [{
                      name: 'Disciples',
                      type: 'line',
                        smooth: false,
                        itemStyle: {
                          normal: {
                            areaStyle: {
                              type: 'default'
                            }
                          }
                        },
                      data: disciples
                    }]
                  });
                },
              error: function(data) {
                console.log(data);
              }
      });

        
        //---------------------------------------------------------------//
        //---------                LOCALITIES             ---------------//
        //---------------------------------------------------------------//
        $('#localitiesTable').DataTable({
          "ajax": {
        "url": "api/funcs.php?fn=fetchLocalities&n="+nation,
        "dataSrc": ""
        },
        "columns": [
          { "data": "localityName" },
          { "data": "datePlanted" },
          { "data": "provinceName" },
          { "data": "locality_leader" },
          { "data": "actions" },
        ],
          dom: "Bfrtip",
                buttons: [{
                  extend: "copy",
                  className: "btn-sm"
                }, {
                  extend: "csv",
                  className: "btn-sm"
                }, {
                  extend: "excel",
                  className: "btn-sm"
                }, {
                  extend: "pdf",
                  className: "btn-sm"
                }, {
                  extend: "print",
                  className: "btn-sm"
                }
        ]
        });
      
     
 

        //---------------------------------------------------------------//
        //---------                HOUSE CHURCHES         ---------------//
        //---------------------------------------------------------------//
        $('#houseChurchesTable').DataTable({
          "ajax": {
        "url": "api/funcs.php?fn=fetchHouseChurches&n="+nation,
        "dataSrc": ""
        },
        "columns": [
          { "data": "familyName" },
          { "data": "address" },
          { "data": "city" },
          { "data": "leaderName" },
          { "data": "actions" }
        ],
          dom: "Bfrtip",
                buttons: [{
                  extend: "copy",
                  className: "btn-sm"
                }, {
                  extend: "csv",
                  className: "btn-sm"
                }, {
                  extend: "excel",
                  className: "btn-sm"
                }, {
                  extend: "pdf",
                  className: "btn-sm"
                }, {
                  extend: "print",
                  className: "btn-sm"
                }
        ]
        });


      


     function initialiseNationTotals(id){
           $.getJSON("api/funcs.php?fn=fetchNationTotals&n="+id, function(res) {
              $('.provTotal').html(res.provinces);
              $('.discTotal').html(res.disciples);
              $('#hcTotal').html(res.housechurches);
              $('#locTotal').html(res.localities);
              $('.ctrlDiscTotal').html(res.ctrlDisciples);
              $('.progress').html('<div class="progress-bar bg-green" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="100" style="width: '+((res.disciples/res.ctrlDisciples)*100)+'%;"><span class="sr-only">3% Complete</span></div>');
              var progress=Math.ceil((res.disciples/res.ctrlDisciples)*100);
              drawGuage(progress);
         
          });
        }

      function initialiseNationDetails(id){
           $.getJSON("api/funcs.php?fn=fetchNation&n="+id, function(res) {
              $('#nName').html(res.name);
              $('#nationName').val(res.name);
              $('#nCode').html(res.code);
              $('#nRegion').html(res.region);
              $('#nregion').val(res.region);
              $('#nRegion').trigger('change');
              $('select#nLeaderDropDown').val(res.leaderId);
              $('#nLeaderDropDown').trigger('change');
              $('#nPhone').html(res.phone);
              $('#nEmail').html(res.email);
              $('#nLeader').html(res.leader);
              $('#nDate').html(res.date);
              $('#nationStartDate').val(res.date);
             
              //alert(response.name);
         
          });
        }

        function drawGuage(p){
      var Chart = echarts.init(document.getElementById('echart_guage'), theme);
          Chart.setOption({
            tooltip: {
              formatter: "{a} <br/>{b} : {c}%"
            },
            toolbox: {
              show: true,
              feature: {
                restore: {
                  show: true
                },
                saveAsImage: {
                  show: true
                }
              }
            },
            series: [{
              name: 'Progress',
              type: 'gauge',
              center: ['50%', '50%'],
              radius: [0, '75%'],
              startAngle: 140,
              endAngle: -140,
              min: 0,
              max: 100,
              precision: 0,
              splitNumber: 10,
              axisLine: {
                show: true,
                lineStyle: {
                  color: [
                    [0.2, '#ff4500'],
                    [0.4, 'orange'],
                    [0.8, 'skyblue'],
                    [1, 'lightgreen']
                  ],
                  width: 30
                }
              },
              axisTick: {
                show: true,
                splitNumber: 5,
                length: 8,
                lineStyle: {
                  color: '#eee',
                  width: 1,
                  type: 'solid'
                }
              },
              axisLabel: {
                show: true,
                formatter: function(v) {
                  switch (v + '') {
                    case '10':
                      return 'a';
                    case '30':
                      return 'b';
                    case '60':
                      return 'c';
                    case '90':
                      return 'd';
                    default:
                      return '';
                  }
                },
                textStyle: {
                  color: '#333'
                }
              },
              splitLine: {
                show: true,
                length: 30,
                lineStyle: {
                  color: '#eee',
                  width: 2,
                  type: 'solid'
                }
              },
              pointer: {
                length: '80%',
                width: 8,
                color: 'auto'
              },
              title: {
                show: true,
                offsetCenter: ['-65%', -10],
                textStyle: {
                  color: '#333',
                  fontSize: 15
                }
              },
              detail: {
                show: true,
                backgroundColor: 'rgba(0,0,0,0)',
                borderWidth: 0,
                borderColor: '#ccc',
                width: 100,
                height: 40,
                offsetCenter: ['-60%', 10],
                formatter: '{value}%',
                textStyle: {
                  color: 'auto',
                  fontSize: 30
                }
              },
              data: [{
                value: p,
                name: 'Progress'
              }]
            }]
          });
        }



        //---------------------------------------------------------------//
        //---------                FORM FUNCTIONS         ---------------//
        //---------------------------------------------------------------//
        


    
    $("#loaderImg").hide(); //hide loader image
  