var URI = window.location,
$BODY = $('body'),
$MENU_TOGGLE = $('#menu_toggle'),
$SIDEBAR_MENU = $('#sidebar-menu'),
$SIDEBAR_FOOTER = $('.sidebar-footer'),
$LEFT_COL = $('.left_col'),
$RIGHT_COL = $('.right_col'),
$NAV_MENU = $('.nav_menu'),
$FOOTER = $('footer');

// Sidebar
$(function () {

// TODO: This is some kind of easy fix, maybe we can improve this
var setContentHeight = function () {
    // reset height
    $RIGHT_COL.css('min-height', $(window).height());

    var bodyHeight = $BODY.height(),
        leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
        contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

    // normalize content
    contentHeight -= $NAV_MENU.height() + $FOOTER.height();

    $RIGHT_COL.css('min-height', contentHeight);
};

$SIDEBAR_MENU.find('a').on('click', function(ev) {
    var $li = $(this).parent();

    if ($li.is('.active')) {
        $li.removeClass('active');
        $('ul:first', $li).slideUp(function() {
            setContentHeight();
        });
    } else {
        // prevent closing menu if we are on child menu
        if (!$li.parent().is('.child_menu')) {
            $SIDEBAR_MENU.find('li').removeClass('active');
            $SIDEBAR_MENU.find('li ul').slideUp();
        }
        
        $li.addClass('active');

        $('ul:first', $li).slideDown(function() {
            setContentHeight();
        });
    }
});

// toggle small or large menu
$MENU_TOGGLE.on('click', function() {
    if ($BODY.hasClass('nav-md')) {
        $BODY.removeClass('nav-md').addClass('nav-sm');
        $LEFT_COL.removeClass('scroll-view').removeAttr('style');

        if ($SIDEBAR_MENU.find('li').hasClass('active')) {
            $SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
        }
    } else {
        $BODY.removeClass('nav-sm').addClass('nav-md');

        if ($SIDEBAR_MENU.find('li').hasClass('active-sm')) {
            $SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
        }
    }

    setContentHeight();
});

// check active menu
$SIDEBAR_MENU.find('a[href="' + URI + '"]').parent('li').addClass('current-page');

$SIDEBAR_MENU.find('a').filter(function () {
    return this.href == URI;
}).parent('li').addClass('current-page').parents('ul').slideDown(function() {
    setContentHeight();
}).parent().addClass('active');

// recompute content when resizing
//$(window).smartresize(function(){  
  //  setContentHeight();
//});

});

// Panel toolbox
$(function () {
$('.collapse-link').on('click', function() {
    var $BOX_PANEL = $(this).closest('.x_panel'),
        $ICON = $(this).find('i'),
        $BOX_CONTENT = $BOX_PANEL.find('.x_content');
    
    // fix for some div with hardcoded fix class
    if ($BOX_PANEL.attr('style')) {
        $BOX_CONTENT.slideToggle(200, function(){
            $BOX_PANEL.removeAttr('style');
        });
    } else {
        $BOX_CONTENT.slideToggle(200); 
        $BOX_PANEL.css('height', 'auto');  
    }

    $ICON.toggleClass('fa-chevron-up fa-chevron-down');
});

$('.close-link').click(function () {
    var $BOX_PANEL = $(this).closest('.x_panel');

    $BOX_PANEL.remove();
});
});

// Tooltip
$(function () {
$('[data-toggle="tooltip"]').tooltip();
});

// Progressbar
if ($(".progress .progress-bar")[0]) {
$('.progress .progress-bar').progressbar(); // bootstrap 3
}

// Switchery
if ($(".js-switch")[0]) {
var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
elems.forEach(function (html) {
    var switchery = new Switchery(html, {
        color: '#26B99A'
    });
});
}

// iCheck
if ($("input.flat")[0]) {
$(document).ready(function () {
    $('input.flat').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });
});
}