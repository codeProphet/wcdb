//Global variables
var sresponse="Success";
var fresponse="Something's wrong";

//api variables
var api_root_url="api/api.php/records/";
var tbl_prefix="cmfi_";




$(document).on('change', 'select#countryName', function(){

            var value = $('select#countryName option:selected').val();
		$("#localities").load("includes/fetchLocalities.php?nationID="+value)

});
$(document).on('change', 'select#countryNamez', function(){

            var value = $('select#countryNamez option:selected').val();
		$("#localitiesz").load("includes/fetchLocalities.php?nationID="+value);

});
//fill localities after nation combo is selected
$(document).on('change', 'select#wnation', function(){

            var value = $('select#wnation option:selected').val();
		$("#wlocalities").load("includes/fetchLocalities.php?nationID="+value);
		$("#finishImportForm").html('<input id="nation" name="nation" type="hidden" value="'+value+'" />');

});


$(document).on('click','.tab',function(){
	//remove the active class from all other tabs and make this only active
	if ( $(this).hasClass('active') ) {
			$(this).removeClass('active');
		}
		else {
			ul.$('li.active').removeClass('active');
			$(this).addClass('active');
		}
	
});

function initialiseFamiliesCombo(n){
	//populate family drop down
        var opt = $(".families");
          $.getJSON("api/api.php/records/cmfi_families?filter=nation,eq,"+n+"&order=familyName,asc", function(response) {
               $.each(response.records, function() {
               opt.append($("<option />").val(this.id).text(this.familyName));
           });
        });
}

 function nationLeaderCombo(){
                  $('.nationLeader').html('<option>Choose Nation Leader</option>'); 
                  var o = $(".nationLeader");
                      $.getJSON("api/api.php/records/cmfi_members?order=firstName,asc", 
                        function(response) {
                           $.each(response.records, function() {
                           o.append($("<option />").val(this.id).text(this.firstName+' '+this.lastName));
                       });
                    });
                 }

function initialiseRegionsCombo(){
       //populate regions drop down
      $('.nregions').html('<option value="0">Choose region</option>'); 
      var opt = $(".nregions");
          $.getJSON("api/funcs.php?fn=fetchRegions", function(response) {
               $.each(response, function() {
               opt.append($("<option />").val(this.code).text(this.name));
           });
        });
    }


function initialiseLoclaitiesCombo(n){
	$('.localities').html('<option value="1">Choose Locality</option>'); 
	var x = $(".localities");
          $.getJSON("api/funcs.php?fn=fetchLocalities&n="+n, function(response) {
               $.each(response, function() {
               x.append($("<option />").val(this.id).text(this.localityName));
           });
        });
}

function initialiseProvincesCombo(n){
	$('.provinces').html('<option value="0">Choose province</option>'); 
	var x = $(".provinces");
          $.getJSON("api/funcs.php?fn=fetchProvinces&n="+n, function(response) {
               $.each(response, function() {
               x.append($("<option />").val(this.id).text(this.provinceName));
           });
        });
}

function refreshMemberCombo(n){
	$('.leaders').html('<option value="0">Choose disciple</option>'); 
	var opt = $(".leaders");
          $.getJSON("api/api.php/records/cmfi_members?filter=country,eq,"+n+"&order=firstName,asc", function(response) {
               $.each(response.records, function() {
               opt.append($("<option />").val(this.id).text(this.firstName+' '+this.lastName));
           });
        });
}

function refreshDMCombo(n){
	$('.dm').html('<option value="0">Choose disciple-maker</option>'); 
	var opt = $(".dm");
          $.getJSON("api/api.php/records/cmfi_members?filter=country,eq,"+n+"&order=firstName,asc", function(response) {
               $.each(response.records, function() {
               opt.append($("<option />").val(this.id).text(this.firstName+' '+this.lastName));
           });
        });
}

 //initialise roles combo
 function initialiseRolesCombo(){
        $('.roleCombo').html('<option value="9">Choose Ministry Role</option>'); 
        var opt = $(".roleCombo");
        $.getJSON("api/api.php/records/cmfi_roles", function(response) {
             $.each(response.records, function() {
             opt.append($("<option />").val(this.roleId).text(this.role));
         });
      });
    }

      //initialise families combo
 function initialiseFamilyCombo(n){
        $('.familyCombo').html(`<option value=0>Choose Family Name</option>`); 
          var opt1 = $(".familyCombo"); console.log('nation: '+n);
          $.getJSON("api/api.php/records/cmfi_families?filter=nation,eq,"+n+"&order=familyName,asc", function(response) {
               $.each(response.records, function() {
               opt1.append($("<option />").val(this.id).text(this.familyName));
           });
        });
      }

 function ConfirmDelete(object)
    {
    	(new PNotify({
                  title: 'Confirmation Needed',
                  text: 'Are you sure you want to delete this '+object+'?',
                  icon: 'glyphicon glyphicon-question-sign',
                  hide: false,
                  confirm: {
                    confirm: true
                  },
                  buttons: {
                    closer: false,
                    sticker: false
                  },
                  history: {
                    history: false
                  },
                  addclass: 'stack-modal',
                  stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
               })).get().on('pnotify.confirm', function(){
					return true;
			});

      
    }

function showHide(div){
	var x=document.getElementById(div);
	if(x.style.display == 'block'){
		x.style.display = 'none';
	}else{
		x.style.display = 'block'; 
	}
}

function showHideForm(div){
	var icon=div+"_icon";
	changeIcon(icon);
	var x=document.getElementById(div);
	if(x.style.display == 'block'){
		x.style.display = 'none';
	}else{
		x.style.display = 'block'; 
	}
}


function show(div){
	var icon=div+"_icon";
	var x=document.getElementById(div);
	if(x.style.display == 'none'){
		x.style.display = 'block';
		changeIcon(icon);
	}
}



function changeIcon(icon){
  var x = document.getElementById(icon);
  if(x){
    if (x.classList.contains("glyphicon-plus-sign")==true) {
        x.classList.remove("glyphicon-plus-sign");
        x.classList.remove("green");
        x.classList.add("glyphicon-remove-sign");
        x.classList.add("red");
    } else {  x.classList.remove("glyphicon-remove-sign");
    	x.classList.remove("red");
        x.classList.add("glyphicon-plus-sign");
        x.classList.add("green");
        
    }
}
}

//function to refresh datatables
function refreshTable(tableId, urlData)
{
  $.getJSON(urlData, null, function( json )
  {
    table = $(tableId).dataTable();
    oSettings = table.fnSettings();

    table.fnClearTable(this);

    for (var i=0; i<json.aaData.length; i++)
    {
      table.oApi._fnAddData(oSettings, json.aaData[i]);
    }

    oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
    table.fnDraw();
  });
}



	
function changeLang(lang){
	window.location.search += '&lang='+lang;

}

function getNations(nation){
		$.getJSON('api/funcs.php?fn=getNationsL&letter='+nation, function(data) {
		var template = $('#nationtpl').html();
		var html = Mustache.to_html(template, data);
		$('#nationsDisplay').html(html);
	});
}
	function nationProfile(id,name){
			//alert (id);
			$("#contentDiv").load("views/nationDash.php?nation="+id+"&nationName="+name)
											
												return false;
		 }
	
	function showUserProfile(id){
			//alert (id);
			$("#contentDiv").load("views/viewProfile.php?userId="+id)
											
												return false;
		 }
  
	function loadProvinces(id){
			//alert (id);
			$("#provinces").load("includes/fetchProvinces.php?nationID="+id)
											
												return false;
		}
	function loadLocalities(id){
			//alert (id);
			$("#contentDiv").load("views/localities.php?nationID="+id)
											
												return false;
		 }
//view member		 
	function viewMemberTab(id,name){
		var tabID="tab_content"+id;
		//$("#homeTab").removeClass("active");
		//$('tab_content0').removeClass('active');$('tab_content0').removeClass('in');
		$("#"+tabID).addClass("active");$("#"+tabID).addClass("in");
		$("#homeTab").after('<li role="presentation" class="tab"><a href="#'+tabID+'" role="tab" id="profile-tab'+id+'" data-toggle="tab" aria-expanded="true">'+name+'</a></li>');
		$("#tab_content0").after( '<div role="tabpanel" class="tab-pane fade in" id="'+tabID+'" aria-labelledby="home-tab"></div>')
		$("#"+tabID).load("views/memberProfile.php?id="+id);
		//open the selected member tab
		$("#profile-tab"+id).trigger("click");
		//'<div role="tabpanel" class="tab-pane fade" id="tab_content<?php echo $row["id"];?>" aria-labelledby="profile-tab"></div>';
	}	

//load Disciple modal
  function loadDisciple(id){
       $.getJSON("api/api.php/records/cmfi_members/?filter=id,eq,"+id,
        function(data){
          var n=data.records[0];
          if(data.records.length!=0)
            {
              discipleFormx.firstNamex.value = n.firstName;
              discipleFormx.lastNamex.value = n.lastName;
              discipleFormx.dobx.value = n.DOB;
              discipleFormx.cityx.value = n.city;
              discipleFormx.emailx.value=n.email;

              discipleFormx.surbubx.value=n.surburb;
              discipleFormx.addressx.value=n.address;
              discipleFormx.proffessionx.value=n.profession;
            $('select#genderx' ).val( n.sex );
            $('#genderx').trigger('change');
            $('select#localityx' ).val( n.locality );
            $('#localityx').trigger('change');
            $('select#familyRolex' ).val( n.familyRole );
            $('#familyRolex').trigger('change');
            $('select#rolex' ).val( n.role );
            $('#rolex').trigger('change');
            $('select#discipleMakerx' ).val( n.discipleMaker );
            $('#discipleMakerx').trigger('change');
              discipleFormx.phonex.value = n.phone;
              discipleFormx.familyID.value=n.familyID;
              discipleFormx.id.value = n.id;
              uploadimagex.memberId.value=n.id;
            $('#uploadimagex_avatar').html(`<img  onclick="showHide('uploadForm')" class="img-responsive avatar-view" src="images/members/${n.image}" alt="Avatar" 
               onerror="if (this.src != 'images/members/${n.image}') this.src = 'images/members/user.png';">`);
            
            discipleFormx.tagsx.value=n.tags;

            }
            
         
       });
       
    }


//close tab	
	function closeTab(tabId){
		$("#"+tabId).hide(500);
		//$("#"+tabId).hide().html($("#homeTab").html()).show(500);
		$('a[href$="'+tabId+'"]').parent().remove();
		$("#homeTab").trigger("click");
	}
	
//Edit settings
	function editSet(id,name,value){
		var rowId="row"+id;
		var currentTD = $("#editset"+id).parents('tr').find('td');
		var x=$("#editset"+id).html();
		//alert (x);
          if ($("#editset"+id).html() == '<i class="glyphicon glyphicon-pencil"></i>') {                  
              $.each(currentTD, function () {
                  $(this).prop('contenteditable', true),
				  $(this).prop('style', 'color:red')
              });
			  
          } else {
             $.each(currentTD, function () {
                  $(this).prop('contenteditable', false),
				  $(this).prop('style', 'color:black')
				  
              });
			  var id_=id;
			  var name_ = $("#name"+id).html();
			  var value_ = $("#value"+id).html();
			  saveSetting(id_,name_,value_);
          }
		$("#editset"+id).html($("#editset"+id).html() == '<i class="glyphicon glyphicon-pencil"></i>' ? '<i class="glyphicon glyphicon-ok"></i> Save' : '<i class="glyphicon glyphicon-pencil"></i>')
		//$("#"+rowId).remove();
		//$("#"+rowId).after('<tr id="rowId"><form id="settings" method="post"><td><input type="text" class="form-control" name="name" id="name"  value="'+name+'" required="required"></td><td><input type="text" class="form-control" id="value" name="value"  value="'+value+'" required="required"><input id="settingId" name="settingID" type="hidden" value="'+id+'" /></td></form><td><a onclick="saveSetting()" id="settingSave" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-ok"></i></a></td></tr>');
	}


//------------------User class -------------------//
function User(id){
	this.id=id;
	this.memberId=0;

}

//------------------event functions-------------------//
//Edit Event
function saveEvent() {
              var title =$('#eventTitlex').val();
              var eventDesc=$('#eventDescx').val();
              var start=$('#startDatex').val();
              var end=$('#endDatex').val();
              var eventColor=$('#eventColorx').val();
              var eventId=$('#eventIdx').val(); //alert(eventId);
              var nation= $('#eventRegion option:selected').val();
              
              $.ajax({
                url: 'api/post.php',
                data: 'type=fullUpdate&title='+title+'&eventId='+eventId+'&start='+start+'&end='+end+'&eventColor='+eventColor+'&eventDesc='+eventDesc+'&nation='+nation,
                type: 'POST',
                dataType: 'json',
                success: function(response){successNote(sresponse,'event saved sussessfully!')
                  //if(response.status == 'success')
                  $('#calendar').fullCalendar('refetchEvents');
                },
                error: function(e){
                  errorNote('Error processing your request: '+e.responseText);
                }
              });
              

  }

 function saveEvent2() {
              var title =$('#eventTitley').val();
              var eventDesc=$('#eventDescy').val();
              var start=$('#reservation').startDate.val();
              var end=$('#reservation').endDate.val();
              var eventColor=$('#eventColory').val();
              var eventId=$('#eventIdy').val(); //alert(eventId);
              var nation= $('#eventRegion option:selected').val();
              
              $.ajax({
                url: 'api/post.php',
                data: 'type=fullUpdate&title='+title+'&eventId='+eventId+'&start='+start+'&end='+end+'&eventColor='+eventColor+'&eventDesc='+eventDesc+'&nation='+nation,
                type: 'POST',
                dataType: 'json',
                success: function(response)
                {successNote(sresponse,'event saved sussessfully!')
                  //if(response.status == 'success')
                  $('#calendar').fullCalendar('refetchEvents');
                },
                error: function(e){
                  errorNote('Error processing your request: '+e.responseText);
                }
              });
              

  }

//register event from form
function eventRegister(){
	var formName_="eventRegForm"; 
			$("#loaderImg").show(); //show loader image
			var members_=$("#membersDropDown").val();
			var id_=$("#eventId").val();
			//alert(id_);
			if(members_=='')
			{
			alert("Please select members ");
			}
			else{
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				formName:formName_,
				members:members_,
				id:id_
			},
			function(data,status){ // Required Callback Function
			var obj= $.parseJSON(data);
				console.log(obj['msg']);
			if(obj['status']==1){successNote(sresponse,obj['msg']);
			}else{
				errorNote(fresponse,obj['msg']+" Event Registration failed!");
			}
				$('#eventsRegTable').DataTable().ajax.reload();
				//$("#membersDropDown")[0].reset();
				$("#loaderImg").hide(); //hide loader image
			});
			
			}

}
 

//register a member to an event automatically
function eventRegisterAuto(memberId){
	var formName_="eventRegFormAuto"; 
			$("#loaderImg").show(); //show loader image
			var member_=memberId;
			var id_=$("#eventId").val();
			if(id_>0 && id_!=null){
				//alert(id_);
				if(member_=='')
				{
				alert("member Id not specified ");
				}
				else{
				$.post("api/post.php", //Required URL of the page on server
				{ // Data Sending With Request To Server
					formName:formName_,
					member:member_,
					id:id_
				},
				function(data,status){ // Required Callback Function
				var obj= $.parseJSON(data);
					console.log(obj['msg']);
				if(obj['status']==1){successNote(sresponse,obj['msg']);
				}else{
					   $.get("../app/api/api.php/records/cmfi_events?filter=eventId,eq,"+eventId_,
                    function(data){
                      var event=data.records[0];
                   //   console.log("after:"+firstName_);
                              $.post("../app/config/sendEmail.php",
                              {
                                  firstname:firstname_,
                                  lastname:lastname_,
                                  email:email_,
                                  eventTitle:event.eventTitle,
                                  eventVenue:event.eventVenue,
                                  startDate:event.startDate,
                                  endDate:event.endDate
                              },

                           function(response){ // Required Callback Function
                            
                             var res=response;
                             console.log(res);
                           
                            if(res.id==1){successNote(sresponse,res.text);
                            }else{
                              errorNote(fresponse,res.text);
                            }
                            })
                          });
(fresponse,obj['msg']+" Event Registration failed!");
				}
					$('#eventsRegTable').DataTable().ajax.reload();
					//$("#membersDropDown")[0].reset();
					$("#loaderImg").hide(); //hide loader image
				});
				
			}
		}

}

 //--------------------------------------------------//
//change admin permissions
function checkAdmin(id_){
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
			
			formName:'checkAdmin',
				userId:id_
			},
			function(response,status){ // Required Callback Function
			if(response==1){successNote(sresponse,'Setting data saved sussessfully!')
				}else{
					errorNote(fresponse,'Setting data not saved!')
				}
			});
			
		}


//save setting data to file	
function saveSetting(id_,name_,value_){
			
			if(name_=='' && value_=='')
			{
			alert("Please fill out the form");
			}
			else{
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				settingId:id_,
				name:name_,
				value:value_
			},
			function(response,status){ // Required Callback Function
			if(response==1){successNote(sresponse,'Admin setting <strong>'+name_+'</strong> changed sussessfully!')
			}else{
				errorNote(fresponse,'Setting data not saved!')
			}
			$("#form"+id_)[0].reset();
			//$("#rowId").remove();
			});
			}
		}

		
		
//import data from excel csv file 
function importFile(){ 
			$("#loading").prop('style', 'display:visible');
			var filesize=$("#file").val().length;
			if(filesize>0){
			$.ajax({
				url: "controllers/importExcel.php", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: new FormData(excelImportForm), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				contentType: false,       // The content type used when sending data to the server.
				cache: false,             // To unable request pages to be cached
				processData:false,        // To send DOMDocument or non processed data file it is set to false
				beforeSend: function () { $("#loaderImg").show(); },
				complete: function () { $("#loaderImg").hide(); },
				success: function(data)   // A function to be called if request succeeds
				{
					successNote(sresponse,'Data uploaded to server successfully!')
					$("#loading").prop('style', 'display:none');
				}
				});
				
			}
			
		}
		
//import data from excel csv file 
function finishImport(){ 
			$("#iloading").prop('style', 'display:visible');
			$.ajax({
				url: "controllers/finishImport.php", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method, 
				data:new FormData(finishImportForm),// Data sent to server, a set of key/value pairs (i.e. form fields and values)
				contentType: false,       // The content type used when sending data to the server.
				cache: false,             // To unable request pages to be cached
				processData:false,        // To send DOMDocument or non processed data file it is set to false
				success: function(data)   // A function to be called if request succeeds
				{

					console.log(data);
					successNote(sresponse,'Data imported to server successfully!')
					clearTemp();
					$("#iloading").prop('style', 'display:none');
				}
				});
			
		}
		
//backup database 
function backupDB(){ 
			$.ajax({
				url: "controllers/backupDB.php", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: new FormData(backupForm), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				contentType: false,       // The content type used when sending data to the server.
				cache: false,             // To unable request pages to be cached
				processData:false,        // To send DOMDocument or non processed data file it is set to false
				success: function(data)   // A function to be called if request succeeds
				{
					successNote(sresponse,'Database /tables backed up successfully :)!')
				}
				});			
		}
				
//clear temporary table
function clearTemp(){
	$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				procedure:"clearTemp"
			},
			function(response,status){ // Required Callback Function
			if(response==1){successNote(sresponse,'data cleared sussessfully!')
			}else{
				errorNote(fresponse,'data not cleared!')
			}
			});
	
}

	
	
//-----------------------------------MANAGING THE NATION OBJECT-------------------------------------------//
//save Nation data to file	
function saveNation(){ 
			var formName_="addNation";
			var ministryDate_=$("#finishDate").html(); 
			var numberOfDesciples=$("#numberOfDesciples").val(); 
			var region_=$("#nregion option:selected").val();
			var name_=document.getElementById("ncountryCode").value
			var leader_=$("#nationLeader option:selected").val();
			var today = new Date();
			
			if(name_=='')
			{
			alert("Please select country ");
			}
			else{
			$.ajax({
				url:"api/api.php/records/cmfi_nations/", //Required URL of the page on server
				type:"POST",
				data: { // Data Sending With Request To Server
						nationName:name_,
						nationDate:ministryDate_,
						nationLeader:leader_,
						nationRegion:region_
					},
				success: function(data,status){ // Required Callback Function
					successNote(sresponse,"nation "+name_+" added successfully");
					// $('#datatable-nations').DataTable().ajax.reload();
					 $.ajax({
						url:"api/api.php/records/cmfi_nation_stats/", 
						type:"POST",
						data: {
							//date:today,
							code:name_,
							disciples:numberOfDesciples	
						},
						success: function(data){
							console.log("stats saved");
							routie( '/nations');
						},
						error: function(data){
							console.log("stats did not save");
						}
						});
					$('#datatable-nations').DataTable().ajax.reload();
					//routie('/nations/new')
					},

				error: function(err){
					console.log(err);
					errorNote(fresponse,"data not saved! "+err.responseJSON.message);
				}
				
			});
			// $('#datatable-nations').DataTable().ajax.reload();
			}
		}
//save Nation data to file	
function editNation(){ 
			var nationName_=$('#nationName').val();
			var ministryDate_=$("#nationStartDate").html(); 
			var region_=$("#nregion option:selected").val();
			var name_=sessionStorage.getItem("nation");
			var id=nationSettingsForm.nationId.value; 
			var leader_=$("#nLeaderDropDown option:selected").val();
			//alert(ministryDate_+'|'+region_+'|'+name_+'|'+leader_);
			
			 $.ajax({
				url:'api/api.php/records/cmfi_nations/'+id,
				type:'PUT',
				data:
				{
					nationDate: nationSettingsForm.nationStartDate.value,
					nationName:nationSettingsForm.nationName.value,
					phone:nationSettingsForm.nPhone.value,
					email:nationSettingsForm.nEmail.value,
					nationLeader:leader_,
					nationRegion:region_
				},
				success: function(data){
						console.log('nation details saved successfully');
						successNote(sresponse,"Nation details saved successfully");
						 $('#datatable-nations').DataTable().ajax.reload(); //refresh nations table
			 },
			 error: function(err){
					console.log(err);
					errorNote(fresponse,"data not saved! "+err.responseJSON.message);
				}
					
			});
			// $.ajax({
			// 	url:'api/api.php/records/cmfi_countries/'+name,
			// 	type:'PUT',
			// 	data:{
			// 		name:nationSettingsForm.nationName.value,
			// 		region:region_
					
			// 	},
			// 	success: function(data){
			// 		console.log('country success');
			// 	}	
			// });

			
			
		
		}
//--------------------------------------------------------------------
//--CALENDAR FUNCTIONS------------------------------------------------
//--------------------------------------------------------------------
/*function(event){
    var title = event.title;
    var start = event.start.format("YYYY-MM-DD[T]HH:MM:SS");
    $.ajax({
      url: 'api/post.php',
      data: 'type=new&title='+title+'&startdate='+start+'&zone='+zone,
      type: 'POST',
      dataType: 'json',
      success: function(response){
        event.id = response.eventid;
        $('#calendar').fullCalendar('updateEvent',event);
      },
      error: function(e){
        console.log(e.responseText);
      }
   });
    $('#calendar').fullCalendar('updateEvent',event);
}

/*rename event
eventClick: function(event, jsEvent, view) {
   var title = prompt('Event Title:', event.title, { buttons: { Ok: true, Cancel: false} });
   if (title){
   event.title = title;
   $.ajax({
     url: 'process.php',
     data: 'type=changetitle&title='+title+'&eventid='+event.id,
     type: 'POST',
     dataType: 'json',
     success: function(response){
       if(response.status == 'success')
       $('#calendar').fullCalendar('updateEvent',event);
     },
     error: function(e){
       alert('Error processing your request: '+e.responseText);
     }
   });
   }
}*/

//drop event to another date
function deleteEvent(id,title){// alert('now in');
		(new PNotify({
                  title: 'Confirmation Needed',
                  text: 'Are you sure you want to delete this event?',
                  icon: 'glyphicon glyphicon-question-sign',
                  hide: false,
                  confirm: {
                    confirm: true
                  },
                  buttons: {
                    closer: false,
                    sticker: false
                  },
                  history: {
                    history: false
                  },
                  addclass: 'stack-modal',
                  stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
               })).get().on('pnotify.confirm', function(){
			
				
					$.ajax({
						url:'api/api.php/records/cmfi_events/'+id,
						type:'DELETE',
						success: function(response,status){
						if(response==1){successNote(sresponse,'Event deleted sussessfully!')
						}else{
							errorNote(fresponse,'Event not deleted!')
						}

						$('#localEventsTable').DataTable().ajax.reload(); //refresh provinces table
								
							}
						});
				
			});
		}

//UPDATE EVENT 
function updateEvent(id){
              var start_=addEventForm.reservation.value.substring(0,10);
              var end_=addEventForm.reservation.value.substring(12,10);

    var data={
        title:addEventForm.eventTitle.value,
        eventDesc:addEventForm.eventDesc.value,
        eventVenue:addEventForm.eventVenue.value,
        eventColor:addEventForm.eventColor.value,
        start:start_,
        end:end_,
        nation:addEventForm.eventRegion.value,
        regFee:addEventForm.regFee.value,
        criteria:addEventForm.criteria.value,
        coordinator:addEventForm.eventLeader.value

    }

     
    $.ajax({
      url:"../app/api/api.php/records/cmfi_events/"+id,
      type:"PUT",
      contentType:'application/json',
      data:JSON.stringify(data),
    })

    .done(function(){
              console.log('saved event details successfully');
              uploadEventImage(id);
    })

    .fail(function(msg){
      console.log('Failed to save event details: '+msg.responseText)
      console.log(msg);
    });


}



//edit event

  function eventEdit(id,title){
          var form="'addLocalityForm'";
          var container="'localityAddForm'";
          $.getJSON("../app/api/api.php/records/cmfi_events?filter=eventId,eq,"+id, function(data) {
              var res=data.records[0];
              var options={
                startDate: res.startDate.substring(0,10),
                endDate: res.endDate.substring(0,10),
                minDate: '2010-01-01',
                maxDate: '2065-12-31',
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                format: 'YYYY-MM-DD',
              }
               // $('#reservation').daterangepicker(options);
               //  addEventForm.eventTitle.value=res.eventTitle;
               //  addEventForm.eventDesc.value=res.eventDesc;
               //  addEventForm.reservation.value=res.startDate.substring(0,10)+' - '+res.endDate.substring(0,10);
               //  addEventForm.eventColor.value=res.eventColor;
               //  $('#eventColor').trigger('change');
               //  addEventForm.eventRegion.value=res.eventRegion;
               //  $('#eventRegion').trigger('change');
               //  $('#eventTitle').html('<h2>Edit Event <small>select leader</small></h2><div class="clearfix"></div>');
               //  $('#eventSubmit').html('<button  onclick="cancel('+form+','+container+')" type="submit" class="btn btn-default" name="submit" ><i class="icon-remove"></i> Cancel</button><button  onclick="saveEvent('+id+')" type="submit" class="btn btn-success" name="submit" ><i class="icon-check"></i>  Save</button>');
               //  //alert(response.name);
            });

        }


//update Nation information

//Delete Nation from file
function deleteNation(nationName){// alert('now in');
			(new PNotify({
                  title: 'Confirmation Needed',
                  text: 'Are you sure you want to delete this nation?',
                  icon: 'glyphicon glyphicon-question-sign',
                  hide: false,
                  confirm: {
                    confirm: true
                  },
                  buttons: {
                    closer: false,
                    sticker: false
                  },
                  history: {
                    history: false
                  },
                  addclass: 'stack-modal',
                  stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
               })).get().on('pnotify.confirm', function(){
			
				
					$.ajax({
						url:'api/api.php/records/cmfi_nations/'+nationName,
						type:'DELETE',
						success: function(response,status){
							if(response==1){
								successNote(sresponse,'Nation deleted sussessfully!');
								$('#datatable-nations').DataTable().ajax.reload(); //refresh provinces table

							}else{
								errorNote(fresponse,'Deleting nation failed');
								$('#datatable-nations').DataTable().ajax.reload(); //refresh provinces table
							}
							$('#datatable-nations').DataTable().ajax.reload(); //refresh provinces table
							
						},
						error:function(response,status){
							console.log(response);
								errorNote(fresponse,'Deleting nation failed');
							
						},
					})

			});
		
		}



//-----------------------------------MANAGING THE LOCALITY OBJECT-------------------------------------------//
//save Locality data to file	
function addLocality(n){ 
			var formName_="addLocalityForm";
			var datePlanted_=$("#locDatePlanted").html();
			var localityName_=$("#locName").html();
			var nation_=localStorage.getItem('nation');
			console.log(nation_);
			var province_=$("#locProvince option:selected").val();
			var leaders_=$("#locleadersDropDown").val();
			//alert(leaders_);
			if(nation_=='')
			{
			alert("Please fill out the nation name");
			}
			else{
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				formName:formName_,
				localityName:addLocalityForm.locName.value,
				province:province_,
				country:nation_,
				leaders:leaders_,
				datePlanted:addLocalityForm.locDatePlanted.value
			},
			function(response,status){ // Required Callback Function
				if(response==1){successNote(sresponse,'New locality <strong>'
						+addLocalityForm.locName.value+'</strong>  added sussessfully!')


				
					
				}else{
						errorNote(fresponse,'Data not saved!')
				}

				$("#addLocalityForm")[0].reset();
				$('#localitiesTable').DataTable().ajax.reload(); //refresh table
				showHide("localityAddForm");
				changeIcon("localityAddForm_icon");
				initialiseNationTotals(nation_);
			});
			}
		}


//save Locality data to file	
function editLocality(id_){ 
			var formName_="editLocalityForm";
			var datePlanted_=$("#locDatePlanted").html();
			var localityName_=$("#locName").val();
			//var nation_=$("#nationCode").val();
			var nation_=localStorage.getItem('nation');
			//console.log(nation_);
			var province_=$("#locProvince option:selected").val();
			var leaders_=$("#locleadersDropDown").val();
			//alert(leaders_);
			if(nation_=='')
			{
			alert("Please fill out the nation name");
			}
			else{
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				formName:formName_,
				localityName:addLocalityForm.locName.value,
				province:province_,
				country:nation_,
				leaders:leaders_,
				datePlanted:addLocalityForm.locDatePlanted.value,
				id:id_
			},
			function(data,status){ // Required Callback Function
			var obj= $.parseJSON(data);
				console.log(obj['msg']);
			if(obj['status']==1){
				successNote(sresponse,'Locality <strong>'+addLocalityForm.locName.value+'</strong>  updated sussessfully!');
			}else{
				errorNote(fresponse,obj['msg']+" data not saved!");
			}
				$('#localitiesTable').DataTable().ajax.reload();
				//$("#localityAddForm")[0].reset();
				showHide("localityAddForm");
				changeIcon("localityAddForm_icon");
			});
			}
		}

function deleteLocality(id){
	(new PNotify({
                  title: 'Confirmation Needed',
                  text: 'Are you sure you want to delete this locality?',
                  icon: 'glyphicon glyphicon-question-sign',
                  hide: false,
                  confirm: {
                    confirm: true
                  },
                  buttons: {
                    closer: false,
                    sticker: false
                  },
                  history: {
                    history: false
                  },
                  addclass: 'stack-modal',
                  stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
                })).get().on('pnotify.confirm', function(){
					var formName_="deleteLocalityForm";
					var nation_=$("#nationCode").val();
					var id_=id;
					
						$.post("api/post.php",
						{
							formName:formName_,
							id:id_
						},
						function(response,status){ // Required Callback Function
								if(response==1){successNote(sresponse,'Locality deleted sussessfully!');
								}else{
									errorNote(fresponse,'operation not performed');
								}
								$('#localitiesTable').DataTable().ajax.reload(); //refresh provinces table
								initialiseNationTotals(nation_);
							});	
				
			});
}

//-----------------------------------MANAGING THE PROVINCE OBJECT-------------------------------------------//
//save province data to file	
function addProvince(n){ 
			(new PNotify({
                  title: 'Confirmation Needed',
                  text: 'Are you sure you want to add this province?',
                  icon: 'glyphicon glyphicon-question-sign',
                  hide: false,
                  confirm: {
                    confirm: true
                  },
                  buttons: {
                    closer: false,
                    sticker: false
                  },
                  history: {
                    history: false
                  },
                  addclass: 'stack-modal',
                  stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
                })).get().on('pnotify.confirm', function(){
					var formName_="addProvinceForm";
					var provincialLeader_=$("#provleaderDropDown option:selected").val();
					var provinceName_=$("#provName").html();
					var disciples_=$("#ctrlTotal").val();
					//var nation_=$("#nationCode").val();
					var nation_=localStorage.getItem('nation');
					//console.log(nation_);
                  if(nation_=='')
					{
					alert("Please fill out the nation name");
					}
					else{
					$.post("api/post.php", //Required URL of the page on server
					{ // Data Sending With Request To Server
						formName:formName_,
						name:addProvinceForm.provName.value,
						leader:provincialLeader_,
						disciples:disciples_,
						nation:nation_
					},
					function(response,status){ // Required Callback Function
						if(response==1)
							{errorNote(fresponse,'data not saved! Province already exists in Database')
						}else{
						successNote(sresponse,'New province in '+nation_+', <strong>'+addProvinceForm.provName.value+'</strong> province added sussessfully!')
						}
						$("#addProvinceForm")[0].reset(); //clear form
						showHide("provinceAddForm"); //hide form
						changeIcon("provinceAddForm_icon");
						$('#provincesTable').DataTable().ajax.reload(); //refresh provinces table
						initialiseNationTotals(nation_);

					});
					}
                });
			
		}



function deleteProvince(id){
	(new PNotify({
          title: 'Confirmation Needed',
          text: 'Are you sure you want to delete this province?',
          icon: 'glyphicon glyphicon-question-sign',
          hide: false,
          confirm: {
            confirm: true
          },
          buttons: {
            closer: false,
            sticker: false
          },
          history: {
            history: false
          },
          addclass: 'stack-modal',
          stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
        })).get().on('pnotify.confirm', function(){
			var formName_="deleteProvinceForm";
			var nation_=$("#nationCode").val();
			var id_=id;
				$.post("api/post.php",
				{
					formName:formName_,
					id:id_
				},
				function(response,status){ // Required Callback Function
						if(response==1){successNote(sresponse,'Province deleted sussessfully!');
						}else{
							errorNote(fresponse,'operation not performed');
						}
						$('#provincesTable').DataTable().ajax.reload(); //refresh provinces table
						initialiseNationTotals(nation_);
					});	
						
			 });
}

//edit province details
function editProvince(id){ 
			var formName_="editProvinceForm";
			var provincialLeader_=$("#provleaderDropDown option:selected").val();
			var provinceName_=$("#provName").html();
			var nation_=localStorage.getItem('nation');
			var disciples_=0;//$("#ctrlTotal").val();
			var id_=id; //alert(provincialLeader_);
			
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				formName:formName_,
				name:addProvinceForm.provName.value,
				leader:provincialLeader_,
				disciples:disciples_,
				nation:nation_,
				id:id_
			},
	function(data,status){ // Required Callback Function
	var obj= $.parseJSON(data);
		console.log(status);
		console.log(obj['text']);
	if(obj['status']=="1")
		{successNote(sresponse,
			'<strong>'+addProvinceForm.provName.value+'</strong> province updated sussessfully!');
			}
			else{
				errorNote(fresponse,obj['text']+" data not saved!");
			}
			
				$("#addProvinceForm")[0].reset(); //clear form
				showHide("provinceAddForm"); //hide form
				changeIcon("provinceAddForm_icon");
				$('#provincesTable').DataTable().ajax.reload();//refresh provinces table
				$('#provDates').DataTable().ajax.reload(); 
			});
			
		}

//-----------------------------------MANAGING THE HOUSE CHURCH OBJECT-------------------------------------------//
//save house church data to file	
function addHouseChurch(n){ 
	var familyName_=$("#familyDropDown option:selected").html();
	(new PNotify({
                  title: 'Confirmation Needed',
                  text: 'Are you sure you want to add the house church at the'+familyName_+'s ?',
                  icon: 'glyphicon glyphicon-question-sign',
                  hide: false,
                  confirm: {
                    confirm: true
                  },
                  buttons: {
                    closer: false,
                    sticker: false
                  },
                  history: {
                    history: false
                  },
                  addclass: 'stack-modal',
                  stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
                })).get().on('pnotify.confirm', function(){
					var formName_="addHCForm";
					var familyId_=$("#familyDropDown option:selected").val();
					var familyName_=$("#familyDropDown option:selected").html();
					var startDate_=$("#hcstartDate").val();
					var locality_=$("#hcLocality option:selected").val();
					var leaders_=$("#leadersDropDown").val();
					var nation_=localStorage.getItem('nation');
					console.log(nation);
				//	var nation_=$("#nationCode").val();
					//alert(leaders_);
					
					if(nation_=='')
					{
					alert("Please fill out the nation name");
					}
					else{
					$.post("api/post.php", //Required URL of the page on server
					{ // Data Sending With Request To Server
						formName:formName_,
						familyId:familyId_,
						locality:locality_,
						startDate:startDate_,
						leaders:leaders_,
						nation:nation_
					},
					function(data,status){ // Required Callback Function
					var obj= $.parseJSON(data);
						console.log(obj['text']);
					if(obj['id']=="1"){successNote(sresponse,'New House Church at the <strong>'+familyName_+'s</strong>  added sussessfully!');
					}else{
						errorNote(fresponse,obj['text']+" data not saved!");
					}
					$("#addHCForm")[0].reset(); //clear form
						showHide("hcAddForm"); //hide form
						changeIcon("hcAddForm_icon");
						$('#houseChurchesTable').DataTable().ajax.reload(); //refresh house churches table
						initialiseNationTotals(nation_);
					});
					
					}
			});
		}

//update house church data to file	
function updateHouseChurch(id_){ 
			var formName_="updateHCForm";
			var familyId_=$("#familyDropDown option:selected").val();
			var familyName_=$("#familyDropDown option:selected").html();
			var startDate_=$("#hcstartDate").val();
			var locality_=$("#hcLocality option:selected").val();
			var leaders_=$("#leadersDropDown").val();
			var nation_=$("#nationCode").val();
			//alert(leaders_);
			
			if(nation_=='')
			{
			alert("Nation name not found");
			}
			else{
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				formName:formName_,
				familyId:familyId_,
				locality:locality_,
				startDate:startDate_,
				leaders:leaders_,
				id:id_,
				nation:nation_
			},
			function(data,status){ // Required Callback Function
			var obj= $.parseJSON(data);
				console.log(obj['text']);
			if(obj['id']=="1"){successNote(sresponse,'House Church at the <strong>'+familyName_+'s</strong>  Updated sussessfully!');
			}else{
				errorNote(fresponse,obj['text']+" data not saved!");
			}
			$("#addHCForm")[0].reset(); //clear form
				showHide("hcAddForm"); //hide form
				changeIcon("hcAddForm_icon");
				$('#houseChurchesTable').DataTable().ajax.reload(); //refresh provinces table
			});
			
			}
		}

function deleteHouseChurch(id){
	(new PNotify({
                  title: 'Confirmation Needed',
                  text: 'Are you sure you want to delete this house church?',
                  icon: 'glyphicon glyphicon-question-sign',
                  hide: false,
                  confirm: {
                    confirm: true
                  },
                  buttons: {
                    closer: false,
                    sticker: false
                  },
                  history: {
                    history: false
                  },
                  addclass: 'stack-modal',
                  stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
                })).get().on('pnotify.confirm', function(){
					var formName_="deleteHouseChurchForm";
					var nation_=$("#nationCode").val();
					var id_=id;
					
						$.post("api/post.php",
						{
							formName:formName_,
							id:id_
						},
						function(response,status){ // Required Callback Function
								if(response==1){successNote(sresponse,'House Church deleted sussessfully!');
								}else{
									errorNote(fresponse,'operation not performed');
								}
								$('#houseChurchesTable').DataTable().ajax.reload(); //refresh provinces table
								initialiseNationTotals(nation_);
							});	
					
			});
}

//-----------------------------------MANAGING THE REGION OBJECT-------------------------------------------//
//save region data to file	
function saveRegion(){ 
	var formName_="addRegionForm";
	var code_=addRegionForm.regionCode.value;
	var name_=addRegionForm.regionName.value;
	
	if(code_==''||name_=="")
	{
	 errorNote('Oops!','Missing region code or name');
	}
	else{
	$.post("api/post.php", //Required URL of the page on server
	{ // Data Sending With Request To Server
		formName:formName_,
		code:code_,
		name:name_
	},
	function(data,status){ // Required Callback Function
			var obj= $.parseJSON(data);
				console.log(obj['msg']);
			if(obj['status']==1){successNote(sresponse,obj['msg']);
				
			}else{
				errorNote(fresponse,obj['msg']);
			}
			$("#addRegionForm")[0].reset();
			initialiseRegionsCombo();
		});
	
	}
}

//-----------------------------------MANAGING THE REPORT OBJECT-------------------------------------------//
//save report data to file	
function addReport(id){ 
	var formName_="addReportForm";
	var members_=addReportForm.statsTotal.value;
	var localities_=addReportForm.statsLoc.value;
	var houseChurches_=addReportForm.statsHC.value;
	var leaders_=addReportForm.statsLeaders.value;
	var province_=id;
	var reportDate_=addReportForm.statsDate.value;
	var notes_="";
	
	if(id==null)
	{
	alert("No province selected: Developer issue");
	}
	else{
	$.post("api/post.php", //Required URL of the page on server
	{ // Data Sending With Request To Server
		formName:formName_,
		members:members_,
		localities:localities_,
		houseChurches:houseChurches_,
		leaders:leaders_,
		province:province_,
		reportDate:reportDate_,
		notes:notes_
	},
	function(response,status){ // Required Callback Function
	if(response==1){successNote(sresponse,'New provincial report  added sussessfully!')
	}else{
		errorNote(fresponse,'data not saved! something went wrong')
	}
	$("#addReportForm")[0].reset();
	$('#provDates').DataTable().ajax.reload(); //refresh province stats table
	});
	}
}

function saveReport(id){ 
	var formName_="saveReportForm";
	var members_=addReportForm.statsTotal.value;
	var localities_=addReportForm.statsLoc.value;
	var houseChurches_=addReportForm.statsHC.value;
	var leaders_=addReportForm.statsLeaders.value;
	var reportDate_=addReportForm.statsDate.value;
	var notes_="";
	
	if(id==null)
	{
	alert("No province selected: Developer issue");
	}
	else{
	$.ajax({
		url:"api/api.php/records/cmfi_nationalreports/"+id, //Required URL of the page on server
		type: 'PUT',
		data:{ // Data Sending With Request To Server
				members:members_,
				localities:localities_,
				houseChurches:houseChurches_,
				leaders:leaders_,
				date:reportDate_,
				notes:notes_
			},
		success: function(response,status){ // Required Callback Function
			if(response==1){successNote(sresponse,'provincial report updated sussessfully!')
			}else{
				errorNote(fresponse,'data not saved! something went wrong')
			}
		}
		});
			$("#addReportForm")[0].reset();
			$('#provReportUpdateBtn').html('');
			$('#provDates').DataTable().ajax.reload(); //refresh province stats table
	
	}
}
//-------------FUNCTIONS TO MANAGE MEMBER OBJECT---------------------------------------------------------//	


//edit member		
function editMember(id){
		var rowId="edit"+id;
		var currentTD = $("#edit"+id).parents('tr').find('td');
		var country=$("#country"+id).html(); 
		var x=$(this).html();
		//alert (x);
          if ($("#edit"+id).html() == '<i class="glyphicon glyphicon-pencil"></i>') {                  
              $.each(currentTD, function () {
                  $(this).prop('contenteditable', true),
				  $(this).prop('style', 'color:red')
              });
			  
          } else {
             $.each(currentTD, function () {
                  $(this).prop('contenteditable', false),
				  $(this).prop('style', 'color:black')
				  
              });
			  var id_=id;
			  var name_ = $("#name"+id).html();
			  var value_ = $("#value"+id).html();
			  saveMember(id_,name_,value_);
          }
		$("#edit"+id).html($("#edit"+id).html() == '<i class="glyphicon glyphicon-pencil"></i>' ? '<i class="glyphicon glyphicon-ok"></i> Save' : '<i class="glyphicon glyphicon-pencil"></i>')
		$("#country"+id).html('<select id="nations'+id+'"></select>');		
		$("#locality"+id).html('<select id="localities"></select>');
		$("#nations"+id).load('includes/fetchNations.php');
		$("#nations"+id+" option:contains(" + country + ")").attr('selected', 'selected');
		}
		
//delete member	
	function deleteMember(id){//alert("now in");
	(new PNotify({
                  title: 'Confirmation Needed',
                  text: 'Are you sure you want to delete this desciple?',
                  icon: 'glyphicon glyphicon-question-sign',
                  hide: false,
                  confirm: {
                    confirm: true
                  },
                  buttons: {
                    closer: false,
                    sticker: false
                  },
                  history: {
                    history: false
                  },
                  addclass: 'stack-modal',
                  stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
        })).get().on('pnotify.confirm', function(){
			
				$.ajax({
					url:'api/api.php/records/cmfi_members/'+id,
					type:'DELETE',
					success: function(res,status){
						successNote(sresponse,'Desciple deleted sussessfully!');
						$('#datatable-disciples').DataTable().ajax.reload(); //refresh provinces table
					},
					error: function(err){
						console.log(err);
						errorNote(fresponse,'Deleting desciple failed '+err.responseJSON.message);
					}

	
			
				});


			
		});
		
		
	}

function showImg(imagefile){
 $("#profileAvatar").html('<img class="img-responsive avatar-view" src="images/members/'+imageFile+'" alt="Avatar" title="Change the avatar" >');
}	

//save member image file
function saveImage(div){ 
			var formName_="saveImage";
			var filepath=document.getElementById(div).image_file.value;
			var imageFile= filepath.split(/[\\\/]/).pop();
			//var imageFile= "user.png";  //research on how to get the filename from the input
			if(imageFile=='')
			{
			alert("Please choose image file first");
			}
			else{
			$.ajax({
				url: "controllers/uploadFromFile.php", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: new FormData(uploadimage), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				contentType: false,       // The content type used when sending data to the server.
				cache: false,             // To unable request pages to be cached
				processData:false,        // To send DOMDocument or non processed data file it is set to false
				success: function(data)   // A function to be called if request succeeds
				{
					successNote(sresponse,'Image uploaded sussessfully!')
					$("#profileAvatar").html('<img class="img-responsive avatar-view" src="images/members/'+imageFile+'" alt="Avatar" title="Change the avatar" >');
				}
				});
			}
			$("#uploadForm").hide(500);
		}

//save member image file
function saveImageFromFile(form){ 
			var imageForm=document.getElementById(form);
			var filepath=imageForm.image_file.value;
			var imageFile= filepath.split(/[\\\/]/).pop();
			//var imageFile= "user.png";  //research on how to get the filename from the input
			
			if(imageFile=='')
			{
			alert("Please choose image file first");
			}
			else{
			$.ajax({
				url: "controllers/uploadFromFile.php", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: new FormData(imageForm), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				contentType: false,       // The content type used when sending data to the server.
				cache: false,             // To unable request pages to be cached
				processData:false,        // To send DOMDocument or non processed data file it is set to false
				success: function(data)   // A function to be called if request succeeds
				{
					successNote(sresponse,'Image uploaded sussessfully!')
					$("#"+form+"_avatar").html('<img class="img-responsive avatar-view" src="images/members/'+imageFile+'" alt="Avatar" title="Change the avatar" >');
				},
				error: function(data)
				{
					errorNote(data,'image not uploaded successfully!')
				}
				});
			}
			$("#uploadForm").hide(500);
		}

function discipleExists(firstname,lastname){
	var exists=false;
	$.ajax({
	  url: 'api/api.php/records/cmfi_members?filter=firstName,eq,'+firstname+'&filter=lastName,eq,'+lastname,
	  async: false,
	  dataType: 'json',
	  success: function (data) {
	    if(data.records.length!=0){
	    	exists=true;
	    }
	  }
	});
	return exists
	
}


//Add New Member data to file
function saveNewDisciple(form,imgForm){ //alert("now in");
			var locality_=$("select[name=locality] option:selected").val(); 
			var familyId_=$("select[name=familyName] option:selected").val(); 
			if(familyId_=0){familyId_=undefined;}
			var familyRole_=$("select[name=familyRole] option:selected").val();
			var gender_=$("input[id='genderF']:checked").val();
			var dm=$("select[name=discipleMaker] option:selected").val();
			var role_=$("select[name=role] option:selected").val();
			if($("input[name='genderF']:checked").val()=="F"){ gender_='F';}
			else{ gender_='M';} 
			var fx=document.getElementById(form);
			var imageForm=document.getElementById(imgForm);
			var filepath=imageForm.image_file.value;
			var imageFile_= filepath.split(/[\\\/]/).pop(); 
			var country_=sessionStorage.getItem('nation'); 
			//var imagefile_=$("#profileAvatar input[name=imagefile]").val(); //default user image
			//alert(imagefile_);
			/*var isWebcam=addMemberForm.webcamImg.value;
			if(isWebcam=='true'){
				imagefile_=addMemberForm.imagefile.value;
			}else{
				var imgfilepath=$("#image_file").val();
				imagefile_= imgfilepath.replace(/^.*[\\\/]/, '');
			}*/
			
			//alert(locality_);
			var tags_=fx.tags.value;
			//check if disciple already exists
			
			$.ajax({
				url: "api/api.php/records/cmfi_members", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: { // Data Sending With Request To Server
						firstName:fx.firstName.value,
						lastName:fx.lastName.value,
						locality:locality_,
						email:fx.email.value,
						address:fx.address.value,
						surburb:fx.surburb.value,
						city:fx.city.value,
						phone:fx.phone.value,
						country:country_,
						discipleMaker:dm,
						tags:tags_,
						sex:gender_,
						country:country_,
						//houseChurch:houseChurch_,
						DOB:fx.dob.value,
						familyID:familyId_,
						familyRole:familyRole_,
						role:role_,
						addedBy:sessionStorage.getItem('memberId'),
						profession:fx.profession.value,
						image:imageFile_
					
					},
				success: function(data)   // A function to be called if request succeeds
				{
						successNote(sresponse,'New disciple created sussessfully!');
						$('#datatable-disciples').DataTable().ajax.reload(); //refresh member table
						$("#"+form)[0].reset();
						$("#"+imgForm+"_avatar").html(`<img  onclick="showHide('${imgForm}')" class="img-responsive avatar-view" src="images/members/user.png" alt="Avatar" 
                              onerror="if (this.src != 'images/members/user.png') this.src = 'images/members/user.png';">`);
				},
				error: function(err){
					errorNote(fresponse,'Disciple not saved! '+err.responseJSON.message);
				}
				
			});
			
		}

//Add New Member data to file	short form
function addShortMember(){ //alert("now in");
			var formName_='addMemberForm';
			var locality_=2;
			var country_=$("#countryx").val();
			console.log(country_);
			var tags_="";
			var address_="";
			var surburb_="";
			var city_="";
			var image_="user.png";
			var discipleMaker_=0;
			var houseChurch_=0;

			//alert(locality_);
			var gender_='M';
			if($('#genderF').isCheck==true){ gender_='F';}
			if(country_=='')
			{
			alert("Please fill out the nation name");
			}
			else{
			$.ajax({
			url:"api/api.php/records/cmfi_members", //Required URL of the page on server
			type:"POST",
			data: { // Data Sending With Request To Server
				firstName:addMemberFormx.firstName.value,
				lastName:addMemberFormx.lastName.value,
				email:addMemberFormx.email.value,
				phone:addMemberFormx.phone.value,
				gender:gender_,
				surburb:surburb_,
				city:addMemberFormx.cityy.value,
				address:address_,
				country:addMemberFormx.countryx.value,
				houseChurch:houseChurch_,
				discipleMaker:discipleMaker_,
				image:image_,
				tags:tags_,
				locality:locality_,
				addedBy:sessionStorage.getItem('memberId'),
				familyID:0,
				familyRole:1,
				role:9,
				DOB:addMemberFormx.dateOfBirth.value
			},
			success: function(data){ // Required Callback Function
				successNote(sresponse,'leader saved successfully!');
				nationLeaderCombo();
				addMemberFormx.leaderId.value=data.responseJSON;
				console.log('leaderId: '+data.responseJSON);
			},
			error: function(err){
				errorNote(fresponse,err.responseJSON.message);
			}
			
			});
			
			}
			 $("#loaderImg").hide(); //hide loader image
			
			
		}



//save Member data to file	
function saveMember(){ //alert("now in");
			var id_=$("#memberID").val();
			var formName_=document.getElementById("saveMemberForm_"+id_); 
			var firstName_=formName_.firstNamex.value; 
			var lastName_=formName_.lastNamex.value;
			var email_=formName_.emailx.value;
			var address_=formName_.addressx.value;
			var surburb_=formName_.surburbx.value;
			var phone_=formName_.phonex.value;
			var DOB_=formName_.birthdayx.value;
			var houseChurch_=$("#houseChurchx option:selected").val();
			var familyId_=$("#familyNamex option:selected").val();
			var familyRole_=$("#familyRolex option:selected").val();
			var tags_=$("#tags_"+id_).val();
			var city_=formName_.cityx.value;
			var locality_=$("#localitiesy option:selected").val();
			var country_=$("#countryNamey option:selected").val();//alert(country_);
			var discipleMaker_=$("#discipleMakerx").val();
			//var country_=$("#nationx").val(); 
			var gender_=$("input[id='genderF']:checked").val();
			var imgfilepath=$("#image_file").val();
			var imagefile_= imgfilepath.replace(/^.*[\\\/]/, '');//alert (imgfilepath);
			if($("input[id='genderF']:checked").val()=="F"){ gender_='F';}
			else{ gender_='M';} 
			if(country_=='')
			{
			alert("Please fill out the nation name");
			}
			else{
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				formName:'saveMember',
				firstName:firstName_,
				lastName:lastName_,
				address:address_,
				email:email_,
				city:city_,
				surburb:surburb_,
				phone:phone_,
				familyId:familyId_,
				familyRole:familyRole_,
				gender:gender_,
				country:country_,
				locality:locality_,
				discipleMaker:discipleMaker_,
				DOB:DOB_,
				tags:tags_,
				id:id_,
				houseChurch:houseChurch_
			
			},
			function(data,status){ // Required Callback Function
			var obj= $.parseJSON(data);
				console.log(obj['msg']);
			if(obj['status']==1){successNote(sresponse,obj['msg']);
			}else{
				errorNote(fresponse,obj['msg']);
			}
			$('#datatable-disciples').DataTable().ajax.reload(); //refresh member table
			});
			
			}
		}




//drop event to another date
function deleteUser(id){// alert('now in');
(new PNotify({
                  title: 'Confirmation Needed',
                  text: 'Are you sure you want to delete this User?',
                  icon: 'glyphicon glyphicon-question-sign',
                  hide: false,
                  confirm: {
                    confirm: true
                  },
                  buttons: {
                    closer: false,
                    sticker: false
                  },
                  history: {
                    history: false
                  },
                  addclass: 'stack-modal',
                  stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
        })).get().on('pnotify.confirm', function(){
			
				$.ajax({
					url:'api/api.php/records/cmfi_users/'+id,
					type:'DELETE',
					success: function(response,status){
						if(response==1){successNote(sresponse,'User deleted sussessfully!');
						}else{
							errorNote(fresponse,'Deleting user failed');
						}
						$('#datatable-users').DataTable().ajax.reload(); //refresh provinces table
						
					}
				});


			
		});

			// var formName_="deleteUser";
			// var userId_=id;
			// if (ConfirmDelete()){
			// 	$.post("api/post.php", //Required URL of the page on server
			// 	{ // Data Sending With Request To Server
			// 		formName:formName_,
			// 		userId:userId_
			// 	},
			// 	function(response,status){ // Required Callback Function
			// 	if(response==1){successNote(sresponse,'User deleted sussessfully!');
			// 	}else{
			// 		errorNote(fresponse,'User not deleted!');
			// 	}
			// 	});
			// }
		}


//----- Save user profile update ----//	
function saveUserProfile(){//alert("now in");
			var formName_="saveUserForm";
			var password_=$("#passwordx").val();
			var firstName_=$("#firstNamex").val();
			var id_=$("#id2").val();
			var lastName_=$("#lastNamex").val();
			var email_=$("#emailx").val();
			var address_=$("#addressx").val();//alert (id_);
			var surburb_=$("#surburbx").val();
			var phone_=$("#phonex").val();
			var DOB_=$("#birthdayx").val();
			var tags_=$("#tags_").val();
			var city_=$("#cityx").val();
			var familyId_=$("#familyNamex option:selected").val();
			var locality_=$("#localitiesz option:selected").val();
			var country_=$("#countryNamez option:selected").val();
			var familyRole_=$("#familyRolex option:selected").val();
			var gender_=$("input[id='genderF']:checked").val();
			var imgfilepath=$("#image_file").val();
			var imagefile_= imgfilepath.replace(/^.*[\\\/]/, ''); 
			if($("input[id='genderF']:checked").val()=="F"){ gender_='F';}
			else{ gender_='M';} 
			if(country_=='')
			{
			alert("Please fill out the nation name");
			}
			else{
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				formName:formName_,
				firstName:firstName_,
				lastName:lastName_,
				familyId:familyId_,
				familyRole:familyRole_,
				address:address_,
				email:email_,
				city:city_,
				surburb:surburb_,
				phone:phone_,
				gender:gender_,
				country:country_,
				locality:locality_,
				DOB:DOB_,
				tags:tags_,
				id:id_,
				image:imagefile_,
				password:password_
			
			},
			function(data,status){ // Required Callback Function
				var obj= $.parseJSON(data);
				console.log(obj[0]['msg']);

			    for (var i=0; i<obj.length; i++)
			    {
			      	if(obj[i]['status']==1){successNote(sresponse,obj[i]['msg']);
					}else{
						errorNote(fresponse,obj[i]['msg']);
					}
			    }
			});
			}
		}


//------create user from member--------//
//save Member as user	
function makeUser(){// alert("now in");
			var formName_="makeUser";
			var id_=$("#memberID").val();
			var email_=$("#emailx").val();
			var firstName_=$("#firstNamex").val();
			var phone_=$("#phonex").val(); 
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				formName:formName_,
				email:email_,
				phone:phone_,
				firstName:firstName_,
				memberId:id_
			
			},
			function(data,status){ // Required Callback Function
				var obj= $.parseJSON(data);
				console.log(obj['text']);
			if(obj['id']=="1"){successNote(sresponse,obj['text']);
			}else{
				errorNote(fresponse,obj['text']);
			}
			});
			
		}
// send user credentials by email
function sendUserCredentials(email){// alert("now in");
		(new PNotify({
            title: 'Confirmation Needed',
            text: 'Are you sure you want to reset this account?',
            icon: 'glyphicon glyphicon-question-sign',
            hide: false,
            confirm: {
              confirm: true
            },
            buttons: {
              closer: false,
              sticker: false
            },
            history: {
              history: false
            },
            addclass: 'stack-modal',
            stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
          })).get().on('pnotify.confirm', function(){
				var formName_="sendUserCredentials";
				$.post("api/post.php", //Required URL of the page on server
				{ // Data Sending With Request To Server
					formName:formName_,
					email:email
				
				},
				function(data,status){ // Required Callback Function
				var obj= $.parseJSON(data);
					console.log(obj['text']);
				if(obj['id']=="1"){successNote(sresponse,obj['text']);
				}else{
					errorNote(fresponse,obj['text']);
				}
				});
			});
		}


//----------------------------------notifications------------------------------------------------//
//success nofication
function successNote(title_,text_){new PNotify({
			title: title_,
			text: text_,
			type: 'success'
		});
}

//failure or Error notifications
function errorNote(title_,text_){new PNotify({
			title: title_,
			text: text_,
			type: 'error'
		});
}

