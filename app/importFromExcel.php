 <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Import Members</h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Import Wizard <small>member import from excel CSV file</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">


                  <!-- Smart Wizard -->
                  <div id="wizard" class="form_wizard wizard_horizontal">
                    <ul class="wizard_steps">
                      <li>
                        <a href="#step-1">
                          <span class="step_no">1</span>
                          <span class="step_descr">
                                            Step 1<br />
                                            <small>csv file preparation</small>
                                        </span>
                        </a>
                      </li>
                      <li>
                        <a href="#step-2">
                          <span class="step_no">2</span>
                          <span class="step_descr">
                                            Step 2<br />
                                            <small>Import Excel CSV Data</small>
                                        </span>
                        </a>
                      </li>
                      <li>
                        <a href="#step-3">
                          <span class="step_no">3</span>
                          <span class="step_descr">
                                            Step 3<br />
                                            <small>Preview Data</small>
                                        </span>
                        </a>
                      </li>
                    <!--   <li>
                        <a href="#step-4">
                          <span class="step_no">4</span>
                          <span class="step_descr">
                                            Step 4<br />
                                            <small>Finish</small>
                                        </span>
                        </a>
                      </li> -->
                    </ul>
          
                    <div id="step-1">
             <div class="col-md-12 col-sm-12 col-xs-12">
             <h2 class="StepTitle">Requirements for Importing Data</h2>
              <p>
              Import your excel file. the file should be in CSV format and the fields should be made is in <a style="cursor:pointer;">this example<span class="fa fa-chevron-down"></span></a>
              </p>
              <div id="excelExample"></div>
              <p>Your CSV file should <strong>not</strong> have column headers, the following column order should be maintained</p>
              <ul>
              <li>Column 1 : FirstName</li>
              <li>Column 2 : LastName</li>
              <li>Column 3 : Gender - should be(M or F)</li>
              <li>Column 4 : Address</li>
              <li>Column 5 : Surburb</li>
              <li>Column 6 : Phone - should <strong>not</strong> contain '+' at the beginning, it should be e.g 263772902572</li>
              <li>Column 7 : Email</li>
              </ul>
             </div>

          </div>
                    <div id="step-2">
            <label  class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInputFile"></label>
             <div class="col-md-9 col-sm-9 col-xs-12">
             <h2 class="StepTitle">Import Excel/CSV file</h2>
              <p>
              Import your excel file. the file should be in CSV format and the fields should be made is in <a>this example</a>
              </p>
              </div>
             <form id="excelImportForm" action="" enctype="multipart/form-data" method="post" role="form" class="form-horizontal form-label-left" novalidate>
             <div class="item form-group">
               <label class="control-label col-md-3 col-sm-3 col-xs-12">Country Name  <span required="required"></span></label>
                <div class="col-md-6 col-sm-9 col-xs-12">
                <select name="wnation" id="wnation" class="form-control" required="">
                  <option>Choose nation</option>
                </select>
                </div>
              
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">City</label>
                <div class="col-md-6 col-sm-9 col-xs-12">
                <input type="text" class="form-control" name="wcity" id="wcity" placeholder="city"
                 required="required">
                </div>
              </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Locality</label>
                <div class="col-md-6 col-sm-9 col-xs-12">
                <select id="wlocalities" name="wlocality" class="form-control" required="">
                  <option>Choose locality</option>
                 </select>
                </div>
            </div>
             
            <div class="item form-group">
              <label  class="control-label col-md-3 col-sm-3 col-xs-12" for="file">File Upload</label>
              <div class="col-md-6 col-sm-9 col-xs-12">
              <input type="file" name="file" id="file" size="150" class="form-control" required="required">
              <p class="help-block">Only Excel/CSV File Import.</p>
              </div>
            </div>
            </form>
            <!-- <div class="form-group">
            <label  class="control-label col-md-3 col-sm-3 col-xs-12" for="import"></label> -->
            <!--div class="col-md-6 col-sm-9 col-xs-12">
              <button onclick="importFile()" type="submit" class="btn btn-default" name="Import" value="Import">Upload <span id="loading" style="display:none;"><img src="images/loading.gif" alt="loading..." width="30px" /></span></button>
              
            </div-->
          <!--  </div> -->
          
                    </div>
          
                    <div id="step-3">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <form id="finishImportForm" method="POST" action="">
                <input id="nation" name="nation" type="hidden" value="" />
                </form>
              <div class="col-md-9 col-sm-9 col-xs-12">
                  <h2 class="StepTitle">Import Data</h2>
                  <p>If you are happy with the data preview click finish </p><br>
                  <span id="iloading" style="display:none;" ><img src="images/loading.gif" alt="loading..." width="30px" /></span>
              </div>
              <!-- <h2 class="StepTitle">Preview Data</h2> -->
              <!-- <div class="button-group"> -->
              <!--a onclick="" class="btn btn-default btn-sm" ><i class="glyphicon glyphicon-eye-open"></i> Show</a-->
            <!--  <a onclick="clearTemp()" class="btn btn-default btn-sm" ><i class="glyphicon glyphicon-trash"></i> Clear All</a>
              </div> -->
            
              <table id="data_preview" class="table  table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th>FirstName</th>
                  <th>LastName</th>
                  <th>Adress</th>
                  <th>Surburb</th>
                  <th>Locality</th>
                  <th>City</th>
                  <th>Country</th>
                  <th>Sex</th>
                  <th>Phone</th>
                  <th>E-mail</th>
                  <th>Tags</th>
                  <th>id</th>
                </tr>
                </thead>
              </table>

              <!-- <a onclick="finishImport()" style="cursor:pointer;">Finish</a> -->
              
                        </div>
                    </div>
            <!-- <div id="step-4">
              <label  class="control-label col-md-3 col-sm-3 col-xs-12" ></label>
            
              <div class="col-md-9 col-sm-9 col-xs-12">
                <h2 class="StepTitle">Import Data</h2>
                <p>All Set Proceed to import Data click <a onclick="finishImport()" style="cursor:pointer;">Finish</a></p>
                <span id="iloading" style="display:none;" ><img src="images/loading.gif" alt="loading..." width="30px" /></span>
               </div> 
            </div> -->

                  </div>
                  <!-- End SmartWizard Content -->

                 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /page content -->
    
    
  <script type="text/javascript">
    $(document).ready(function() {


      //populate localities drop down
        var opt2 = $("#wnation");
          $.getJSON("api/funcs.php?fn=fetchNations", function(response) {
               $.each(response, function() {
               opt2.append($("<option />").val(this.code).text(this.name));
           });
       });

      // Smart Wizard
      $('#wizard').smartWizard({
        transitionEffect: 'slide',
        ajaxType: "POST", 
        onLeaveStep: onLeaveStepFunction,
        onFinish: onFinishCallback
        
      });

      function onLeaveStepFunction(obj, context){
        console.log("Do you want to leave the step "+context.fromStep+"?");
        

        if(context.fromStep==2){
          importFile();
          //destroy the previous table if exists
          $('#data_preview').DataTable().destroy();
          var user=sessionStorage.getItem('userId');
          loadPreview(user)

        }
        return validateSteps(context.fromStep);
         
      }


       function validateSteps(step){
        var x=0;
        switch(step){
          case 1:
            return true;
            break;

          case 2:
          var excelImportForm=$('#excelImportForm');
          if (!validator.checkAll(excelImportForm)) {
            return false;
          }
            return true;
            break;
      
        }
      }

      function onFinishCallback(){
            var additionalForm=$('#additionalForm');
            var submit = true;
            /*if (!validator.checkAll(additionalForm)) {
              submit=false;
              return false;
            }*/
            
            if (submit)
              {
                  finishImport();
                  clearTemp();

              }

        }

    });

  $(function(){
     $("#loaderImg").hide(); //hide loader after successful page load
  });
  </script>
<script type="text/javascript">
  
          function loadPreview(user) {
            var url = window.location.href; 
            var table=$('#data_preview').DataTable({
        "ajax": {
        "url": "api/funcs.php?fn=fetchTempmembers&user="+user,
        "dataSrc": ""
        },
        "columns": [
          { "data": "firstName" },
          { "data": "lastName" },
          { "data": "address" },
          { "data": "surburb" },
          { "data": "locality" },
          { "data": "city" },
          { "data": "country" },
          { "data": "sex" },
          { "data": "phone" },
          { "data": "email" },
          { "data": "tags" },
          { "data": "id" }
        ],
        "columnDefs": [
            {
                "targets": [ 11 ],
                "visible": false,
                "searchable": false
            },
       {
                "targets": [ 10 ],
                "visible": false
            }]
                
      });
      
    

    }
        </script>
        <script src="../app/js/validator/validator.js"></script>