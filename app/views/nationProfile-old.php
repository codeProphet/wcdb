<?php 
//get global settings
require("../config-small.php"); //loads the system configuration settings
//$nationId=
$nationId=$_REQUEST["id"];

//get nation details and countries
//echo $nationId;
$nation=getNation($nationId);
$countries=getCountries();
echo $nation["name"];
?>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <script src="js/custom-sm.js"></script>

  <style>
  a.boxclose{
    float:right;
    margin-top:-10px;
    margin-right:-10px;
    cursor:pointer;
    color: #fff;
    border: 1px solid #AEAEAE;
    border-radius: 10px;
    background: red;
    font-size: 11px;
    font-weight: bold;
    display: inline-block;
    line-height: 0px;
    padding: 6px 3px;       
}

#uploadForm{
	display:none;
	border:2px dashed #d2d2d2;
	background-color:#f7f7f7; 
	padding:2px;
}

.boxclose:before {
    content: "×";
}
  
  </style>
				
  <div class="col-md-12">
	<div class="x_panel">
	 
	  <div class="x_title">
		<h2>Nation Details</h2>
		<ul class="nav navbar-right panel_toolbox">
		  <li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
			<ul class="dropdown-menu" role="menu">
			  <li><a href="#"><i class="icon-printer"></i> Print</a>
			  </li>
			  <li><a href="#"><i class="icon-shield"></i> Deactivate</a>
			  </li>
				 <li><a href="#"><i class="icon-user-follow" type="submit" onclick="makeUser()"  name="makeuser" id="makeuser"></i> Make User</a>
			  </li>
			  <li><a onclick="deleteNation(<?php echo $nation['id']; ?>)"><i class="icon-trash"></i> Delete</a>
			  </li>
			</ul>
		  </li>
		  <li><a onclick="" class="close-link"><i class="fa fa-close"></i></a>
		  </li>
		</ul>
		<div class="clearfix"></div>
	  </div>
	  
	   <div class="x_content">
	   <div class="x_body">
		<div class="col-md-3 col-sm-12 col-xs-12">
		<div id="uploadForm">
			<a onclick="showHide('uploadForm')" class="boxclose" type="button"></a>
			<form id="uploadimage" action="" method="post" enctype="multipart/form-data">
				<input id="id" name="nationId" value="<?php echo $nation["id"]; ?>" type="hidden">
				<input class="form-control" id="image_file" name="image_file" type="file">
			</form>
			<!--button class="btn btn-default" data-dismiss="modal" type="button">Close</button--><div style="margin:10px"></div>
			<button class="btn btn-primary btn-sm" onclick="saveImage('uploadimage')" type="submit" name="submit" value="Upload">Upload</button>
		</div>
		   <div class="profile_img">
			
			  <!-- end of image cropping -->
			  <div id="avatar">
				<!-- Current avatar -->
				<div id="profileAvatar"  data-toggle="tooltip" data-placement="bottom" title="Change the avatar">
				<img onclick="showHide('uploadForm')" class="img-responsive avatar-view" src="images/members/<?php echo $nation["image"];?>" alt="Avatar" 
				onerror="if (this.src != 'images/members/user.png') this.src = 'images/members/user.png';"></div>
                      
				<!-- Loading state -->
				<div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
			  </div>
			  <!-- end of image cropping -->

			</div>
		<div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center;" >
			<h3><?php echo $nation["leader"];?></h3>
		</div>
		  
		</div>
		<div class="col-md-9 col-sm-12 col-xs-12">
		
			
			<div class="col-xs-9">
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div class="tab-pane active" id="home-r">
					  <form id="saveMember" class="form-horizontal form-label-left input_mask" action="" method="post">
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Nation Name">
						  <input type="text" class="form-control has-feedback-left" id="firstNamex"  placeholder="Nation Name" value="<?php echo $nation["name"]; ?>">
						  <span class="icon-user form-control-feedback left" aria-hidden="true"></span>
						</div>

						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Surname">
						  <input type="text" class="form-control has-feedback-left" id="lastNamex" value="<?php echo $nation["region"]; ?>" placeholder="Last Name">
						  <span class="icon-users form-control-feedback left" aria-hidden="true"></span>
						</div>

						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Email">
						  <input type="text" class="form-control has-feedback-left" id="emailx" value="<?php echo $nation["email"]; ?>" name="email" placeholder="Email">
						  <span class="icon-envelope form-control-feedback left" aria-hidden="true"></span>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Address">
						  <input type="text" class="form-control has-feedback-left" id="addressx" value="<?php echo $nation["address"]; ?>" placeholder="Address">
						  <span class="icon-pointer form-control-feedback left" aria-hidden="true"></span>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Surburb">
						  <input type="text" class="form-control has-feedback-left" id="surburbx" value="<?php echo $nation["continent"]; ?>" placeholder="Surburb">
						  <span class="icon-home form-control-feedback left" aria-hidden="true"></span>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="City">
						  <input type="text" class="form-control has-feedback-left" id="cityx" value="<?php echo $nation["region"]; ?>" placeholder="City">
						  <span class="icon-map form-control-feedback left" aria-hidden="true"></span>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Locality">
								
							<select id="localitiesx" name="locality" class="form-control has-feedback-left">
							  <option>Choose Locality</option>
							 </select>
						
						  <span class="icon-pin form-control-feedback left" aria-hidden="true"></span>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback">
							<select id="countryNamex" name="country" class="form-control has-feedback-left">
							  <option value="">Choose Country</option>
							 <?php 
								foreach ($nations as $n) 
									{
										echo '<option  value="'.$n["code"].'">'.$n["name"].'</option>';
									}
							  ?>
							</select>
						 <span class="icon-globe form-control-feedback left" aria-hidden="true"></span>
								
						</div>
						<!--function to set the selected country-->
						<?php echo"<script>
								$( 'select#countryNamex' ).val( '".$nation['country']."' );
								$('select#countryNamex').trigger('change');
						</script>"; ?>	
						<script>
							$(document).on('change', 'select#countryNamex', function(){
									var value = $('select#countryNamex option:selected').val();
									$("#localitiesx").load("includes/fetchLocalities.php?nationID="+value);
							})
						</script>
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Phone Number">
						  <input type="text" class="form-control has-feedback-left" id="phonex" value="<?php echo $nation["phone"]; ?> "data-validate-length-range="8,20" placeholder="Phone">
						  <span class="icon-screen-smartphone form-control-feedback left" aria-hidden="true"></span>
						</div>
						
						<div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Date of Birth">
							<input id="birthdayx" id="DOB" class="date-picker form-control has-feedback-left birthdayx" value="<?php echo $nation["DOB"]; ?>" type="text" >
						   <span class="icon-calendar form-control-feedback left" aria-hidden="true" ></span>
						</div>
					<script>
											$( function() {
												$( "#birthdayx" ).datepicker({ changeMonth: true,
      									changeYear: true});
												$( "#birthdayx" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
												 $( "#birthdayx" ).datepicker( "option", "showAnim", "clip");
											} );
											</script>
						
						<!--div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback">
						 <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender  <span class="required">*</span></label>
							
							 <div class="col-md-9 col-sm-9 col-xs-12">
							 <p>
							  M:
							  <input type="radio" class="flat" name="gender" id="genderM" value="M" <?php if( $nation["sex"]=="M"){echo 'checked=""';}?> required /> F:
							  <input type="radio" class="flat" name="gender" id="genderF" value="F"  <?php if( $nation["sex"]=="F"){echo 'checked=""';}?>/>
							</p>
							</div>
						</div-->
						<div class="col-md-12 col-sm-12 col-xs-12 control-group" data-toggle="tooltip" data-placement="top" title="Tags">
						  
							<input id="tags_<?php echo $nation["id"];?>" type="text" class="tags form-control" value="<?php echo $nation["tags"];?>" />
							<div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
						
						</div>
						</form>	
					 </div>
                      <div class="tab-pane" id="profile-r">Coming Soon.....</div>
                      <div class="tab-pane" id="messages-r">Coming Soon....</div>
                      <div class="tab-pane" id="settings-r">Coming Soon....</div>
                    </div>
                  </div>
				  <div class="col-xs-3">
                    <!-- required for floating -->
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs tabs-right">
                      <li class="active"><a href="#home-r" data-toggle="tab">General Info</a>
                      </li>
                      <li><a href="#profile-r" data-toggle="tab">Contact</a>
                      </li>
                      <li><a href="#messages-r" data-toggle="tab">Leaders</a>
                      </li>
                      <li><a href="#settings-r" data-toggle="tab">Prophecies</a>
                      </li>
                    </ul>
                  </div>
				
				
				 <div class="x_footer pull-right" style="margin-top:10px;margin-right:5px;">
						<input id="memberID" type="hidden" value="<?php echo $nation['id']; ?>" />
												<button class="btn btn-warning" type="button" onclick="deleteMember(<?php echo $nation['id']; ?>)"  name="delete" id="delete">
												<i class="icon-trash"></i> Delete</button>
                        <button type="button" onclick="closeTab('<?php echo $tabID;?>')" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" onclick="saveMember()" class="btn btn-primary" name="submit" >Save</button>
                  </div>
			
				</div>	   
			</div>
			
		 </div>
	 </div>
 </div>
 
  

  
   <!-- tags -->
  <script src="js/tags/jquery.tagsinput.min.js"></script>
   <!-- input tags -->
   

  
  <script>
    function onAddTag(tag) {
      alert("Added a tag: " + tag);
    }

    function onRemoveTag(tag) {
      alert("Removed a tag: " + tag);
    }

    function onChangeTag(input, tag) {
      alert("Changed a tag: " + tag);
    }

	   
  </script>
  <?php 
  echo "<script> $(function() {
      $('#tags_".$nation['id']."').tagsInput({
        width: 'auto'
      });
    });
	</script>
  ";
  ?>
  <!-- /input tags -->
  
