
 <div class="body">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="mainNationTab" class="nav nav-tabs bar_tabs" role="tablist" style="background: #fff;margin: px 0 10px;">
                        <li role="presentation" class="active"><a href="#tab_c1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true"><i class="icon-info"></i> Dashboard</a>
                        </li>
                        <!--li role="presentation" class=""><a href="#tab_c2" role="tab" id="profile-t" data-toggle="tab" aria-expanded="false">Leaders</a>
                        </li-->
                        <li role="presentation" class=""><a href="#tab_c3" role="tab" id="profile-t2" data-toggle="tab" aria-expanded="false"><span  class="badge bg-gray provTotal"></span> Provinces</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_c4" role="tab" id="profile-t3" data-toggle="tab" aria-expanded="false"><span id="locTotal" class="badge bg-gray"></span> Localities</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_c5" role="tab" id="profile-t4" data-toggle="tab" aria-expanded="false"><span id="hcTotal" class="badge bg-gray"></span> House Churches</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_c6" role="tab" id="profile-t5" data-toggle="tab" aria-expanded="false"><span class="badge bg-gray discTotal"></span> Disciples</a>
                        </li>
                        <li id="calendarTab" role="presentation" class=""><a href="#tab_c7" role="tab" id="profile-t7" data-toggle="tab" aria-expanded="false"> <i class="icon-calendar"></i> Calendar</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_c8" role="tab" id="profile-t8" data-toggle="tab" aria-expanded="false"><i class="icon-layers"></i> Events</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_c9" role="tab" id="profile-t9" data-toggle="tab" aria-expanded="false"><i class="icon-settings"></i> Settings</a>
                        </li>
                      </ul>
                      <div id="mainNationTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_c1" aria-labelledby="home-tab">
                          <!-- start recent activity -->
                          <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="row">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                            <!-- detail row -->
                            <div class="x_panel">
                              <div class="x_title">
                                <h2>Summary <small>nation details</small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                  </li>
                                  <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                      <li><a href="#">Settings 1</a>
                                      </li>
                                      <li><a href="#">Settings 2</a>
                                      </li>
                                    </ul>
                                  </li>
                                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                                  </li>
                                </ul>
                                <div class="clearfix"></div>
                              </div>
                              <div class="x_content">
                            <div class="table-responsive">
                            <table class="table">
                              <tbody>
                                <tr>
                                  <th style="width:50%"><i class="icon-globe"></i> Country Name:</th>
                                  <td id="nName"></td>
                                </tr>
                                <tr>
                                  <th><i class="icon-speech"></i> Code</th>
                                  <td id="nCode"></td>
                                </tr>
                                <tr>
                                  <th><i class="icon-pointer"></i> Region:</th>
                                  <td id="nRegion"></td>
                                </tr>
                                <tr>
                                  <th><i class="icon-screen-smartphone"></i> Telephone:</th>
                                  <td id="nPhone"></td>
                                </tr>
                                <tr>
                                  <th><i class="icon-envelope-letter"></i> Email:</th>
                                  <td id="nEmail"></td>
                                </tr>
                                <tr>
                                  <th><i class="icon-calendar"></i> Ministry start Date:</th>
                                  <td id="nDate"></td>
                                </tr>
                                <tr>
                                  <th><i class="icon-user"></i> Leader No.1</th>
                                  <td id="nLeader"></td>
                                </tr>
                                <tr>
                                  <th><i class="icon-users"></i> Total Disciples</th>
                                  <td class="ctrlDiscTotal"></td>
                                </tr>
                                <tr>
                                  <th><i class="icon-users"></i> Disciples Registered in DB</th>
                                  <td class="discTotal"></td>
                                </tr>
                                <tr>
                                  <th><i class="icon-map"></i> Spiritual Provinces</th>
                                  <td class="provTotal"></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                        </div> <!--close detail row-->

                      <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="x_panel">
                        <div class="x_title">
                          <h2>Profile Progress <small>percentage</small></h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                         <!--div class="widget_summary">
                            <div class="w_left w_25">
                             <i class="icon-users"></i> <span class="discTotal"></span>
                            </div>
                            <div class="w_center w_55">
                              <div class="progress">
                                
                              </div>
                            </div>
                            <div class=" w_20">
                              &nbsp;&nbsp;<i class="icon-lock"></i><span class="ctrlDiscTotal"></span>
                            </div>
                            <div class="clearfix"></div>
                          </div-->

                          <div id="echart_guage" style="height:370px;"></div>
                          <p>Disciple Registration Progress</p>

                          
                        </div>
                      </div>
                    </div>
                  </div>
                  </div><!--col md 4 -->
                       <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="row">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2>Disciples <small>by province</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                              <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                  <li><a href="#">print Report</a>
                                  </li>
                                  <li><a href="#">email Report</a>
                                  </li>
                                </ul>
                              </li>
                              <!--li><a class="close-link"><i class="fa fa-close"></i></a>
                              </li-->
                            </ul>
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content">
                             <div id="echart_bar_horizontal" style="height:370px;"></div>
                            <!--canvas id="chartCanvas"></canvas-->
                          </div>
                        </div>
                      </div>
                     <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="x_panel tile fixed_height_320 overflow_hidden">
                          <div class="x_title">
                            <h2>Top Dates</h2>
                            <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                              <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                  <li><a href="#">View All</a>
                                  </li>
                                  <li><a href="#">New Event</a>
                                  </li>
                                  <li><a href="#">Refresh Event</a>
                                  </li>
                                </ul>
                              </li>
                              <li><a class="close-link"><i class="fa fa-close"></i></a>
                              </li>
                            </ul>
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content" id="topNationalEvents">
                          </div>
                        </div>
                      </div>
                          <!-- Display online user profiles -->
                      <div class="col-md-6 col-sm-6 col-xs-12">
                              <div class="x_panel tile fixed_height_320 overflow_hidden">
                                  <div class="x_title">
                                    <h2>Online Users<small>active sessions</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                      </li>
                                      <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                          <li><a href="#">view all profiles</a>
                                          </li>
                                          <li><a href="#"></a>
                                          </li>
                                        </ul>
                                      </li>
                                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                                      </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                  </div>
                                 <div class="x_content" id="onlineNationalUsers" style="overflow: scroll;">
                                  <ul class="list-unstyled top_profiles scroll-view">                        
                                  </ul>
                            </div>
                        </div>
                      </div>
                    </div>

                  </div><!--col md 8 -->
                </div>
                    <!-- end row -->

                        </div>

                        <!-- National leaders, Elders, missionaries -->

                        <div role="tabpanel" class="tab-pane fade" id="tab_c2" aria-labelledby="profile-t">

                          <div id="leadersTable"></div>

                          <!-- start user projects -->
                          <table class="data table table-striped no-margin">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Leader Name</th>
                                <th>Roles</th>
                                <th class="hidden-phone">Contact</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>1</td>
                                <td>Leader 1</td>
                                <td>Missionary number 1</td>
                                <td class="hidden-phone">52625262</td>
                              </tr>
                              <tr>
                                <td>2</td>
                                <td>Leader 2</td>
                                <td>Missionary number 2</td>
                                <td class="hidden-phone">34743874</td>
                              </tr>
                              <tr>
                                <td>3</td>
                                <td>Leader 3</td>
                                <td>Elder</td>
                                <td class="hidden-phone">97237232</td>
                              </tr>
                              <tr>
                                <td>4</td>
                                <td>Leader ...</td>
                                <td>Pastor</td>
                                <td class="hidden-phone">743387343</td>                                
                              </tr>
                            </tbody>
                          </table>
                          <!-- end user projects -->
                        </div>

                        <!-- national spiritual provinces -->
                        <div role="tabpanel" class="tab-pane fade" id="tab_c3" aria-labelledby="profile-t">
                           <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                             <div class="x_content">

                             <ul class="nav navbar-right panel_toolbox">
                   
                                    <li>
                                      <a id="provinceBtn" onclick="showHideForm('provinceAddForm');changeProvTitle();" role="button" aria-expanded="false"><i style="font-size: 36px" id="provinceAddForm_icon" class="glyphicon glyphicon-plus-sign green"></i></a>
                                      
                                    </li>
                                    <!--li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li-->
                                  </ul>
                              <div id="provinceAddForm" style="display: none;" >
                               <div class="clearfix"></div>
                            
                                  <div class="x_panel" style="background-color: #F7F9F9;">
                                    <div class="x_title" id="provTitle">
                                      <h2>Add New Province <small>select leader</small></h2>
                                      
                                      <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">

                                      <div class="col-xs-2">
                                          <!-- required for floating -->
                                          <!-- Nav tabs -->
                                          <ul class="nav nav-tabs tabs-left">
                                            <li class="active"><a href="#home" data-toggle="tab"><i class="icon-list"></i> Details</a>
                                            </li>
                                            <li><a href="#stats" data-toggle="tab"><i class="icon-equalizer"></i> Statistics</a>
                                            </li>
                                            <li><a href="#chart" data-toggle="tab"><i class="icon-graph"></i> Chart</a>
                                            </li>
                                            
                                          </ul>
                                        </div>

                                        <div class="col-xs-10">
                                          <!-- Tab panes -->
                                          <div class="tab-content">
                                            <div class="tab-pane active" id="home">
                                              <form id="addProvinceForm" class="form-horizontal form-label-left input_mask" action="" method="post">
                                                <input id="nationCode" name="nationCode" value="" type="hidden">
                                                <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Name of Province<span class="required">*</span></label>
                                                  <div id="provName_div" class="col-md-6 col-sm-6 col-xs-12">
                                                  <input type="text" class="form-control" name="provName" id="provName" placeholder="province" required="required">
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Provincial Leader<span class="required">*</span></label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                      <select id="provleaderDropDown" name="provleaderDropDown" class="select2_multiple form-control leaders" multiple="multiple" style="width:100%;">
                                                        <option value="select2"></option>
                                                      </select>
                                                       
                                                    </div>
                                                    <span><a href="#"  data-toggle="modal" data-target=".newMember"  class="btn btn-success btn-sm"><i class="icon-note"></i> Create Leader</a></span>
                                                  </div>
                                                <div class="form-group">
                                                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Control Total Disciples<span class="required">*</span></label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <input type="number" class="form-control" name="ctrlTotal" id="ctrlTotal" placeholder="0" required="required">
                                                  </div>
                                                </div>
                                               
                                              </form>
                                              <div class="modal-footer">
                                                <div class="form-group" id="provSubmit">
                                                  <!--button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button-->
                                                  
                                                </div>
                                              </div>
                                            </div>
                                            <!-- Statistics tab pane -->
                                            <div class="tab-pane" id="stats">
                                              <div class="row">
                                                <div class="col-xs-4">
                                              <form id="addReportForm" class="form-horizontal form-label-left input_mask" action="" method="post">
                                               <div class="form-group">
                                                  <label class="control-label col-md-4 col-sm-4 col-xs-12">Control Total Disciples<span class="required">*</span></label>
                                                  <div class="col-md-8 col-sm-8 col-xs-12">
                                                  <input type="number" class="form-control" name="statsTotal" id="statsTotal" placeholder="0" required="required">
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="control-label col-md-4 col-sm-64col-xs-12">Leaders<span class="required">*</span></label>
                                                  <div class="col-md-8 col-sm-8 col-xs-12">
                                                  <input type="number" class="form-control" name="statsLeaders" id="statsLeaders" placeholder="0" required="required">
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="control-label col-md-4 col-sm-4 col-xs-12">Localities<span class="required">*</span></label>
                                                  <div class="col-md-8 col-sm-8 col-xs-12">
                                                  <input type="number" class="form-control" name="statsLoc" id="statsLoc" placeholder="0" required="required">
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="control-label col-md-4 col-sm-4 col-xs-12">House Churches<span class="required">*</span></label>
                                                  <div class="col-md-8 col-sm-8 col-xs-12">
                                                  <input type="number" class="form-control" name="statsHC" id="statsHC" placeholder="0" required="required">
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <label class="control-label col-md-4 col-sm-4 col-xs-12">As at Date<span class="required">*</span></label>
                                                  <div class="col-md-8 col-sm-8 col-xs-12">
                                                  <input type="datepicker" class="form-control" name="statsDate" id="statsDate" placeholder="0" required="required">
                                                  </div>
                                                </div>
                                                <script>
                                                $( function() {
                                                  $( "#statsDate" ).datepicker({ 
                                                    changeMonth: true,
                                                    changeYear: true,
                                                    dateFormat:"yy-mm-dd",
                                                    showAnim:"clip",
                                                    showButtonPanel: true,
                                                  });
                                                } );
                                                </script>
                                                <div class="form-group">
                                                  <label class="control-label col-md-4 col-sm-4 col-xs-12"></label>
                                                  <div class="col-md-4 col-sm-4 col-xs-12" id="provReportBtn"></div>
                                                  <div class="col-md-4 col-sm-4 col-xs-12" id="provReportUpdateBtn"></div>
                                                </div>

                                              </form>
                                              </div>
                                              <div class="col-xs-8">
                                                 <table id="provDates" class="table  table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                                  <thead>
                                                  <tr>
                                                    <!--th>id</th-->
                                                    <th>Date</th>
                                                    <th>Disciples</th>
                                                    <th>House Churches</th>
                                                    <th>Localities</th>
                                                    <th>Leaders</th>
                                                    <th>Actions</th>
                                                  </tr>
                                                  </thead>
                                                </table>
                                              </div>
                                            </div>
                                            </div>
                                            <!-- Chart tab pane -->
                                            <div class="tab-pane" id="chart">
                                              <div class="x_content">

                                                <div id="echart_line" style="height:350px;"></div>

                                              </div>
                                            </div>
                                        
                                        </div>
                                      <br />
                                     

                                      
                                    </div>

                              </div>
                            </div>
                              
                             </div> 
                                  <table id="provincesTable" class="table  table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                      <!--th>id</th-->
                                      <th>Province Name</th>
                                      <th>Leader</th>
                                      <th>Tags</th>
                                      <th>Control Disciples*</th>
                                      <th>Actions</th>
                                    </tr>
                                    </thead>
                                  </table>
                                  <br><br>
                                  <div class="product_price">
                                    <h2 class="price ctrlDiscTotal"></h2>
                                    <span class="price-tax">Total Disciples</span>
                                    <br>
                                  </div>
                            </div>
                          </div>
                        </div>

                        </div>
                        <!-- /national spiritual provinces -->

                        <!-- national  localities -->
                        <div role="tabpanel" class="tab-pane fade" id="tab_c4" aria-labelledby="profile-t">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                             <div class="x_content">

                            <ul class="nav navbar-right panel_toolbox">
                   
                                    <li>
                                      <a id="localityBtn" onclick="showHideForm('localityAddForm')" role="button" aria-expanded="false"><i style="font-size: 36px" id="localityAddForm_icon" class="glyphicon glyphicon-plus-sign green"></i></a>
                                      
                                    </li>
                                    <!--li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li-->
                                  </ul>
                              <div id="localityAddForm" style="display: none;" >
                               <div class="clearfix"></div>
                            
                                  <div class="x_panel" style="background-color: #ededed;">
                                    <div class="x_title" id="locTitle">
                                      <h2>Add new Locality <small></small></h2>
                                      
                                      <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                      <br />

                                          <form id="addLocalityForm" class="form-horizontal form-label-left input_mask" action="" method="POST">

                                            <div class="form-group">
                                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Locality Name</label>
                                              <div class="col-md-6 col-sm-6 col-xs-12">
                                              <input type="text" class="form-control" name="locName" id="locName" placeholder="locality" required="required">
                                              </div>
                                            </div>
                                            <div class="form-group">
                                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Start Date<span class="required">*</span>
                                              </label>
                                              <div class="col-md-6 col-sm-6 col-xs-12">
                                              <input id="locDatePlanted" name="locDatePlanted" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text" >
                                              </div>
                                            </div>
                                            <script>
                                              $( function() {
                                                $( "#locDatePlanted" ).datepicker({ changeMonth: true,
                                                changeYear: true});
                                                $( "#locDatePlanted" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
                                                 $( "#locDatePlanted" ).datepicker( "option", "showAnim", "clip");
                                              } );
                                              </script>
                                              
                                              <div class="form-group">
                                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Province</label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select name="locProvince" id="locProvince" class="select2_group form-control provinces">
                                                
                                                </select>
                                                </div>
                                              
                                              </div>
                                        
                                          <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Leaders</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                              <select id="locleadersDropDown" name="locleadersDropDown" class="select2_multiple form-control leaders" multiple="multiple" style="width:100%;">
                                                <option value="select2"></option>
                                              </select>
                                               
                                            </div>
                                            <span><a href="#"  data-toggle="modal" data-target=".newMember"  class="btn btn-success btn-sm"><i class="icon-note"></i> Create Leader</a></span>
                                          </div>
                                        </form>
                                        <div class="modal-footer">
                                          <div class="form-group" id="locSubmit">
                                            <button type="submit" onclick="
                                            (new PNotify({
                                                  title: 'Confirmation Needed',
                                                  text: 'Are you sure you want to add this locality?',
                                                  icon: 'glyphicon glyphicon-question-sign',
                                                  hide: false,
                                                  confirm: {
                                                    confirm: true
                                                  },
                                                  buttons: {
                                                    closer: false,
                                                    sticker: false
                                                  },
                                                  history: {
                                                    history: false
                                                  },
                                                  addclass: 'stack-modal',
                                                  stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
                                                })).get().on('pnotify.confirm', function(){
                                                   var nation = sessionStorage.getItem('nation'); 
                                                  addLocality(nation);
                                                });
                                            " class="btn btn-success"><i class="icon-check "></i> Submit</button>
                                          </div>
                                        </div>

                                     
                                    </div>
                                 
                              </div>
                              
                             </div> 
                             <table id="localitiesTable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                              <thead>
                                    <tr>
                                      <!--th>id</th-->
                                      <th>localityName</th>
                                      <th>Date</th>
                                      <th>Province</th>
                                      <th>Leaders</th>
                                      <th>Actions</th>
                                    </tr>
                                    </thead>
                                    
                            </table>
                           </div>
                         </div>
                       </div>
                        </div>
                        <!-- /national  localities -->

                        <!-- House churches -->
                        <div role="tabpanel" class="tab-pane fade" id="tab_c5" aria-labelledby="profile-t">
                           <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                             <div class="x_content">

                            <ul class="nav navbar-right panel_toolbox">
                   
                                    <li>
                                      <a id="addHC" href="#"  onclick="showHideForm('hcAddForm')" role="button" aria-expanded="false"><i style="font-size: 36px" id="hcAddForm_icon" class="glyphicon glyphicon-plus-sign green"></i></a>
                                      
                                    </li>
                                    <!--li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li-->
                                  </ul>
                             <div id="hcAddForm" style="display: none;" >
                               <div class="clearfix"></div>
                            
                                  <div class="x_panel" style="background-color: #ededed;">
                                    <div class="x_title" id="hcTitle">
                                      <h2>Add New House Church <small>select family</small></h2>
                                      
                                      <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                      <br />
                                      <form id="addHCForm" data-parsley-validate class="form-horizontal form-label-left" method="POST" action="">

                                         <div class="form-group">
                                          <label class="control-label col-md-2 col-sm-2 col-xs-12">Family Name</label>
                                          <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select id="familyDropDown" class="select2_single form-control families">
                                              <option value="select"></option>
                                            </select>
                                          </div>
                                        </div>
                                         <div class="form-group">
                                              <label class="control-label col-md-2 col-sm-2 col-xs-12">Locality</label>
                                              <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select name="hcLocality" id="hcLocality" class="select2_group form-control">
                                                </select>
                                              </div>
                                              
                                        </div>
                                        <div class="form-group">
                                          <label class="control-label col-md-2 col-sm-2 col-xs-12">Date started <span class="required">*</span>
                                          </label>
                                          <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="hcstartDate"  name="hcstartDate" class="date-picker form-control" required="required" type="text">
                                          </div>
                                        </div>
                                        <script>
                                          $( function() {
                                            $( "#hcstartDate" ).datepicker({ changeMonth: true,
                                            changeYear: true});
                                            $( "#hcstartDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
                                             $( "#hcstartDate" ).datepicker( "option", "showAnim", "clip");
                                          } );
                                          </script>
                                          <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Leaders</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                              <select id="leadersDropDown" class="select2_multiple form-control leaders" multiple="multiple" style="width:100%;">
                                                <option value="select2"></option>
                                              </select>
                                            </div>
                                            <span><a href="#"  data-toggle="modal" data-target=".newMember"  class="btn btn-success btn-sm"><i class="icon-note"></i> Create Leader</a></span>
                                          </div>
                                           </form>
                                        <div class="modal-footer">
                                          <div class="form-group" id="hcSubmit">
                                            <!--button type="submit" class="btn btn-primary">Cancel</button-->
                                            <button type="submit" onclick="
                                              (new PNotify({
                                                  title: 'Confirmation Needed',
                                                  text: 'Are you sure you want to add this house church?',
                                                  icon: 'glyphicon glyphicon-question-sign',
                                                  hide: false,
                                                  confirm: {
                                                    confirm: true
                                                  },
                                                  buttons: {
                                                    closer: false,
                                                    sticker: false
                                                  },
                                                  history: {
                                                    history: false
                                                  },
                                                  addclass: 'stack-modal',
                                                  stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
                                                })).get().on('pnotify.confirm', function(){
                                                  var nation = sessionStorage.getItem('nation'); 
                                                  addHouseChurch(nation);
                                                });

                                            " class="btn btn-success"><i class="icon-check "></i> Submit</button>
                                          </div>
                                        </div>
                                    </div>
                              </div>                             
                             </div>     
                             <table id="houseChurchesTable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                              <thead>
                                    <tr>
                                      <!--th>id</th-->
                                      <th>Family Name</th>
                                      <th>Address</th>
                                      <th>City</th>
                                      <th>Leaders</th>
                                      <th>Actions</th>
                                    </tr>
                                    </thead>
                            </table>
                           </div>
                           </div>
                         </div>
                        </div>
                        <!-- /House churches   -->

                        <!-- Disciples -->
                        <div role="tabpanel" class="tab-pane fade" id="tab_c6" aria-labelledby="profile-t">
                          <div id="memberTabs" class="" role="tabpanel" data-example-id="togglable-tabs">
                            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                              <li id="homeTab" role="presentation" class="active tab"><a class="purple" href="#tab_content0" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true"><i class="icon-users purple"></i> Home</a>
                              </li>
                              <li id="newMemberTab" role="presentation" ><a class="green" href="#tab_contentx" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false"><i class="icon-user green"></i> New Person</a>
                              </li>
                              <li id="newMemberTab" role="presentation" ><a class="green" href="#tab_families" role="tab" id="family-tab" data-toggle="tab" aria-expanded="false"><i class="icon-home green"></i> Families</a>
                              </li>
                              <!--li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Profile</a>
                              </li-->
                            </ul>
                            <div id="myTabContent" class="tab-content">
                              <div role="tabpanel" class="tab-pane fade active in" id="tab_content0" aria-labelledby="home-tab" tabindex='1'>
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                  <div class="x_content">

                                  <table id="datatable-disciples" class="table  table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                      <th>FirstName</th>
                                      <th>LastName</th>
                                      <th>Locality</th>
                                      <th>City</th>
                                      <th>Country</th>
                                      <th>Sex</th>
                                      <th>Phone</th>
                                      <th>E-mail</th>
                                      <th>Tags</th>
                                      <th>Actions</th>
                                    </tr>
                                    </thead>
                                  </table>
                                </div>
                              </div>
                                  </div>
                                </div>
                                <!-- New member tab -->
                                <div role="tabpanel" class="tab-pane fade active in" id="tab_contentx" aria-labelledby="home-tab" tabindex='1'>

                                  <div id="newMemberDiv">
                                  </div>

                                </div>
                                 <!-- Family tab -->
                                <div role="tabpanel" class="tab-pane fade in" id="tab_families" aria-labelledby="home-tab" tabindex='1'>

                                  <div id="familiesDiv">
                                  </div>

                                </div>
                                  
                                  
                                
                                </div>

          

                          </div>
                           
                        </div>
                        <!-- /Disciples   -->

                        <!-- Calendar  -->
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_c7" aria-labelledby="profile-t7">
                          <div id="calendarDiv"></div>
                        </div>
                        <!-- /Calendar -->

                        <div role="tabpanel" class="tab-pane fade" id="tab_c8" aria-labelledby="profile-t8">
                          <div id="eventsDiv"></div>
                        </div>
                         <div role="tabpanel" class="tab-pane fade" id="tab_c9" aria-labelledby="profile-t9">
                          <div id="settingsDiv">
                            <div class="x_panel">
                             <div class="x_content">

                                <div class="col-xs-10">
                                  <!-- Tab panes -->
                                  <div class="tab-content">
                                    <div class="tab-pane active" id="home-l">
                                      <p class="lead">General settings</p>
                                       <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="x_panel">
                                          <div class="x_title">
                                            <h2>Nation Details<small>Update</small></h2>
                                            <ul class="nav navbar-right panel_toolbox">
                                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                              </li>
                                              <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                <ul class="dropdown-menu" role="menu">
                                                  <li><a href="#">Settings 1</a>
                                                  </li>
                                                  <li><a href="#">Settings 2</a>
                                                  </li>
                                                </ul>
                                              </li>
                                              <li><a class="close-link"><i class="fa fa-close"></i></a>
                                              </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                          </div>
                                          <div class="x_content">

                                            <form class="form-horizontal form-label-left">
                                              <div class="form-group">
                                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Country Name</label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <input id="nationName" name="nationName" type="text" class="form-control" placeholder="" value="">
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <label class="control-label col-md-2 col-sm-2 col-xs-12">Date started <span class="required">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <input id="nationStartDate"  name="nationStartDate" class="date-picker form-control" value="" required="required" type="text">
                                                </div>
                                              </div>
                                              <script>
                                                $( function() {
                                                  $( "#nationStartDate" ).datepicker({ changeMonth: true,
                                                  changeYear: true});
                                                  $( "#nationStartDate" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
                                                   $( "#nationStartDate" ).datepicker( "option", "showAnim", "clip");
                                                } );
                                                </script>
                                                <div class="form-group">
                                                   <label class="control-label col-md-2 col-sm-2 col-xs-12">Region Name  <span class="required">*</span></label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <select name="nregion" id="nregion" class="form-control nregions">
                                                      <option>Choose region</option>
                                                    </select>
                                                    </div>
                                                  <span><a href="#"  data-toggle="modal" data-target=".newRegion" id="newRegionBtn" class="btn btn-success btn-sm"><i class="icon-note"></i> New Region</a></span>
                                                </div>

                                                <div class="form-group">
                                                  <label class="control-label col-md-2 col-sm-2 col-xs-12">Leader No.1</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <select id="nLeaderDropDown"  name="nLeaderDropDown" class="form-control leaders" >
                                                      <option>Choose leader</option>
                                                    </select>
                                                  </div>
                                                  <span><a href="#"  data-toggle="modal" data-target=".newMember"  class="btn btn-success btn-sm"><i class="icon-note"></i> Create Leader</a></span>
                                                </div>

                                              <div class="divider-dashed"></div>
                                                <div class="form-group">
                                                  <label class="control-label col-md-2 col-sm-2 col-xs-12">Time Zone GMT</label>
                                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" class="form-control" placeholder="">
                                                  </div>
                                                </div>
                                              
                                            </form>

                                          </div>
                                          <div class="modal-footer">
                                            <button class="btn btn-sm btn-primary"><i class="fa fa-check-circle" onclick="editNation();"></i> Save</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="tab-pane" id="users-l">
                                      <div id="usersDiv"></div>
                                    </div>
                                    <div class="tab-pane" id="messages-l">User Permissions Tab.</div>
                                    <div class="tab-pane" id="settings-l">Settings Tab.</div>
                                  </div>
                                </div>

                                <div class="col-xs-2">
                                  <!-- required for floating -->
                                  <!-- Nav tabs -->
                                  <ul class="nav nav-tabs tabs-right">
                                    <li class="active"><a href="#home-l" data-toggle="tab">General</a>
                                    </li>
                                    <li><a href="#users-l" data-toggle="tab">Users</a>
                                    </li>
                                    <!--li><a href="#messages-l" data-toggle="tab">User Permissions</a>
                                    </li>
                                    <li><a href="#settings-l" data-toggle="tab">Settings</a>
                                    </li-->
                                  </ul>
                                </div>

                              </div>
                            </div>
                          </div>
                        </div>
                        

                      </div>
                    </div>
                  </div>
                
            </div>
          </div>
        </div>
      </div>
	 

  <!-- daterangepicker -->
  <script type="text/javascript" src="js/moment/moment.min.js"></script>
  <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>

 <!-- echart -->
  <script src="js/echart/echarts-all.js"></script>
  <script src="js/echart/green.js"></script>

  <!-- Nation Profile scripts -->
 <script type="text/javascript" src="js/includes/nationProfile.js"></script>
 <script type="text/javascript" src="js/includes/events.js"></script>
  
<script type="text/javascript">
     
</script>