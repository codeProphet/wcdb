 
//Global variables
var sresponse="Success";
var fresponse="Something's wrong";

//api variables
var api_root_url="api/api.php/records/";
var tbl_prefix="cmfi_";




$(document).on('change', 'select#countryName', function(){

            var value = $('select#countryName option:selected').val();
		$("#localities").load("includes/fetchLocalities.php?nationID="+value)

});
$(document).on('change', 'select#countryNamez', function(){

            var value = $('select#countryNamez option:selected').val();
		$("#localitiesz").load("includes/fetchLocalities.php?nationID="+value);

});
//fill localities after nation combo is selected
$(document).on('change', 'select#wnation', function(){

            var value = $('select#wnation option:selected').val();
		$("#wlocalities").load("includes/fetchLocalities.php?nationID="+value);
		$("#finishImportForm").html('<input id="nation" name="nation" type="hidden" value="'+value+'" />');

});


$(document).on('click','.tab',function(){
	//remove the active class from all other tabs and make this only active
	if ( $(this).hasClass('active') ) {
			$(this).removeClass('active');
		}
		else {
			ul.$('li.active').removeClass('active');
			$(this).addClass('active');
		}
	
});

function initialiseFamiliesCombo(n){
	//populate family drop down
        var opt = $(".families");
          $.getJSON("api/funcs.php?fn=fetchFamilies&n="+n, function(response) {
               $.each(response, function() {
               opt.append($("<option />").val(this.id).text(this.familyName));
           });
        });
}


function initialiseRegionsCombo(){
       //populate leaders drop down
      $('.nregions').html('<option>Choose region</option>'); 
      var opt = $(".nregions");
          $.getJSON("api/funcs.php?fn=fetchRegions", function(response) {
               $.each(response, function() {
               opt.append($("<option />").val(this.code).text(this.name));
           });
        });
    }


function initialiseLoclaitiesCombo(n){
	$('.localities').html('<option>Choose Locality</option>'); 
	var x = $(".localities");
          $.getJSON("api/funcs.php?fn=fetchLocalities&n="+n, function(response) {
               $.each(response, function() {
               x.append($("<option />").val(this.id).text(this.localityName));
           });
        });
}

function initialiseProvincesCombo(n){
	$('.provinces').html('<option>Choose province</option>'); 
	var x = $(".provinces");
          $.getJSON("api/funcs.php?fn=fetchProvinces&n="+n, function(response) {
               $.each(response, function() {
               x.append($("<option />").val(this.id).text(this.provinceName));
           });
        });
}

function refreshMemberCombo(n){
	$('.leaders').html('<option>Choose disciple</option>'); 
	var opt = $(".leaders");
          $.getJSON("api/funcs.php?fn=fetchmembers&n="+n, function(response) {
               $.each(response, function() {
               opt.append($("<option />").val(this.id).text(this.firstName+' '+this.lastName));
           });
        });
}
 function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete?");
      if (x)
          return true;
      else
        return false;
    }

function showHide(div){
	var x=document.getElementById(div);
	if(x.style.display == 'block'){
		x.style.display = 'none';
	}else{
		x.style.display = 'block'; 
	}
}

function showHideForm(div){
	var icon=div+"_icon";
	changeIcon(icon);
	var x=document.getElementById(div);
	if(x.style.display == 'block'){
		x.style.display = 'none';
	}else{
		x.style.display = 'block'; 
	}
}


function show(div){
	var icon=div+"_icon";
	var x=document.getElementById(div);
	if(x.style.display == 'none'){
		x.style.display = 'block';
		changeIcon(icon);
	}
}



function changeIcon(icon){
  var x = document.getElementById(icon);
  if(x){
    if (x.classList.contains("glyphicon-plus-sign")==true) {
        x.classList.remove("glyphicon-plus-sign");
        x.classList.remove("green");
        x.classList.add("glyphicon-remove-sign");
        x.classList.add("red");
    } else {  x.classList.remove("glyphicon-remove-sign");
    	x.classList.remove("red");
        x.classList.add("glyphicon-plus-sign");
        x.classList.add("green");
        
    }
}
}

//function to refresh datatables
function refreshTable(tableId, urlData)
{
  $.getJSON(urlData, null, function( json )
  {
    table = $(tableId).dataTable();
    oSettings = table.fnSettings();

    table.fnClearTable(this);

    for (var i=0; i<json.aaData.length; i++)
    {
      table.oApi._fnAddData(oSettings, json.aaData[i]);
    }

    oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
    table.fnDraw();
  });
}



	
function changeLang(lang){
	window.location.search += '&lang='+lang;

}

function getNations(nation){
		$.getJSON('api/funcs.php?fn=getNationsL&letter='+nation, function(data) {
		var template = $('#nationtpl').html();
		var html = Mustache.to_html(template, data);
		$('#nationsDisplay').html(html);
	});
}
	function nationProfile(id,name){
			//alert (id);
			$("#contentDiv").load("views/nationDash.php?nation="+id+"&nationName="+name)
											
												return false;
		 }
	
	function showUserProfile(id){
			//alert (id);
			$("#contentDiv").load("views/viewProfile.php?userId="+id)
											
												return false;
		 }
  
	function loadProvinces(id){
			//alert (id);
			$("#provinces").load("includes/fetchProvinces.php?nationID="+id)
											
												return false;
		}
	function loadLocalities(id){
			//alert (id);
			$("#contentDiv").load("views/localities.php?nationID="+id)
											
												return false;
		 }
//view member		 
	function viewMemberTab(id,name){
		var tabID="tab_content"+id;
		//$("#homeTab").removeClass("active");
		//$('tab_content0').removeClass('active');$('tab_content0').removeClass('in');
		$("#"+tabID).addClass("active");$("#"+tabID).addClass("in");
		$("#homeTab").after('<li role="presentation" class="tab"><a href="#'+tabID+'" role="tab" id="profile-tab'+id+'" data-toggle="tab" aria-expanded="true">'+name+'</a></li>');
		$("#tab_content0").after( '<div role="tabpanel" class="tab-pane fade in" id="'+tabID+'" aria-labelledby="home-tab"></div>')
		$("#"+tabID).load("views/memberProfile.php?id="+id);
		//open the selected member tab
		$("#profile-tab"+id).trigger("click");
		//'<div role="tabpanel" class="tab-pane fade" id="tab_content<?php echo $row["id"];?>" aria-labelledby="profile-tab"></div>';
	}	

//close tab	
	function closeTab(tabId){
		$("#"+tabId).hide(500);
		//$("#"+tabId).hide().html($("#homeTab").html()).show(500);
		$('a[href$="'+tabId+'"]').parent().remove();
		$("#homeTab").trigger("click");
	}
	
//Edit settings
	function editSet(id,name,value){
		var rowId="row"+id;
		var currentTD = $("#editset"+id).parents('tr').find('td');
		var x=$("#editset"+id).html();
		//alert (x);
          if ($("#editset"+id).html() == '<i class="glyphicon glyphicon-pencil"></i>') {                  
              $.each(currentTD, function () {
                  $(this).prop('contenteditable', true),
				  $(this).prop('style', 'color:red')
              });
			  
          } else {
             $.each(currentTD, function () {
                  $(this).prop('contenteditable', false),
				  $(this).prop('style', 'color:black')
				  
              });
			  var id_=id;
			  var name_ = $("#name"+id).html();
			  var value_ = $("#value"+id).html();
			  saveSetting(id_,name_,value_);
          }
		$("#editset"+id).html($("#editset"+id).html() == '<i class="glyphicon glyphicon-pencil"></i>' ? '<i class="glyphicon glyphicon-ok"></i> Save' : '<i class="glyphicon glyphicon-pencil"></i>')
		//$("#"+rowId).remove();
		//$("#"+rowId).after('<tr id="rowId"><form id="settings" method="post"><td><input type="text" class="form-control" name="name" id="name"  value="'+name+'" required="required"></td><td><input type="text" class="form-control" id="value" name="value"  value="'+value+'" required="required"><input id="settingId" name="settingID" type="hidden" value="'+id+'" /></td></form><td><a onclick="saveSetting()" id="settingSave" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-ok"></i></a></td></tr>');
	}
	
//------------------event functions-------------------//
//Edit Event
function saveEvent() {
              var title =$('#eventTitlex').val();
              var eventDesc=$('#eventDescx').val();
              var start=$('#startDatex').val();
              var end=$('#endDatex').val();
              var eventColor=$('#eventColorx').val();
              var eventId=$('#eventIdx').val(); //alert(eventId);
              var nation= $('#eventRegion option:selected').val();
              
              $.ajax({
                url: 'api/post.php',
                data: 'type=fullUpdate&title='+title+'&eventId='+eventId+'&start='+start+'&end='+end+'&eventColor='+eventColor+'&eventDesc='+eventDesc+'&nation='+nation,
                type: 'POST',
                dataType: 'json',
                success: function(response){successNote(sresponse,'event saved sussessfully!')
                  //if(response.status == 'success')
                  $('#calendar').fullCalendar('refetchEvents');
                },
                error: function(e){
                  errorNote('Error processing your request: '+e.responseText);
                }
              });
              

  }

 function saveEvent2() {
              var title =$('#eventTitley').val();
              var eventDesc=$('#eventDescy').val();
              var start=$('#reservation').startDate.val();
              var end=$('#reservation').endDate.val();
              var eventColor=$('#eventColory').val();
              var eventId=$('#eventIdy').val(); //alert(eventId);
              var nation= $('#eventRegion option:selected').val();
              
              $.ajax({
                url: 'api/post.php',
                data: 'type=fullUpdate&title='+title+'&eventId='+eventId+'&start='+start+'&end='+end+'&eventColor='+eventColor+'&eventDesc='+eventDesc+'&nation='+nation,
                type: 'POST',
                dataType: 'json',
                success: function(response){successNote(sresponse,'event saved sussessfully!')
                  //if(response.status == 'success')
                  $('#calendar').fullCalendar('refetchEvents');
                },
                error: function(e){
                  errorNote('Error processing your request: '+e.responseText);
                }
              });
              

  }

//register event from form
function eventRegister(){
	var formName_="eventRegForm"; 
			$("#loaderImg").show(); //show loader image
			var members_=$("#membersDropDown").val();
			var id_=$("#eventId").val();
			//alert(id_);
			if(members_=='')
			{
			alert("Please select members ");
			}
			else{
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				formName:formName_,
				members:members_,
				id:id_
			},
			function(data,status){ // Required Callback Function
			var obj= $.parseJSON(data);
				console.log(obj['msg']);
			if(obj['status']==1){successNote(sresponse,obj['msg']);
			}else{
				errorNote(fresponse,obj['msg']+" Event Registration failed!");
			}
				$('#eventsRegTable').DataTable().ajax.reload();
				//$("#membersDropDown")[0].reset();
				$("#loaderImg").hide(); //hide loader image
			});
			
			}

}

//register a member to an event automatically
function eventRegisterAuto(memberId){
	var formName_="eventRegFormAuto"; 
			$("#loaderImg").show(); //show loader image
			var member_=memberId;
			var id_=$("#eventId").val();
			if(id_>0 && id_!=null){
				//alert(id_);
				if(member_=='')
				{
				alert("member Id not specified ");
				}
				else{
				$.post("api/post.php", //Required URL of the page on server
				{ // Data Sending With Request To Server
					formName:formName_,
					member:member_,
					id:id_
				},
				function(data,status){ // Required Callback Function
				var obj= $.parseJSON(data);
					console.log(obj['msg']);
				if(obj['status']==1){successNote(sresponse,obj['msg']);
				}else{
					errorNote(fresponse,obj['msg']+" Event Registration failed!");
				}
					$('#eventsRegTable').DataTable().ajax.reload();
					//$("#membersDropDown")[0].reset();
					$("#loaderImg").hide(); //hide loader image
				});
				
			}
		}

}

 //--------------------------------------------------//
//change admin permissions
function checkAdmin(id_){
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
			
			formName:'checkAdmin',
				userId:id_
			},
			function(response,status){ // Required Callback Function
			if(response==1){successNote(sresponse,'Setting data saved sussessfully!')
				}else{
					errorNote(fresponse,'Setting data not saved!')
				}
			});
			
		}


//save setting data to file	
function saveSetting(id_,name_,value_){
			
			if(name_=='' && value_=='')
			{
			alert("Please fill out the form");
			}
			else{
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				settingId:id_,
				name:name_,
				value:value_
			},
			function(response,status){ // Required Callback Function
			if(response==1){successNote(sresponse,'Admin setting <strong>'+name_+'</strong> changed sussessfully!')
			}else{
				errorNote(fresponse,'Setting data not saved!')
			}
			$("#form"+id_)[0].reset();
			//$("#rowId").remove();
			});
			}
		}

		
		
//import data from excel csv file 
function importFile(){ 
			$("#loading").prop('style', 'display:visible');
			var filesize=$("#file").val().length;
			if(filesize>0){
			$.ajax({
				url: "controllers/importExcel.php", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: new FormData(excelImportForm), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				contentType: false,       // The content type used when sending data to the server.
				cache: false,             // To unable request pages to be cached
				processData:false,        // To send DOMDocument or non processed data file it is set to false
				beforeSend: function () { $("#loaderImg").show(); },
				complete: function () { $("#loaderImg").hide(); },
				success: function(data)   // A function to be called if request succeeds
				{
					successNote(sresponse,'Data uploaded to server successfully!')
					$("#loading").prop('style', 'display:none');
				}
				});
				
			}
			
		}
		
//import data from excel csv file 
function finishImport(){ 
			$("#iloading").prop('style', 'display:visible');
			$.ajax({
				url: "controllers/finishImport.php", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method, 
				data:new FormData(finishImportForm),// Data sent to server, a set of key/value pairs (i.e. form fields and values)
				contentType: false,       // The content type used when sending data to the server.
				cache: false,             // To unable request pages to be cached
				processData:false,        // To send DOMDocument or non processed data file it is set to false
				success: function(data)   // A function to be called if request succeeds
				{

					console.log(data);
					successNote(sresponse,'Data imported to server successfully!')
					clearTemp();
					$("#iloading").prop('style', 'display:none');
				}
				});
			
		}
		
//backup database 
function backupDB(){ 
			$.ajax({
				url: "controllers/backupDB.php", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: new FormData(backupForm), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				contentType: false,       // The content type used when sending data to the server.
				cache: false,             // To unable request pages to be cached
				processData:false,        // To send DOMDocument or non processed data file it is set to false
				success: function(data)   // A function to be called if request succeeds
				{
					successNote(sresponse,'Database /tables backed up successfully :)!')
				}
				});			
		}
				
//clear temporary table
function clearTemp(){
	$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				procedure:"clearTemp"
			},
			function(response,status){ // Required Callback Function
			if(response==1){successNote(sresponse,'data cleared sussessfully!')
			}else{
				errorNote(fresponse,'data not cleared!')
			}
			});
	
}

	
	
//-----------------------------------MANAGING THE NATION OBJECT-------------------------------------------//
//save Nation data to file	
function saveNation(){ 
			var formName_="addNation";
			var ministryDate_=$("#finishDate").html(); 
			var region_=$("#nregion option:selected").val();
			var name_=document.getElementById("ncountryCode").value
			var leader_=$("#nleader option:selected").val();
		    // var timeZone =$('#timeZone').val();
			//alert(ministryDate_+'|'+region_+'|'+name_+'|'+leader_);
			if(name_=='')
			{
			alert("Please select country ");
			}
			else{
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				formName:formName_,
				ministryDate:ministryDate_,
				name:name_,
				leader:leader_,
				region:region_,
			   // timeZone:timeZone.value=res.timeZone,
			},
			function(data,status){ // Required Callback Function
				var obj= $.parseJSON(data);
					console.log(obj['msg']);
				if(obj['status']==1){successNote(sresponse,obj['msg']);

				}
				else{
					errorNote(fresponse,obj['msg']+" data not saved!");
				}
			});
			
			
			}
		}
//save Nation data to file	
function editNation(){ 
			var formName_="editNation";
			var nationName_=$('#nationName').val();
			var ministryDate_=$("#nationStartDate").html(); 
			var region_=$("#nregion option:selected").val();
			var name_=sessionStorage.getItem("nation");
			var leader_=$("#nLeaderDropDown option:selected").val();
			//alert(ministryDate_+'|'+region_+'|'+name_+'|'+leader_);
			if(name_=='')
			{
			alert("Please select country ");
			}
			else{
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				formName:formName_,
				ministryDate:ministryDate_,
				name:name_,
				nationName:nationName_,
				leader:leader_,
				region:region_
			},
			function(data,status){ // Required Callback Function
				var obj= $.parseJSON(data);
					console.log(obj['msg']);
				if(obj['status']==1){successNote(sresponse,obj['msg']);
				}else{
					errorNote(fresponse,obj['msg']+" data not saved!");
				}
			});
			
			}
		}
//this is the function i want to use the value for
//where excatly g
function selectCountryOnMapAndCombo(value){
		
         var data=JSON.stringify(value);
          console.log(data);
          var mapObject = $('#world-map-gdp1').vectorMap('get', 'mapObject');
          mapObject.setSelectedRegions(JSON.parse(data)); //to set
          var name = mapObject.getRegionName(value);
        
          $("#ncountryCode").html(name);
          document.getElementById("ncountryCode").value=value;
          $("#finishNation").html(name);
          $("#finishNationChk").html("<i class='icon-check green'></i>");
          document.getElementById("countryx").value=value;
         
          refreshMemberCombo(value);
		}
//--------------------------------------------------------------------
//--CALENDAR FUNCTIONS------------------------------------------------
//--------------------------------------------------------------------
/*function(event){
    var title = event.title;
    var start = event.start.format("YYYY-MM-DD[T]HH:MM:SS");
    $.ajax({
      url: 'api/post.php',
      data: 'type=new&title='+title+'&startdate='+start+'&zone='+zone,
      type: 'POST',
      dataType: 'json',
      success: function(response){
        event.id = response.eventid;
        $('#calendar').fullCalendar('updateEvent',event);
      },
      error: function(e){
        console.log(e.responseText);
      }
   });
    $('#calendar').fullCalendar('updateEvent',event);
}

/*rename event
eventClick: function(event, jsEvent, view) {
   var title = prompt('Event Title:', event.title, { buttons: { Ok: true, Cancel: false} });
   if (title){
   event.title = title;
   $.ajax({
     url: 'process.php',
     data: 'type=changetitle&title='+title+'&eventid='+event.id,
     type: 'POST',
     dataType: 'json',
     success: function(response){
       if(response.status == 'success')
       $('#calendar').fullCalendar('updateEvent',event);
     },
     error: function(e){
       alert('Error processing your request: '+e.responseText);
     }
   });
   }
}*/

//drop event to another date
function deleteEvent(id,title){// alert('now in');
		(new PNotify({
                  title: 'Confirmation Needed',
                  text: 'Are you sure you want to delete this event: <strong>'+title+'</strong> ?',
                  icon: 'glyphicon glyphicon-question-sign',
                  hide: false,
                  confirm: {
                    confirm: true
                  },
                  buttons: {
                    closer: false,
                    sticker: false
                  },
                  history: {
                    history: false
                  },
                  addclass: 'stack-modal',
                  stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
                })).get().on('pnotify.confirm', function(){
					var formName_="deleteEvent";
					var eventId=id;
					$.post("api/post.php", //Required URL of the page on server
					{ // Data Sending With Request To Server
					formName:formName_,
					code:eventId
					},
					function(response,status){ // Required Callback Function
					if(response==1){successNote(sresponse,'Event '+title+' deleted sussessfully!')
					}else{
						errorNote(fresponse,'Event '+title+' not deleted!')
					}
					$("#deleteEvent"+id)[0].reset();
					});
			});
		}



//update Nation information

//Delete Nation from file
function deleteNation(id,nationName){// alert('now in');
			(new PNotify({
                  title: 'Confirmation Needed',
                  text: 'Are you sure you want to delete this nation?',
                  icon: 'glyphicon glyphicon-question-sign',
                  hide: false,
                  confirm: {
                    confirm: true
                  },
                  buttons: {
                    closer: false,
                    sticker: false
                  },
                  history: {
                    history: false
                  },
                  addclass: 'stack-modal',
                  stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
                })).get().on('pnotify.confirm', function(){
					//$("[data-dismiss=modal]").trigger({type:"click"});
					var formName_="deleteNation";
					var nationId=id;
					//var nationName=$("#nationNamez").val();
					//alert (nationName);
					$.post("api/post.php", //Required URL of the page on server
					{ // Data Sending With Request To Server
					formName:formName_,
					code:nationId
					},
					function(response,status){ // Required Callback Function
					if(response==1){successNote(sresponse,'Nation '+nationName+' deleted sussessfully!')
					}else{
						errorNote(fresponse,'Nation '+nationName+' not deleted!')
					}
					 $('#datatable-nations').DataTable().ajax.reload(); //refresh table
					});
			});
		}



//-----------------------------------MANAGING THE LOCALITY OBJECT-------------------------------------------//
//save Locality data to file	
function addLocality(n){ 
			var formName_="addLocalityForm";
			var datePlanted_=$("#locDatePlanted").html();
			var localityName_=$("#locName").html();
			var nation_=n;
			var province_=$("#locProvince option:selected").val();
			var leaders_=$("#locleadersDropDown").val();
			//alert(leaders_);
			if(nation_=='')
			{
			alert("Please fill out the nation name");
			}
			else{
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				formName:formName_,
				localityName:addLocalityForm.locName.value,
				province:province_,
				country:nation_,
				leaders:leaders_,
				datePlanted:addLocalityForm.locDatePlanted.value
			},
			function(response,status){ // Required Callback Function
				if(response==1){successNote(sresponse,'New locality <strong>'+addLocalityForm.locName.value+'</strong>  added sussessfully!')
				}else{
					errorNote(fresponse,'data not saved! Locality already exists in Database')
				}

				$("#addLocalityForm")[0].reset();
				$('#localitiesTable').DataTable().ajax.reload(); //refresh table
				showHide("localityAddForm");
				changeIcon("localityAddForm_icon");
				initialiseNationTotals(nation_);
			});
			}
		}

//save Locality data to file	
function editLocality(id_){ 
			var formName_="editLocalityForm";
			var datePlanted_=$("#locDatePlanted").html();
			var localityName_=$("#locName").val();
			var nation_=$("#nationCode").val();
			var province_=$("#locProvince option:selected").val();
			var leaders_=$("#locleadersDropDown").val();
			//alert(leaders_);
			if(nation_=='')
			{
			alert("Please fill out the nation name");
			}
			else{
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				formName:formName_,
				localityName:addLocalityForm.locName.value,
				province:province_,
				country:nation_,
				leaders:leaders_,
				datePlanted:addLocalityForm.locDatePlanted.value,
				id:id_
			},
			function(data,status){ // Required Callback Function
			var obj= $.parseJSON(data);
				console.log(obj['msg']);
			if(obj['status']==1){successNote(sresponse,'Locality <strong>'+addLocalityForm.locName.value+'</strong>  updated sussessfully!');
			}else{
				errorNote(fresponse,obj['msg']+" data not saved!");
			}
				$('#localitiesTable').DataTable().ajax.reload();
				//$("#localityAddForm")[0].reset();
				showHide("localityAddForm");
				changeIcon("localityAddForm_icon");
			});
			}
		}

function deleteLocality(id){
	(new PNotify({
                  title: 'Confirmation Needed',
                  text: 'Are you sure you want to delete this locality?',
                  icon: 'glyphicon glyphicon-question-sign',
                  hide: false,
                  confirm: {
                    confirm: true
                  },
                  buttons: {
                    closer: false,
                    sticker: false
                  },
                  history: {
                    history: false
                  },
                  addclass: 'stack-modal',
                  stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
                })).get().on('pnotify.confirm', function(){
					var formName_="deleteLocalityForm";
					var nation_=$("#nationCode").val();
					var id_=id;
					if (ConfirmDelete()){
						$.post("api/post.php",
						{
							formName:formName_,
							id:id_
						},
						function(response,status){ // Required Callback Function
								if(response==1){successNote(sresponse,'Locality deleted sussessfully!');
								}else{
									errorNote(fresponse,'operation not performed');
								}
								$('#localitiesTable').DataTable().ajax.reload(); //refresh provinces table
								initialiseNationTotals(nation_);
							});	
					}	
			});
}

//-----------------------------------MANAGING THE PROVINCE OBJECT-------------------------------------------//
//save province data to file	
function addProvince(n){ 
			(new PNotify({
                  title: 'Confirmation Needed',
                  text: 'Are you sure you want to add this province?',
                  icon: 'glyphicon glyphicon-question-sign',
                  hide: false,
                  confirm: {
                    confirm: true
                  },
                  buttons: {
                    closer: false,
                    sticker: false
                  },
                  history: {
                    history: false
                  },
                  addclass: 'stack-modal',
                  stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
                })).get().on('pnotify.confirm', function(){
					var formName_="addProvinceForm";
					var provincialLeader_=$("#provleaderDropDown option:selected").val();
					var provinceName_=$("#provName").html();
					var disciples_=$("#ctrlTotal").val();
					var nation_=$("#nationCode").val();
                  if(nation_=='')
					{
					alert("Please fill out the nation name");
					}
					else{
					$.post("api/post.php", //Required URL of the page on server
					{ // Data Sending With Request To Server
						formName:formName_,
						name:addProvinceForm.provName.value,
						leader:provincialLeader_,
						disciples:disciples_,
						nation:nation_
					},
					function(response,status){ // Required Callback Function
						if(response==1){successNote(sresponse,'New province in '+nation_+', <strong>'+addProvinceForm.provName.value+'</strong> province added sussessfully!')
						}else{
							errorNote(fresponse,'data not saved! Province already exists in Database')
						}
						$("#addProvinceForm")[0].reset(); //clear form
						showHide("provinceAddForm"); //hide form
						changeIcon("provinceAddForm_icon");
						$('#provincesTable').DataTable().ajax.reload(); //refresh provinces table
						initialiseNationTotals(nation_);

					});
					}
                });
			
		}

function editProvince(i){ 
			var formName_="editProvinceForm";
			var provincialLeader_=$("#provleaderDropDown option:selected").val();
			var provinceName_=$("#provName").html();
			var nation_=$("#nationCode").val();
			var id_=i; //alert(provincialLeader_);
			
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				formName:formName_,
				name:addProvinceForm.provName.value,
				leader:provincialLeader_,
				disciples:addProvinceForm.ctrlTotal.value,
				nation:nation_,
				id:id_
			},
			function(data,status){ // Required Callback Function
			var obj= $.parseJSON(data);
				console.log(obj['text']);
			if(obj['status']=="1"){successNote(sresponse,'<strong>'+addProvinceForm.provName.value+'</strong> province updated sussessfully!');
			}else{
				errorNote(fresponse,obj['text']+" data not saved!");
			}
			
				$("#addProvinceForm")[0].reset(); //clear form
				showHide("provinceAddForm"); //hide form
				changeIcon("provinceAddForm_icon");
				$('#provincesTable').DataTable().ajax.reload(); //refresh provinces table
			});
			
		}

function deleteProvince(id){
	(new PNotify({
                  title: 'Confirmation Needed',
                  text: 'Are you sure you want to delete this province?',
                  icon: 'glyphicon glyphicon-question-sign',
                  hide: false,
                  confirm: {
                    confirm: true
                  },
                  buttons: {
                    closer: false,
                    sticker: false
                  },
                  history: {
                    history: false
                  },
                  addclass: 'stack-modal',
                  stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
                })).get().on('pnotify.confirm', function(){
					var formName_="deleteProvinceForm";
					var nation_=$("#nationCode").val();
					var id_=id;
						$.post("api/post.php",
						{
							formName:formName_,
							id:id_
						},
						function(response,status){ // Required Callback Function
								if(response==1){successNote(sresponse,'Province deleted sussessfully!');
								}else{
									errorNote(fresponse,'operation not performed');
								}
								$('#provincesTable').DataTable().ajax.reload(); //refresh provinces table
								initialiseNationTotals(nation_);
							});	
						
			 });
}

//-----------------------------------MANAGING THE HOUSE CHURCH OBJECT-------------------------------------------//
//save house church data to file	
function addHouseChurch(n){ 
	var familyName_=$("#familyDropDown option:selected").html();
	(new PNotify({
                  title: 'Confirmation Needed',
                  text: 'Are you sure you want to add the house church at the'+familyName_+'s ?',
                  icon: 'glyphicon glyphicon-question-sign',
                  hide: false,
                  confirm: {
                    confirm: true
                  },
                  buttons: {
                    closer: false,
                    sticker: false
                  },
                  history: {
                    history: false
                  },
                  addclass: 'stack-modal',
                  stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
                })).get().on('pnotify.confirm', function(){
					var formName_="addHCForm";
					var familyId_=$("#familyDropDown option:selected").val();
					var familyName_=$("#familyDropDown option:selected").html();
					var startDate_=$("#hcstartDate").val();
					var locality_=$("#hcLocality option:selected").val();
					var leaders_=$("#leadersDropDown").val();
					var nation_=$("#nationCode").val();
					//alert(leaders_);
					
					if(nation_=='')
					{
					alert("Please fill out the nation name");
					}
					else{
					$.post("api/post.php", //Required URL of the page on server
					{ // Data Sending With Request To Server
						formName:formName_,
						familyId:familyId_,
						locality:locality_,
						startDate:startDate_,
						leaders:leaders_,
						nation:nation_
					},
					function(data,status){ // Required Callback Function
					var obj= $.parseJSON(data);
						console.log(obj['text']);
					if(obj['id']=="1"){successNote(sresponse,'New House Church at the <strong>'+familyName_+'s</strong>  added sussessfully!');
					}else{
						errorNote(fresponse,obj['text']+" data not saved!");
					}
					$("#addHCForm")[0].reset(); //clear form
						showHide("hcAddForm"); //hide form
						changeIcon("hcAddForm_icon");
						$('#houseChurchesTable').DataTable().ajax.reload(); //refresh house churches table
						initialiseNationTotals(nation_);
					});
					
					}
			});
		}

//update house church data to file	
function updateHouseChurch(id_){ 
			var formName_="updateHCForm";
			var familyId_=$("#familyDropDown option:selected").val();
			var familyName_=$("#familyDropDown option:selected").html();
			var startDate_=$("#hcstartDate").val();
			var locality_=$("#hcLocality option:selected").val();
			var leaders_=$("#leadersDropDown").val();
			var nation_=$("#nationCode").val();
			//alert(leaders_);
			
			if(nation_=='')
			{
			alert("Nation name not found");
			}
			else{
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				formName:formName_,
				familyId:familyId_,
				locality:locality_,
				startDate:startDate_,
				leaders:leaders_,
				id:id_,
				nation:nation_
			},
			function(data,status){ // Required Callback Function
			var obj= $.parseJSON(data);
				console.log(obj['text']);
			if(obj['id']=="1"){successNote(sresponse,'House Church at the <strong>'+familyName_+'s</strong>  Updated sussessfully!');
			}else{
				errorNote(fresponse,obj['text']+" data not saved!");
			}
			$("#addHCForm")[0].reset(); //clear form
				showHide("hcAddForm"); //hide form
				changeIcon("hcAddForm_icon");
				$('#houseChurchesTable').DataTable().ajax.reload(); //refresh provinces table
			});
			
			}
		}

function deleteHouseChurch(id){
	(new PNotify({
                  title: 'Confirmation Needed',
                  text: 'Are you sure you want to delete this house church?',
                  icon: 'glyphicon glyphicon-question-sign',
                  hide: false,
                  confirm: {
                    confirm: true
                  },
                  buttons: {
                    closer: false,
                    sticker: false
                  },
                  history: {
                    history: false
                  },
                  addclass: 'stack-modal',
                  stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
                })).get().on('pnotify.confirm', function(){
					var formName_="deleteHouseChurchForm";
					var nation_=$("#nationCode").val();
					var id_=id;
					if (ConfirmDelete()){
						$.post("api/post.php",
						{
							formName:formName_,
							id:id_
						},
						function(response,status){ // Required Callback Function
								if(response==1){successNote(sresponse,'House Church deleted sussessfully!');
								}else{
									errorNote(fresponse,'operation not performed');
								}
								$('#houseChurchesTable').DataTable().ajax.reload(); //refresh provinces table
								initialiseNationTotals(nation_);
							});	
					}	
			});
}

//-----------------------------------MANAGING THE REGION OBJECT-------------------------------------------//
//save region data to file	
function saveRegion(){ 
	var formName_="addRegionForm";
	var code_=addRegionForm.regionCode.value;
	var name_=addRegionForm.regionName.value;
	
	if(code_==''||name_=="")
	{
	 errorNote('Oops!','Missing region code or name');
	}
	else{
	$.post("api/post.php", //Required URL of the page on server
	{ // Data Sending With Request To Server
		formName:formName_,
		code:code_,
		name:name_
	},
	function(data,status){ // Required Callback Function
			var obj= $.parseJSON(data);
				console.log(obj['msg']);
			if(obj['status']==1){successNote(sresponse,obj['msg']);
				
			}else{
				errorNote(fresponse,obj['msg']);
			}
			$("#addRegionForm")[0].reset();
			initialiseRegionsCombo();
		});
	
	}
}

//-----------------------------------MANAGING THE REPORT OBJECT-------------------------------------------//
//save report data to file	
function addReport(id){ 
	var formName_="addReportForm";
	var members_=addReportForm.statsTotal.value;
	var localities_=addReportForm.statsLoc.value;
	var houseChurches_=addReportForm.statsHC.value;
	var leaders_=addReportForm.statsLeaders.value;
	var province_=id;
	var reportDate_=addReportForm.statsDate.value;
	var notes_="";
	
	if(id==null)
	{
	alert("No province selected: Developer issue");
	}
	else{
	$.post("api/post.php", //Required URL of the page on server
	{ // Data Sending With Request To Server
		formName:formName_,
		members:members_,
		localities:localities_,
		houseChurches:houseChurches_,
		leaders:leaders_,
		province:province_,
		reportDate:reportDate_,
		notes:notes_
	},
	function(response,status){ // Required Callback Function
	if(response==1){successNote(sresponse,'New provincial report  added sussessfully!')
	}else{
		errorNote(fresponse,'data not saved! something went wrong')
	}
	$("#addReportForm")[0].reset();
	$('#provDates').DataTable().ajax.reload(); //refresh province stats table
	});
	}
}

function saveReport(id){ 
	var formName_="saveReportForm";
	var members_=addReportForm.statsTotal.value;
	var localities_=addReportForm.statsLoc.value;
	var houseChurches_=addReportForm.statsHC.value;
	var leaders_=addReportForm.statsLeaders.value;
	var reportDate_=addReportForm.statsDate.value;
	var notes_="";
	
	if(id==null)
	{
	alert("No province selected: Developer issue");
	}
	else{
	$.ajax({
		url:"api/api.php/records/cmfi_nationalreports/"+id, //Required URL of the page on server
		type: 'PUT',
		data:{ // Data Sending With Request To Server
				members:members_,
				localities:localities_,
				houseChurches:houseChurches_,
				leaders:leaders_,
				date:reportDate_,
				notes:notes_
			},
		success: function(response,status){ // Required Callback Function
			if(response==1){successNote(sresponse,'provincial report updated sussessfully!')
			}else{
				errorNote(fresponse,'data not saved! something went wrong')
			}
		}
		});
			$("#addReportForm")[0].reset();
			$('#provReportUpdateBtn').html('');
			$('#provDates').DataTable().ajax.reload(); //refresh province stats table
	
	}
}
//-------------FUNCTIONS TO MANAGE MEMBER OBJECT---------------------------------------------------------//				
//edit member		
function editMember(id){
		var rowId="edit"+id;
		var currentTD = $("#edit"+id).parents('tr').find('td');
		var country=$("#country"+id).html(); 
		var x=$(this).html();
		//alert (x);
          if ($("#edit"+id).html() == '<i class="glyphicon glyphicon-pencil"></i>') {                  
              $.each(currentTD, function () {
                  $(this).prop('contenteditable', true),
				  $(this).prop('style', 'color:red')
              });
			  
          } else {
             $.each(currentTD, function () {
                  $(this).prop('contenteditable', false),
				  $(this).prop('style', 'color:black')
				  
              });
			  var id_=id;
			  var name_ = $("#name"+id).html();
			  var value_ = $("#value"+id).html();
			  saveMember(id_,name_,value_);
          }
		$("#edit"+id).html($("#edit"+id).html() == '<i class="glyphicon glyphicon-pencil"></i>' ? '<i class="glyphicon glyphicon-ok"></i> Save' : '<i class="glyphicon glyphicon-pencil"></i>')
		$("#country"+id).html('<select id="nations'+id+'"></select>');		
		$("#locality"+id).html('<select id="localities"></select>');
		$("#nations"+id).load('includes/fetchNations.php');
		$("#nations"+id+" option:contains(" + country + ")").attr('selected', 'selected');
		}
		
//delete member	
	function deleteMember(id){//alert("now in");
		var rowId="edit"+id;
		//if($("#delete"+id).html()=='<i class="glyphicon glyphicon-trash"></i>'){
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				formName:'deleteMember',
				memberId:id
			},
			function(response,status){ // Required Callback Function
			if(response==1){successNote("Success!",'Record deleted sussessfully!');
			$("#member"+id).remove(); //remove deleted row from table
			}else{
				errorNote("Error!",'Record not deleted!')
			}
			$("#addNation")[0].reset();
			//$("#newNation").;
			});
		//}
		$("#delete"+id).html($("#delete"+id).html() == '<i class="glyphicon glyphicon-remove"></i>' ? '<i class="glyphicon glyphicon-trash"></i>' : '<i class="glyphicon glyphicon-remove"></i>')
		
	}

function showImg(imagefile){
 $("#profileAvatar").html('<img class="img-responsive avatar-view" src="images/members/'+imageFile+'" alt="Avatar" title="Change the avatar" >');
}	

//save member image file
function saveImage(div){ 
			var formName_="saveImage";
			var filepath=document.getElementById(div).image_file.value;
			var imageFile= filepath.split(/[\\\/]/).pop();
			//var imageFile= "user.png";  //research on how to get the filename from the input
			if(imageFile=='')
			{
			alert("Please choose image file first");
			}
			else{
			$.ajax({
				url: "controllers/uploadFromFile.php", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: new FormData(uploadimage), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				contentType: false,       // The content type used when sending data to the server.
				cache: false,             // To unable request pages to be cached
				processData:false,        // To send DOMDocument or non processed data file it is set to false
				success: function(data)   // A function to be called if request succeeds
				{
					successNote(sresponse,'Image uploaded sussessfully!')
					$("#profileAvatar").html('<img class="img-responsive avatar-view" src="images/members/'+imageFile+'" alt="Avatar" title="Change the avatar" >');
				}
				});
			}
			$("#uploadForm").hide(500);
		}

//save member image file
function saveImageFromFile(form){ 
			var filepath=document.getElementById(form).image_file.value;
			var imageFile= filepath.split(/[\\\/]/).pop();
			//var imageFile= "user.png";  //research on how to get the filename from the input
			if(imageFile=='')
			{
			alert("Please choose image file first");
			}
			else{
			$.ajax({
				url: "controllers/uploadFromFile.php", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: new FormData(uploadimage), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				contentType: false,       // The content type used when sending data to the server.
				cache: false,             // To unable request pages to be cached
				processData:false,        // To send DOMDocument or non processed data file it is set to false
				success: function(data)   // A function to be called if request succeeds
				{
					successNote(sresponse,'Image uploaded sussessfully!')
					$("#profileAvatar").html('<img class="img-responsive avatar-view" src="images/members/'+imageFile+'" alt="Avatar" title="Change the avatar" >');
				},
				error: function(data)
				{
					errorNote(data,'image not uploaded successfully!')
				}
				});
			}
			$("#uploadForm").hide(500);
		}



//Add New Member data to file	
function addMember(){ //alert("now in");
			var formName_='addMemberForm';
			var locality_=$("#localityx option:selected").val();
			var country_=$("#countryNamex option:selected").val();
			var houseChurch_=$("#houseChurch option:selected").val();
			//var familyId_=$("#familyNamex option:selected").val();
			//ar familyRole_=$("#familyRolex option:selected").val();
			var gender_=$("input[id='genderF']:checked").val();
			if($("input[id='genderF']:checked").val()=="F"){ gender_='F';}
			else{ gender_='M';} 
			var imgfilepath=$("#image_file").val();
			var imagefile_= imgfilepath.replace(/^.*[\\\/]/, '');//alert (imgfilepath);
			//var imagefile_=$("#profileAvatar input[name=imagefile]").val(); //default user image
			//alert(imagefile_);
			/*var isWebcam=addMemberForm.webcamImg.value;
			if(isWebcam=='true'){
				imagefile_=addMemberForm.imagefile.value;
			}else{
				var imgfilepath=$("#image_file").val();
				imagefile_= imgfilepath.replace(/^.*[\\\/]/, '');
			}*/
			
			//alert(locality_);
			var tags_=$("#tags").val();
			
			if(country_=='')
			{
			alert("Please fill out the nation name");
			}
			else{
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				formName:formName_,
				firstName:addMemberForm.firstName.value,
				lastName:addMemberForm.lastName.value,
				email:addMemberForm.email.value,
				address:addMemberForm.address.value,
				surburb:addMemberForm.surburb.value,
				city:addMemberForm.city.value,
				discipleMaker:addMemberForm.discipleMaker.value,
				phone:addMemberForm.phone.value,
				tags:tags_,
				gender:gender_,
				country:country_,
				locality:locality_,
				houseChurch:houseChurch_,
				DOB:addMemberForm.birthday.value,
				image:imagefile_
			
			},
			function(data,status){ // Required Callback Function
			var obj= $.parseJSON(data);
				console.log(obj['msg']);
			if(obj['status']==1){successNote(sresponse,obj['msg']);
			}else{
				errorNote(fresponse,obj['msg']);
			}
			$('#datatable-disciples').DataTable().ajax.reload(); //refresh member table
			$("#addMemberForm")[0].reset();
			initialiseNationTotals(country_);
			});
			
			}
		}

//Add New Member data to file	short form
function addShortMember(){ //alert("now in");
			var formName_='addMemberForm';
			var locality_=$("#localitiez option:selected").val();
			var country_=$("#countryx").val();
			var tags_="";
			var address_="";
			var surburb_="";
			var city_="";
			var image_="user.png";
			var discipleMaker_=0;
			var houseChurch_=0;
			//alert(locality_);
			var gender_='M';
			if($('#genderF').isCheck==true){ gender_='F';}
			if(country_=='')
			{
			alert("Please fill out the nation name");
			}
			else{
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				formName:formName_,
				firstName:addMemberFormx.firstName.value,
				lastName:addMemberFormx.lastName.value,
				email:addMemberFormx.email.value,
				phone:addMemberFormx.phone.value,
				gender:gender_,
				surburb:surburb_,
				city:city_,
				address:address_,
				country:country_,
				houseChurch:houseChurch_,
				discipleMaker:discipleMaker_,
				image:image_,
				tags:tags_,
				locality:locality_,
				DOB:addMemberFormx.dateOfBirth.value
			
			},
			function(data,status){ // Required Callback Function
			var obj= $.parseJSON(data);
				console.log(obj['msg']);
			if(obj['status']==1){successNote(sresponse,obj['msg']);
				var id=obj['id'];
				eventRegisterAuto(id);
			}else{
				errorNote(fresponse,obj['msg']);
			}
			//$("#addMemberFormx")[0].reset();
			
			});
			
			}
			 $("#loaderImg").hide(); //hide loader image
			refreshMemberCombo(country_);
			initialiseNationTotals(country_);
		}



//save Member data to file	
function saveMember(){ //alert("now in");
			var id_=$("#memberID").val();
			var formName_=document.getElementById("saveMemberForm_"+id_); 
			var firstName_=formName_.firstNamex.value; 
			var lastName_=formName_.lastNamex.value;
			var email_=formName_.emailx.value;
			var address_=formName_.addressx.value;
			var surburb_=formName_.surburbx.value;
			var phone_=formName_.phonex.value;
			var DOB_=formName_.birthdayx.value;
			var houseChurch_=$("#houseChurchx option:selected").val();
			var familyId_=$("#familyNamex option:selected").val();
			var familyRole_=$("#familyRolex option:selected").val();
			var tags_=$("#tags_"+id_).val();
			var city_=formName_.cityx.value;
			var locality_=$("#localitiesy option:selected").val();
			var country_=$("#countryNamey option:selected").val();//alert(country_);
			var discipleMaker_=$("#discipleMakerx").val();
			//var country_=$("#nationx").val(); 
			var gender_=$("input[id='genderF']:checked").val();
			var imgfilepath=$("#image_file").val();
			var imagefile_= imgfilepath.replace(/^.*[\\\/]/, '');//alert (imgfilepath);
			if($("input[id='genderF']:checked").val()=="F"){ gender_='F';}
			else{ gender_='M';} 
			if(country_=='')
			{
			alert("Please fill out the nation name");
			}
			else{
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				formName:'saveMember',
				firstName:firstName_,
				lastName:lastName_,
				address:address_,
				email:email_,
				city:city_,
				surburb:surburb_,
				phone:phone_,
				familyId:familyId_,
				familyRole:familyRole_,
				gender:gender_,
				country:country_,
				locality:locality_,
				discipleMaker:discipleMaker_,
				DOB:DOB_,
				tags:tags_,
				id:id_,
				houseChurch:houseChurch_
			
			},
			function(data,status){ // Required Callback Function
			var obj= $.parseJSON(data);
				console.log(obj['msg']);
			if(obj['status']==1){successNote(sresponse,obj['msg']);
			}else{
				errorNote(fresponse,obj['msg']);
			}
			$('#datatable-disciples').DataTable().ajax.reload(); //refresh member table
			});
			
			}
		}




//drop event to another date
function deleteUser(id){// alert('now in');
			var formName_="deleteUser";
			var userId_=id;
			if (ConfirmDelete()){
				$.post("api/post.php", //Required URL of the page on server
				{ // Data Sending With Request To Server
					formName:formName_,
					userId:userId_
				},
				function(response,status){ // Required Callback Function
				if(response==1){successNote(sresponse,'User deleted sussessfully!');
				}else{
					errorNote(fresponse,'User not deleted!');
				}
				});
			}
		}


//----- Save user profile update ----//	
function saveUserProfile(){//alert("now in");
			var formName_="saveUserForm";
			var password_=$("#passwordx").val();
			var firstName_=$("#firstNamex").val();
			var id_=$("#id2").val();
			var lastName_=$("#lastNamex").val();
			var email_=$("#emailx").val();
			var address_=$("#addressx").val();//alert (id_);
			var surburb_=$("#surburbx").val();
			var phone_=$("#phonex").val();
			var DOB_=$("#birthdayx").val();
			var tags_=$("#tags_").val();
			var city_=$("#cityx").val();
			var familyId_=$("#familyNamex option:selected").val();
			var locality_=$("#localitiesz option:selected").val();
			var country_=$("#countryNamez option:selected").val();
			var familyRole_=$("#familyRolex option:selected").val();
			var gender_=$("input[id='genderF']:checked").val();
			var imgfilepath=$("#image_file").val();
			var imagefile_= imgfilepath.replace(/^.*[\\\/]/, ''); 
			if($("input[id='genderF']:checked").val()=="F"){ gender_='F';}
			else{ gender_='M';} 
			if(country_=='')
			{
			alert("Please fill out the nation name");
			}
			else{
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				formName:formName_,
				firstName:firstName_,
				lastName:lastName_,
				familyId:familyId_,
				familyRole:familyRole_,
				address:address_,
				email:email_,
				city:city_,
				surburb:surburb_,
				phone:phone_,
				gender:gender_,
				country:country_,
				locality:locality_,
				DOB:DOB_,
				tags:tags_,
				id:id_,
				image:imagefile_,
				password:password_
			
			},
			function(data,status){ // Required Callback Function
				var obj= $.parseJSON(data);
				console.log(obj[0]['msg']);

			    for (var i=0; i<obj.length; i++)
			    {
			      	if(obj[i]['status']==1){successNote(sresponse,obj[i]['msg']);
					}else{
						errorNote(fresponse,obj[i]['msg']);
					}
			    }
			});
			}
		}


//------create user from member--------//
//save Member as user	
function makeUser(){// alert("now in");
			var formName_="makeUser";
			var id_=$("#memberID").val();
			var email_=$("#emailx").val();
			var firstName_=$("#firstNamex").val();
			var phone_=$("#phonex").val(); 
			$.post("api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				formName:formName_,
				email:email_,
				phone:phone_,
				firstName:firstName_,
				memberId:id_
			
			},
			function(data,status){ // Required Callback Function
				var obj= $.parseJSON(data);
				console.log(obj['text']);
			if(obj['id']=="1"){successNote(sresponse,obj['text']);
			}else{
				errorNote(fresponse,obj['text']);
			}
			});
			
		}
// send user credentials by email
function sendUserCredentials(email){// alert("now in");
		(new PNotify({
            title: 'Confirmation Needed',
            text: 'Are you sure you want to reset this account?',
            icon: 'glyphicon glyphicon-question-sign',
            hide: false,
            confirm: {
              confirm: true
            },
            buttons: {
              closer: false,
              sticker: false
            },
            history: {
              history: false
            },
            addclass: 'stack-modal',
            stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
          })).get().on('pnotify.confirm', function(){
				var formName_="sendUserCredentials";
				$.post("api/post.php", //Required URL of the page on server
				{ // Data Sending With Request To Server
					formName:formName_,
					email:email
				
				},
				function(data,status){ // Required Callback Function
				var obj= $.parseJSON(data);
					console.log(obj['text']);
				if(obj['id']=="1"){successNote(sresponse,obj['text']);
				}else{
					errorNote(fresponse,obj['text']);
				}
				});
			});
		}


//----------------------------------notifications------------------------------------------------//
//success nofication
function successNote(title_,text_){new PNotify({
			title: title_,
			text: text_,
			type: 'success'
		});
}

//failure or Error notifications
function errorNote(title_,text_){new PNotify({
			title: title_,
			text: text_,
			type: 'error'
		});
}

