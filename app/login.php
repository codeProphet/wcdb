<?php
/*
World Conquest DB: 1.0.0
*/
 ob_start();
require("config/config.php");
if (!securePage($_SERVER['PHP_SELF'])){ exit();}

//get nations and countries
$nations=getNations();

//Prevent the user visiting the logged in page if he/she is already logged in
if(isUserLoggedIn()) { @header('Location: .');  exit(); 
}

//Forms posted
if (isset($_POST['submit']) && $_POST['submit'] == 'Login') { 
	$errors = array();
	$username = sanitize(trim($_POST["username"]));
	$password = trim($_POST["password"]);
	$langx=trim($_POST["lang"]);
	if(empty($langx)){$langx="en";} //setting default language to English

	//Perform some validation
	//Feel free to edit / change as required
	if($username == "")
	{
		$errors[] = lang("ACCOUNT_SPECIFY_EMAIL");
	}
	if($password == "")
	{
		$errors[] = lang("ACCOUNT_SPECIFY_PASSWORD");
	}

	if(count($errors) == 0)
	{
		//A security note here, never tell the user which credential was incorrect
		if(!emailExists($username))
		{
			$errors[] = lang("ACCOUNT_EMAIL_OR_PASS_INVALID");
		}
		else
		{
			$userdetails = fetchUserDetails($username);
			//See if the user's account is activated
			if($userdetails["active"]==0)
			{
				$errors[] = lang("ACCOUNT_INACTIVE");
			}
			else
			{
				//Hash the password and use the salt from the database to compare the password.
				$entered_pass = generateHash($password,$userdetails["password"]);
				
				if($entered_pass != $userdetails["password"])
				{
					//Again, we know the password is at fault here, but lets not give away the combination incase of someone bruteforcing
					$errors[] = lang("ACCOUNT_PASS_OR_EMAIL_INVALID");
				}
				else
				{
					
					//Passwords match! we're good to go'
					
					//Construct a new logged in user object
					//Transfer some db data to the session object
					$loggedInUser = new loggedInUser();
					$loggedInUser->email = $userdetails["email"];
					$loggedInUser->user_id = $userdetails["id"];
					$loggedInUser->memberId=$userdetails["memberId"];
					$loggedInUser->hash_pw = $userdetails["password"];
					$loggedInUser->title = $userdetails["title"];
					$loggedInUser->phone = $userdetails["phone"];
					$loggedInUser->displayname = $userdetails["display_name"];
					$loggedInUser->username = $userdetails["user_name"];
					$loggedInUser->userFirst = $userdetails["userFirst"];
					$loggedInUser->image=$userdetails["image"];
					$loggedInUser->linkedin=$userdetails["linkedin_profile"];
					$loggedInUser->facebook=$userdetails["facebook_profile"];
					$loggedInUser->twitter=$userdetails["twitter_profile"];
					$loggedInUser->pinterest=$userdetails["pinterest_profile"];
					$loggedInUser->lastChatWith=0;
					$loggedInUser->lastSignIn=$userdetails["last_sign_in_stamp"];
					$loggedInUser->lastChat=0;
					$loggedInUser->isAdmin= $userdetails['isAdmin'];
					$loggedInUser->fullName= $userdetails['userFirst'].' '.$userdetails['userLast'];
					$loggedInUser->recEmails= $userdetails['recEmails'];
					$loggedInUser->weatherLoc= $userdetails['weatherLoc'];
					$loggedInUser->notes=$userdetails['userNotes'];
					$loggedInUser->dob=$userdetails['DOB'];
					$loggedInUser->nation=$userdetails['nationId'];
					$loggedInUser->visits=$userdetails['visits'];
					$loggedInUser->reportTo=$userdetails['reportTo'];
					$loggedInUser->lang=$langx; //set default user language
					
					
					//Update last sign in
					$loggedInUser->updateLastSignIn();
					$loggedInUser->addVisit();

					

					$_SESSION["wcdbUser"] = $loggedInUser;
					
					addLog($loggedInUser->user_id,'signin','signin');
					//Redirect to user account page
					@header("Location: .");
					
					 exit();
				}
			}
		}
	}
}

//Forms posted
if (isset($_POST['submit']) && $_POST['submit'] == 'Register') {
	$errors = array();
	$email = trim($_POST["email"]);
	$firstname = trim($_POST["firstname"]);
	$lastname = trim($_POST["lastname"]);
	$phone = trim($_POST["phone"]);
	$password = trim($_POST["password"]);
	$confirm_pass = trim($_POST["passwordc"]);
	$captcha = md5($_POST["captcha"]);
	$image="images/members/user1.png";

	if ($captcha != $_SESSION['captcha'])
	{
		$errors[] = lang("CAPTCHA_FAIL");
	}
    if($firstname == ""){
	    $errors[] = lang("ACCOUNT_FIRSTNAME");
	}
	if($lastname == ""){
	    $errors[] = lang("ACCOUNT_LASTNAME");
	}
	if(minMaxRange(8,50,$password) && minMaxRange(8,50,$confirm_pass))
	{
		$errors[] = lang("ACCOUNT_PASS_CHAR_LIMIT",array(8,50));
	}
	else if($password != $confirm_pass)
	{
		$errors[] = lang("ACCOUNT_PASS_MISMATCH");
	}
	if(!isValidEmail($email))
	{
		$errors[] = lang("ACCOUNT_INVALID_EMAIL");
	}
	

	//End data validation
	if(count($errors) == 0){	
		//Construct a user object
		$user = new User($firstname,$lastname,$password,$email,$image,$phone);
		
		//Checking this flag tells us whether there were any errors such as possible data duplication occured
		if(!$user->status){
		 //if($user->username_taken) $errors[] = lang("ACCOUNT_USERNAME_IN_USE",array($username));
			if($user->email_taken) 	  $errors[] = lang("ACCOUNT_EMAIL_IN_USE",array($email));		
		}
		else{
			//Attempt to add the user to the database, carry out finishing  tasks like emailing the user (if required)
			if(!$user->AddUser()){
				if($user->mail_failure) $errors[] = lang("MAIL_ERROR");
				if($user->sql_failure)  $errors[] = lang("SQL_ERROR");
			}
		}
	}
	if(count($errors) == 0) {
		$successes[] = $user->success;
	}
}


?>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>wcdb| login </title>

  <!-- Bootstrap core CSS -->

  <link href="css/bootstrap.min.css" rel="stylesheet">

  <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="css/animate.min.css" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="css/custom.css" rel="stylesheet">
  <link href="css/icheck/flat/green.css" rel="stylesheet">


  <script src="js/jquery.min.js"></script>

  <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

<style>
.input-group{
	width:100%;
}

body{
	background-image:url("images/collage1.png");
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-position: center;
}

</style>
		
</head>
<body >

<div class="">
    <a class="hiddenanchor" id="toregister"></a>
    <a class="hiddenanchor" id="tologin"></a>

    <div id="wrapper" >
      <div id="login" class="animate form well" style="background-color:rgba(255, 255, 255, 0.92);">
        <div>
			 <h3><img src="images/logo.png" style="width: 26px;">
			 Christian Missionary Fellowship International</h3>
			
			
		</div>
		<section class="login_content">
		
		 <?php echo resultBlock($errors,$successes); ?>
			<?php 
			echo"<form action='".$_SERVER['PHP_SELF']."' method='POST'> ";
			?>
         	
            <div >
             <label class="input-group">
						<input id="email" type="email" name="username" required="" value="" placeholder="Email" class="form-control" />
						<span for="username" class="form-control-feedback right">
							<i class="fa fa-envelope"></i>
						</span>
					</label>
            </div>
            <div>
					<label class="input-group">
						<input id="password" type="password" name="password" placeholder="Password" class="form-control" required="required"  />
						<label for="password" class="form-control-feedback right">
							<i class="fa fa-unlock"></i>
						</label>
                </label>
            </div>
			<!--div class="form-group has-feedback">
				<select id="countryNamex" name="country" class="form-control has-feedback-right">
				  <option value="">Choose Country</option>
				 <?php 
					foreach ($nations as $n) 
						{
							echo '<option  value="'.$n["code"].'">'.$n["name"].'</option>';
						}
				  ?>
				</select>
			 <span class="icon-globe form-control-feedback right" aria-hidden="true"></span>
					
			</div-->
			<!--div class="form-group has-feedback">
				<select id="lang" name="lang" class="form-control has-feedback-right">
				  <option value="">Choose Language</option>
						<option  value="en">English</option>
						<option  value="fr">French</option>			  
				</select>
			 <span class="icon-globe form-control-feedback right" aria-hidden="true"></span>
					
			</div-->
						
            <div>				
              <button class="btn btn-default submit" type="submit" name="submit" value="Login">Log in</button>
              <a class="reset_pass" href="forgot-password.php">Lost your password?</a>
            </div>
            <div class="clearfix"></div>
            <div class="separator">

              <!--p class="change_link">New to site?
                <a href="#toregister" class="to_register"> Create Account </a>
              </p-->
              <div class="clearfix"></div>
              <br />
              <div>
                

                <p>©2018 All Rights Reserved. CMFI World Conquest Database. Privacy and Terms</p>
              </div>
            </div>
          </form>
          <!-- form -->
        </section>
        <!-- content -->
     </div>
	  
	
	
	
      <div id="register" class="animate form well" style="background-color:rgba(255, 255, 255, 0.84); width:580px;">
        <div>
			<h3><img src="images/logo.png" style="width: 26px;"></img> Christian Missionary Fellowship International</h3>
		</div>
		<!--section class="login_content register_content">
          <?php 
			echo"<form  action='".$_SERVER['PHP_SELF']."' method='POST'> ";
			?>
            <h1>Create Account</h1>
			<div>
			
			<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                      <input type="text" name="firstname" class="form-control has-feedback-left" id="inputSuccess2" placeholder="First Name" value="<?php if(isset($firstname)){ echo $firstname;} ?>">
                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                      <input type="text" class="form-control" name="lastname" id="inputSuccess3" placeholder="Last Name" value="<?php if(isset($lastname)){ echo $lastname;} ?>">
                      <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                      <input type="text"   name="email" class="form-control has-feedback-left" id="inputSuccess4" placeholder="Email" value="<?php if(isset($email)){ echo $email;}  ?>">
                      <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                      <input type="text" name="phone" class="form-control" id="inputSuccess5" placeholder="Phone">
                      <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
                    </div>

                  </div>
				  <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Password</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <input  class="form-control"  type="password" name="password" value="" placeholder="*">
                      </div>
                   </div>
				   <div class="form-group">					
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Confirm Password</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <input  class="form-control"  type="password" name="passwordc" value="" placeholder="*">
                      </div>
                   </div>
				   <div class="form-group">
                    
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <span><img src='config/captcha.php'></span>
						
                      </div>
                    
						<div class="col-md-9 col-sm-9 col-xs-12">
						<input name='captcha' type='text' class="form-control" placeholder="input security code" />	  
					 </div>
					</div>
				
				<div class="form-group">	
				<p >
					<input type="checkbox" name="check" value="checked"  class="flat"  required /> I Accept all the terms and conditions
				</p>
				</div>
		<button class="btn btn-default submit" type="submit" name="submit" value="Register">Submit</button>		
            
            </div>
            <div class="clearfix"></div>
            <div class="separator">

              <p class="change_link">Already a member ?
                <a href="#tologin" class="to_register"> Log in </a>
              </p>
              <div class="clearfix"></div>
              <br />
              <div>
                <p>©2016 All Rights Reserved. CMFI World Conquest Database. Privacy and Terms</p>
              </div>
            </div>
          </form>
          <!-- form -->
        </section-->
        <!-- content -->
      </div>
 
 
 
 
 
 
  </div>

 <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>
  
  
  <script src="js/bootstrap.min.js"></script>

  <!-- gauge js -->
  <script type="text/javascript" src="js/gauge/gauge.min.js"></script>
  <script type="text/javascript" src="js/gauge/gauge_demo.js"></script>
  <!-- bootstrap progress js -->
  <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <!-- daterangepicker -->
  <script type="text/javascript" src="js/moment/moment.min.js"></script>
  <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>

  
  <!-- input mask -->
  <script src="js/input_mask/jquery.inputmask.js"></script>
  <!-- chart js -->
  <script src="js/chartjs/chart.min.js"></script>

  <script src="js/custom.js"></script>
<!-- pace -->
  <script src="js/pace/pace.min.js"></script>
  
  <!-- PNotify -->
  <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
  <script type="text/javascript" src="js/notify/pnotify.buttons.js"></script>
  <script type="text/javascript" src="js/notify/pnotify.nonblock.js"></script>



</body>
</html>

