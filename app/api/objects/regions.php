<?php
/*
WCDB Version: 1.0.0
*/
include_once('dbconfig.php');
include_once('class.region.php');


//initialise new region
$regions=new region($db);




function create(){
	$q="INSERT INTO 
	".$db_table_prefix."regions 
	SET code=:code,
		name=:name
	";
	$stmt=$mysqli->prepare($q);

	$this->name=htmlspecialchars(strip_tags($this->name));
	$this->code=htmlspecialchars(strip_tags($this->code));	
	// bind new values
    $stmt->bindParam(':name', $this->name);
    $stmt->bindParam(':code', $this->code);
    // execute the query
    if($stmt->execute()){
        return true;
    }
 
    return false;
}

function update(){
	$q="UPDATE 
	".$db_table_prefix."regions 
	SET code=:code,
		name=:name
	WHERE
		id=:id
	";
	$stmt=$mysqli->prepare($q);

	$this->name=htmlspecialchars(strip_tags($this->name));
	$this->code=htmlspecialchars(strip_tags($this->code));
	$this->id=htmlspecialchars(strip_tags($this->id));	
	// bind new values
    $stmt->bindParam(':name', $this->name);
    $stmt->bindParam(':code', $this->code);
    $stmt->bindParam(':id', $this->id);
    // execute the query
    if($stmt->execute()){
        return true;
    }
 
    return false;
}

function Delete(){ 
	$time=date("Y-m-d");
	$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."regions 
		WHERE code = ?
		)");
	$stmt->bind_param(1, $this->code);
	
	if ($stmt->execute()) { 
		   $stmt->close();
		   return true;
		} else {
			$stmt->close();
		   return false;
		}
}



?>