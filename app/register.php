<?php
/*
Quest Version: 1.0.0
*/
require_once("config/config.php");
if (!securePage($_SERVER['PHP_SELF'])){die();}

//Prevent the user visiting the logged in page if he/she is already logged in
if(isUserLoggedIn()) { header("Location: index.php?page=home"); die(); }

//Forms posted
if(!empty($_POST))
{
	$errors = array();
	$email = trim($_POST["email"]);
	$firstname = trim($_POST["firstname"]);
	$lastname = trim($_POST["lastname"]);
	$password = trim($_POST["password"]);
	$confirm_pass = trim($_POST["passwordc"]);
	$captcha = md5($_POST["captcha"]);
	$image="images/users/user1.png";

	if ($captcha != $_SESSION['captcha'])
	{
		$errors[] = lang("CAPTCHA_FAIL");
	}
    if($firstname == ""){
	    $errors[] = lang("ACCOUNT_FIRSTNAME");
	}
	if($lastname == ""){
	    $errors[] = lang("ACCOUNT_LASTNAME");
	}
	if(minMaxRange(8,50,$password) && minMaxRange(8,50,$confirm_pass))
	{
		$errors[] = lang("ACCOUNT_PASS_CHAR_LIMIT",array(8,50));
	}
	else if($password != $confirm_pass)
	{
		$errors[] = lang("ACCOUNT_PASS_MISMATCH");
	}
	if(!isValidEmail($email))
	{
		$errors[] = lang("ACCOUNT_INVALID_EMAIL");
	}
	

	//End data validation
	if(count($errors) == 0){	
		//Construct a user object
		$user = new User($firstname,$lastname,$password,$email,$image);
		
		//Checking this flag tells us whether there were any errors such as possible data duplication occured
		if(!$user->status){
		 //if($user->username_taken) $errors[] = lang("ACCOUNT_USERNAME_IN_USE",array($username));
			if($user->email_taken) 	  $errors[] = lang("ACCOUNT_EMAIL_IN_USE",array($email));		
		}
		else{
			//Attempt to add the user to the database, carry out finishing  tasks like emailing the user (if required)
			if(!$user->userCakeAddUser()){
				if($user->mail_failure) $errors[] = lang("MAIL_ERROR");
				if($user->sql_failure)  $errors[] = lang("SQL_ERROR");
			}
		}
	}
	if(count($errors) == 0) {
		$successes[] = $user->success;
	}
}


?>




<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	
	
	<title>Home</title>
	<meta name="description" content="This application is to provide information and resources for Barclays employees" />
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
	
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

	



  <!-- Bootstrap core CSS -->

  <link href="css/bootstrap.min.css" rel="stylesheet">

  <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="css/animate.min.css" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="css/custom.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.3.css" />
  <link href="css/icheck/flat/green.css" rel="stylesheet" />
  <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />

  <script src="js/jquery.min.js"></script>
  <script src="js/nprogress.js"></script>
		
     




</head>
<body>

<div class="wrapper wrapperHome">

	<header class="home">

	</header>
	<br/><br/>
		
			
	<div id="wrapper" class="register_box">
    

		<div class="form-header header-primary login_content">
            	<h3><i class="fa fa-unlock-alt"></i> Register</h3>
            </div><!-- end .form-header section -->
 			<br/>

	<div class="x_form well">

		<div class="formBoxHead">
		</div>

		<div class="x_content">
			<p class="formIntro" style=" text-align:center;">Please enter your details below to register with the system.</p>
			
				
			 <?php echo resultBlock($errors,$successes); ?></div>
           <?php  
		   
		   echo"<form class='form-horizontal form-label-left' id='form' name='newUser' action='".$_SERVER['PHP_SELF']."' method='post'>"; ?>
				
				
				<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                      <input type="text" name="firstname" class="form-control has-feedback-left" id="inputSuccess2" placeholder="First Name" value="<?php if(isset($firstname)){ echo $firstname;} ?>">
                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                      <input type="text" class="form-control" name="lastname" id="inputSuccess3" placeholder="Last Name" value="<?php if(isset($lastname)){ echo $lastname;} ?>">
                      <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                      <input type="text"   name="email" class="form-control has-feedback-left" id="inputSuccess4" placeholder="Email" value="<?php if(isset($email)){ echo $email;}  ?>">
                      <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                      <input type="text" name="phone" class="form-control" id="inputSuccess5" placeholder="Phone">
                      <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
                    </div>

                  </div>
				  <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Password</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" class="form-control"  type="password" name="password" value="" placeholder="*">
                      </div>
                   </div>
				   <div class="form-group">					
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Confirm Password</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" class="form-control"  type="password" name="passwordc" value="" placeholder="*">
                      </div>
                   </div>
				   <div class="form-group">
                    
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <span><img src='config/captcha.php'></span>
						
                      </div>
                    
						<div class="col-md-9 col-sm-9 col-xs-12">
						<input name='captcha' type='text' class="form-control" placeholder="input security code" />	  
					 </div>
					</div>
				
				<div class="form-group">	
				<p >
					<input type="checkbox" name="check" value="checked"  class="flat"  required /> I Accept all the terms and conditions
				</p>
				</div>
				
				<div style="clear:both;"></div><!-- END clear -->
				<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
					  <div class="form-group">
				         <input type="submit" class="submit btn btn-success" value="Sign Up">
					  </div>
				</div>
				<a href="login.php" title="Login" class="nextToButton"><i class="fa fa-back"></i> Back to Login</a>
			</form>
		</div>
	</div>
</div>
</body>
</html>