<?php
include_once'../config-small.php';
session_start();
$userId=$_SESSION["wcdbUser"]->user_id;
$count=0;

//import preview members into the members table

	$m=new member;
	$temp=fetchTempMembers($userId);
	foreach ($temp as $t){
			$m->firstName=$t["firstName"];
			$m->lastName=$t["lastName"];
			$m->phone=$t["phone"];
			$m->email=$t["email"];
			$m->sex=$t["sex"];
			$m->address=$t["address"];
			$m->surburb=$t["surburb"];
			$m->city=$t["city"];
			$m->locality=$t["locality"];
			$m->country=countryCode($t["country"]);
			$m->DOB=$t["DOB"];
			$m->modifiedDate=$t["modifiedDate"];
			$m->userId=$t["addedBy"];
			
			if($m->Add()){
			$count++;}
	}


if($count>0){
echo "success";}
else{echo "error";}

function countryCode($country){
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT code
		FROM ".$db_table_prefix."countries
		WHERE
		name = ?
		LIMIT 1");
	$stmt->bind_param("s", $country);	
	$stmt->execute();
	$stmt->store_result();
	$num_returns = $stmt->num_rows;
	$stmt->bind_result($code);
	$stmt->close();
	if ($num_returns > 0)
	{		
		return $code;
	}
	else
	{
		return $country;	
	}
	
}
?> 