<?php
/*
DSP Version: 1.0.0
*/


class memberStats 
{
	public $country = "";
	public $total=0;
	public $cflastyearTot = 0;
	public $nations=0;
	public $adults=0;
	public $youths=0;
	public $members=0;
	public $cfadults=0;
	public $cfchildren =0;
	public $houseChurches = 0;
	public $localities = 0;
	public $leaders = 0;
	public $memberChange=0;
	public $events=0;
	
	
	function __construct($country_)
	{
		global $mysqli,$db_table_prefix;
		//Used for display only
		$this->country = $country_;	
		$this->memberChange=number_format(getChangesMembers($country_));
		
		
		if ($this->country=="")
		{
			$this->country=$country_;
			//total members
			$this->total=getTotalMembers($country_);
			
			//count nations
			$q2 = "SELECT * FROM ".$db_table_prefix."nations";
			$r2 = mysqli_query($mysqli, $q2);
			$this->nations=mysqli_num_rows($r2);
			
			//count events
			$q3="SELECT * FROM ".$db_table_prefix."events WHERE nation='GLB' and endDate >now()";
			$r3=mysqli_query($mysqli, $q3);
			$this->events=mysqli_num_rows($r3);

			//count house churches
			$q4="SELECT * FROM ".$db_table_prefix."housechurches";
			$r4=mysqli_query($mysqli, $q4);
			$this->houseChurches=mysqli_num_rows($r4);

			//count leaders
			$q5="SELECT * FROM ".$db_table_prefix."members WHERE tags!='' ";
			$r5=mysqli_query($mysqli, $q5);
			$this->leaders=mysqli_num_rows($r5);
			
			//Count localities
			$this->localities=getTotalLocalities($country_);
		}
	
	}
	
	


}



?>