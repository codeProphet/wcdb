<?php
/*
WCDB Version: 1.0.0
*/

class event 
{
	public $eventTitle = "";
	public $eventDescription="";
	public $startDate = "";
	public $endDate="";
	public $eventImage="";
	public $location="";
	public $url="";
	public $eventColor="#78a32d";
	public $userId=0;
	public $id=0;
	public $token="";
	public $memberId=0;
	public $remark="normal registration";
	public $registeredBy=0;
	public $eventId=0;
	public $members="";
	
	
	
//Functions that interact with events table
//------------------------------------------------------------------------------

public function Register(){
	$count=0;$result="";
	$memberArray = preg_split('/\,/', $this->members);
			//-- just output
		try{
			foreach ($memberArray as $item) {
				$x=intval($item);
				if($this->isRegistered($this->eventId,$x)==0){ //register only members who have not registered before
					$this->registerMember($x);
					$count++;
				}
				
			}
			$result=array('status'=>1,'msg'=>$count.' members registered to event');
		}catch(Exception $e){
			$result= array('status'=>0,'msg'=>$e);
		}
		return $result;
}

//register member to event
public function registerMember($id=null){
		global $mysqli,$db_table_prefix;
		$time=date("Y-m-d");

	try{
			$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."event_registration (
				eventId,
				memberId,
				token,
				registeredBy,
				remark,
				dateRegistered
				)
				VALUES (
				?,
				?,
				?,
				?,
				?,
				?
				)");
			$stmt->bind_param("iiiss", 
								$this->eventId,
								$id,
								$this->token,
								$this->registeredBy,
								$this->remark,
								$time
								);
			$result = $stmt->execute();
			$stmt->close();	
			$result=array('status'=>1,'msg'=>'New member registered to event');
		}catch(Exception $e){
			$result= array('status'=>0,'msg'=>$e);
		}
	return $result;
	}

//Add new event

	public function Add() {
	global $mysqli,$db_table_prefix; 
	$time=date("Y-m-d");
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."events (
		userId,
		eventTitle,
		eventDesc,
		eventImage,
		startDate,
		endDate,
		url,
		eventColor,
		lastUpdated
		)
		VALUES (
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?
		)");
	$stmt->bind_param("ssssssssss", 
						$this->userId,
						$this->eventTitle,
						$this->eventDescription,
						$this->eventImage,
						$this->startDate,
						$this->endDate,
						$this->url,
						$this->eventColor,
						$time
						);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;
}

public function isRegistered($eventId,$memberId){
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT eventId,memberId FROM ".$db_table_prefix."event_registration 
		WHERE eventId=? AND memberId=?");
		$stmt->bind_param('ii', $eventId,$memberId);
		$stmt->execute();
		$stmt->store_result();
		$check = $stmt->num_rows;
		$stmt->close();
		if($check > 0){
			return 1;
		}else{
			return 0;
		}
}



//Update member details
public function Update()
	{
		global $mysqli,$db_table_prefix;
		$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."events 
		SET  
			eventTitle = ?, 
			eventDesc=? ,
			startDate=? ,
			endDate=? ,
			url=?,
			eventColor=?,
			lastUpdated=?
			
		WHERE id=? 
		LIMIT 1");
		$stmt->bind_param('sssssssisssssi', 
						$this->eventTitle,
						$this->eventDesc,
						$this->startDate,
						$this->endDate,
						$this->url,
						$this->eventColor,
						$time,
						$this->id);
		$stmt->execute();
		$stmt->close();
		return 1;
	}
	
	
//Delete member 
public function Delete()
	{
		global $mysqli,$db_table_prefix;
		$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."events 
		WHERE eventId=? ");
		$stmt->bind_param('i', 	$this->id);
		$stmt->execute();
		$stmt->close();
		return 1;
	}


}



?>