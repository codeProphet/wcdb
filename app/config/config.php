<?php
/*
wcdb Version: 1.0.0
*/

require_once("db-settings.php"); //Require DB connection

date_default_timezone_set('Africa/Harare');

$stmt = $mysqli->prepare("SELECT id, name, value
	FROM ".$db_table_prefix."configuration");	
$stmt->execute();
$stmt->bind_result($id, $name, $value);

while ($stmt->fetch()){
	$settings[$name] = array('id' => $id, 'name' => $name, 'value' => $value);
}
$stmt->close();

//Set Settings
$emailActivation = $settings['activation']['value'];
$mail_templates_dir = $settings['mail-templates-dir']['value'];
$websiteName = $settings['website_name']['value'];
$websiteUrl = $settings['website_url']['value'];
$emailAddress = $settings['email']['value'];
$resend_activation_threshold = $settings['resend_activation_threshold']['value'];
$emailDate = date('dmy');


$template = $settings['template']['value'];

$master_account = -1;

$default_hooks = array("#WEBSITENAME#","#WEBSITEURL#","#DATE#");
$default_replace = array($websiteName,$websiteUrl,$emailDate);
$lang= $settings['language']['value'];
$language="config/languages/".$lang.".php";

//if (!file_exists($language)) {
//	$language = "config/languages/fr.php";
//}

/*if(!isset($language)) $language = "config/languages/en.php";*/


//Pages to require
require_once($language);
require_once("class.mail.php");
require_once("class.user.php");
require_once("class.users.php");
require_once("class.newuser.php");
require_once("class.memberStats.php");
require_once("class.nation.php");
require_once("class.newMember.php");
require_once("class.member.php");
require_once("class.report.php");
require_once("class.locality.php");
require_once("class.province.php");
require_once("class.alert.php");
require_once("class.event.php");
require_once("class.region.php");
require_once("class.houseChurch.php");
require_once("api/funcs.php");
//require_once("modules/php-barcode-generator-master/src/BarcodeGeneratorPNG.php");

session_start();


//Global User Object Var
//loggedInUser can be used globally if constructed
if(isset($_SESSION["wcdbUser"]) && is_object($_SESSION["wcdbUser"]))
{
	$loggedInUser = $_SESSION["wcdbUser"];

}

?>
