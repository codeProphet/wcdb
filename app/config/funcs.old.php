<?php
/*
DSP Version: 1.0.0
*/

//Functions that do not interact with DB
//------------------------------------------------------------------------------


//Retrieve a list of all .php files in models/languages
function getLanguageFiles()
{
	$directory = "models/languages/";
	$languages = glob($directory . "*.php");
	//print each file name
	return $languages;
}

//Retrieve a list of all .css files in models/site-templates 
function getTemplateFiles()
{
	$directory = "models/site-templates/";
	$languages = glob($directory . "*.css");
	//print each file name
	return $languages;
}

//Retrieve a list of all .php files in root files folder
function getPageFiles()
{
	$directory = "";
	$pages = glob($directory . "*.php");
	//print each file name
	foreach ($pages as $page){
		$row[$page] = $page;
	}
	return $row;
}

//Destroys a session as part of logout
function destroySession($name)
{
	if(isset($_SESSION[$name]))
	{
		$_SESSION[$name] = NULL;
		unset($_SESSION[$name]);
	}
}

//Generate a unique code
function getUniqueCode($length = "")
{	
	$code = md5(uniqid(rand(), true));
	if ($length != "") return substr($code, 0, $length);
	else return $code;
}

//Generate an activation key
function generateActivationToken($gen = null)
{
	do
	{
		$gen = md5(uniqid(mt_rand(), false));
	}
	while(validateActivationToken($gen));
	return $gen;
}

//@ Thanks to - http://phpsec.org
function generateHash($plainText, $salt = null)
{
	if ($salt === null)
	{
		$salt = substr(md5(uniqid(rand(), true)), 0, 25);
	}
	else
	{
		$salt = substr($salt, 0, 25);
	}
	
	return $salt . sha1($salt . $plainText);
}

//Checks if an email is valid
function isValidEmail($email)
{
	if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
		return true;
	}
	else {
		return false;
	}
}

//Inputs language strings from selected language.
function lang($key,$markers = NULL)
{
	global $lang;
	if($markers == NULL)
	{
		$str = $lang[$key];
	}
	else
	{
		//Replace any dyamic markers
		$str = $lang[$key];
		$iteration = 1;
		foreach($markers as $marker)
		{
			$str = str_replace("%m".$iteration."%",$marker,$str);
			$iteration++;
		}
	}
	//Ensure we have something to return
	if($str == "")
	{
		return ("No language key found");
	}
	else
	{
		return $str;
	}
}

//Checks if a string is within a min and max length
function minMaxRange($min, $max, $what)
{
	if(strlen(trim($what)) < $min)
		return true;
	else if(strlen(trim($what)) > $max)
		return true;
	else
	return false;
}

//Replaces hooks with specified text
function replaceDefaultHook($str)
{
	global $default_hooks,$default_replace;	
	return (str_replace($default_hooks,$default_replace,$str));
}

//Displays error and success messages
function resultBlock($errors,$successes){
	//Error block
	if(count($errors) > 0)
	{
		echo "<div class='alert alert-danger' role='alert' style='margin-left:5px;margin-right:5px;padding:5 0 5 5'>
		<a href='#' onclick=\"showHide('error');\">[X]</a>
		<span class='fa fa-warning-circle' aria-hidden='true'></span>
			  <span class='sr-only'>Error:</span>
		<ul>";
		foreach($errors as $error)
		{
			echo "<li>".$error."</li>";
		}
		echo "</ul>";
		echo "</div>";
	}
	//Success block
	if(count($successes) > 0)
	{
		echo "<div class='alert alert-success' role='alert' style='margin-left:5px;margin-right:5px;padding:5 0 5 5'>
		<a href='#' onclick=\"showHide('success');\">[X]</a>
		<span class='fa fa-info-circle' aria-hidden='true'></span>
			  <span class='sr-only'>Success:</span>
		<ul>";
		foreach($successes as $success)
		{
			echo "<li>".$success."</li>";
		}
		echo "</ul>";
		echo "</div>";
	}
}

//Completely sanitizes text
function sanitize($str)
{
	return strtolower(strip_tags(trim(($str))));
}

//Functions that interact mainly with .users table
//------------------------------------------------------------------------------

//Delete a defined array of users
function deleteUsers($users) {
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."users 
		WHERE id = ?");
	$stmt2 = $mysqli->prepare("DELETE FROM ".$db_table_prefix."user_permission_matches 
		WHERE user_id = ?");
	foreach($users as $id){
		$stmt->bind_param("i", $id);
		$stmt->execute();
		$stmt2->bind_param("i", $id);
		$stmt2->execute();
		$i++;
	}
	$stmt->close();
	$stmt2->close();
	return $i;
}

//Check if a display name exists in the DB
function displayNameExists($displayname)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT active
		FROM ".$db_table_prefix."users
		WHERE
		display_name = ?
		LIMIT 1");
	$stmt->bind_param("s", $displayname);	
	$stmt->execute();
	$stmt->store_result();
	$num_returns = $stmt->num_rows;
	$stmt->close();
	
	if ($num_returns > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
}

//Check if an email exists in the DB
function emailExists($email)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT active
		FROM ".$db_table_prefix."users
		WHERE
		email = ?
		LIMIT 1");
	$stmt->bind_param("s", $email);	
	$stmt->execute();
	$stmt->store_result();
	$num_returns = $stmt->num_rows;
	$stmt->close();
	
	if ($num_returns > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
}

//Check if a user name and email belong to the same user
function emailUsernameLinked($email,$username)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT active
		FROM ".$db_table_prefix."users
		WHERE user_name = ?
		AND
		email = ?
		LIMIT 1
		");
	$stmt->bind_param("ss", $username, $email);	
	$stmt->execute();
	$stmt->store_result();
	$num_returns = $stmt->num_rows;
	$stmt->close();
	
	if ($num_returns > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
}

//Retrieve information for all users
function fetchAllUsers()
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		user_name,
		display_name,
		password,
		email,
		activation_token,
		last_activation_request,
		lost_password_request,
		active,
		title,
		sign_up_stamp,
		last_sign_in_stamp,
		image,
		facebook_profile,
		twitter_profile,
		linkedin_profile,
		pinterest_profile
		FROM ".$db_table_prefix."users");
	$stmt->execute();
	$stmt->bind_result($id, $user, $display, $password, $email, $token, $activationRequest, $passwordRequest, $active, $title, $signUp, $signIn,$image,$facebook,$twitter,$linkedin,$pinterest);
	
	while ($stmt->fetch()){
		$row[] = array('id' => $id, 'user_name' => $user, 'display_name' => $display, 'password' => $password, 'email' => $email, 'activation_token' => $token, 'last_activation_request' => $activationRequest, 'lost_password_request' => $passwordRequest, 'active' => $active, 'title' => $title, 'sign_up_stamp' => $signUp, 'last_sign_in_stamp' => $signIn, 'image'=>$image, 'facebook_profile'=>$facebook,	'twitter_profile'=>$twitter,'linkedin_profile'=>$linkedin,		'pinterest_profile'=>$pinterest);
	}
	$stmt->close();
	return ($row);
}

//Retrieve information for all users
function getmeAllUsers()
{
	global $db_table_prefix; 
	$con = mysqli_connect('localhost','root','','quest');
    $db = mysqli_select_db($con,'quest');
	$stmt = mysqli_query($con,"SELECT id
		FROM ".$db_table_prefix."users");
	$stmt1 = mysqli_num_rows($stmt);
	echo $stmt1;

}
function getmeAllProperties()
{
	global $db_table_prefix; 
	$con = mysqli_connect('localhost','root','','deva');
    $db = mysqli_select_db($con,'deva');
	$stmt = mysqli_query($con,"SELECT propID
		FROM ".$db_table_prefix."properties");
	$stmt1 = mysqli_num_rows($stmt);
	echo $stmt1;

}

//get userName
function fetchUserName($id=NULL){
if($id!=NULL) {
		$column = "id";
		$data = $id;
	}
	
global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		userFirst,
		userLast	
	FROM ".$db_table_prefix."users
		WHERE
		$column = ?
		LIMIT 1");
		$stmt->bind_param("s", $data);
	
	$stmt->execute();
	$stmt->bind_result($id,$userFirst,$userLast);
	while ($stmt->fetch()){
			$row = array('id' => $id, 'userFirst'=>$userFirst, 'userLast'=>$userLast);}
	$stmt->close();
	return ($row);
}


//Retrieve complete user information by username, token or ID
function fetchUserDetails($username=NULL,$token=NULL, $id=NULL)
{
	if($username!=NULL) {
		$column = "email";
		$data = $username;
	}
	elseif($token!=NULL) {
		$column = "activation_token";
		$data = $token;
	}
	elseif($id!=NULL) {
		$column = "id";
		$data = $id;
	}
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		user_name,
		display_name,
		password,
		email,
		activation_token,
		last_activation_request,
		lost_password_request,
		active,
		title,
		sign_up_stamp,
		last_sign_in_stamp,
		image,
		facebook_profile,
		twitter_profile,
		linkedin_profile,
		pinterest_profile,
		isAdmin,
		userFirst,
		userLast,
		recEmails,
		weatherLoc,
		qualification,
		gender,
		userNotes,
		phone,
		DOB,
		visits,
		companyId
		FROM ".$db_table_prefix."users
		WHERE
		$column = ?
		LIMIT 1");
		$stmt->bind_param("s", $data);
	
	$stmt->execute();
	$stmt->bind_result($id, $user, $display, $password, $email, $token, $activationRequest, $passwordRequest, $active, $title, $signUp, $signIn,$image,$facebook,$twitter,$linkedin,$pinterest,$isAdmin,$userFirst,$userLast,$recEmails,$weatherLoc,$qualification,$gender,$userNotes,$phone,$dob,$visits,$companyId);
	while ($stmt->fetch()){
			$row = array('id' => $id, 'user_name' => $user, 'display_name' => $display, 'password' => $password, 'email' => $email, 'activation_token' => $token, 'last_activation_request' => $activationRequest, 'lost_password_request' => $passwordRequest, 'active' => $active, 'title' => $title,
			'sign_up_stamp' => $signUp, 'last_sign_in_stamp' => $signIn, 'image'=>$image, 'facebook_profile'=>$facebook,	'twitter_profile'=>$twitter, 'linkedin_profile'=>$linkedin,'pinterest_profile'=>$pinterest,'isAdmin'=>$isAdmin, 'userFirst'=>$userFirst, 'userLast'=>$userLast, 'recEmails'=>$recEmails,	
			'weatherLoc'=>$weatherLoc,'qualification'=>$qualification,'gender'=>$gender,'userNotes'=>$userNotes, 'phone'=>$phone, 'DOB'=>$dob,'visits'=>$visits, 'companyId'=>$companyId);}
	$stmt->close();
	return ($row);
}

//Toggle if lost password request flag on or off
function flagLostPasswordRequest($username,$value)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET lost_password_request = ?
		WHERE
		user_name = ?
		LIMIT 1
		");
	$stmt->bind_param("ss", $value, $username);
	$result = $stmt->execute();
	$stmt->close();
	return $result;
}

//Check if a user is logged in
function isUserLoggedIn()
{
	global $loggedInUser,$mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT 
		id,
		password
		FROM ".$db_table_prefix."users
		WHERE
		id = ?
		AND 
		password = ? 
		AND
		active = 1
		LIMIT 1");
	$stmt->bind_param("is", $loggedInUser->user_id, $loggedInUser->hash_pw);	
	$stmt->execute();
	$stmt->store_result();
	$num_returns = $stmt->num_rows;
	$stmt->close();
	
	if($loggedInUser == NULL)
	{
		return false;
	}
	else
	{
		if ($num_returns > 0)
		{
			return true;
		}
		else
		{
			destroySession("userCakeUser");
			return false;	
		}
	}
}

//Change a user from inactive to active
function setUserActive($token)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET active = 1
		WHERE
		activation_token = ?
		LIMIT 1");
	$stmt->bind_param("s", $token);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;
}

//Change a user's display name
function updateDisplayName($id, $display)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET display_name = ?
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("si", $display, $id);
	$result = $stmt->execute();
	$stmt->close();
	return $result;
}

//Change a user's first name and surname
function updateUserNames($userId,$firstName,$lastName)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET userFirst = ?,
		userLast=?
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("ssi", $firstName,$lastName,$userId);
	$result = $stmt->execute();
	$stmt->close();
	return $result;
}


function updateUserDetails($userId,$userNotes,$Gender,$dob)
{
    global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET 
		userNotes=?,
		gender=?,
		DOB=?
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("sssi", $userNotes,$Gender,$dob,$userId);
	$result = $stmt->execute();
	$stmt->close();
	return $result;

}



//Update a user's email
function updateEmail($id, $email)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET 
		email = ?
		WHERE
		id = ?");
	$stmt->bind_param("si", $email, $id);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;
}


//Change a user's social profile links
function updateUserContacts($id,$linkedin,$facebook,$google,$twitter,$city,$phone)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET 
		linkedin_profile = ?,
		facebook_profile=?,
		pinterest_profile=?,
		twitter_profile=?,
		weatherLoc=?,
		phone=?
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("ssssssi", $linkedin,$facebook,$google,$twitter,$city,$phone,$id);
	$result = $stmt->execute();
	$stmt->close();
	return $result;
}


//Update a user's password
function updatePassword($id, $newpassword)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET 
		password = ?
		WHERE
		id = ?");
	$stmt->bind_param("si", $newpassword, $id);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;
}


//Input new activation token, and update the time of the most recent activation request
function updateLastActivationRequest($new_activation_token,$username,$email)
{
	global $mysqli,$db_table_prefix; 	
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET activation_token = ?,
		last_activation_request = ?
		WHERE email = ?
		AND
		user_name = ?");
	$stmt->bind_param("ssss", $new_activation_token, time(), $email, $username);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;
}

//Generate a random password, and new token
function updatePasswordFromToken($pass,$token)
{
	global $mysqli,$db_table_prefix;
	$new_activation_token = generateActivationToken();
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET password = ?,
		activation_token = ?
		WHERE
		activation_token = ?");
	$stmt->bind_param("sss", $pass, $new_activation_token, $token);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;
}

//Update a user's title
function updateTitle($id, $title)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET 
		title = ?
		WHERE
		id = ?");
	$stmt->bind_param("si", $title, $id);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;	
}

//Check if a user ID exists in the DB
function userIdExists($id)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT active
		FROM ".$db_table_prefix."users
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("i", $id);	
	$stmt->execute();
	$stmt->store_result();
	$num_returns = $stmt->num_rows;
	$stmt->close();
	
	if ($num_returns > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
}

//Checks if a username exists in the DB
function usernameExists($username)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT active
		FROM ".$db_table_prefix."users
		WHERE
		user_name = ?
		LIMIT 1");
	$stmt->bind_param("s", $username);	
	$stmt->execute();
	$stmt->store_result();
	$num_returns = $stmt->num_rows;
	$stmt->close();
	
	if ($num_returns > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
}

//Check if activation token exists in DB
function validateActivationToken($token,$lostpass=NULL)
{
	global $mysqli,$db_table_prefix;
	if($lostpass == NULL) 
	{	
		$stmt = $mysqli->prepare("SELECT active
			FROM ".$db_table_prefix."users
			WHERE active = 0
			AND
			activation_token = ?
			LIMIT 1");
	}
	else 
	{
		$stmt = $mysqli->prepare("SELECT active
			FROM ".$db_table_prefix."users
			WHERE active = 1
			AND
			activation_token = ?
			AND
			lost_password_request = 1 
			LIMIT 1");
	}
	$stmt->bind_param("s", $token);
	$stmt->execute();
	$stmt->store_result();
		$num_returns = $stmt->num_rows;
	$stmt->close();
	
	if ($num_returns > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
}


function createUserFolder($username)
{
	$Default='./resources/filemanager/default';
	$New='./resources/filemanager/users/'.$username;
	
	//exec("xcopy $Default $New /e/i"); //windows server
	
	define('DS', DIRECTORY_SEPARATOR); // I always use this short form in my code.

	if (!file_exists($New)) {
    $oldmask = umask(0);
     function copy_r($source, $dest, $options=array('folderPermission'=>0755,'filePermission'=>0755)) 
    { 
        $result=false; 
        
        if (is_file($source)) { 
            if ($dest[strlen($dest)-1]=='/') { 
                if (!file_exists($dest)) { 
                    cmfcDirectory::makeAll($dest,$options['folderPermission'],true); 
                } 
                $__dest=$dest."/".basename($source); 
            } else { 
                $__dest=$dest; 
            } 
            $result=copy($source, $__dest); 
            chmod($__dest,$options['filePermission']); 
            
        } elseif(is_dir($source)) { 
            if ($dest[strlen($dest)-1]=='/') { 
                if ($source[strlen($source)-1]=='/') { 
                    //Copy only contents 
                } else { 
                    //Change parent itself and its contents 
                    $dest=$dest.basename($source); 
                    @mkdir($dest); 
                    chmod($dest,$options['filePermission']); 
                } 
            } else { 
                if ($source[strlen($source)-1]=='/') { 
                    //Copy parent directory with new name and all its content 
                    @mkdir($dest,$options['folderPermission']); 
                    chmod($dest,$options['filePermission']); 
                } else { 
                    //Copy parent directory with new name and all its content 
                    @mkdir($dest,$options['folderPermission']); 
                    chmod($dest,$options['filePermission']); 
                } 
            } 

            $dirHandle=opendir($source); 
            while($file=readdir($dirHandle)) 
            { 
                if($file!="." && $file!="..") 
                { 
                     if(!is_dir($source."/".$file)) { 
                        $__dest=$dest."/".$file; 
                    } else { 
                        $__dest=$dest."/".$file; 
                    } 
                    //echo "$source/$file ||| $__dest<br />"; 
                    $result=copy_r($source."/".$file, $__dest, $options); 
                } 
            } 
            closedir($dirHandle); 
            
        } else { 
            $result=false; 
        } 
        return $result; 
    } 
	umask($oldmask);
	exec(copy_r ($Default,$New));  //linux
	}
}

//function to calculate folder size
function GetDirectorySize($path){
    $bytestotal = 0;
    $path = realpath($path);
    if($path!==false){
        foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS)) as $object){
            $bytestotal += $object->getSize();
        }
    }
    return $bytestotal;
}

//function to convert folder size into bit notation
function formatBytes($size, $precision = 2)
{
    $base = log($size) / log(1024);
    $suffixes = array('', 'k', 'M', 'G', 'T');   

    return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
}

//Functions that interact mainly with .permissions table
//------------------------------------------------------------------------------

//Create a permission level in DB
function createPermission($permission) {
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."permissions (
		name
		)
		VALUES (
		?
		)");
	$stmt->bind_param("s", $permission);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;
}

//Delete a permission level from the DB
function deletePermission($permission) {
	global $mysqli,$db_table_prefix,$errors; 
	$i = 0;
	$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."permissions 
		WHERE id = ?");
	$stmt2 = $mysqli->prepare("DELETE FROM ".$db_table_prefix."user_permission_matches 
		WHERE permission_id = ?");
	$stmt3 = $mysqli->prepare("DELETE FROM ".$db_table_prefix."permission_page_matches 
		WHERE permission_id = ?");
	foreach($permission as $id){
		if ($id == 1){
			$errors[] = lang("CANNOT_DELETE_NEWUSERS");
		}
		elseif ($id == 2){
			$errors[] = lang("CANNOT_DELETE_ADMIN");
		}
		else{
			$stmt->bind_param("i", $id);
			$stmt->execute();
			$stmt2->bind_param("i", $id);
			$stmt2->execute();
			$stmt3->bind_param("i", $id);
			$stmt3->execute();
			$i++;
		}
	}
	$stmt->close();
	$stmt2->close();
	$stmt3->close();
	return $i;
}

//Retrieve information for all permission levels
function fetchAllPermissions()
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		name
		FROM ".$db_table_prefix."permissions");
	$stmt->execute();
	$stmt->bind_result($id, $name);
	while ($stmt->fetch()){
		$row[] = array('id' => $id, 'name' => $name);
	}
	$stmt->close();
	return ($row);
}

//Retrieve information for a single permission level
function fetchPermissionDetails($id)
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		name
		FROM ".$db_table_prefix."permissions
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("i", $id);
	$stmt->execute();
	$stmt->bind_result($id, $name);
	while ($stmt->fetch()){
		$row = array('id' => $id, 'name' => $name);
	}
	$stmt->close();
	return ($row);
}

//Check if a permission level ID exists in the DB
function permissionIdExists($id)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT id
		FROM ".$db_table_prefix."permissions
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("i", $id);	
	$stmt->execute();
	$stmt->store_result();
	$num_returns = $stmt->num_rows;
	$stmt->close();
	
	if ($num_returns > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
}

//Check if a permission level name exists in the DB
function permissionNameExists($permission)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT id
		FROM ".$db_table_prefix."permissions
		WHERE
		name = ?
		LIMIT 1");
	$stmt->bind_param("s", $permission);	
	$stmt->execute();
	$stmt->store_result();
	$num_returns = $stmt->num_rows;
	$stmt->close();
	
	if ($num_returns > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
}

//Change a permission level's name
function updatePermissionName($id, $name)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."permissions
		SET name = ?
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("si", $name, $id);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;	
}

//Functions that interact mainly with .user_permission_matches table
//------------------------------------------------------------------------------

//Match permission level(s) with user(s)
function addPermission($permission, $user) {
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."user_permission_matches (
		permission_id,
		user_id
		)
		VALUES (
		?,
		?
		)");
	if (is_array($permission)){
		foreach($permission as $id){
			$stmt->bind_param("ii", $id, $user);
			$stmt->execute();
			$i++;
		}
	}
	elseif (is_array($user)){
		foreach($user as $id){
			$stmt->bind_param("ii", $permission, $id);
			$stmt->execute();
			$i++;
		}
	}
	else {
		$stmt->bind_param("ii", $permission, $user);
		$stmt->execute();
		$i++;
	}
	$stmt->close();
	return $i;
}

//Retrieve information for all user/permission level matches
function fetchAllMatches()
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		user_id,
		permission_id
		FROM ".$db_table_prefix."user_permission_matches");
	$stmt->execute();
	$stmt->bind_result($id, $user, $permission);
	while ($stmt->fetch()){
		$row[] = array('id' => $id, 'user_id' => $user, 'permission_id' => $permission);
	}
	$stmt->close();
	return ($row);	
}

//Retrieve list of permission levels a user has
function fetchUserPermissions($user_id)
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT
		id,
		permission_id
		FROM ".$db_table_prefix."user_permission_matches
		WHERE user_id = ?
		");
	$stmt->bind_param("i", $user_id);	
	$stmt->execute();
	$stmt->bind_result($id, $permission);
	while ($stmt->fetch()){
		$row[$permission] = array('id' => $id, 'permission_id' => $permission);
	}
	$stmt->close();
	if (isset($row)){
		return ($row);
	}
}

//Retrieve list of users who have a permission level
function fetchPermissionUsers($permission_id)
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT id, user_id
		FROM ".$db_table_prefix."user_permission_matches
		WHERE permission_id = ?
		");
	$stmt->bind_param("i", $permission_id);	
	$stmt->execute();
	$stmt->bind_result($id, $user);
	while ($stmt->fetch()){
		$row[$user] = array('id' => $id, 'user_id' => $user);
	}
	$stmt->close();
	if (isset($row)){
		return ($row);
	}
}

//Unmatch permission level(s) from user(s)
function removePermission($permission, $user) {
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."user_permission_matches 
		WHERE permission_id = ?
		AND user_id =?");
	if (is_array($permission)){
		foreach($permission as $id){
			$stmt->bind_param("ii", $id, $user);
			$stmt->execute();
			$i++;
		}
	}
	elseif (is_array($user)){
		foreach($user as $id){
			$stmt->bind_param("ii", $permission, $id);
			$stmt->execute();
			$i++;
		}
	}
	else {
		$stmt->bind_param("ii", $permission, $user);
		$stmt->execute();
		$i++;
	}
	$stmt->close();
	return $i;
}

//Functions that interact mainly with .configuration table
//------------------------------------------------------------------------------

//Update configuration table
function updateConfig($id, $value)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."configuration
		SET 
		value = ?
		WHERE
		id = ?");
	foreach ($id as $cfg){
		$stmt->bind_param("si", $value[$cfg], $cfg);
		$stmt->execute();
	}
	$stmt->close();	
}

//Functions that interact mainly with .pages table
//------------------------------------------------------------------------------

//Add a page to the DB
function createPages($pages) {
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."pages (
		page
		)
		VALUES (
		?
		)");
	foreach($pages as $page){
		$stmt->bind_param("s", $page);
		$stmt->execute();
	}
	$stmt->close();
}

//Delete a page from the DB
function deletePages($pages) {
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."pages 
		WHERE id = ?");
	$stmt2 = $mysqli->prepare("DELETE FROM ".$db_table_prefix."permission_page_matches 
		WHERE page_id = ?");
	foreach($pages as $id){
		$stmt->bind_param("i", $id);
		$stmt->execute();
		$stmt2->bind_param("i", $id);
		$stmt2->execute();
	}
	$stmt->close();
	$stmt2->close();
}

//Fetch information on all pages
function fetchAllPages()
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		page,
		private
		FROM ".$db_table_prefix."pages");
	$stmt->execute();
	$stmt->bind_result($id, $page, $private);
	while ($stmt->fetch()){
		$row[$page] = array('id' => $id, 'page' => $page, 'private' => $private);
	}
	$stmt->close();
	if (isset($row)){
		return ($row);
	}
}

//Fetch information for a specific page
function fetchPageDetails($id)
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		page,
		private
		FROM ".$db_table_prefix."pages
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("i", $id);
	$stmt->execute();
	$stmt->bind_result($id, $page, $private);
	while ($stmt->fetch()){
		$row = array('id' => $id, 'page' => $page, 'private' => $private);
	}
	$stmt->close();
	return ($row);
}

//Check if a page ID exists
function pageIdExists($id)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT private
		FROM ".$db_table_prefix."pages
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("i", $id);	
	$stmt->execute();
	$stmt->store_result();	
	$num_returns = $stmt->num_rows;
	$stmt->close();
	
	if ($num_returns > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
}

//Toggle private/public setting of a page
function updatePrivate($id, $private)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."pages
		SET 
		private = ?
		WHERE
		id = ?");
	$stmt->bind_param("ii", $private, $id);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;	
}

//Functions that interact mainly with .permission_page_matches table
//------------------------------------------------------------------------------

//Match permission level(s) with page(s)
function addPage($page, $permission) {
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."permission_page_matches (
		permission_id,
		page_id
		)
		VALUES (
		?,
		?
		)");
	if (is_array($permission)){
		foreach($permission as $id){
			$stmt->bind_param("ii", $id, $page);
			$stmt->execute();
			$i++;
		}
	}
	elseif (is_array($page)){
		foreach($page as $id){
			$stmt->bind_param("ii", $permission, $id);
			$stmt->execute();
			$i++;
		}
	}
	else {
		$stmt->bind_param("ii", $permission, $page);
		$stmt->execute();
		$i++;
	}
	$stmt->close();
	return $i;
}

//Retrieve list of permission levels that can access a page
function fetchPagePermissions($page_id)
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT
		id,
		permission_id
		FROM ".$db_table_prefix."permission_page_matches
		WHERE page_id = ?
		");
	$stmt->bind_param("i", $page_id);	
	$stmt->execute();
	$stmt->bind_result($id, $permission);
	while ($stmt->fetch()){
		$row[$permission] = array('id' => $id, 'permission_id' => $permission);
	}
	$stmt->close();
	if (isset($row)){
		return ($row);
	}
}

//Retrieve list of pages that a permission level can access
function fetchPermissionPages($permission_id)
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT
		id,
		page_id
		FROM ".$db_table_prefix."permission_page_matches
		WHERE permission_id = ?
		");
	$stmt->bind_param("i", $permission_id);	
	$stmt->execute();
	$stmt->bind_result($id, $page);
	while ($stmt->fetch()){
		$row[$page] = array('id' => $id, 'permission_id' => $page);
	}
	$stmt->close();
	if (isset($row)){
		return ($row);
	}
}

//Unmatched permission and page
function removePage($page, $permission) {
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."permission_page_matches 
		WHERE page_id = ?
		AND permission_id =?");
	if (is_array($page)){
		foreach($page as $id){
			$stmt->bind_param("ii", $id, $permission);
			$stmt->execute();
			$i++;
		}
	}
	elseif (is_array($permission)){
		foreach($permission as $id){
			$stmt->bind_param("ii", $page, $id);
			$stmt->execute();
			$i++;
		}
	}
	else {
		$stmt->bind_param("ii", $permission, $user);
		$stmt->execute();
		$i++;
	}
	$stmt->close();
	return $i;
}

//Check if a user has access to a page
function securePage($uri){
	
	//Separate document name from uri
	$tokens = explode('/', $uri);
	$page = $tokens[sizeof($tokens)-1];
	global $mysqli,$db_table_prefix,$loggedInUser;
	//retrieve page details
	$stmt = $mysqli->prepare("SELECT 
		id,
		page,
		private
		FROM ".$db_table_prefix."pages
		WHERE
		page = ?
		LIMIT 1");
	$stmt->bind_param("s", $page);
	$stmt->execute();
	$stmt->bind_result($id, $page, $private);
	while ($stmt->fetch()){
		$pageDetails = array('id' => $id, 'page' => $page, 'private' => $private);
	}
	$stmt->close();
	//If page does not exist in DB, allow access
	if (empty($pageDetails)){
		return true;
	}
	//If page is public, allow access
	elseif ($pageDetails['private'] == 0) {
		return true;	
	}
	//If user is not logged in, deny access
	elseif(!isUserLoggedIn()) 
	{
		header("Location: login.php");
		return false;
	}
	else {
		//Retrieve list of permission levels with access to page
		$stmt = $mysqli->prepare("SELECT
			permission_id
			FROM ".$db_table_prefix."permission_page_matches
			WHERE page_id = ?
			");
		$stmt->bind_param("i", $pageDetails['id']);	
		$stmt->execute();
		$stmt->bind_result($permission);
		while ($stmt->fetch()){
			$pagePermissions[] = $permission;
		}
		$stmt->close();
		//Check if user's permission levels allow access to page
		if ($loggedInUser->checkPermission($pagePermissions)){ 
			return true;
		}
		//Grant access if master user
		elseif ($loggedInUser->user_id == $master_account){
			return true;
		}
		else {
			header("Location: account.php");
			return false;	
		}
	}
}

//Retrieve information for all properties
function fetchAllProperties()
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		propID,
		Address,
		Bedrooms,
		SittingRooms,
		BathRooms,
		DateAdded,
		DistanceFromCampus,
		Town,
		Comments,
		img1,
		img2,
		img3,
		img4,
		img5,
		Status,
		costPM,
		availableFrom
		FROM ".$db_table_prefix."properties");
	$stmt->execute();
	$stmt->bind_result($propID, $Address, $Bedrooms, $SittingRooms, $BathRooms, $DateAdded, $DistanceFromCampus, $Town, $Comments, $img1, $img2, $img3,$img4,$img5,$Status,$costPM,$availableFrom);
	
	while ($stmt->fetch()){
		$row[] = array('propID' => $propID, 'Address' => $Address, 'Bedrooms' => $Bedrooms, 'SittingRooms' => $SittingRooms, 'BathRooms' => $BathRooms, 'DateAdded' => $DateAdded, 'DistanceFromCampus' => $DistanceFromCampus, 'Town' => $Town, 'Comments' => $Comments, 'img1' => $img1, 'img2' => $img2, 'img3' => $img3,'img4' => $img4,'img5' => $img5,'Status' => $Status,'costPM' => $costPM,'availableFrom'=>$availableFrom);
	}
	$stmt->close();
	return ($row);
}

//Delete a defined array of properties
function deleteProperties($properties) {
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."properties 
		WHERE propID = ?");
	$stmt2 = $mysqli->prepare("DELETE FROM ".$db_table_prefix."rooms 
		WHERE propID = ?");
	foreach($properties as $id){
		$stmt->bind_param("i", $id);
		$stmt->execute();
		$stmt2->bind_param("i", $id);
		$stmt2->execute();
		$i++;
	}
	$stmt->close();
	$stmt2->close();
	return $i;
}

//Retrieve information for all announcements
function fetchAllNews()
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		title,
		announcement,
		postedby,
		active,
		datestamp,
		url
		FROM ".$db_table_prefix."announcements");
	$stmt->execute();
	$stmt->bind_result($id, $title, $announcement, $postedby, $active, $datestamp, $url);
	
	while ($stmt->fetch()){
		$row[] = array('id' => $id, 'title' => $title, 'announcement' => $announcement, 'postedby' => $postedby, 'active' => $active, 'datestamp' => $datestamp, 'url' => $url);
	}
	$stmt->close();
	return ($row);
}

function fetchPropertyDetails($id=NULL)
{
 if($id!=NULL) {
		$column = "propID";
		$data = $id;
	}
	
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
	    propID,
		Address,
		Bedrooms,
		SittingRooms,
		BathRooms,
		DateAdded,
		DistanceFromCampus,
		Town,
		Comments,
		img1,
		img2,
		img3,
		img4,
		img5,
		Status,
		Costpm
		FROM ".$db_table_prefix."properties
		WHERE
		$column = ?
		LIMIT 1");
		$stmt->bind_param("s", $data);
	    $stmt->execute();
	
	    $stmt->bind_result($propID, $Address, $Bedrooms, $SittingRooms, $BathRooms, $DateAdded, $DistanceFromCampus,
	    $Town, $Comments, $img1, $img2, $img3,$img4,$img5,$Status,$costPM);
	 while ($stmt->fetch()){
	 
		$row = array('propID' => $propID, 'Address' => $Address, 'Bedrooms' => $Bedrooms, 'SittingRooms' => $SittingRooms, 'BathRooms' => $BathRooms,
		'DateAdded' => $DateAdded, 'DistanceFromCampus' => $DistanceFromCampus, 'Town' => $Town, 'Comments' => $Comments, 'img1' => $img1, 
		'img2' => $img2,'img3' => $img3,'img4' => $img4,'img5' => $img5,'Status' => $Status, 'Costpm' => $costPM);
	 }
	$stmt->close();
	return ($row);
}


 function insertChatMessage($chatUserID,$chatText){
		$req=$mysqli->prepare("INSERT INTO gantt_chat(chatUserID,ChatText) values(:chatUserID, :chatText)");
		$req->execute(array(
		'chatUserID'=>$chatUserID,
		'chatText'=>$chatText
		));	
	}
	
 function displayMessages(){
		$chatReq=$mysqli->prepare("SELECT * FROM gantt_chat ORDER BY chatID DESC");
		$chatReq->execute();
		
		while($DataChat=$ChatReq->fetch()){
			$userReq=$bdd->prepare("SELECT * FROM gantt_users WHERE UserID=:UserID");
			$userReq->execute(array(
				'userID'=>$DataChat["chatUserID"]
			));
			$DataUser=$userReq['chatUserID'];
			?>
            <span class="UserNames"><?php echo $DataUser['userName'] ;?></span><br/>
             <span class="chatMessages"><?php echo $DataChat['chatText'];?></span>
            <?php	
		}
	}
	
 function countTasksToday(){
	 global $mysqli;
		$query="SELECT  gl.text, DATE_ADD( gl.start_date, INTERVAL (gl.duration-1) DAY) as due_date,gl.progress FROM gantt_tasks gl 
WHERE CURDATE()=DATE_ADD( gl.start_date, INTERVAL (gl.duration-1) DAY)";
		
		
		if ($result = $mysqli->query($query)) {
			$rowcount=mysqli_num_rows($result);
			return $rowcount;
		}
 }
 
 
 //this section deals with functions for formatting dates

function formatDate($date){
$h1 = "2";// Hour for time zone goes here e.g. +7 or -4, just remove the + or -
$hm = $h1 * 60; 
$ms = $hm * 60;
$time=time()+($ms); 

$h=date("H",$date);
$m=date("m",$date);
$today=date("d",$time);
$chatday=date("d",$date);
$returnDate='xx';

$dateDiff=$today-$chatday;



if($dateDiff==0){
	$returnDate="Today"." ".$h.":".$m;
	
}
else{
	if($dateDiff<8){
		$returnDate=date("l H:m",$date);
	}
	else{
		$returnDate=date("Y-m-d H:m",$date);
	}
	
}

 
 return $returnDate;
}


/*----------------------------------------*/
/* functions relating to KPAs and tasks   */
/*----------------------------------------*/


function getRecentTasks($userId){
	global $mysqli,$db_table_prefix,$row; 
	$stmt = $mysqli->prepare("SELECT
				taskId,
				taskTitle,
				lastUpdated,
				UNIX_TIMESTAMP(lastUpdated) AS orderDate
			FROM
				tasks
			WHERE
				userId = ".$userId." AND
				lastUpdated != '0000-00-00 00:00:00'
			ORDER BY
				orderDate DESC
			LIMIT 5");
	$stmt->execute();
	$stmt->bind_result($taskId, $taskTitle,$LastUpdated,$orderDate);
	
	while ($stmt->fetch()){
		$row[] = array('taskId' => $taskId, 'taskTitle' => $taskTitle, 'lastUpdated' => $LastUpdated);
	}
	$stmt->close();
	return ($row);
	
	}

function getRecentEvents($userId){
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT
				eventId,
				eventTitle,
				lastUpdated,
				UNIX_TIMESTAMP(lastUpdated) AS orderDate
			FROM
				events
			WHERE
				userId = ".$userId." AND
				lastUpdated != '0000-00-00 00:00:00'
			ORDER BY
				orderDate DESC
			LIMIT 5");
    $check = $stmt->num_rows();
	if($check != 0){
	
	$stmt->execute();
	$stmt->bind_result($eventId, $eventTitle, $LastUpdated,$orderDate);
	
	while ($stmt->fetch()){
		$row[] = array('eventId' => $eventId, 'eventTitle' => $eventTitle, 'lastUpdated' => $LastUpdated);
	}
	$stmt->close();
	return ($row);
	}else{
	  echo '';
	}
	
}

function getKPAs($userId){
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT
				catId,
				catName,
				UNIX_TIMESTAMP(catDate) AS orderDate
			FROM
				categories
			WHERE
				userId = ".$userId." AND
				isActive = 1
			ORDER BY
				orderDate DESC");
    $check = $stmt->num_rows();
	if($check != 0){
	$stmt->execute();
	$stmt->bind_result($catId, $catName, $orderDate);
	
	while ($stmt->fetch()){
		$row[] = array('catId' => $catId, 'catName' => $catName);
	}
	$stmt->close();
	return ($row);
	
	}else{
	  echo '';
	}
	
}
	
	//function to calculate the percentage complete of a parent task
	function percentComplete($task){
		global $mysqli,$db_table_prefix,$row; 
		$query="SELECT sum(taskPercent) as percentage, count(taskPercent) as num FROM tasks where parent=".$task;
		$result=mysqli_query($mysqli,$query) ;
		$row = mysqli_fetch_assoc($result);
		if($row["num"]>0){
		return round( $row["percentage"]/$row["num"],0,1);}
		else{ return "0";}
		
	}
	
		//function to calculate the percentage complete of a Goal
	function percentGoalComplete($goal){
		global $mysqli,$db_table_prefix,$row; 
		$query="SELECT sum(taskPercent) as percentage, count(taskPercent) as num FROM tasks where catId=".$goal;
		$result=mysqli_query($mysqli,$query) ;
		$row = mysqli_fetch_assoc($result);
		if($row["num"]>0){
		return round( $row["percentage"]/$row["num"],0,1);}
		else{ return "0";}
		
	}
	
	//function to calculate percentage progress for a user
function upercentComplete($user){
		global $mysqli,$db_table_prefix,$row; 
		$query="SELECT sum(taskPercent) as percentage, count(taskPercent) as num FROM tasks where userId=".$user;
		$result=mysqli_query($mysqli,$query) ;
		$row = mysqli_fetch_assoc($result);
		if($row["num"]>0){
		return round( $row["percentage"]/$row["num"],0,1);}
		else{ return "0";}
		
	}

	//function to insert a new item into the logs table
	//Create a permission level in DB
function addLog($userId,$activity, $logtype,$companyId) {
	global $mysqli,$db_table_prefix; 
	$time=date("Y-m-d h:i:s");
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."log (
		logDate,
		activity,
		userId,
		logType,
		companyId
		)
		VALUES (
		?,
		?,
		?,
		?,
		?
		)");
	$stmt->bind_param("ssssi", 
						$time,
						$activity,
						$userId,
						$logtype,
						$companyId
						);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;
}

//update task percentages for tasks that have nested subtasks
function updateParentPercentage($userId){
global $mysqli;
$query="SELECT parent,(sum(taskPercent)/count(taskPercent)) as taskPerc FROM tasks WHERE parent>0 and userId=".$userId." GROUP BY parent";
$res=$mysqli->query($query);
if(mysqli_num_rows($res)>0){

while($row=mysqli_fetch_array($res)){
$taskPerc=round($row['taskPerc'],2);
$stmt = $mysqli->prepare("
								UPDATE tasks set taskPercent=?
								WHERE
									taskId=?
								LIMIT 1		
			");
			$stmt->bind_param('ss',
								$taskPerc,
								$row['parent']
			);
			$stmt->execute();
			$stmt->close();		

		}

	}
}

//function to send email when an event is created
function emailEvent($team,$author,$eventTitle,$startDate,$endDate,$description)
			{
			global $mysqli;
				$q="select memberID from gantt_team_members where teamID=".$team;
				$res=$mysqli->query($q);
				while($row=mysqli_fetch_array($res)){
					$id=$row['memberID'];
					//get userName
					$u='select userFirst,email from gantt_users where id='.$id;
					$r=$mysqli->query($u);
					$userdetails=mysqli_fetch_assoc($r);
					$userName=$userdetails['userFirst'];
					
					$mail = new questMail();
					
					
					//Setup our custom hooks
					$hooks = array(
						"searchStrs" => array("#AUTHOR#","#USERNAME#","#EVENT-TITLE#","#START-DATE#","#END-DATE#","#DESCRIPTION#"),
						"subjectStrs" => array($author,$userName,$eventTitle,$startDate,$endDate,$description)
						);
					
					if(!$mail->newTemplateMsg("new-event.txt",$hooks))
					{
						$errors[] = lang("MAIL_TEMPLATE_BUILD_ERROR");
					}
					else
					{
						if(!$mail->sendMail($userdetails["email"],"New Event Scheduled"))
						{
							$errors[] = lang("MAIL_ERROR");
						}
						
					}
				}
			}

//function to create template for user appraisal ratings for a user			
function createUserRatings($userId){
	global $mysqli; //echo " UserID=".$userId."<br>";
	$uQuery="select * from gantt_users";
	$userRes=$mysqli->query($uQuery);
	while($urow=mysqli_fetch_array($userRes)){ //iterate through all users
		$company=$urow['companyId'];
		$rQuery="select * from gantt_rating_domains where companyId=".$company;
		$ratingRes=$mysqli->query($rQuery);
		
		while($row=mysqli_fetch_array($ratingRes)){
		$rDomain=$row['rdomainId'];
		$rActive=1;
		$friendId=$urow['id'];//echo $friendId;
		$rating=0;
		$date=date("Y-m-d h:i:s");
		//add new row only if it doesnt exist at all
		//echo checkRatingEntry($userId,$rDomain,$friendId);
		if(checkRatingEntry($userId,$rDomain,$friendId)==0){
		
			$stmt = $mysqli->prepare("INSERT INTO 
										gantt_ratings(
										userId,
										friendId,
										userRating,
										ratingDate,
										companyId,
										rActive,
										rDomain
										)
										VALUES(
										 ?,
										 ?,
										 ?,
										 ?,
										 ?,
										 ?,
										 ?)"
			);
			$stmt->bind_param('sssssss',
								$userId,
								$friendId,
								$rating,
								$date,
								$company,
								$rActive,
								$rDomain
			);
			$stmt->execute();
			$stmt->close();
			}
		}
	}
}

function checkRatingEntry($uId,$domain,$fId){
	global $mysqli;
	$qry="select * from gantt_ratings where userId=".$uId." and friendId=".$fId." and rDomain=".$domain;
	$res=$mysqli->query($qry);
	if(mysqli_num_rows($res)>0){$result=1;}
	else{$result=0;}
	
	return $result;
}


?>
