<?php

require_once("../config-small.php");

//User must activate their account first
		global $mysqli,$emailActivation,$websiteUrl,$db_table_prefix,$result;
	
		$email=$_POST['email'];
		$firstname=$_POST['firstName'];
		$memberId=$_POST['memberId'];

		 $errText="";
         $sText="";
         $status=false;
		 $result= array("id"=>"0","text"=>$errText.$sText);
        
         $clean_email = sanitize($email);

         //Completely sanitizes text
        function sanitize($str)
        {
            return strtolower(strip_tags(trim(($str))));
        }
            
         if(emailExists($clean_email))
		{
			$email_taken = true;
			$status = false;
		}
		else
		{
			//No problems have been found.
			$status = true;
		}

try{

    global $mysqli,$emailActivation,$websiteUrl,$db_table_prefix,$result;
		
    //generate auto password
    $clean_password = trim(password_generate(7));

    //Prevent this function being called if there were construction errors
    if($status==true)
    {
        //Construct a secure hash for the plain text password
        $secure_pass = generateHash($clean_password);
        
        //Construct a unique activation token
        $activation_token = generateActivationToken();
        
        //Do we need to send out an activation email?
        if($emailActivation == true)
        {
            //User must activate their account first
            $user_active = 0;
            
            $mail = new wcdbMail();
            
            //Build the activation message
            //$activation_message = lang("ACCOUNT_ACTIVATION_MESSAGE",array($websiteUrl,$activation_token));
            
            //Define more if you want to build larger structures
            $hooks = array(
                "searchStrs" => array("#SITE_URL","#ACTIVATION-KEY","#FIRSTNAME#","#USERNAME#","#PASSWORD#"),
                "subjectStrs" => array($websiteUrl,$activation_token,$firstname,$clean_email, $clean_password)
                );
            
            /* Build the template - Optional, you can just use the sendMail function 
            Instead to pass a message. */
            
            if(!$mail->newTemplateMsg("new-registration.html",$hooks))
            {
                $mail_failure = true;
                $errText=$errText." Failed to create mail message.";
            }
            else
            {
                //Send the mail. Specify users email here and subject. 
                //SendMail can have a third parementer for message if you do not wish to build a template.
                
                $result=$mail->sendMail($clean_email,"New User");
                if($result=='false')
                {
                    $mail_failure = true;
                    $errText=$errText." Failed to send mail message.";
                }
                echo "Send Mail: ".$result;
            }
            $success = lang("ACCOUNT_REGISTRATION_COMPLETE_TYPE2");
            $sText=$sText." Email sent successfully ";
        }
        else
        {
            //Instant account activation
            $user_active = 1;
            $success = lang("ACCOUNT_REGISTRATION_COMPLETE_TYPE1");
        }	
        
        
        if(!$mail_failure)
        {
            //Insert the user into the database providing no errors have been found.
            $stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."users (
                memberId,
                password,
                email,
                activation_token,
                last_activation_request,
                lost_password_request, 
                active,
                title,
                sign_up_stamp,
                last_sign_in_stamp
                )
                VALUES (
                ?,
                ?,
                ?,
                ?,
                '".time()."',
                '0',
                ?,
                'New Member',
                '".time()."',
                '0'
                )");
            
            $stmt->bind_param("ssssi", 
                $memberId,
                $secure_pass, 
                $clean_email, 
                $activation_token, 
                $user_active);
            $stmt->execute();
            $inserted_id = $mysqli->insert_id;
            $stmt->close();
            $sText=$sText." user created";

            //Insert default permission into matches table
            $stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."user_permission_matches  (
                user_id,
                permission_id
                )
                VALUES (
                ?,
                '1'
                )");
            $stmt->bind_param("s", $inserted_id);
            $stmt->execute();
            $stmt->close();
            $sText=$sText.", permissions updated";
            $result= array("id"=>"1","text"=>$errText.$sText);
            //$result="true";
            echo json_encode($result);
        }
        $errText=$errText." Mail send failure.";
        $result= array("id"=>"0","text"=>$errText.$sText);
        //$result="false";
        echo json_encode($result);
    }
    $errText=$errText." User already exists.";
    $result= array("id"=>"0","text"=>$errText.$sText);
    //$result="false";
    echo json_encode($result);
        
}catch(Exception $e){
    echo $e.getMessage();
}


?>