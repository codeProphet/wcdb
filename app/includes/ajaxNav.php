<?php
echo '
<script>
	function openPage(p){
		
		if(p==1){
		var page="perspectives";
		}
		else if(p==2){ var page="calendar";	}
		else if(p==3){ var page="employees";	}
		else if(p==4){ var page="timeline";	}
		else if(p==5){ var page="goals";	}
		else if(p==6){ var page="openTasks";	}
		
		var getPage="pages/pages/"+page+".php?userId='.$userId.'";
		$("#contentDiv").load(getPage).show();
	}
	
	function openGoal(goalId){
		var id=goalId;
		var page="viewGoal";
		var getPage="pages/pages/"+page+".php?catId="+id+"&userId='.$userId.'";
		$("#contentDiv").load(getPage).show();
	
	}
	
	function openScoreCard(user){
		var id=user;
		var page="performancePlan";
		var getPage="pages/pages/"+page+".php?userId="+id+"&coId='.$loggedInUser->company.'";
		$("#contentDiv").load(getPage).show();
	}
	
	function editGoal(goalId){
		var id=goalId;
		var page="editGoal";
		var getPage="pages/pages/"+page+".php?catId="+id+"&userId='.$userId.'";
		$("#contentDiv").load(getPage).show();
	}
	
	function openTask(taskId){
		var id=taskId;
		var page="viewTask";
		var getPage="pages/pages/"+page+".php?taskId="+id+"&userId='.$userId.'";
		$("#contentDiv").load(getPage).show();
	
	}

	
</script>';
?>