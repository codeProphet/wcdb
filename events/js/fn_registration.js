
    $(function() {
      'use strict';
      var langArray = $.map(languages, function(lang) {
        return {
          value: lang.name,
          data: lang.code
        };
      });
      // Initialize autocomplete with custom appendTo:
      $('#autocomplete-custom-append').autocomplete({
        lookup: langArray,
        appendTo: '#autocomplete-container'
      });
    });

    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
      .on('blur', 'input[required], input.optional, select.required', validator.checkField)
      .on('change', 'select.required', validator.checkField)
      .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
      .on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function(e) {
      e.preventDefault();
      var submit = true;
      // evaluate the form using generic validaing
      if (!validator.checkAll($(this))) {
        submit = false;
      }

      if (submit)
        this.submit();
      return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function() {
      $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function() {
      validator.defaults.alerts = (this.checked) ? false : true;
      if (this.checked)
        $('form .alert').remove();
    }).prop('checked', false);

    $(document).on('change', 'select#dietRestrictions', function(){
                  var value = $('select#dietRestrictions option:selected').val();
                  //alert (value);
                  console.log(value);

                  switch (value)
                  {
                    case "Yes":
                 $("#specifydietRestrictions").html('<label for="specifydietRestrictions">Specify the dietary restrictions</label>'+
                  '<textarea id="specifydietRestrictions" name="specifydietRestrictions" row="4"  placeholder="Specify the dietary restrictions here" class="form-control" >'+
                  '</textarea>');
                         break;

                    case "No":
                     $("#specifydietRestrictions").html('');

                    break;
                   
                  }
              });

    $(document).on('change', 'select#meansoftransport', function(){
                  var value = $('select#meansoftransport option:selected').val();
                  //alert (value);
                  console.log(value);

                  switch (value)
                  {
               case "Air":
                  $("#port").html('<label for="air">Select Airport</label>'+
                      '<select id="air" name="air" class="form-control" >'+
                          '<option value="">Select </option>'+
                          '<option value="Douala">Douala</option>'+
                          '<option value="Yaounde">Yaounde</option>'+ 
                          '<option value="Garaou">Garaou</option>'+
                       '</select>');
                    break;

               case "Road":
                   $("#port").html('<label for="air">Select Boader Crossing</label>'+
                      '<select id="road" name="road" class="form-control" >'+
                          '<option  value="">Select </option>'+
                          '<option  value="Ekok">Ekok</option>'+
                          '<option  value="Ambam-Minko">Ambam-Minko</option>'+  
                          '<option  value="Kenzou">Kenzou </option>'+ 
                          '<option  value="Touboro">Touboro </option>'+ 
                          '<option  value="Kousseri">Kousseri </option>'+ 
                          '<option  value="Garoua-Boulai">Garoua-Boulai</option>'+ 
                          '<option  value="Ouesso">Ouesso</option>'+ 
                          '<option  value="Demsa"> Demsa</option>'+      
                          '<option  value="Mintam">Mintam </option>'+ 
                          '<option  value="Kyeossi">Kyeossi</option>'+                          
                      '</select>');
                    break;
               case "Water":
                   $("#port").html('<label for="water">Select Seaport</label>'+
                      '<select id="water" name="water" class="form-control" >'+
                          '<option value="">Select </option>'+
                          '<option value="Tiko">Tiko </option>'+
                          '<option value="Limbe">Limbe</option>'+  
                          '<option value="Nkambe"> Nkambe </option>'+
                          '<option value="Ekondo Titi"> Ekondo Titi</option>'+
                       '</select>');
                    break;

                  }
              });
     
                    

    $(document).ready(function() {
      // Smart Wizard
      $('#wizard').smartWizard({
          transitionEffect: 'fade',
          includeFinishButton : true,
          labelFinish:'Finish',
          labelNext:'Next',
          labelPrevious:'Previous',
          //buttonOrder:['prev', 'next', 'finish'],
          reverseButtonsOrder: true,
          onLeaveStep: onLeaveStepFunction,
          onFinish: onFinishCallback
      });

      function onLeaveStepFunction(obj, context){
        console.log("Do you want to leave the step "+context.fromStep+"?");
        return validateSteps(context.fromStep);
      }


      function onFinishCallback(){
            var regForm=$('#additionalForm');
            var submit = true;
            /*if (!validator.checkAll(regForm)) {
              submit=false;
              return false;
            }*/
            
            if (submit)
              {
                  createNewMember(56);
              }

        }



      function validateSteps(step){
        var x=0;
        switch(step){
          /*case 1:
            var regForm=$('#regForm');
            if (!validator.checkAll(regForm)) {
              return false;
            }
            return true;
            break;
          case 2:
            //validate step 2
            var regForm=$('#regForm1');
            if (!validator.checkAll(regForm)) {
              return false;
            }
            return true;
            break;
          case 3:
            //validate step 2
            var regForm=$('#regForm2');
            if (!validator.checkAll(regForm)) {
              return false;
            }
            return true;
            break;
          case 4:
            //validate step 2
            var regForm=$('#regForm3');
            if (!validator.checkAll(regForm)) {
              return false;
            }
            return true;
            break;*/
          default:
            return true;
            break;
        }
      }
      
      //initialise countries combo
        $('.country').html('<option>Choose nation</option>'); 
        var opt = $(".country");
        $.getJSON("api/funcs.php?fn=fetchCountries", function(response) {
             $.each(response, function() {
             opt.append($("<option />").val(this.code).text(this.name));
         });
      });
      
       $('#arrivaldate').datepicker({
         changeMonth: true,
          changeYear: true,
          dateFormat: 'yy-mm-dd'

      });

        $('#depaturedate').datepicker({
         changeMonth: true,
          changeYear: true,
          dateFormat: 'yy-mm-dd'

      });
        $('#birthday').datepicker({
         changeMonth: true,
          changeYear: true,
          dateFormat: 'yy-mm-dd'

      });

    });

    var sresponse="Success";
    var fresponse="Something's wrong";
    //save member using event manager
    function createNewMember(eventId_){ //alert("now in");
          var country_=$("#country option:selected").val();
          var nationality_=$("#nationality option:selected").val();
          var gender_=$("#gender option:selected").val();
          var preferredAcc_=$("#preferredAcc option:selected").val();
          var room_=$("#room option:selected").val();
          var babySitting_=$("#needBabySitting option:selected").val();
          var transportMeans_=$("#meansoftransport option:selected").val();
          var transporttoKoume_=$("#transporToKoume option:selected").val();
          var accommodation_=$("#accommodation option:selected").val();
          var arrivaldate_=$("#arrivaldate").val();
          var depaturedate_=$("#depaturedate").val();
          var time=Date.now();
          var childrenNames_="";
          var dietRestrictions_=$("#dietRestrictions option:selected").val();
          var entryPort_=$("#pickupport option:selected").val();
          var dietRestrictionsSpec_="";
          var transDetails_="";
          var accDetails_="";
          if(regForm.children=="Yes"){
            childrenNames_=regForm.elements["childrennames"].value;
          }
          if(dietRestrictions_=="Yes"){
            dietRestrictionsSpec_=regForm.elements["specifydietRestrictions"].value;
          }
          if(regForm.transporToKoume=="Yes"){
            transDetails_=regForm.elements["transportDetails"].value;
          }
          if(regForm.accommodation=="Yes"){
            accDetails_=regForm.elements["accomodationDetails"].value;
          }
            function registerMember(id){
              $.post("api/api.php/records/cmfi_event_registration", //Required URL of the page on server
                      { // Data Sending With Request To Server
                        eventId:eventId_,
                        memberId:id,
                        children:regForm.children.value,
                        needBabySitting:babySitting_,
                        childrenNames:childrenNames_,
                        transportMode:transportMeans_,
                        entryPort:entryPort_,
                        arrivalDate:arrivaldate_,
                        departureDate:depaturedate_,
                        arrangedTransport:transporttoKoume_,
                        dietRestrictions:dietRestrictions_,
                        dietRestSpec:dietRestrictionsSpec_,
                        accommodation:accommodation_,
                        transDetails:transDetails_,
                        accDetails: accDetails_,
                        preferredAcc:preferredAcc_,
                        remark:'Web',
                        accType:room_
                      },
                      function(id,status){ 
                        console.log("event reg id: "+id);
                        if(id){successNote(sresponse,'you have been registered sussessfully!');
                          }else{
                            errorNote(fresponse,'registration failed try again!');
                          }
                      });
            }
            $.get("api/api.php/records/cmfi_members?filter=email,eq,"+regForm.email.value,
              function(data,status){
                var data2=data.records;
                var id=data2[0].id;
                console.log(id);
                if(id){
                  //this member is registered
                   registerMember(id);
                }else{
                      //not registered registered
                      $.post("api/api.php/records/cmfi_members", //Required URL of the page on server
                            { // Data Sending With Request To Server
                              firstName:regForm.firstname.value,
                              lastName:regForm.lastname.value,
                              email:regForm.email.value,
                              city:regForm.city.value,
                              phone:regForm.phone.value,
                              profession:regForm.profession.value,
                              sex:gender_,
                              country:country_,
                              language:regForm.language.value,
                              nationality:nationality_,
                              DOB:regForm.birthday.value
                            },
                            function(memberId,status){ 
                              console.log("member id: "+memberId);
                              //use the id to create event registration
                              registerMember(memberId);
                      });
                }

            });
          
        }



    function Lang(lang){
      var lang = sessionStorage.setItem("lang"); 
      switch (lang){
        case 'en':
          //assign form elements to english language

          break;
        case 'fr':
          //assign form elements to french

          break;


      }
    }

    //----------------------------------notifications------------------------------------------------//
    //success nofication
    function successNote(title_,text_){new PNotify({
          title: title_,
          text: text_,
          type: 'success'
        });
    }

    //failure or Error notifications
    function errorNote(title_,text_){new PNotify({
          title: title_,
          text: text_,
          type: 'error'
        });
    }

 