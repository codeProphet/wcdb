export default {
    data: () => ({
    }),
    template:`
     <div style="background-color: #fff">
      <footer>
        <div class="pull-right">
          <p>© 2018 - 2019  <a href="https://www.cmfionline.org">CMFI Online Team</a></p>
        </div>
        <div class="clearfix"></div>
      </footer>
    </div>
    `
}