<?php
/*
DSP Version: 1.0.0
*/


class Property 
{
	public $country = "";
	private $total=0;
	public $cflastyearTot = 0;
	public $adults=0;
	public $children;
	public $cfadults=0;
	public $cfchildren =0;
	public $houseChurches = 0;
	public $localities = 0;
	public $leaders = 0;
	
	
	function __construct($country_)
	{
		//Used for display only
		$this->country = $country_;	
		
	}
	
	public function getTotalMembers()
	{
		global $mysqli,$db_table_prefix;
		if $this->country=""
		{
			// Open members & Count
			$q1 = "SELECT * FROM `cmfi_members`";
			$r1 = mysqli_query($mysqli, $q1);
			$totMembers = mysqli_num_rows($r1);
			return $totMembers;
		}

	}
	
	
//Functions that interact with admin_properties table
//------------------------------------------------------------------------------

//Add new properties
	public function addProperty() 
	{
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."properties (
		Address,
		Bedrooms,
		SittingRooms,
		BathRooms,
		DateAdded,
		DistanceFromCampus,
		Town,
		Comments,
		img1,
		img2,
		img3,
		img4,
		img5,
		Status,
		costPM
		)
		VALUES (
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?
		)");
		/* Execute the statement */
		$stmt->bind_param("siiisissssssssi",$this->Address, $this->Bedrooms, $this->SittingRooms, $this->BathRooms, $this->DateAdded, $this->DistanceFromCampus, $this->Town, $this->Comments, $this->img1, $this->img2, $this->img3,$this->img4,$this->img5,$this->Status,$this->costPM);
		
		$stmt->execute();
		$stmt->close();
	$i++;
	return $i;
}


}



?>