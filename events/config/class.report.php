<?php
/*
WCDB Version: 1.0.0
*/


class report 
{
	public $members = "";
	private $localities="";
	public $houseChurches = "";
	public $user=0;
	public $leaders="";
	public $notes="";
	public $country="";
	public $province=0;
	public $date = "0000-00-00";
	
	
	function __construct($members_,$localities_,$houseChurches_,$leaders_,$notes_,$province_,$date_,$user_)
	{
		//assign property values
		$this->members=$members_;
		$this->localities=$localities_;
		$this->houseChurches=$houseChurches_;
		$this->leaders=$leaders_;
		$this->notes=$notes_;
		$this->province=$province_;
		$this->date = $date_;
		$this->user=$user_;
		
		
	}
	
//Functions that interact with members table
//------------------------------------------------------------------------------

//Add new member

	public function Add() {
	global $mysqli,$db_table_prefix; 
	$time=date("Y-m-d");
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."nationalreports (
		members,
		localities,
		houseChurches,
		leaders,
		user,
		date,
		province,
		notes,
		modifiedDate
		)
		VALUES (
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?
		)");
	$stmt->bind_param("iiiiisiss", 
						$this->members,
						$this->localities,
						$this->houseChurches,
						$this->leaders,
						$this->user,
						$this->date,
						$this->province,
						$this->notes,
						$time
						);
	
	if ($stmt->execute()) { 
		   $stmt->close();
		   return 1;
		} else {
			$stmt->close();
		   return 0;
		}
	
}




}



?>