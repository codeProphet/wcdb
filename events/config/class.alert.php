<?php
/*
WCDB Version: 1.0.0
*/


class alert 
{
	public $readStatus = 0;
	public $alertMsg="";
	public $alertDate = "0000-00-00 00:00:0000";
	public $alertFrom=0;
	public $priority="";
	public $id=1;
	
	
	
//Functions that interact with alerts table
//------------------------------------------------------------------------------

//Add new alert
	public function Add() {
	global $mysqli,$db_table_prefix; 
	$time=date("Y-m-d");
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."alerts (
		readStatus,
		userId,
		alertFrom,
		priority,
		alertDate,
		alertMsg,
		modifiedDate
		)
		VALUES (
		?,
		?,
		?,
		?,
		?,
		?,
		?		?
		)");
	$stmt->bind_param("sssssss", 
						$this->readStatus,
						$this->userId,
						$this->alertFrom,
						$this->priority,
						$this->alertDate,
						$this->alertMsg,
						$time
						);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;
}

//Update member details
public function Read()
	{
		global $mysqli,$db_table_prefix;
		$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."alerts 
		SET  
			readStatus = ?, 
			lastUpdated=?
			
		WHERE id=? 
		LIMIT 1");
		$stmt->bind_param('ssi', 
						$this->readStatus,
						$time,
						$this->id);
		$stmt->execute();
		$stmt->close();
		return 1;
	}
	
	
//Delete member 
public function Delete()
	{
		global $mysqli,$db_table_prefix;
		$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."alerts 
		WHERE eventId=? ");
		$stmt->bind_param('i', 	$this->id);
		$stmt->execute();
		$stmt->close();
		return 1;
	}
public function number()
	{
		global $mysqli,$db_table_prefix;
		
		If ($stmt = $mysqli->prepare("SELECT count(id) FROM ".$db_table_prefix."alerts 
		WHERE userId=? AND readStatus=0"))
		{
			$stmt->bind_param('i', 	$this->id);
			$stmt->execute();
			$stmt->bind_result($number);
			$stmt->fetch();
			$stmt->close();
			return ($number);
		} else {
			$error = $mysqli->errno . ' ' . $mysqli->error;
    		echo $error; // 1054 Unknown column 'foo' in 'field list'
			return ($error);
		}
	

		}

}



?>