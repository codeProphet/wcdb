<?php
/*
wcdb Version: 1.0.0
*/

class houseChurch{
	public $id=0;

	public function Delete()
	{
		global $mysqli,$db_table_prefix;
		$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."housechurches 
		WHERE id=? ");
		$stmt->bind_param('i', 	$this->id);
		$stmt->execute();
		$stmt->close();
		return 1;
	}


}


class newhouseChurch 
{
	public $country = "";
	public $familyId=0;
	public $locality = 0;
	public $datePlanted="0000-00-00";
	public $leaders="";
	public $createdBy=0;
	

	
	function __construct($familyId_,$datePlanted_,$leaders_,$locality_,$createdBy_)
	{
		//assign values
		$this->locality = $locality_;
		$this->familyId=$familyId_;
		$this->datePlanted=$datePlanted_;
		$this->leaders=$leaders_;
		$this->createdBy=$createdBy_;
		
	}
	
	
//Functions that interact with locality data
//------------------------------------------------------------------------------

//Add new properties
 function Add() 
	{
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$time=date("Y-m-d h:i:s");
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."housechurches (
		locality,
		familyid,
		leaderid,
		createdBy,
		datePlanted,
		modifiedDate
		
		)
		VALUES (
		?,
		?,
		?,
		?,
		?,
		?
		)");
		/* Execute the statement */
		$stmt->bind_param("iisiss",$this->locality, $this->familyId, $this->leaders,$this->createdBy, $this->datePlanted, $time);
		try{
			$stmt->execute();
			$stmt->close();
			$result=array('id'=>'1','text'=>'success');
		}catch(Exception $e){
			$result=array('id'=>'0','text'=>$e->getMessage());
		}	
		
	return $result;
}

function Update($id){
global $mysqli,$db_table_prefix; 
	$i = 0;
	$time=date("Y-m-d h:i:s");
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."housechurches 
		SET 
			locality=?,
			familyid=?,
			leaderid=?,
			createdBy=?,
			datePlanted=?,
			modifiedDate=?
		WHERE id=? 
		LIMIT 1");
		/* Execute the statement */
		$stmt->bind_param("iisissi",$this->locality, $this->familyId, $this->leaders,$this->createdBy, $this->datePlanted, $time, $id);
		try{
			$stmt->execute();
			$stmt->close();
			$result=array('id'=>'1','text'=>'Successfully updated House Church Details');
		}catch(Exception $e){
			$result=array('id'=>'0','text'=>$e->getMessage());
		}	
		
	return $result;

}


function Exists()
{
	global $mysqli,$db_table_prefix;
		$stmt = $mysqli->prepare("SELECT familyid FROM ".$db_table_prefix."housechurches 
		WHERE familyid=? ");
		$stmt->bind_param('i', $this->familyId);
		$stmt->execute();
		$stmt->store_result();
		$check = $stmt->num_rows;
		$stmt->close();
		if($check != 0){
			return 1;
		}else{
			return 0;
		}
}

}



?>