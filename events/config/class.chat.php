<?php
include("db-settings.php");

class chat {
	private $chatID, $chatUserID, $chatText;
	
	public function getChatID(){
		return $this->chatID;
	}
	
	public function setChatID($ChatID){
		$this->chatID=$ChatID;	
	}
	
	public function getChatUSerID(){
		return $this->chatUserID;	
	}
	
	public function setChatUSerID($ChatUSerID){
		$this->chatUserID=$ChatUSerID;	
	}
	
	public function getChatText(){
		return $this->chatText;
	}
	
	public function setChatText($ChatText){
		$this->chatText=$ChatText;	
	} 
	
	public function checkifRoomExists($friendID){
		global $mysqli,$db_table_prefix,$master_account;
		
		$stmt = $mysqli->prepare( "SELECT senderID,chatroom FROM ".$db_table_prefix."_chat
		WHERE senderID=?
		AND chatroom=?
		LIMIT 1
		" 
		);
		
		$roomFound = 0;
		$stmt->bind_param("ii", $loggedInUser->user_id, $friendID);
				$stmt->execute();
				$stmt->store_result();
				if ($stmt->num_rows > 0){
					$roomFound = 1;
				}
		 if ($roomFound == 1)
		{
			return true;
		}
		else
		{
			$stmt2 = $mysqli->prepare( "SELECT senderID,chatroom FROM ".$db_table_prefix."_chat
			WHERE senderID=?
			AND chatroom=?
			LIMIT 1
			" 
			);
			$stmt2->bind_param("ii", $friendID, $loggedInUser->user_id);
				$stmt2->execute();
				$stmt2->store_result();
				if ($stmt2->num_rows > 0){
					$roomFound = 1;
					return true;
				}
				else{
			return false;}	
		}
		$stmt->close();
		$stmt2->close();
	}
	
	public function AddChatRoom($friendID){
		global $mysqli,$db_table_prefix;
		
		$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."chat
			SET
			chatroom = ? 
			WHERE
			id = ?");
		$stmt->bind_param("si", $secure_pass, $loggedInUser->user_id);
		$stmt->execute();
		$stmt->close();	
		
	}
	
	
}

?>