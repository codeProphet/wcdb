<?php

function sendAssignEmail($subject,$email,$userFirst,$userLast,$author,$taskTitle,$startDate,$endDate,$description){
		    $mail = new questMail();
			$mail_failure=0;	
			global $mail_templates_dir;
			//echo $mail_templates_dir ;
				
			//Define more if you want to build larger structures
			$hooks = array(
				"searchStrs" => array("#USERFIRST#","#USERLAST#","#AUTHOR#","#TASKTITLE#","#STARTDATE#","#ENDDATE#","#DESCRIPTION#"),
				"subjectStrs" => array($userFirst,$userLast,$author,$taskTitle,$startDate,$endDate,$description)
					);
				
			if(!$mail->newTemplateMsg("assign_task.txt",$hooks))
				{
					$mail_failure = 1;
					
				}
				else
				{
					//echo "found template";
					//Send the mail. Specify users email here and subject. 
					//SendMail can have a third parementer for message if you do not wish to build a template.
					
					if(!$mail->sendMail($email,$subject))
					{
						$mail_failure = 1; 
						
					}
					
				}
				return $mail_failure;
	}
	
	?>