<?php
/*
DSP Version: 1.0.0
*/


class Room 
{

	public $propID = 0;
	private $roomNo=0;
	public $availableFrom;
	public $Costpm=0;
	public $status =NULL;
	public $rimg1 = NULL;
	public $rimg2 = NULL;
	public $rimg3 = NULL;
	

	
	function __construct($propID_, $roomNo_, $availableFrom_, $Costpm_, $status_, $Comments_, $rimg1_, $rimg2_, $rimg3_)
	{
		//Used for display only
		$this->propID = $propID_;
		
		//Sanitize
		$this->roomNo = $roomNo_;
		$this->availableFrom = $availableFrom_;
		$this->Costpm = $Costpm_;
		$this->status = $status_;
		$this->rimg1 = $rimg1_;
		$this->rimg2 = $rimg2_;
		$this->rimg3 = $rimg3_;
		
		
	}
	
	public function userCakeAddUser()
	{
		global $mysqli,$emailActivation,$websiteUrl,$db_table_prefix;
		
		//Prevent this function being called if there were construction errors
		if($this->status)
		{
			//Construct a secure hash for the plain text password
			$secure_pass = generateHash($this->clean_password);
			
			//Construct a unique activation token
			$this->activation_token = generateActivationToken();
			
			//Do we need to send out an activation email?
			if($emailActivation == "true")
			{
				//User must activate their account first
				$this->user_active = 0;
				
				$mail = new userCakeMail();
				
				//Build the activation message
				$activation_message = lang("ACCOUNT_ACTIVATION_MESSAGE",array($websiteUrl,$this->activation_token));
				
				//Define more if you want to build larger structures
				$hooks = array(
					"searchStrs" => array("#ACTIVATION-MESSAGE","#ACTIVATION-KEY","#USERNAME#"),
					"subjectStrs" => array($activation_message,$this->activation_token,$this->displayname)
					);
				
				/* Build the template - Optional, you can just use the sendMail function 
				Instead to pass a message. */
				
				if(!$mail->newTemplateMsg("new-registration.txt",$hooks))
				{
					$this->mail_failure = true;
				}
				else
				{
					//Send the mail. Specify users email here and subject. 
					//SendMail can have a third parementer for message if you do not wish to build a template.
					
					if(!$mail->sendMail($this->clean_email,"New User"))
					{
						$this->mail_failure = true;
					}
				}
				$this->success = lang("ACCOUNT_REGISTRATION_COMPLETE_TYPE2");
			}
			else
			{
				//Instant account activation
				$this->user_active = 1;
				$this->success = lang("ACCOUNT_REGISTRATION_COMPLETE_TYPE1");
			}	
			
			
			if(!$this->mail_failure)
			{
				//Insert the user into the database providing no errors have been found.
				$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."users (
					user_name,
					display_name,
					password,
					email,
					activation_token,
					last_activation_request,
					lost_password_request, 
					active,
					title,
					sign_up_stamp,
					last_sign_in_stamp
					)
					VALUES (
					?,
					?,
					?,
					?,
					?,
					'".time()."',
					'0',
					?,
					'New Member',
					'".time()."',
					'0'
					)");
				
				$stmt->bind_param("sssssi", $this->username, $this->displayname, $secure_pass, $this->clean_email, $this->activation_token, $this->user_active);
				$stmt->execute();
				$inserted_id = $mysqli->insert_id;
				$stmt->close();
				
				//Insert default permission into matches table
				$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."user_permission_matches  (
					user_id,
					permission_id
					)
					VALUES (
					?,
					'1'
					)");
				$stmt->bind_param("s", $inserted_id);
				$stmt->execute();
				$stmt->close();
			}
		}
	}
	
	
//Functions that interact with admin_properties table
//------------------------------------------------------------------------------


public function fetchAllRooms($propID=NULL)
{
	if($propID!=NULL) {
		$column = "propId";
		$data = $propID;
	}
	
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		propId,
		roomNo,
		availableFrom,
		Costpm,
		status,
		rimg1,
		rimg2,
		rimg3
		FROM ".$db_table_prefix."rooms
		WHERE 
		propId=?"
		) or die($mysqli->error);
		$stmt->bind_param("s", $propID);
		$stmt->execute();
	    $stmt->bind_result($propId, $roomNo, $availableFrom,$costpm, $status, $rimg1,$rimg2,$rimg3 );
	    while ($stmt->fetch()){
		$row[] = array('propId' => $propId, 'roomNo' => $roomNo, 'availableFrom' => $availableFrom,
		'Costpm' => $costpm, 'status' => $status, 'rimg1' => $rimg1 , 'rimg2' => $rimg2, 'rimg3' => $rimg3);
	    }
	$stmt->close();
	return ($row);
	//if(var_dump(isset($row))==true){return ($row);}
}




//Add new rooms
	public function addRoom() 
	{
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."rooms (
		propID,
		roomNo,
		availableFrom,
		Costpm,
		status,
		rimg1,
		rimg2,
		rimg3
		)
		VALUES (
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?
		)");
		/* Execute the statement */
		$stmt->bind_param("iisissss",$this->propID, $this->roomNo, $this->availableFrom, $this->Costpm, $this->status, $this->rimg1, $this->rimg2, $this->rimg3);
		
		$stmt->execute();
		$stmt->close();
	$i++;
	return $i;
}


}



?>