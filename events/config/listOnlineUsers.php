<style>

.badge {
  display:inline-block;
  position: relative;
  float:right;
  top: 25px;
  right: 3px;
  line-height: 16px;
  vertical-align:central;
  padding: 0 5px;
  font-family: Arial, sans-serif;
  color: white;
  text-shadow: 0 1px rgba(0, 0, 0, 0.25);
  border: 1px solid;
  border-radius: 10px;
  -webkit-box-shadow: inset 0 1px rgba(255, 255, 255, 0.3), 0 1px 1px rgba(0, 0, 0, 0.08);
  box-shadow: inset 0 1px rgba(255, 255, 255, 0.3), 0 1px 1px rgba(0, 0, 0, 0.08);
}
.badge.red{
  background: #fa623f;
  border-color: #fa5a35;
  background-image: -webkit-linear-gradient(top, #fc9f8a, #fa623f);
  background-image: -moz-linear-gradient(top, #fc9f8a, #fa623f);
  background-image: -o-linear-gradient(top, #fc9f8a, #fa623f);
  background-image: linear-gradient(to bottom, #fc9f8a, #fa623f);
}

</style>
<script src="js/chat.js" type="text/javascript"></script>

<?php 
require("config-small.php");
require("../chat/chat_functions.php");

		
		$query="SELECT gu.user_name,gu.image,gu.last_sign_in_stamp,gu.id,gu.facebook_profile,gu.twitter_profile, gu.linkedin_profile, gu.pinterest_profile FROM  gantt_users gu ORDER by gu.last_sign_in_stamp DESC";
		
		
		if ($result = $mysqli->query($query)) {
			
		

    /* fetch associative array */
    	foreach ($result as $user) {
			$twitter=$user['twitter_profile'];
			$facebook=$user['facebook_profile'];
			$linkedin=$user['linkedin_profile'];
			$pinterest=$user['pinterest_profile'];
        	
			echo '<form method="post" name="userForm">
			';		
			echo '<div class="userRow" onclick="showSelected('.$user['id'].')" ondblclick="removeSelection(this)" style="cursor:pointer;" >';
			echo '';
			$userImage=$user['image'];
			if(empty($userImage)){$userImage='user1.png';}
			echo " <div class='smcircular-image' style='float:left; width:15%;'><img src='images/users/".$userImage."' alt='' width='15px' />
			</div>";
			echo'<!--Show inactivity circle-->
			<div style="display:inline-block;">';
			$time=date('H:i:s',$user['last_sign_in_stamp']);
			if (checkUserOnline($user['id'])==1){echo '<img src="images/circle_green_medium.png" width="10px"></img>';}
									
			echo'</div>';
			echo '<div class="onlineuser" style="">';
			echo '&nbsp;'.$user['user_name']."<br />";
			echo '<span style="font-size:9px; font-style: italic; text-align: left;"> &nbsp;&nbsp;'.date('H:i:s',$user['last_sign_in_stamp']).'</span>';
			echo '</div>
			<!--show social network links-->
			 <span class="userSocial">';
             if(!empty($twitter)){echo'<a href="'.$twitter.'"><i class="fa fa-twitter"></i></a>&nbsp;';}
			if(!empty($facebook)){echo'<a href="'.$facebook.'"><i class="fa fa-facebook"></i></a>&nbsp;';}
			if(!empty($linkedin)){echo'<a href="'.$linkedin.'"><i class="fa fa-linkedin"></i></a>&nbsp;';}
			if(!empty($pinterest)){echo'<a href="'.$pinterest.'"><i class="fa fa-google-plus"></i></a>';}
           
            echo'<input type="button" id="userID" name="userID" value="'.$user['id'].'" style="display:none" /></span>';
			$badge=getPendingChats($user['id'],$loggedInUser->user_id);
			if($badge>0){echo '<span class="badge red" >'.$badge.'</span> '; } 
			echo '
			<div style="display:none; float:right; margin:10px 40px 0px 0px;" >
			
			<a onClick="showSelected('.$user['id'].')"  ><img src="images/next.png" height="50px"/></a>
			</div>
			
				
			</div></form>';
		}
		}
			
	 ?>