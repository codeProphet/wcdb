<?php
/*
WCDB Version: 1.0.0
*/


class region 
{
	public $code="";
	public $name="";
	public $leader=0;
	public $id=0;
	private $modifiedDate="0000-00-00";
	
	
	function __construct( $code_,$name_)
	{
		//Used for display only
		$this->code = $code_;
		$this->name = $name_;
		$this->modifiedDate=date("Y-m-d H:i:s");
		
	}
	
	public function Add()
	{
		global $mysqli,$emailActivation,$websiteUrl,$db_table_prefix;
		
		try{
			//Insert the user into the database providing no errors have been found.
			$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."regions (
				code,
				name
				)
				VALUES (
				?,
				?			
				)");
		
			$stmt->bind_param("ss", $this->code, $this->name);
			$stmt->execute();
			$stmt->close();
		   	return array("status"=>1,"msg"=>"Successfully added <strong>".$this->name."</strong> region");
		} catch (Exception $e){
			$stmt->close();
		   	return array("status"=>0,"msg"=>$e.getMessage());
		}
		
		
	}

	public function Delete(){
		global $mysqli,$db_table_prefix;
		$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."regions 
		WHERE id=? ");
		$stmt->bind_param('i', 	$this->id);
		$stmt->execute();
		$stmt->close();
		return 1;

	}
	
	public function Get()
	{
		global $mysqli,$emailActivation,$websiteUrl,$db_table_prefix;
		$stmt = $mysqli->prepare("SELECT 
				name,
				region,
				continent,
				CONCAT(firstName, ', ', LastName) as Leader, 
				phone, 
				email 
			FROM ".$db_table_prefix."nations 
			LEFT JOIN ".$db_table_prefix."countries 
			ON ".$db_table_prefix."nations.nationName=".$db_table_prefix."countries.code
			LEFT JOIN ".$db_table_prefix."members 
			ON ".$db_table_prefix."members.id=".$db_table_prefix."nations.nationLeader
		");
	$stmt->execute();
	$stmt->bind_result($name, $region,$continet,$leader,$phone,$email);
	
	while ($stmt->fetch()){
		$row[] = array('taskId' => $taskId, 'taskTitle' => $taskTitle, 'lastUpdated' => $LastUpdated);
	}
	$stmt->close();
	return ($row);
	
		
	}


	
}



?>