const api_base_url="app/api/api.php/records";

routie({
      '/register/:id': function(id) {
          $('#contentDiv').load('views/register.html').show();
          $('#btnRegister').hide();
      },
      '/events': function() {//show landing page
          $('#contentDiv').load('views/events.html').show();
      },
      '/rvc': function() {//show landing page
          $('#contentDiv').load('views/radio.html').show();
      },
      '/': function() {//show landing page
          $('#contentDiv').load('views/landing.html').show();
      },
       '/event/:eventId': function(eventId) {//show landing page
          $('#contentDiv').load('views/eventProfile.html').show();
          console.log(eventId);
      },
      '/admin': function() {//show landing page
        if(isAuth()){
          $('#contentDiv').load('views/admin.html').show();
          }else{
            errorNote("Insufficient privileges to access page","You do not have permission to access this page; please login as admin first!");
            $('#contentDiv').load('views/landing.html').show();
            sessionStorage.setItem("reqURL",window.location.href);
            logout();
          }
      },
      '/admin/main': function() {//show landing page
        if(isAuth()){
          $('#contentDiv').load('views/admin.html').show();
          setTimeout(function(){
            $('#admin_content').load('views/events_admin.html').show();
         }, 1000);
          
          }else{
            errorNote("Insufficient privileges to access page","You do not have permission to access this page; please login as admin first!");
            $('#contentDiv').load('views/landing.html').show();
            sessionStorage.setItem("reqURL",window.location.href);
            logout();
          }
      },
      '/admin/event/:eventId': function(eventId) {//show landing page
        if(isAuth()){
          $('#contentDiv').load('views/admin.html').show();
          setTimeout(function(){
            $('#admin_content').load('views/admin_dashboard.html').show();
          }, 1000);
        }else{
          errorNote("Insufficient privileges to access page","You do not have permission to access this page; please login as admin first!");
          $('#contentDiv').load('views/landing.html').show();
          sessionStorage.setItem("reqURL",window.location.href);
          logout();
        }
      },
      '/registration/:token': function() {//show landing page
        $('#contentDiv').load('views/registrationProfile.html').show();
      },
  });

function showPage(page,id){
  switch (page){
    case "registration":
        $('#admin_content').load('views/admin_registration.html').show();
      break;
    case "settings":
        $('#admin_content').load('views/admin_settings.html').show();
        break;
    case "event_dashboard":
        $('#admin_content').load('views/admin_dashboard.html').show();
        break;
    case "view_registrations":
        $('#admin_content').load('views/admin_view_registrations.html').show();
        break;
    case "id_cards":
        $('#admin_content').load('views/id_cards.html').show();
        break;
    case "attendanceDash":
      $('#admin_content').load('views/admin_attendance_dash.html').show();
      break;
    case "check-in":
      $('#admin_content').load('views/admin_attendance.html').show();
      break;
    case "users":
      $('#admin_content').load('views/users.html').show();
      break;
    case "userProfile":
      $('#admin_content').load('views/userProfile.php?id='+id).show();
      break;
  }
}

  var sresponse="Success";
  var fresponse="Something's wrong";
  var xp=3600;
  var tkx= new Date();
  tkx.setSeconds(new Date().getSeconds() + parseInt(this.xp))

   function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete?");
      if (x)
          return true;
      else
        return false;
    }

  //function to add days to a date
  Date.prototype.addDays = function(days) {
    this.setDate(this.getDate() + parseInt(days));
    return this;
  }

  //get filename from image src
  String.prototype.filename=function(extension){
    var s= this.replace(/\\/g, '/');
    s= s.substring(s.lastIndexOf('/')+ 1);
    return extension? s.replace(/[?#].+$/, ''): s.split('.')[0];
  }

  function monthName(monthNumber) {
      var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
      return month[monthNumber];
    }

  function getImage(img){
    var image='event-default.jpg';
    if(img!=null){
      image=img;
    }
    return image;
  }

  function show(div){
    var icon=div+"_icon";
    var x=document.getElementById(div);
    if(x.style.display == 'none'){
      x.style.display = 'block';
      changeIcon(icon);
    }
  }

  function showHide(div){
    var x=document.getElementById(div);
    if(x.style.display == 'block'){
      x.style.display = 'none';
    }else{
      x.style.display = 'block'; 
    }
  }

  function showHideForm(div){
    var icon=div+"_icon";
    changeIcon(icon);
    var x=document.getElementById(div);
    if(x.style.display == 'block'){
      x.style.display = 'none';
    }else{
      x.style.display = 'block'; 
    }
  }

function changeIcon(icon){
  var x = document.getElementById(icon);
  if(x){
    if (x.classList.contains("glyphicon-plus-sign")==true) {
        x.classList.remove("glyphicon-plus-sign");
        x.classList.remove("green");
        x.classList.add("glyphicon-remove-sign");
        x.classList.add("red");
    } else {  x.classList.remove("glyphicon-remove-sign");
      x.classList.remove("red");
        x.classList.add("glyphicon-plus-sign");
        x.classList.add("green");
        
    }
}
}

function getImageName(img_input) {
  var fullPath = document.getElementById(img_input).src;
  //var filename = fullPath.replace(/^.*[\\\/]/, '');
  // or, try this, 
  var filename = fullPath.split("/").pop();
  return filename;
}

function initialiseCountryCombo(){
  $('.countries').html(`<option value=0>Choose country</option>`); 
    var opt1 = $(".countries"); 
    $.getJSON(api_base_url+"/cmfi_countries?order=name,asc", function(response) {
         $.each(response.records, function() {
         opt1.append($("<option />").val(this.code).text(this.name));
     });
  });
}

function AppLogin(){
  var form=$("#loginForm");
  var d = new FormData();
  d.append("submit","Login");
  d.append("username",loginForm.username.value);
  d.append("password",loginForm.password.value);
  
  
    $.ajax({
      url: "controllers/login.php", // Url to which the request is send
      type: "POST",             
      data:d
      /*data: {
        username:loginForm.username.value,
        password:loginForm.password.value,
        submit:"Login"
      }*/, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
      contentType: false,       // The content type used when sending data to the server.
      cache: false,             
      processData:false,        
      success: function(response)   
      {
        var dt=$.parseJSON(response);
        var res=dt[0];
          console.log("data:",res.data);
          sessionStorage.setItem('data', JSON.stringify(res.data));//set user in session memory
          sessionStorage.setItem('expires',tkx);

          if(res.status=='success'){
            //login user
            showLoggedInMenu();
          }else{
            errorNote(res.status,res.message);
          }
          },
      error: function(data)
      {
        console.log("error: ",data.responseText);
        errorNote(fresponse,"Login error: "+data.responseText);
      }
      });
  
}

function logout(){
  //sessionStorage.clear();
  sessionStorage.removeItem("data");
  showLoggedOutMenu();
}
function showLoggedOutMenu(){
  $("#user").html(`<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
  <img src="images/user.png" alt="">User Login
  <!--span class=" fa fa-angle-down"></span-->
</a>
<div class="dropdown-menu dropdown-usermenu pull-right">
    <section class="" style="margin:10px;">

         <form id="loginForm" name="loginForm" action="" method='POST'>
               <div >
                <label class="input-group">
               <input id="email" type="email" name="username" value="" placeholder="Email" class="form-control" required="required" autocomplete="username" />
               <span for="username" class="form-control-feedback right">
                 <i class="fa fa-envelope"></i>
               </span>
             </label>
               </div>
               <div>
             <label class="input-group">
               <input id="password" type="password" name="password" placeholder="Password" class="form-control" required="required" autocomplete="current-password" />
               <label for="password" class="form-control-feedback right">
                 <i class="fa fa-unlock"></i>
               </label>
                   </label>
               </div>
               
               <div class="clearfix"></div>
               <div class="separator">
   
                 <!--p class="change_link">New to site?
                   <a href="#toregister" class="to_register"> Create Account </a>
                 </p-->
                 <div class="clearfix"></div>
                 <br />
                </form>
                 <div>				
                    <a onclick="AppLogin();" class="btn btn-default"  name="submit" value="Login">Log in</a>
                    <!--a class="reset_pass" href="forgot-password.php">Lost your password?</a-->
                  </div>
               </div>
             
             <!-- form -->
           </section> 
</div>`);
}

function showLoggedInMenu(){
  var data=$.parseJSON(sessionStorage.getItem("data"));
  var x="";
  //console.log("data:",data);
  var user=data.user;
  console.log("user :",user);
  if(user.isAdmin==1){
    x=`<li><a onclick="routie('/admin/main')"><i class="icon-wrench pull-right"></i> Admin</a></li>`;
  }
  $("#user").html(`<a id="loggedInUser" href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <img src="../app/images/members/${user.image}" alt=""><span class="username">${user.fullName}</span>
                <span class=" fa fa-angle-down"></span>
              </a>
              <ul class="dropdown-menu dropdown-usermenu pull-right">
                <li><a onclick="routie('/user/profile')"><i class="icon-user pull-right"></i>  Profile</a>
                </li>
                ${x}
                <li><a onclick="logout();"><i class="icon-logout pull-right"></i> Log Out</a>
                </li>
              </ul>`);
}

function isAuth(){
  if(sessionStorage.data){
            //check if token is valid
            if(validtoken()==false){
              return false;
            }else{
              return true;
            }
       
      }else{

        return false
      }

}

function validtoken(){
  if(sessionStorage.expires){
    var t= new Date();
    //console.log(t);
    if(moment(sessionStorage.expires)<=moment(t)){
      //destroy session and go to login
      return false;
    }else{
      return true;
    }
  }else{
    return false; 
  }
}


//save member image file
function saveImageFromFile(form){ 
  var imageForm=document.getElementById(form);
  var filepath=imageForm.image_file.value;
  var imageFile= filepath.split(/[\\\/]/).pop();
  var data_ = new FormData(imageForm);
   // data_.append('memberId',imageForm.id.value);
  sessionStorage.setItem("delegateImage",imageFile);
  //var imageFile= "user.png";  //research on how to get the filename from the input
  
  if(imageFile=='')
  {
  alert("Please choose image file first");
  }
  else{
  $.ajax({
    url: "controllers/delegatesImageUpload.php", // Url to which the request is send
    type: "POST",             // Type of request to be send, called as method
    data: data_,              // Data sent to server, a set of key/value pairs (i.e. form fields and values)
    contentType: false,       // The content type used when sending data to the server.
    cache: false,             // To unable request pages to be cached
    processData:false,        // To send DOMDocument or non processed data file it is set to false
    success: function(data)   // A function to be called if request succeeds
    {
      successNote(sresponse,'Image uploaded sussessfully!')
      $("#"+form+"_avatar").html('<img id="delegateImg" class="img-responsive avatar-view" src="../app/images/members/'+imageFile+'" alt="Avatar" title="Change the avatar" >');
    },
    error: function(data)
    {
      errorNote(data,'image not uploaded successfully!')
    }
    });
  }
  $("#uploadForm").hide(500);
}

//save member image file
function saveImageFromFile_existingDel(form){ 
  var imageForm=document.getElementById(form);
  var filepath=imageForm.image_file.value;
  var imageFile= filepath.split(/[\\\/]/).pop();
  var id=imageForm.memberId.value;console.log(id);
  var data_ = new FormData(imageForm);
  sessionStorage.setItem("delegateImage",imageFile);
  //var imageFile= "user.png";  //research on how to get the filename from the input
  
  if(imageFile=='')
  {
  alert("Please choose image file first");
  }
  else{
  $.ajax({
    url: "controllers/existingDelegatesImgUpload.php", // Url to which the request is send
    type: "POST",             // Type of request to be send, called as method
    data: data_,              // Data sent to server, a set of key/value pairs (i.e. form fields and values)
    contentType: false,       // The content type used when sending data to the server.
    cache: false,             // To unable request pages to be cached
    processData:false,        // To send DOMDocument or non processed data file it is set to false
    success: function(data)   // A function to be called if request succeeds
    {
      successNote(sresponse,'Image uploaded sussessfully!')
      $("#"+form+"_avatar").html('<img id="delegateImg" class="img-responsive avatar-view" src="../app/images/members/'+id+'.jpg" alt="Avatar" title="Change the avatar" >');
    },
    error: function(data)
    {
      errorNote(data,'image not uploaded successfully!')
    }
    });
  }
  $("#uploadForm").hide(500);
}

//Add New Member data to file	short form
function addShortMember(){ //alert("now in");
			var formName_='addMemberFormx';
			var locality_=2;
			var country_=$("#countryx option:selected").val();
			console.log(country_);
			var tags_="";
			var address_="";
			var surburb_="";
			var image_="user.png";
			var discipleMaker_=0;
      var houseChurch_=0;
      var data=$.parseJSON(sessionStorage.data);
      var eventId_=$("#eventId").val();
      var firstName_=addMemberFormx.firstName.value;
      var lastName_=addMemberFormx.lastName.value;
      var email_=addMemberFormx.email.value;
     
			//alert(locality_);
			var gender_='M';
			if($('#genderF').isCheck==true){ gender_='F';}
			if(country_=='')
			{
			alert("Please fill out the nation name");
			}
			else{
			$.ajax({
			url:api_base_url+"/cmfi_members", //Required URL of the page on server
			type:"POST",
			data: { // Data Sending With Request To Server
				firstName:firstName_,
				lastName:lastName_,
				email:email_,
				phone:addMemberFormx.phone.value,
				gender:gender_,
				surburb:surburb_,
				city:addMemberFormx.cityy.value,
				address:address_,
				country:addMemberFormx.countryx.value,
				houseChurch:houseChurch_,
				discipleMaker:discipleMaker_,
				image:image_,
				tags:tags_,
				locality:locality_,
				addedBy:data.user.user_id,
				familyID:0,
				familyRole:1,
				role:9
			},
			success: function(id){ // Required Callback Function
        successNote(sresponse,'New delegate saved successfully!');
        console.log(id);
				addMemberFormx.leaderId.value=id;
        //new member is now being registered to event
        $.post(api_base_url+"/cmfi_event_registration", //Required URL of the page on server
          { // Data Sending With Request To Server
            eventId:eventId_,
            memberId:id,
            remark:'Web',
          },
          function(id,status){ 
            console.log("event reg id: "+id);
            if(id){
              successNote(sresponse,'<p> Member has been registered sussessfully!</p>');
              //send email to registrant after successful registration
              if(email_!=""){
                $.get(api_base_url+"/cmfi_events?filter=eventId,eq,"+eventId_,
                function(data){
                  var event=data.records[0];
                          $.post("../app/config/sendEmail.php",
                          {
                              firstname:firstName_,
                              lastname:lastName_,
                              email:email_,
                              eventTitle:event.eventTitle,
                              eventVenue:event.eventVenue,
                              startDate:event.startDate,
                              endDate:event.endDate
                          },

                      function(response){ // Required Callback Function
                        
                        var res=response;
                        console.log(res);
                      
                        if(res.id==1){successNote(sresponse,res.text);
                        }else{
                          errorNote(fresponse,res.text);
                        }
                        })
                      });
                  }
                    $('#eventsRegTable').DataTable().ajax.reload();
                  }else{
                    errorNote(fresponse,'registration failed try again!');
                  }
          });

			},
			error: function(err){
				errorNote(fresponse,err.responseJSON.message);
			}
			
			});
			
			}
			
			
}

function resendRegEmail(token,eventId,memberId){
  //get event record
  $.get(api_base_url+"/cmfi_events?filter=eventId,eq,"+eventId,
                function(data){
                var event=data.records[0];
              
                $.get(api_base_url+"/cmfi_members?filter=id,eq,"+memberId,
                  function(data){
                  var member=data.records[0];
                      //send email
                      $.ajax({
                        url:"../app/config/sendEmail.php",
                        type:"POST",
                        data:{
                          firstname:member.firstName,
                          lastname:member.lastName,
                          email:member.email,
                          eventTitle:event.eventTitle,
                          eventVenue:event.eventVenue,
                          startDate:event.startDate,
                          endDate:event.endDate,
                          token:token
                        },
                        success: function(data){
                          console.log("email sent successfully");
                        },
                        error: function(err){
                          errorNote(fresponse,err.responseText);
                        }
                      });

              });
          });
}

function registerMember(id,eventId_){
          var preferredAcc_=$("#preferredAcc option:selected").val();
          var room_=$("#room option:selected").val();
          var babySitting_=$("#needBabySitting option:selected").val();
          var transportMeans_=$("#meansoftransport option:selected").val();
          var transporttoKoume_=$("#transporToKoume option:selected").val();
          var accommodation_=$("#accommodation option:selected").val();
          var arrivaldate_=$("#arrivaldate").val();
          var depaturedate_=$("#depaturedate").val();
          var healthQn_=$("#healthQn option:selected").val();
          var time=Date.now();
          var childrenNames_="";
          var pickUpEntry_=$("#pickupport option:selected").val();
          var dietRestrictions_=$("#dietRestrictions option:selected").val();
          var dietRestrictionsSpec_="";
          var transDetails_=$('#transportDetails').val();
          var accDetails_="";
          var healthConditionSpec_="";
          var email_=regForm.email.value;
          var firstname_=regForm.firstname.value;
          var lastname_=regForm.lastname.value;
          var entryPort_=$("select.port option:selected").val();
          var token_=Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
         
          var medfile="";
          var medForm=document.getElementById('regForm2');
          //var imgVal = $('#medicalform').val();
          /*if(imgVal!=''){
            var filepath=medForm.medicalform.value;
            medfile= filepath.split(/[\\\/]/).pop();
          }*/
          

          if(regForm1.children=="1"){
            childrenNames_=regForm1.elements["childrennames"].value;
          }
          if(dietRestrictions_=="1"){
            dietRestrictionsSpec_=$("#dietRestrictionSpecification").val();
          }
          if(regForm1.transporToKoume=="1"){
            transDetails_=regForm1.elements["transportDetails"].value;
          }
          if(regForm3.accommodation=="1"){
            accDetails_=regForm3.elements["accommodationDetails"].value;
          }
          if(healthQn_=="1"){
            healthConditionSpec_=regForm2.healthConditionSpec.value;
          }
          if(preferredAcc_==""){preferredAcc_=0;}
          if(dietRestrictions_==""){dietRestrictions_=0;}
          if(transporttoKoume_==""){transporttoKoume_=0;}
          if(babySitting_==""){babySitting_=0;}
          if(pickUpEntry_==""){pickUpEntry_=0;}

          
  //check if delegate is already registered
  $.get(api_base_url+"/cmfi_event_registration?filter=eventId,eq,"+eventId_+"&filter=memberId,eq,"+id,function(data){
    if(data.records.length!=0){
      console.log(data);
      var token=data.records[0].token;
      //delegate exists
      new PNotify({
        title: 'Registration Previously done',
        text: `Would you like to update your profile for this event instead?  \n
        Use the button/link in the email you received after your initial registration to update your details\n
        If you have not received a registration alert email please click the button below \n\n
        <a onclick="resendRegEmail('${token}',${eventId_},${id})" class="btn btn-sm btn-default" >Resend Email</a> `,
        type: 'notice'
      });
    }else{
      //delegate does not exist .. proceed to register
      //uploadMedicalForm(id);
      $.ajax({
          url:api_base_url+"/cmfi_event_registration", //Required URL of the page on server
          type:"POST",
          data:{ // Data Sending With Request To Server
            eventId:eventId_,
            memberId:id,
            children:regForm.children.value,
            needBabySitting:babySitting_,
            childrenNames:childrenNames_,
            transportMode:transportMeans_,
            entryPort:entryPort_,
            pickUpEntry:pickUpEntry_,
            arrivalDate:arrivaldate_,
            departureDate:depaturedate_,
            arrangedTransport:transporttoKoume_,
            dietRestrictions:dietRestrictions_,
            dietRestSpec:dietRestrictionsSpec_,
            accommodation:accommodation_,
            transDetails:transDetails_,
            accDetails: accDetails_,
            preferredAcc:preferredAcc_,
            healthCond:healthConditionSpec_,
            //medicalForm:medfile,
            remark:'Web',
            accType:room_,
            token:token_
          },
          success: function(id,status){ 
            console.log(">>successfully registered to event with registration id: "+id);
            if(id){
              successNote(sresponse,'<p> you have been registered sussessfully! \n\nPlease check your email for more information on how to update your registration details.</p>');
                  //send email to registrant after successful registration
              $.get(api_base_url+"/cmfi_events?filter=eventId,eq,"+eventId_,
                function(data){
                var event=data.records[0];
              
                      $.ajax({
                        url:"../app/config/sendEmail.php",
                        type:"POST",
                        data:{
                          firstname:firstname_,
                          lastname:lastname_,
                          email:email_,
                          eventTitle:event.eventTitle,
                          eventVenue:event.eventVenue,
                          startDate:event.startDate,
                          endDate:event.endDate,
                          token:token_
                        },
                        success: function(data){
                          console.log("email sent successfully");
                        },
                        error: function(err){
                          errorNote(fresponse,err.responseText);
                        }
                      });

                    
                });

              }
          },
          error: function(err){
            errorNote(fresponse,'registration failed try again! '+err.responseText);
          }
        });
    }
  })
  setTimeout(function(){
    routie('/event/'+eventId_);
 }, 2500);
}

function updateMember(id){
  var country_=$("#country option:selected").val();
  var nationality_=$("#nationality option:selected").val();
  var gender_=$("#gender option:selected").val();
  var img=sessionStorage.getItem("delegateImage"); 

  var passport_="";
  if ($("#invitation option:selected").val()=="1"){
    passport_=$("#passportNo").val();
  }
 
  var img=getImageName('delegateImg');
  console.log(">>image File at save:",img);

      var data={ // Data Sending With Request To Server
                        firstName:regForm.firstname.value,
                        lastName:regForm.lastname.value,
                        email:regForm.email.value,
                        city:regForm.city.value,
                        phone:regForm.phone.value,
                        profession:regForm.profession.value,
                        sex:gender_,
                        country:country_,
                        language:regForm.language.value,
                        nationality:nationality_,
                        passport:passport_,
                        DOB:regForm.birthday.value,
                        image:img
                      }
      $.ajax({
        type:'PUT',
        url:api_base_url+"/cmfi_members/"+id, 
        contentType:'application/json',
        data:JSON.stringify(data),
      }).done(function(){
        console.log('updated member details successfully');
      }).fail(function(msg){
        console.log('Failed to save member details: '+msg)
      });
        
}

function uploadMedicalForm(id){
      var data_ = new FormData();
        var medform=document.getElementById('medicalform');
        var file=medform.files[0];
        data_.append('medicalform', file);
        data_.append('memberId',id);
        console.log(data_);
    /*if(medFile=='')
      {
      alert("Please select medical form file first");
      }
      else{*/
      $.ajax({
        url: "controllers/fileUpload.php", // Url to which the request is send
        type: "POST",             // Type of request to be send, called as method
        data: data_, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
        contentType: false,       // The content type used when sending data to the server.
        cache: false,             // To unable request pages to be cached
        processData:false,        // To send DOMDocument or non processed data file it is set to false
        success: function(data)   // A function to be called if request succeeds
        {
          //successNote(sresponse,'medical form uploaded sussessfully! ')
          console.log("medical form uploaded successfully!");
          },
        error: function(data)
        {
          errorNote(fresponse,'medical form not uploaded successfully!')
        }
        });
 // }
}

  //save member using event manager
    function createNewMember(eventId_){ //alert("now in");
          var country_=$("#country option:selected").val();
          var nationality_=$("#nationality option:selected").val();
          var gender_=$("#gender option:selected").val();
          var firstname_=regForm.firstname.value;
          var lastname_=regForm.lastname.value;
          var passport_="";
          if ($("#invitation option:selected").val()=="1"){
            passport_=$("#passportNo").val();
          }

          //checking if the delegate already exists in the database
          $.get(api_base_url+"/cmfi_members?filter=firstName,eq,"+firstname_+"&filter=lastName,eq,"+lastname_,
              function(data){
                var data2=data.records;
                if(data2 && data2.length){
                  var id=data2[0].id;
                  //this member is already in the database
                    updateMember(id);
                    registerMember(id,eventId_);

                }else{
                      //not registered
                      var img=getImageName('delegateImg');
                      console.log(">>image File at save:",img);

                      $.ajax({
                        url: api_base_url+"/cmfi_members", //Required URL of the page on server
                        type:"POST",
                        data: { // Data Sending With Request To Server
                              firstName:regForm.firstname.value,
                              lastName:regForm.lastname.value,
                              email:regForm.email.value,
                              city:regForm.city.value,
                              phone:regForm.phone.value,
                              profession:regForm.profession.value,
                              sex:gender_,
                              country:country_,
                              language:regForm.language.value,
                              nationality:nationality_,
                              DOB:regForm.birthday.value,
                              passport:passport_,
                              image:img
                            },
                        success: function(data){
                              console.log("member id: "+data);
                              //use the id to create event registration
                              registerMember(data,eventId_);
                        },
                        error: function(err){
                            console.log(err.responseText);
                            errorNote("Oops something's wrong!",err.responseText);
                        }
                              
                      });
                }

            });
          
        }
 function uploadEventImage(){
              var imageForm=document.getElementById('uploadimage');
              var filepath=imageForm.image_file.value;
			        var imageFile= filepath.split(/[\\\/]/).pop();
   
            if(imageFile=='')
            {
            alert("Please choose image file first");
            }
            else{
              $.ajax({
                url: "controllers/eventImageUpload.php", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: new FormData(imageForm), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: function(data)   // A function to be called if request succeeds
                {
                  //successNote(sresponse,'event image uploaded sussessfully! ')
                  var res=$.parseJSON(data);
                  console.log(res);
                  $("#profileAvatar").html(`<img onclick="showHide('uploadForm')" class="img-responsive avatar-view" src="images/events/${imageFile}" alt="Avatar" title="Change the avatar" >`);
                   },
                error: function(err)
                {
                  console.log(err);
                  errorNote(fresponse,'event image did not upload successfully!')
                }
                });
                $("#uploadForm").hide(500);
              }
          }

function updateEvent(id){
              var start_=addEventForm.reservation.value.substring(0,10);
              var end_=addEventForm.reservation.value.substring(12,10);

              var data={
                  eventTitle:addEventForm.eventTitle.value,
                  eventDesc:addEventForm.eventDesc.value,
                  eventVenue:addEventForm.eventVenue.value,
                  eventColor:addEventForm.eventColor.value,
                  start:start_,
                  end:end_,
                  nation:addEventForm.eventRegion.value,
                  regFee:addEventForm.regFee.value,
                  criteria:addEventForm.criteria.value,
                  eventLeader:addEventForm.eventLeader.value
              }

              $.ajax({
                url:api_base_url+"/cmfi_events/"+id,
                type:"PUT",
                contentType:'application/json',
                data:JSON.stringify(data),
              })
              .done(function(){
                        console.log('saved event details successfully');
                        successNote(sresponse,"Event details updated successfully!");
                        //uploadEventImage(id);
              })
              .fail(function(msg){
                console.log('Failed to save event details: '+msg.responseText)
                console.log(msg);
              });


}

//register event from form
function eventRegister(){
	var formName_="eventRegForm"; 
			$("#loaderImg").show(); //show loader image
			var members_=$("#membersDropDown").val();
			var id_=$("#eventId").val();
			//alert(id_);
			if(members_=='')
			{
			alert("Please select members ");
			}
			else{
			$.post("../app/api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				formName:formName_,
				members:members_,
				id:id_
			},
			function(data,status){ // Required Callback Function
			var obj= $.parseJSON(data);
				console.log(obj['msg']);
			if(obj['status']==1){successNote(sresponse,obj['msg']);
			}else{
				errorNote(fresponse,obj['msg']+" Event Registration failed!");
			}
        $('#eventsRegTable').DataTable().ajax.reload();
        $("#membersDropDown").select2("val", "");
				$("#loaderImg").hide(); //hide loader image
			});
			
			}

}
 

//register a member to an event automatically
function eventRegisterAuto(memberId){
	var formName_="eventRegFormAuto"; 
			$("#loaderImg").show(); //show loader image
			var member_=memberId;
      var id_=$("#eventId").val();
      var token_=Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

			if(id_>0 && id_!=null){
				//alert(id_);
				if(member_=='')
				{
				alert("member Id not specified ");
				}
				else{
				$.post("../app/api/post.php", //Required URL of the page on server
				{ // Data Sending With Request To Server
					formName:formName_,
					member:member_,
          id:id_,
          token:token_
				},
				function(data,status){ // Required Callback Function
				var obj= $.parseJSON(data);
					console.log(obj['msg']);
				if(obj['status']==1){successNote(sresponse,obj['msg']);
				}else{
					   $.get(api_base_url+"/cmfi_events?filter=eventId,eq,"+eventId_,
                    function(data){
                      var event=data.records[0];
                              $.post("../app/config/sendEmail.php",
                              {
                                  firstname:firstname_,
                                  lastname:lastname_,
                                  email:email_,
                                  eventTitle:event.eventTitle,
                                  eventVenue:event.eventVenue,
                                  startDate:event.startDate,
                                  endDate:event.endDate,
                                  token:token_
                              },

                           function(response){ // Required Callback Function
                            
                             var res=response;
                             console.log(res);
                           
                            if(res.id==1){successNote(sresponse,res.text);
                            }else{
                              errorNote(fresponse,res.text);
                            }
                            })
                          });
          (fresponse,obj['msg']+" Event Registration failed!");
				}
					$('#eventsRegTable').DataTable().ajax.reload();
          $("#membersDropDown").select2("val", "");
					$("#loaderImg").hide(); //hide loader image
				});
				
			}
		}

}

function eventDeregister(id){
          (new PNotify({
            title: 'Confirmation Needed',
            text: 'Are you sure you want to delete this registration?',
            icon: 'glyphicon glyphicon-question-sign',
            hide: false,
            confirm: {
              confirm: true
            },
            buttons: {
              closer: false,
              sticker: false
            },
            history: {
              history: false
            },
            addclass: 'stack-modal',
            stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
        })).get().on('pnotify.confirm', function(){
              $.ajax({
              url:'../app/api/api.php/records/cmfi_event_registration/'+id,
              type:'DELETE',
              success: function(response,status){
                  successNote(sresponse,'Delegate deregistered sussessfully!');
                  $('#eventsRegTable').DataTable().ajax.reload(); //refresh registrations table 
                },
              error: function(err){
                errorNote(fresponse,'Deregistration not successful!'+err.responseJSON);
              }
              });
        });
}

function saveEvent(id) {
              var title =$('#eventTitle').val();
              var eventDesc=$('#eventDesc').val();
              var start=addEventForm.reservation.value.substring(0,10);
              var end=addEventForm.reservation.value.substring(12,10);
              var eventColor=$('#eventColor').val();
              var eventId=$('#eventId').val(); //alert(eventId);
              var nation= $('#eventRegion option:selected').val();
              var regFee=$('#regFee').val();
              var eventVenue=$('#eventVenue').val();
              var image=$('#image').val();
              var criteria=$('#criteria').val();
              var coodinator=$('#eventLeader').val();
              uploadEventImage(id);

              
              $.ajax({
                url: '../app/api/post.php',
                data: 'type=fullUpdate&title='+title+'&eventId='+eventId+'&start='+start+'&end='+end+'&eventColor='+eventColor+'&eventDesc='+eventDesc+'&nation='+nation+'&eventVenue='+eventVenue+'&criteria='+criteria+'&regFee='+regFee+'&image='+image+'&eventLeader='+eventLeader,
                type: 'POST',
                dataType: 'json',
                success: function(response)
                {successNote(sresponse,'event saved sussessfully!')
                  //if(response.status == 'success')
                  //$('#calendar').fullCalendar('refetchEvents');
                  //initialise event form
                  uploadEventImage(id);
                  $("#addEventForm")[0].reset(); //clear form
                  $('#globalEventsTable').DataTable().ajax.reload(); //refresh events table
                },
                error: function(e){
                  errorNote('Error processing your request: '+e.responseText);
                }
              });
              

  }

  function eventEdit(id){
          var form="'addLocalityForm'";
          var container="'localityAddForm'";
          $.getJSON(api_base_url+"/cmfi_events?filter=eventId,eq,"+id, function(data) {
              var res=data.records[0];
              var options={
                startDate: res.startDate.substring(0,10),
                endDate: res.endDate.substring(0,10),
                minDate: '2010-01-01',
                maxDate: '2065-12-31',
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                format: 'YYYY-MM-DD',
              }
               $('#reservation').daterangepicker(options);
                addEventForm.eventTitle.value=res.eventTitle;
                addEventForm.eventDesc.value=res.eventDesc;
                addEventForm.reservation.value=res.startDate.substring(0,10)+' - '+res.endDate.substring(0,10);
                addEventForm.eventColor.value=res.eventColor;
                addEventForm.eventLeader.value=res.eventLeader;
                addEventForm.eventVenue.value=res.eventVenue;
                addEventForm.criteria.value=res.criteria;
                addEventForm.regFee.value=res.regFee;
                $('#eventColor').trigger('change');
                addEventForm.eventRegion.value=res.nation;
                $('#eventRegion').trigger('change');
                $('#event-id').html(`<input type="hidden" name="eventId" id="eventId" value="${id}" >`)
                $('#eventTitle').html('<h2>Edit Event <small>select leader</small></h2><div class="clearfix"></div>');
                $('#eventSubmit').html('<button  onclick="cancel('+form+','+container+')" type="submit" class="btn btn-default" name="submit" ><i class="icon-remove"></i> Cancel</button><button  onclick="updateEvent('+id+')" type="submit" class="btn btn-success" name="submit" ><i class="icon-check"></i>  Save</button>');
                $('#profileAvatar').html(`<img  onclick="showHide('uploadForm1')" class="img-responsive avatar-view" src="images/events/${res.image}" alt="Avatar" width="100%"
                                              onerror="if (this.src != 'images/events/${res.image}') this.src = 'images/events/event-default.jpg';">`)
                $('#evTitle').html('<h2>Edit Event <small>global event update</small></h2>');
                //alert(response.name);
            });

  }


  function deleteEvent(id,title){// alert('now in');
      (new PNotify({
                    title: 'Confirmation Needed',
                    text: 'Are you sure you want to delete this event?',
                    icon: 'glyphicon glyphicon-question-sign',
                    hide: false,
                    confirm: {
                      confirm: true
                    },
                    buttons: {
                      closer: false,
                      sticker: false
                    },
                    history: {
                      history: false
                    },
                    addclass: 'stack-modal',
                    stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
                })).get().on('pnotify.confirm', function(){
        
            $.ajax({
              url:'../app/api/api.php/records/cmfi_events/'+id,
              type:'DELETE',
              success: function(response,status){
              if(response==1){successNote(sresponse,'Event deleted sussessfully!')
              }else{
                errorNote(fresponse,'Event not deleted!')
              }

              $('#globalEventsTable').DataTable().ajax.reload(); //refresh provinces table
                  
                }
              });
          
        });
      }

  function save(){
        var regForm=document.getElementById("regForm");
        var regForm1=document.getElementById('regForm1');
        var regForm2=document.getElementById('regForm2');
        var id=regForm.memberId.value;
        var eventId=getid();
        var country_=$("#country option:selected").val();
        var nationality_=$("#nationality option:selected").val();
        var gender_=$("#gender option:selected").val();
        //save member details
        $.ajax({
            url: api_base_url+"/cmfi_members/"+id,
            type: "PUT",
            data:{
              firstName:regForm.firstname.value,
              lastName:regForm.lastname.value,
              email:regForm.email.value,
              city:regForm.city.value,
              phone:regForm.phone.value,
              profession:regForm.profession.value,
              sex:gender_,
              country:country_,
              language:regForm.language.value,
              nationality:nationality_,
              DOB:regForm.birthday.value
            },
            success: function(res){
              console.log('saved member details successfully');
            },
            error: function(err){
                console.log("error:",err);
            }
        });

        //user details
        var data=$.parseJSON(sessionStorage.data);
        var userId=data.user.user_id;

        //save event personal details


        var children_=$("#children option:selected").val();
        var childrenNo_=$("#howmany option:selected").val();
        var babySittingArr_=$("#babysittingarrangements option:selected").val();

        var meansOfTransport_=$("#meansoftransport option:selected").val();
        var entryPort_="";

        var preferredAcc_=$("#preferredAcc option:selected").val();
        var room_=$("#room option:selected").val();
        var babySitting_=$("#needBabySitting option:selected").val();
        var transportMeans_=$("#meansoftransport option:selected").val();
        var transporttoKoume_=$("#transporToKoume option:selected").val();
        var accommodation_=$("#accommodation option:selected").val();
        var arrivaldate_=$("#arrivaldate").val();
        var depaturedate_=$("#depaturedate").val();
        var time=Date.now();
        var childrenNames_="";
        var pickUpEntry_=$("#pickupport option:selected").val();
        var dietRestrictions_=$("#dietRestrictions option:selected").val();
        var dietRestrictionsSpec_="";
        var transDetails_="";
        var accDetails_="";
        

        if(regForm.children=="Yes"){
          childrenNames_=regForm.elements["childrennames"].value;
        }
        if(dietRestrictions_=="Yes"){
          dietRestrictionsSpec_=$("#dietRestrictionSpecification").val();
        }
        if(regForm.transporToKoume=="Yes"){
          transDetails_=regForm.elements["transportDetails"].value;
        }
        if(regForm3.accommodation=="Yes"){
          accDetails_=regForm.elements["accommodationDetails"].value;
        }

        if(meansOfTransport_==="air"){
          entryPort_=$("#air option:selected").val();
        }else if(meansOfTransport_==="road"){
          entryPort_=$("#road option:selected").val();
        }else if(meansOfTransport_==="water"){
          entryPort_=$("#water option:selected").val();
        }

        $.ajax({
            url: api_base_url+"/cmfi_event_registration/"+sessionStorage.getItem("regId"),
            type: "PUT",
            data:{
              children:regForm.children.value,
              needBabySitting:babySitting_,
              childrenNames:childrenNames_,
              transportMode:transportMeans_,
              entryPort:entryPort_,
              pickUpEntry:pickUpEntry_,
              arrivalDate:arrivaldate_,
              departureDate:depaturedate_,
              arrangedTransport:transporttoKoume_,
              dietRestrictions:dietRestrictions_,
              dietRestSpec:dietRestrictionsSpec_,
              accommodation:accommodation_,
              transDetails:transDetails_,
              accDetails: accDetails_,
              preferredAcc:preferredAcc_,
              remark:'Admin Update',
              accType:room_,
              registeredBy:userId
            },
            success: function(res){
              successNote(sresponse,'saved member details successfully!');
            },
            error: function(err){
                errorNote("Oops!",err.responseText);
            }
        });
}


function showProfile(token){
  $.getJSON(api_base_url+"/cmfi_event_registration?filter=token,eq,"+token,function(response){
       var data=response.records[0];
                console.log(data);
                sessionStorage.setItem("regId",data.id);
                $('select#meansoftransport').val(data.transportMode);
                $('select#meansoftransport').trigger('change');
                if(data.transportMode=="Air"){
                  $('select#air').val(data.entryPort);
                  $('select#air').trigger('change');
                }else if(data.transportMode=="Road"){
                  $('select#road').val(data.entryPort);
                  $('select#road').trigger('change');
                }else{
                  $('select#water').val(data.entryPort);
                  $('select#water').trigger('change');
                }
                regForm1.arrivaldate.value=data.arrivalDate;
                regForm1.depaturedate.value=data.departureDate;
                $('select#pickupport').val(data.pickUpEntry);
                $('select#pickupport').trigger('change');
                $('select#children').val(data.children);
                $('select#children').trigger('change');
                $('select#transporToKoume').val(data.transKoume);
                $('select#transporToKoume').trigger('change');
                $('select#dietRestrictions').val(data.dietRestrictions);
                $('select#dietRestrictions').trigger('change');
                if(regForm1.transporToKoume=="1"){
                  regForm1.transportDetails.value=data.transDetails;
                }
  
            var form="uploadForm";
            var temp = new Array();
            var id=data.memberId;
            uploadimage.memberId.value=id; //set memberId in form
      
          $.ajax({
            url:api_base_url+"/cmfi_members?join=cmfi_countries&filter=id,eq,"+id,
            type:"GET",
            success: function(res) {
            var d=res.records[0];
                    $( '#memberName').html(d.firstName+' '+d.lastName);
                    $('#memberEmail').html(d.email);
                    $('#memberPhone').html(d.phone);
                    if(d.sex==="M"){$('#memberGender').html("<i class='icon-symbol-male'></i> Male");}
                    else{$('#memberGender').html("<i class='icon-symbol-female'></i> Female");}
                    //$('#memberGender').html(response.sex);
                    $('#memberAddress').html(d.address+', '+d.surburb+', '+d.city);
                    $('#memberCountry').html(d.country.name);
                    $('#photo').html(`<img onclick="showHide('+form+')" class="img-responsive avatar-view" src="../app/images/members/${d.image}"  alt="Avatar" 
                    onerror="if (this.src != '../app/images/members/user.png') this.src = '../app/images/members/user.png';"/>`);
                    $('#uploadimage_avatar').html(`<img id="delegateImg" name="delegateImg"  onclick="showHide('uploadForm')"  class="img-responsive avatar-view" src="../app/images/members/${d.image}"  alt="Avatar" 
                    onerror="if (this.src != '../app/images/members/user.png') this.src = '../app/images/members/user.png';"/>`);
                    
                    $("#tags_container").html('');
                    $("#tags_container").html('<input id="tags" type="text" class="tags form-control" value="" />');
                    var name=d.firstName+' '+d.lastName;
      
                    JsBarcode("#barcode", name, {
                      displayValue:false,
                      //fontSize:24,
                      lineColor: "#0cc"
                    });
                    //alert(response.name);

                    //regForm.country.value=data.country;
                    $( 'select#country' ).val( d.country.code);
                    $('select#country').trigger('change');
                    regForm.firstname.value=d.firstName;
                    regForm.lastname.value=d.lastName;
                    regForm.email.value=d.email;
                    regForm.city.value=d.city;
                    regForm.memberId.value=d.id;
                    regForm.phone.value=d.phone;
                    regForm.profession.value=d.profession;
                    $( 'select#gender' ).val( d.sex);
                    $('select#gender').trigger('change');
                    $( 'select#nationality' ).val( d.nationality);
                    $('select#nationality').trigger('change');
                    regForm.language.value=d.language;
                    regForm.nationality.value=d.nationality;
                    regForm.birthday.value=d.DOB;
                    regForm1.passport=d.passport;
                    $('#tags').val(d.tags);
            
                    $('#tags').tagsInput({
                          width: 'auto'
                        });
      
                },
                error: function(err){
                  console.log(err.responseText);
                }
              });
      
  });
  
      
            
  }
      function Lang(lang){
        var lang = sessionStorage.setItem("lang"); 
        switch (lang){
          case 'en':
            //assign form elements to english language

            break;
          case 'fr':
            //assign form elements to french

            break;


        }
      }

//----------------------------------- USER FUNCTIONS -------------------------------------//
//save Member as user	
function makeUser(id_,firstName_,email_,phone_){// alert("now in");
      console.log(id_+","+firstName_+","+email_+","+phone_);

			var formName_="makeUser";
			$.post("../app/api/post.php", //Required URL of the page on server
			{ // Data Sending With Request To Server
				formName:formName_,
				email:email_,
				phone:phone_,
				firstName:firstName_,
				memberId:id_
			
			},
      function(data,status){ // Required Callback Function
        console.log("response:",data);
				//var obj= $.parseJSON(data);
			if(data.includes("true")){successNote(sresponse, "User created successfully");
			}else{
				errorNote(fresponse,"user not created");
			}
			});
			
		}
// send user credentials by email
function sendUserCredentials(email){// alert("now in");
		(new PNotify({
            title: 'Confirmation Needed',
            text: 'Are you sure you want to reset this account?',
            icon: 'glyphicon glyphicon-question-sign',
            hide: false,
            confirm: {
              confirm: true
            },
            buttons: {
              closer: false,
              sticker: false
            },
            history: {
              history: false
            },
            addclass: 'stack-modal',
            stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
          })).get().on('pnotify.confirm', function(){
				var formName_="sendUserCredentials";
				$.post("api/post.php", //Required URL of the page on server
				{ // Data Sending With Request To Server
					formName:formName_,
					email:email
				
				},
				function(data,status){ // Required Callback Function
				var obj= $.parseJSON(data);
					console.log(obj['text']);
				if(obj['id']=="1"){successNote(sresponse,obj['text']);
				}else{
					errorNote(fresponse,obj['text']);
				}
				});
			});
		}




    //----------------------------------notifications------------------------------------------------//
    //success nofication
    function successNote(title_,text_){new PNotify({
          title: title_,
          text: text_,
          type: 'success'
        });
    }

    //failure or Error notifications
    function errorNote(title_,text_){new PNotify({
          title: title_,
          text: text_,
          type: 'error'
        });
    }