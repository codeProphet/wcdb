DROP TABLE IF EXISTS cmfi_alerts;

CREATE TABLE `cmfi_alerts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `readStatus` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `alertMsg` varchar(200) NOT NULL,
  `alertFrom` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `alertDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO cmfi_alerts (id,readStatus,userId,alertMsg,alertFrom,priority,alertDate) VALUES
	("1","0","1","Please send your report as at 30 June 2016","0","1","0000-00-00 00:00:00");


