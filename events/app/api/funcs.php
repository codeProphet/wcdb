<?php
/*
WCDB Version: 1.0.0
*/
//Functions that return JSON data to client requests
//------------------------------------------------------------------------------

if(isset($_REQUEST["fn"])){
	$nation=null;
	include_once'../config-small.php';
	$function=$_REQUEST["fn"];
	if(isset($_REQUEST["id"])){$id=$_REQUEST["id"];}
	if(isset($_REQUEST["u"])){$user=$_REQUEST["u"];}
	if(isset($_REQUEST["n"])){$nation=$_REQUEST["n"];}
	if ($function=="fetchmembers"){
		$output = fetchMembers($nation);
		echo json_encode($output);
	}elseif ($function=="fetchTempmembers"){
		$userId=$_REQUEST["user"];
		$output = fetchTempMembers($userId);
		echo json_encode($output);}
	elseif($function=="fetchMemberNames"){
		$nation=$_REQUEST["name"];
		echo '{ "data" :'. json_encode(fetchMemberNames($nation)).'}';
	}elseif($function=="fetchFamilies"){echo json_encode(getFamilies($nation));
	}elseif($function=="fetchProvince"){echo json_encode(getProvince($id));
	}elseif($function=="fetchHouseChurch"){echo json_encode(getHouseChurch($id));
	}elseif($function=="fetchPersonName"){echo json_encode(fetchPersonName($id));
	}elseif($function=="fetchHouseChurches"){echo json_encode(fetchHouseChurches($nation));
	}elseif($function=="fetchCountries"){echo json_encode(getCountries());
	}elseif($function=="fetchNations"){echo  json_encode(getNations())	;
	}elseif($function=="fetchProvinces"){echo json_encode(getProvinces($nation));
	}elseif($function=="fetchLocalities"){echo json_encode(fetchLocalities($nation));
	}elseif($function=="fetchReports"){echo json_encode(getReports($id));
	}elseif($function=="fetchLocality"){echo json_encode(fetchLocality($id));
	}elseif($function=="fetchSettings"){echo json_encode(getSettings());
	}elseif($function=="fetchAllUsers"){echo json_encode(fetchAllUsers($nation));
	}elseif($function=="fetchOnlineUsers"){echo '{ "users" :'. json_encode(fetchOnlineUsers($nation)).'}';
	}elseif($function=="fetchTopUsers"){echo '{ "users" :'. json_encode(fetchAllUsers("visits","LIMIT 3")).'}';
	}elseif($function=="fetchRecentMembers"){echo '{ "members" :'. json_encode(fetchRecentMembers()).'}';
	}elseif($function=="fetchEvents"){echo  '{ "events" :'.json_encode(getTopEvents($nation)).'}';
	}elseif($function=="fetchEvent"){echo  json_encode(fetchEvent($id));
	}elseif($function=="fetchAllEvents"){echo  json_encode(fetchEvents($nation,$id));
	}elseif($function=="fetchEventRegistrations"){echo json_encode(getEventRegistrations($id));
	}elseif($function=="fetchEventRegNatBar"){echo json_encode(getEventGenderNationBar($id));
	}elseif($function=="fetchReports"){echo  json_encode(getReports($nation));
	}elseif($function=="getNationsL"){$nation=$_REQUEST["letter"];echo  '{ "nation" :'.json_encode(getNations($nation)).'}';
	}elseif($function=="alerts"){echo  '{ "alerts" :'.json_encode(fetchAlerts($user)).'}';
	}elseif($function=="tables"){echo  json_encode(showTables());
	}elseif($function=="fetchEventLocRegBar"){echo  json_encode(getEventGenderLocRegBar($id));
	}elseif($function=="fetchMemberDetails"){echo  json_encode(fetchMemberDetails($id));
	}elseif($function=="fetchRegions"){echo  json_encode(getRegions());
	}elseif($function=="fetchRecordsSparkline"){echo  json_encode(getMemberRegSparkline());
	}elseif($function=="fetchLogs"){echo  '{ "logs" :'.json_encode(getLogs($nation)).'}';
	}elseif($function=="fetchNationTotals"){echo  json_encode(getNationTotals($nation));
	}elseif($function=="fetchNation"){echo  json_encode(getNationDetails($nation));
	}elseif($function=="fetchGlobalTotals"){echo  json_encode(getGlobalTotals());
	}elseif($function=="fetchNationCode"){echo  json_encode(getNationCode($nation));
	}elseif($function=="fetchNationsDash"){echo  json_encode(getNationsDash($nation));
	}elseif($function=="fetchProvincesBar"){echo  json_encode(getProvinceBar($nation));
	}elseif($function=="fetchPages"){echo  json_encode(getPages());
	}elseif($function=="fetchTopNationMembers"){echo  '{ "nations" :'.json_encode(getTopNations()).'}';
	}elseif($function=="fetchFamily"){echo  '{ "family" :'.json_encode(getFamily($id)).'}';
	}
}

//Functions that do not interact with DB
//------------------------------------------------------------------------------

//Retrieve a list of all .php files in models/languages
function getLanguageFiles()
{
	$directory = "models/languages/";
	$languages = glob($directory . "*.php");
	//print each file name
	return $languages;
}

//Retrieve a list of all .css files in models/site-templates 
function getTemplateFiles()
{
	$directory = "models/site-templates/";
	$languages = glob($directory . "*.css");
	//print each file name
	return $languages;
}

//Retrieve a list of all .php files in root files folder
function getPageFiles()
{
	$directory = "";
	$pages = glob($directory . "*.php");
	//print each file name
	foreach ($pages as $page){
		$row[$page] = $page;
	}
	return $row;
}

//Destroys a session as part of logout
function destroySession($name)
{
	if(isset($_SESSION[$name]))
	{
		$_SESSION[$name] = NULL;
		unset($_SESSION[$name]);
	}
}

function set_cookie($name, $value) {
    if (!isset($_COOKIE[$name]) || ($_COOKIE[$name] != $value)) {
        $_COOKIE[$name] = $value;
    }
    setcookie($name, $value, strtotime('+1 week'), '/');
}

//Generate a unique code
function getUniqueCode($length = "")
{	
	$code = md5(uniqid(rand(), true));
	if ($length != "") return substr($code, 0, $length);
	else return $code;
}

//Generate random password
 function password_generate($chars) 
	{
	  $data = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
	  return substr(str_shuffle($data), 0, $chars);
	}


//Generate an activation key
function generateActivationToken($gen = null)
{
	do
	{
		$gen = md5(uniqid(mt_rand(), false));
	}
	while(validateActivationToken($gen));
	return $gen;
}

//@ Thanks to - http://phpsec.org
function generateHash($plainText, $salt = null)
{
	if ($salt === null)
	{
		$salt = substr(md5(uniqid(rand(), true)), 0, 25);
	}
	else
	{
		$salt = substr($salt, 0, 25);
	}
	
	return $salt . sha1($salt . $plainText);
}

function decodeHash($salt){
	$md5=substr($salt,26,strlen($salt));
	return md5($md5);
}

//Checks if an email is valid
function isValidEmail($email)
{
	if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
		return true;
	}
	else {
		return false;
	}
}

function redirect($url) {
    ob_start();
    header('Location: '.$url);
    ob_end_flush();
    die();
}

//Inputs language strings from selected language.
function lang($key,$markers = NULL)
{
	global $lang;
	if($markers == NULL)
	{
		$str = $lang[$key];
	}
	else
	{
		//Replace any dyamic markers
		$str = $lang[$key];
		$iteration = 1;
		foreach($markers as $marker)
		{
			$str = str_replace("%m".$iteration."%",$marker,$str);
			$iteration++;
		}
	}
	//Ensure we have something to return
	if($str == "")
	{
		return ("No language key found");
	}
	else
	{
		return $str;
	}
}




//Checks if a string is within a min and max length
function minMaxRange($min, $max, $what)
{
	if(strlen(trim($what)) < $min)
		return true;
	else if(strlen(trim($what)) > $max)
		return true;
	else
	return false;
}

//Replaces hooks with specified text
function replaceDefaultHook($str)
{
	global $default_hooks,$default_replace;	
	return (str_replace($default_hooks,$default_replace,$str));
}

//Displays error and success messages
function resultBlock($errors,$successes){
	//Error block
	if(count($errors) > 0)
	{
		echo "<div>";
		foreach($errors as $error)
		{
			echo "<div class='alert alert-danger alert-dismissible fade in' role='alert'>
                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                  </button>
                  <i class='fa fa-times-circle'></i>&nbsp;&nbsp;&nbsp;".$error."
                </div>
				";
		}
		echo "</div>";
	}
	//Success block
	if(count($successes) > 0)
	{
		echo "<div>";
		foreach($successes as $success)
		{
			echo "<div class='alert alert-success alert-dismissible fade in' role='alert'>
                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
                  </button>
                  <i class='fa fa-check-circle'></i>&nbsp;&nbsp;&nbsp;".$success."
                </div>
				";
		}
		echo "</div>";
	}
}

//Completely sanitizes text
function sanitize($str)
{
	return strtolower(strip_tags(trim(($str))));
}

//Functions that interact mainly with .users table
//------------------------------------------------------------------------------

//Delete a defined array of users
function deleteUsers($users) {
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."users 
		WHERE id = ?");
	$stmt2 = $mysqli->prepare("DELETE FROM ".$db_table_prefix."user_permission_matches 
		WHERE user_id = ?");
	foreach($users as $id){
		$stmt->bind_param("i", $id);
		$stmt->execute();
		$stmt2->bind_param("i", $id);
		$stmt2->execute();
		$i++;
	}
	$stmt->close();
	$stmt2->close();
	return $i;
}

//Check if a display name exists in the DB
function displayNameExists($displayname)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT active
		FROM ".$db_table_prefix."users
		WHERE
		display_name = ?
		LIMIT 1");
	$stmt->bind_param("s", $displayname);	
	$stmt->execute();
	$stmt->store_result();
	$num_returns = $stmt->num_rows;
	$stmt->close();
	
	if ($num_returns > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
}

//Check if an email exists in the DB
function emailExists($email)
{
	$num_returns=0;
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT active
		FROM ".$db_table_prefix."users
		WHERE
		email = ?
		LIMIT 1");
	if(!$stmt){echo null;
	}else{
	$stmt->bind_param("s", $email);	
	$stmt->execute();
	$stmt->store_result();
	$num_returns = $stmt->num_rows;
	$stmt->close();
	}
	if ($num_returns > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
}

//Check if a user name and email belong to the same user
function emailUsernameLinked($email,$username)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT active
		FROM ".$db_table_prefix."users
		WHERE user_name = ?
		AND
		email = ?
		LIMIT 1
		");
	$stmt->bind_param("ss", $username, $email);	
	$stmt->execute();
	$stmt->store_result();
	$num_returns = $stmt->num_rows;
	$stmt->close();
	
	if ($num_returns > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
}

function fetchOnlineUsers($nation=null){
	global $mysqli,$db_table_prefix; 
	if($nation!=null){
		$condition="WHERE m.country='".$nation."'";
	}else{
		$condition="";
	}
	$stmt = $mysqli->prepare("SELECT 
		u.id,
		m.firstName,
		m.lastName,
		ou.time,
		LEFT(Monthname(ou.time),3) as month,
		weekday(ou.time) as day,
		m.image,
		c.name	
	FROM ".$db_table_prefix."users_online ou
	INNER JOIN ".$db_table_prefix."users u
	ON ou.userId=u.id 
	LEFT JOIN ".$db_table_prefix."members m
	on u.memberId=m.id
	LEFT JOIN ".$db_table_prefix."countries c
	on m.country=c.code
	".$condition."
	ORDER BY u.visits DESC
	");
			
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($id,$firstName,$lastName,$signInTime,$month,$day,$image,$country);
	
	while ($stmt->fetch()){
		$date=date('Y-m-d G:i:s',$signInTime);
		$signInTime=defineTime($month,$day,$signInTime);
		$row[] = array('id'=>$id,'firstName'=>$firstName,'lastName'=>$lastName,'signInTime'=>$signInTime,'image'=>$image,'country'=>$country);
	}
	$stmt->close();
	return ($row);
	}else{
		return 0;
	}
}

//Retrieve information for all users
function fetchAllUsers($n=NULL)
{
	
	if($n!=null){
		$condition="WHERE m.country='".$n."'";
	}else{
		$condition="";
	}
	
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		u.id,
		m.id as memberId,
		m.firstName,
		m.lastName,
		password,
		u.email,
		activation_token,
		last_activation_request,
		lost_password_request,
		active,
		title,
		sign_up_stamp,
		last_sign_in_stamp,
		m.image,
		m.phone,
		isAdmin,
		linkedin_profile,
		pinterest_profile,
		visits,
		m.country
		FROM ".$db_table_prefix."users u
		LEFT JOIN ".$db_table_prefix."members m ON u.memberId=m.id
		  ".$condition);
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check != 0){
		$stmt->bind_result($id, $memberId,$userFirst, $userLast, $password, $email, $token, $activationRequest, $passwordRequest, $active, $title, 
		$signUp, $signIn,$image,$phone,$isAdmin,$linkedin,$pinterest,$visits,$country);
		
		while ($stmt->fetch()){
			$email_="'".$email."'";
			$checkActive="";$checked="";
			$signInt=date( 'Y-m-d G:i',$signIn);
			$date='<strong>'.$signInt .'</strong>';
			if($isAdmin==1){$checked= 'checked';}
			if($active==1){$checkActive='checked';}
			$active_='<input id="active_check" type="checkbox" class="flat checkbox_flat-green"  checked='.$checkActive.'>';
			$admin_='<div class="icheckbox_flat-green flat"><input id="admin_check" onclick="checkAdmin('.$id.')" name="admin_check" type="checkbox" style="vertical-align: middle;margin:0;padding:0;position: absolute;border:none;opacity: 1;width: 20px;height: 20px;" '.$checked.'></div>';
			$image_='<a class="user-profile"><img src="images/members/'.$image.'" alt="" ></a>';
			$name_='<a onclick="openPage(26,'.$memberId.')" cursor="pointer">'.$userFirst.' '.$userLast.'</a>';
			$visits_='<a class="badge badge-success"><i class="icon-login"></i>'.$visits.'</a>';
			$actions='<a type="button" class="btn btn-danger btn-xs"  onclick="deleteUser('.$id.')"><i class="glyphicon glyphicon-trash"></i></a>
			<a type="button"  data-toggle="tooltip" data-placement="bottom" title="Reset Account (user will be required to re-activate their account)" class="btn btn-primary btn-xs" onclick="sendUserCredentials('.$email_.')"><i class="glyphicon glyphicon-repeat"></i></a>';
			 
			$row[] = array('id' => $id,'memberId' => $memberId, 'name' =>$name_ , 'password' => $password, 'email' => $email, 
			'activation_token' => $token, 'last_activation_request' => $activationRequest, 'lost_password_request' => $passwordRequest,'active_'=>$active_, 
			'active' => $active, 'title' => $title, 'sign_up_stamp' => $signUp, 'last_sign_in_stamp' => $signIn, 'image'=>$image_, 
			'phone'=>$phone,'admin'=>$admin_,	'isAdmin'=>$isAdmin,'linkedin_profile'=>$linkedin,		'pinterest_profile'=>$pinterest, 'visits'=>$visits_,'lastSeen'=>$date, 'actions'=>utf8_encode($actions));
		}
		$stmt->close();
		return ($row);
	}
}

//Retrieve information for all users
function getmeAllUsers()
{
	global $db_table_prefix; 
	$con = mysqli_connect('localhost','root','','quest');
    $db = mysqli_select_db($con,'quest');
	$stmt = mysqli_query($con,"SELECT id
		FROM ".$db_table_prefix."users");
	$stmt1 = mysqli_num_rows($stmt);
	echo $stmt1;

}
function getmeAllProperties()
{
	global $db_table_prefix; 
	$con = mysqli_connect('localhost','root','','deva');
    $db = mysqli_select_db($con,'deva');
	$stmt = mysqli_query($con,"SELECT propID
		FROM ".$db_table_prefix."properties");
	$stmt1 = mysqli_num_rows($stmt);
	echo $stmt1;

}

function userOnline($id){
	global $mysqli,$db_table_prefix; 
	$tbl=$db_table_prefix."users_online";
	$session=session_id();
	$time=time();
	$timeChk=$time-600;

	//if over 10 min delete session
	$s4="DELETE FROM $tbl WHERE time<$timeChk";
	$r4=mysqli_query($mysqli,$s4);

	$s="SELECT * from $tbl WHERE userId=$id ";
	$r=$mysqli->query($s);

	if (!$r){
			$stmt = $mysqli->prepare("INSERT INTO ".$tbl." (session,time,userId) VALUES(?,?,?)");
			$stmt->bind_param("ssi", $session,$time,$id);
			$stmt->execute();
	}else{
		$c=mysqli_num_rows($r);
		if($c==0){
			$stmt1 = $mysqli->prepare("INSERT INTO ".$tbl." (session,time,userId) VALUES(?,?,?)");
			$stmt1->bind_param("ssi", $session,$time,$id);
			$stmt1->execute();
		}else{
			$stmt1 = $mysqli->prepare("UPDATE ".$tbl." SET time=? WHERE session=?");
			$stmt1->bind_param("ss", $time,$session);
			$stmt1->execute();
		}
	}
	$s3="SELECT * FROM $tbl";
	$r3=mysqli_query($mysqli,$s3);
	$onlineUsers=mysqli_num_rows($r3);
	
	//return $onlineUsers;

}

//fetch all alerts for a user
function fetchAlerts($id=NULL){
if($id!=NULL) {
		$column = "userId";
		$data = $id;
	}
	
global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		readStatus,
		userId,
		Concat(u.userFirst,', ',u.userLast) as alertFrom,
		u.image,
		priority,
		alertDate,
		alertMsg,
		modifiedDate	
	FROM ".$db_table_prefix."alerts a
	INNER JOIN ".$db_table_prefix."users u 
	on a.alertFrom=u.id
		WHERE
		readStatus=0 and
		$column = ?");
	if (!$stmt)
	{
		echo "false";
	}
	else{
		$stmt->bind_param("s", $data);
	
		$stmt->execute();
		$stmt->store_result();
		$num_returns = $stmt->num_rows;
		if ($num_returns > 0)
		{
		$stmt->bind_result($readStatus,$userId,$alertFrom,$image,$priority,$alertDate,$alertMsg,$modifiedDate);
		while ($stmt->fetch()){
				$row[] = array('readStatus' => $readStatus, 'userId'=>$userId, 'alertFrom'=>$alertFrom,'image'=>$image,'priority'=>$priority,'alertDate'=>$alertDate,
				'alertMsg'=>$alertMsg,'modifiedDate'=>$modifiedDate);}
		$stmt->close();
		return ($row);
		}
	}
}


function getFamily($id){
	
	$condition="WHERE f.id=".$id;
	
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		m.id,
		firstName,
		lastName,
		familyRole,
		image	
	FROM ".$db_table_prefix."families f
	INNER JOIN ".$db_table_prefix."members m
	ON f.id=m.familyID ".$condition."
	ORDER BY familyRole asc
	");
			
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($id,$firstName,$lastName,$familyRole,$image);
	
	while ($stmt->fetch()){
		if($familyRole==1){
            $fhead=true;
          }elseif($familyRole==2){
            $fhead=true;
          }elseif($familyRole==3){
            $fhead=false;
          }
		$row[] = array('id'=>$id,'firstName'=>$firstName,'lastName'=>$lastName,'familyRole'=>$familyRole,'image'=>$image,'fhead'=>$fhead);
	}
	$stmt->close();
	return ($row);
	}else{
		return 0;
	}
}

function getFamilies($nation=null){
	if($nation==null){
		$condition="";
	}
	else{
		$condition="WHERE nation='".$nation."'";
	}
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		familyName,
		address,
		surburb,
		city,
		nation	
	FROM ".$db_table_prefix."families 
	".$condition."
	ORDER BY familyName ASC");
			
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($id,$familyName,$address,$surburb,$city,$nation);
	
	while ($stmt->fetch()){
		$row[] = array('id'=>$id,'familyName'=>$familyName,'nation'=>$nation,'address'=>$address,'surburb'=>$surburb,'city'=>$city,'actions'=>'<a href="#" class="btn btn-xs btn-info"><i class="icon-eye"></i></a><a href="#" class="btn btn-xs btn-primary"><i class="icon-pencil"></i></a><a href="#" class="btn btn-xs btn-danger"><i class="icon-trash"></i></a>');
	}
	$stmt->close();
	return ($row);
	}else{
		return 0;
	}
}

function getHouseChurches($nation=null){
	if($nation==null){
		$condition="";
	}
	else{
		$condition="WHERE nation='".$nation."'";
	}
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		h.id,
		familyName,
		nation	
	FROM ".$db_table_prefix."housechurches h 
	INNER JOIN ".$db_table_prefix."families f
	ON h.familyid=f.id 
	".$condition."
	ORDER BY familyName asc");
			
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($id,$familyName,$nation);
	
	while ($stmt->fetch()){
		$row[] = array('id'=>$id,'familyName'=>$familyName,'nation'=>$nation);
	}
	$stmt->close();
	return ($row);
	}else{
		return 0;
	}
}

function countHouseChurches($nation=null){
	if($nation==null){
		$condition="";
	}
	else{
		$condition="WHERE l.country='".$nation."'";
	}
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		h.id
	FROM ".$db_table_prefix."housechurches h
	INNER JOIN ".$db_table_prefix."localities l
	ON l.id=h.locality
	         ".$condition);
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	$stmt->close();
	if($check !=0){
		return $check;
	}else{
		return 0;
	}
}

function getctrlTotalDisciples($nation=null){
	if($nation==null){
		$condition="";
	}
	else{
		$condition="WHERE code='".$nation."'";
	}
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		disciples 
	FROM cmfi_nation_stats
	".$condition."
	Order by date desc
	LIMIT 1");
	$stmt->execute();
	$stmt->bind_result($disciples);
	while ($stmt->fetch()){
			$row = array('disciples' => $disciples);
		}
	$stmt->close();
	return $row["disciples"];
	
}

//get userName
function fetchUserName($id=NULL){
if($id!=NULL) {
		$column = "id";
		$data = $id;
	}
	
global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		userFirst,
		userLast	
	FROM ".$db_table_prefix."users
		WHERE
		$column = ?
		LIMIT 1");
		$stmt->bind_param("s", $data);
	
	$stmt->execute();
	$stmt->bind_result($id,$userFirst,$userLast);
	while ($stmt->fetch()){
			$row = array('id' => $id, 'userFirst'=>$userFirst, 'userLast'=>$userLast);}
	$stmt->close();
	return ($row);
}

//Get nation name
 function nationName($nation){
	global $mysqli,$db_table_prefix;
		
		//Grant access if master user
		
		$stmt = $mysqli->prepare("SELECT name 
			FROM ".$db_table_prefix."countries
			WHERE code = ?
			LIMIT 1
			");
		$stmt->bind_param("s", $nation);
		$stmt->execute();
		$stmt->bind_result($nationName);
		$stmt->fetch();
		$stmt->close();
		return ($nationName);
		}
	
//Get number of records captured by user
 function getRecords($id){
	global $mysqli,$db_table_prefix;
		
		//Grant access if master user
		
		$stmt = $mysqli->prepare("SELECT count(id) as records 
			FROM ".$db_table_prefix."members
			WHERE addedBy = ?
			");
		$stmt->bind_param("s", $id);
		$stmt->execute();
		$stmt->bind_result($records);
		$stmt->fetch();
		$stmt->close();
		return ($records);
		}
	
//Retrieve complete user information by username, token or ID
function fetchUserDetails($username=NULL,$token=NULL, $id=NULL)
{
	if($username!=NULL) {
		$column = "u.email";
		$data = $username;
	}
	/*elseif($token!=NULL) {
		$column = "activation_token";
		$data = $token;
	}
	elseif($id!=NULL) {
		$column = "u.memberId";
		$data = $id;
	}*/
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		u.id,
        u.memberId,
		user_name,
		display_name,
		password,
		u.email,
		activation_token,
		last_activation_request,
		lost_password_request,
		active,
		title,
		sign_up_stamp,
		last_sign_in_stamp,
		m.image,
		facebook_profile,
		twitter_profile,
		linkedin_profile,
		pinterest_profile,
		isAdmin,
		m.firstName,
		m.lastName,
		recEmails,
		weatherLoc,
		qualification,
		gender,
		userNotes,
		m.phone,
		m.DOB,
		reportTo,
		visits,
		nationId
		FROM cmfi_users u 
        INNER JOIN cmfi_members m on u.memberId=m.id
		WHERE
		$column = ? 
		LIMIT 1");

	$stmt->bind_param("s", $data);
	
	$stmt->execute();
	$stmt->bind_result($id,$memberId, $user, $display, $password, $email, $token, $activationRequest, $passwordRequest, $active, $title, $signUp, $signIn,$image,$facebook,$twitter,$linkedin,$pinterest,$isAdmin,$userFirst,$userLast,$recEmails,$weatherLoc,$qualification,$gender,$userNotes,$phone,$dob,$reportTo,$visits,$nationId);
	while ($stmt->fetch()){
			$row = array('id' => $id,'memberId' => $memberId, 'user_name' => $user, 'display_name' => $display, 'password' => $password, 'email' => $email, 'activation_token' => $token, 'last_activation_request' => $activationRequest, 'lost_password_request' => $passwordRequest, 'active' => $active, 'title' => $title,
			'sign_up_stamp' => $signUp, 'last_sign_in_stamp' => $signIn, 'image'=>$image, 'facebook_profile'=>$facebook,	'twitter_profile'=>$twitter, 'linkedin_profile'=>$linkedin,'pinterest_profile'=>$pinterest,'isAdmin'=>$isAdmin, 'userFirst'=>$userFirst, 'userLast'=>$userLast, 'recEmails'=>$recEmails,	
			'weatherLoc'=>$weatherLoc,'qualification'=>$qualification,'gender'=>$gender,'userNotes'=>$userNotes, 'phone'=>$phone, 'DOB'=>$dob,'reportTo'=>$reportTo,'visits'=>$visits, 'nationId'=>$nationId);}
	$stmt->close();
	return ($row);
}

//Toggle if lost password request flag on or off
function flagLostPasswordRequest($username,$value)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET lost_password_request = ?
		WHERE
		user_name = ?
		LIMIT 1
		");
	$stmt->bind_param("ss", $value, $username);
	$result = $stmt->execute();
	$stmt->close();
	return $result;
}

//Check if a user is logged in
function isUserLoggedIn()
{
	global $loggedInUser,$mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT 
		id,
		password
		FROM ".$db_table_prefix."users
		WHERE
		id = ?
		AND 
		password = ? 
		AND
		active = 1
		LIMIT 1");

	$stmt->bind_param("is", $loggedInUser->user_id, $loggedInUser->hash_pw);	
	$stmt->execute();
	$stmt->store_result();
	$num_returns = $stmt->num_rows;
	$stmt->close();
	
	
	
	if($loggedInUser == NULL)
	{
		return false;
	}
	else
	{
		if ($num_returns > 0)
		{
			return true;
		}
		else
		{
			destroySession("wcdbUser");
			return false;	
		}
	}
}

//Change a user from inactive to active
function setUserActive($token)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET active = 1
		WHERE
		activation_token = ?
		LIMIT 1");
	$stmt->bind_param("s", $token);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;
}

//Change a user's display name
function updateDisplayName($id, $display)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET display_name = ?
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("si", $display, $id);
	$result = $stmt->execute();
	$stmt->close();
	return $result;
}

//Change a user's first name and surname
function updateUserNames($userId,$firstName,$lastName)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET userFirst = ?,
		userLast=?
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("ssi", $firstName,$lastName,$userId);
	$result = $stmt->execute();
	$stmt->close();
	return $result;
}


function updateUserDetails($userId,$userNotes,$Gender,$dob,$qualification,$reportTo)
{
    global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET 
		userNotes=?,
		gender=?,
		DOB=?,
		reportTo=?,
		qualification=?
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("sssssi", $userNotes,$Gender,$dob,$qualification,$reportTo,$userId);
	$result = $stmt->execute();
	$stmt->close();
	return $result;
}



//Update a user's email
function updateEmail($id, $email)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET 
		email = ?
		WHERE
		id = ?");
	$stmt->bind_param("si", $email, $id);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;
}


//Change a user's social profile links
function updateUserContacts($id,$linkedin,$facebook,$google,$twitter,$city,$phone)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET 
		linkedin_profile = ?,
		facebook_profile=?,
		pinterest_profile=?,
		twitter_profile=?,
		weatherLoc=?,
		phone=?
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("ssssssi", $linkedin,$facebook,$google,$twitter,$city,$phone,$id);
	$result = $stmt->execute();
	$stmt->close();
	return $result;
}


//Update a user's password
function updatePassword($id, $newpassword)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET 
		password = ?
		WHERE
		id = ?");
	$stmt->bind_param("si", $newpassword, $id);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;
}


//Input new activation token, and update the time of the most recent activation request
function updateLastActivationRequest($new_activation_token,$username,$email)
{
	global $mysqli,$db_table_prefix; 	
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET activation_token = ?,
		last_activation_request = ?
		WHERE email = ?
		AND
		user_name = ?");
	$stmt->bind_param("ssss", $new_activation_token, time(), $email, $username);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;
}

//Generate a random password, and new token
function updatePasswordFromToken($pass,$token)
{
	global $mysqli,$db_table_prefix;
	$new_activation_token = generateActivationToken();
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET password = ?,
		activation_token = ?
		WHERE
		activation_token = ?");
	$stmt->bind_param("sss", $pass, $new_activation_token, $token);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;
}

//Update a user's title
function updateTitle($id, $title)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
		SET 
		title = ?
		WHERE
		id = ?");
	$stmt->bind_param("si", $title, $id);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;	
}

//Check if a user ID exists in the DB
function userIdExists($id)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT active
		FROM ".$db_table_prefix."users
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("i", $id);	
	$stmt->execute();
	$stmt->store_result();
	$num_returns = $stmt->num_rows;
	$stmt->close();
	
	if ($num_returns > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
}

//Checks if a username exists in the DB
function usernameExists($username)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT active
		FROM ".$db_table_prefix."users
		WHERE
		user_name = ?
		LIMIT 1");
	$stmt->bind_param("s", $username);	
	$stmt->execute();
	$stmt->store_result();
	$num_returns = $stmt->num_rows;
	$stmt->close();
	
	if ($num_returns > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
}

//Check if activation token exists in DB
function validateActivationToken($token,$lostpass=NULL)
{
	global $mysqli,$db_table_prefix;
	if($lostpass == NULL) 
	{	
		$stmt = $mysqli->prepare("SELECT active
			FROM ".$db_table_prefix."users
			WHERE active = 0
			AND
			activation_token = ?
			LIMIT 1");
	}
	else 
	{
		$stmt = $mysqli->prepare("SELECT active
			FROM ".$db_table_prefix."users
			WHERE active = 1
			AND
			activation_token = ?
			AND
			lost_password_request = 1 
			LIMIT 1");
	}
	$stmt->bind_param("s", $token);
	$stmt->execute();
	$stmt->store_result();
		$num_returns = $stmt->num_rows;
	$stmt->close();
	
	if ($num_returns > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
}


function createUserFolder($username)
{
	$Default='./resources/filemanager/default';
	$New='./resources/filemanager/users/'.$username;
	
	//exec("xcopy $Default $New /e/i"); //windows server
	
	define('DS', DIRECTORY_SEPARATOR); // I always use this short form in my code.

	if (!file_exists($New)) {
    $oldmask = umask(0);
     function copy_r($source, $dest, $options=array('folderPermission'=>0755,'filePermission'=>0755)) 
    { 
        $result=false; 
        
        if (is_file($source)) { 
            if ($dest[strlen($dest)-1]=='/') { 
                if (!file_exists($dest)) { 
                    cmfcDirectory::makeAll($dest,$options['folderPermission'],true); 
                } 
                $__dest=$dest."/".basename($source); 
            } else { 
                $__dest=$dest; 
            } 
            $result=copy($source, $__dest); 
            chmod($__dest,$options['filePermission']); 
            
        } elseif(is_dir($source)) { 
            if ($dest[strlen($dest)-1]=='/') { 
                if ($source[strlen($source)-1]=='/') { 
                    //Copy only contents 
                } else { 
                    //Change parent itself and its contents 
                    $dest=$dest.basename($source); 
                    @mkdir($dest); 
                    chmod($dest,$options['filePermission']); 
                } 
            } else { 
                if ($source[strlen($source)-1]=='/') { 
                    //Copy parent directory with new name and all its content 
                    @mkdir($dest,$options['folderPermission']); 
                    chmod($dest,$options['filePermission']); 
                } else { 
                    //Copy parent directory with new name and all its content 
                    @mkdir($dest,$options['folderPermission']); 
                    chmod($dest,$options['filePermission']); 
                } 
            } 

            $dirHandle=opendir($source); 
            while($file=readdir($dirHandle)) 
            { 
                if($file!="." && $file!="..") 
                { 
                     if(!is_dir($source."/".$file)) { 
                        $__dest=$dest."/".$file; 
                    } else { 
                        $__dest=$dest."/".$file; 
                    } 
                    //echo "$source/$file ||| $__dest<br />"; 
                    $result=copy_r($source."/".$file, $__dest, $options); 
                } 
            } 
            closedir($dirHandle); 
            
        } else { 
            $result=false; 
        } 
        return $result; 
    } 
	umask($oldmask);
	exec(copy_r ($Default,$New));  //linux
	}
}

//function to calculate folder size
function GetDirectorySize($path){
    $bytestotal = 0;
    $path = realpath($path);
    if($path!==false){
        foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS)) as $object){
            $bytestotal += $object->getSize();
        }
    }
    return $bytestotal;
}

//function to convert folder size into bit notation
function formatBytes($size, $precision = 2)
{
    $base = log($size) / log(1024);
    $suffixes = array('', 'k', 'M', 'G', 'T');   

    return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
}

//Functions that interact mainly with .permissions table
//------------------------------------------------------------------------------

//Create a permission level in DB
function createPermission($permission) {
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."permissions (
		name
		)
		VALUES (
		?
		)");
	$stmt->bind_param("s", $permission);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;
}

//Delete a permission level from the DB
function deletePermission($permission) {
	global $mysqli,$db_table_prefix,$errors; 
	$i = 0;
	$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."permissions 
		WHERE id = ?");
	$stmt2 = $mysqli->prepare("DELETE FROM ".$db_table_prefix."user_permission_matches 
		WHERE permission_id = ?");
	$stmt3 = $mysqli->prepare("DELETE FROM ".$db_table_prefix."permission_page_matches 
		WHERE permission_id = ?");
	foreach($permission as $id){
		if ($id == 1){
			$errors[] = lang("CANNOT_DELETE_NEWUSERS");
		}
		elseif ($id == 2){
			$errors[] = lang("CANNOT_DELETE_ADMIN");
		}
		else{
			$stmt->bind_param("i", $id);
			$stmt->execute();
			$stmt2->bind_param("i", $id);
			$stmt2->execute();
			$stmt3->bind_param("i", $id);
			$stmt3->execute();
			$i++;
		}
	}
	$stmt->close();
	$stmt2->close();
	$stmt3->close();
	return $i;
}

//Retrieve information for all permission levels
function fetchAllPermissions()
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		name
		FROM ".$db_table_prefix."permissions");
	$stmt->execute();
	$stmt->bind_result($id, $name);
	while ($stmt->fetch()){
		$row[] = array('id' => $id, 'name' => $name);
	}
	$stmt->close();
	return ($row);
}

//Retrieve information for a single permission level
function fetchPermissionDetails($id)
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		name
		FROM ".$db_table_prefix."permissions
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("i", $id);
	$stmt->execute();
	$stmt->bind_result($id, $name);
	while ($stmt->fetch()){
		$row = array('id' => $id, 'name' => $name);
	}
	$stmt->close();
	return ($row);
}

//Check if a permission level ID exists in the DB
function permissionIdExists($id)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT id
		FROM ".$db_table_prefix."permissions
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("i", $id);	
	$stmt->execute();
	$stmt->store_result();
	$num_returns = $stmt->num_rows;
	$stmt->close();
	
	if ($num_returns > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
}

//Check if a permission level name exists in the DB
function permissionNameExists($permission)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT id
		FROM ".$db_table_prefix."permissions
		WHERE
		name = ?
		LIMIT 1");
	$stmt->bind_param("s", $permission);	
	$stmt->execute();
	$stmt->store_result();
	$num_returns = $stmt->num_rows;
	$stmt->close();
	
	if ($num_returns > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
}

//Change a permission level's name
function updatePermissionName($id, $name)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."permissions
		SET name = ?
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("si", $name, $id);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;	
}

//Functions that interact mainly with .user_permission_matches table
//------------------------------------------------------------------------------

//Match permission level(s) with user(s)
function addPermission($permission, $user) {
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."user_permission_matches (
		permission_id,
		user_id
		)
		VALUES (
		?,
		?
		)");
	if (is_array($permission)){
		foreach($permission as $id){
			$stmt->bind_param("ii", $id, $user);
			$stmt->execute();
			$i++;
		}
	}
	elseif (is_array($user)){
		foreach($user as $id){
			$stmt->bind_param("ii", $permission, $id);
			$stmt->execute();
			$i++;
		}
	}
	else {
		$stmt->bind_param("ii", $permission, $user);
		$stmt->execute();
		$i++;
	}
	$stmt->close();
	return $i;
}

//Retrieve information for all user/permission level matches
function fetchAllMatches()
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		user_id,
		permission_id
		FROM ".$db_table_prefix."user_permission_matches");
	$stmt->execute();
	$stmt->bind_result($id, $user, $permission);
	while ($stmt->fetch()){
		$row[] = array('id' => $id, 'user_id' => $user, 'permission_id' => $permission);
	}
	$stmt->close();
	return ($row);	
}

//Retrieve list of permission levels a user has
function fetchUserPermissions($user_id)
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT
		id,
		permission_id
		FROM ".$db_table_prefix."user_permission_matches
		WHERE user_id = ?
		");
	$stmt->bind_param("i", $user_id);	
	$stmt->execute();
	$stmt->bind_result($id, $permission);
	while ($stmt->fetch()){
		$row[$permission] = array('id' => $id, 'permission_id' => $permission);
	}
	$stmt->close();
	if (isset($row)){
		return ($row);
	}
}

//Retrieve list of users who have a permission level
function fetchPermissionUsers($permission_id)
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT id, user_id
		FROM ".$db_table_prefix."user_permission_matches
		WHERE permission_id = ?
		");
	$stmt->bind_param("i", $permission_id);	
	$stmt->execute();
	$stmt->bind_result($id, $user);
	while ($stmt->fetch()){
		$row[$user] = array('id' => $id, 'user_id' => $user);
	}
	$stmt->close();
	if (isset($row)){
		return ($row);
	}
}

//Unmatch permission level(s) from user(s)
function removePermission($permission, $user) {
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."user_permission_matches 
		WHERE permission_id = ?
		AND user_id =?");
	if (is_array($permission)){
		foreach($permission as $id){
			$stmt->bind_param("ii", $id, $user);
			$stmt->execute();
			$i++;
		}
	}
	elseif (is_array($user)){
		foreach($user as $id){
			$stmt->bind_param("ii", $permission, $id);
			$stmt->execute();
			$i++;
		}
	}
	else {
		$stmt->bind_param("ii", $permission, $user);
		$stmt->execute();
		$i++;
	}
	$stmt->close();
	return $i;
}

//Functions that interact mainly with .configuration table
//------------------------------------------------------------------------------

//Update configuration table
function updateConfig($id, $value)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."configuration
		SET 
		value = ?
		WHERE
		id = ?");
	foreach ($id as $cfg){
		$stmt->bind_param("si", $value[$cfg], $cfg);
		$stmt->execute();
	}
	$stmt->close();	
}

//Functions that interact mainly with .pages table
//------------------------------------------------------------------------------

//Add a page to the DB
function createPages($pages) {
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."pages (
		page
		)
		VALUES (
		?
		)");
	foreach($pages as $page){
		$stmt->bind_param("s", $page);
		$stmt->execute();
	}
	$stmt->close();
}

//Delete a page from the DB
function deletePages($pages) {
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."pages 
		WHERE id = ?");
	$stmt2 = $mysqli->prepare("DELETE FROM ".$db_table_prefix."permission_page_matches 
		WHERE page_id = ?");
	foreach($pages as $id){
		$stmt->bind_param("i", $id);
		$stmt->execute();
		$stmt2->bind_param("i", $id);
		$stmt2->execute();
	}
	$stmt->close();
	$stmt2->close();
}

//Fetch information on all pages
function fetchAllPages()
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		page,
		private
		FROM ".$db_table_prefix."pages");
	$stmt->execute();
	$stmt->bind_result($id, $page, $private);
	while ($stmt->fetch()){
		$row[$page] = array('id' => $id, 'page' => $page, 'private' => $private);
	}
	$stmt->close();
	if (isset($row)){
		return ($row);
	}
}

//Fetch information for a specific page
function fetchPageDetails($id)
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		page,
		private
		FROM ".$db_table_prefix."pages
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("i", $id);
	$stmt->execute();
	$stmt->bind_result($id, $page, $private);
	while ($stmt->fetch()){
		$row = array('id' => $id, 'page' => $page, 'private' => $private);
	}
	$stmt->close();
	return ($row);
}

//Check if a page ID exists
function pageIdExists($id)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT private
		FROM ".$db_table_prefix."pages
		WHERE
		id = ?
		LIMIT 1");
	$stmt->bind_param("i", $id);	
	$stmt->execute();
	$stmt->store_result();	
	$num_returns = $stmt->num_rows;
	$stmt->close();
	
	if ($num_returns > 0)
	{
		return true;
	}
	else
	{
		return false;	
	}
}

//Toggle private/public setting of a page
function updatePrivate($id, $private)
{
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."pages
		SET 
		private = ?
		WHERE
		id = ?");
	$stmt->bind_param("ii", $private, $id);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;	
}

//Functions that interact mainly with .permission_page_matches table
//------------------------------------------------------------------------------

//Match permission level(s) with page(s)
function addPage($page, $permission) {
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."permission_page_matches (
		permission_id,
		page_id
		)
		VALUES (
		?,
		?
		)");
	if (is_array($permission)){
		foreach($permission as $id){
			$stmt->bind_param("ii", $id, $page);
			$stmt->execute();
			$i++;
		}
	}
	elseif (is_array($page)){
		foreach($page as $id){
			$stmt->bind_param("ii", $permission, $id);
			$stmt->execute();
			$i++;
		}
	}
	else {
		$stmt->bind_param("ii", $permission, $page);
		$stmt->execute();
		$i++;
	}
	$stmt->close();
	return $i;
}

//Retrieve list of permission levels that can access a page
function fetchPagePermissions($page_id)
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT
		id,
		permission_id
		FROM ".$db_table_prefix."permission_page_matches
		WHERE page_id = ?
		");
	$stmt->bind_param("i", $page_id);	
	$stmt->execute();
	$stmt->bind_result($id, $permission);
	while ($stmt->fetch()){
		$row[$permission] = array('id' => $id, 'permission_id' => $permission);
	}
	$stmt->close();
	if (isset($row)){
		return ($row);
	}
}

//Retrieve list of pages that a permission level can access
function fetchPermissionPages($permission_id)
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT
		id,
		page_id
		FROM ".$db_table_prefix."permission_page_matches
		WHERE permission_id = ?
		");
	$stmt->bind_param("i", $permission_id);	
	$stmt->execute();
	$stmt->bind_result($id, $page);
	while ($stmt->fetch()){
		$row[$page] = array('id' => $id, 'permission_id' => $page);
	}
	$stmt->close();
	if (isset($row)){
		return ($row);
	}
}

//Unmatched permission and page
function removePage($page, $permission) {
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."permission_page_matches 
		WHERE page_id = ?
		AND permission_id =?");
	if (is_array($page)){
		foreach($page as $id){
			$stmt->bind_param("ii", $id, $permission);
			$stmt->execute();
			$i++;
		}
	}
	elseif (is_array($permission)){
		foreach($permission as $id){
			$stmt->bind_param("ii", $page, $id);
			$stmt->execute();
			$i++;
		}
	}
	else {
		$stmt->bind_param("ii", $permission, $user);
		$stmt->execute();
		$i++;
	}
	$stmt->close();
	return $i;
}

//Check if a user has access to a page
function securePage($uri){
	
	//Separate document name from uri
	$tokens = explode('/', $uri);
	$page = $tokens[sizeof($tokens)-1];
	global $mysqli,$db_table_prefix,$loggedInUser;
	//retrieve page details
	$stmt = $mysqli->prepare("SELECT 
		id,
		page,
		private
		FROM ".$db_table_prefix."pages
		WHERE
		page = ?
		LIMIT 1");
	$stmt->bind_param("s", $page);
	$stmt->execute();
	$stmt->bind_result($id, $page, $private);
	while ($stmt->fetch()){
		$pageDetails = array('id' => $id, 'page' => $page, 'private' => $private);
	}
	$stmt->close();
	//If page does not exist in DB, allow access
	if (empty($pageDetails)){
		return true;
	}
	//If page is public, allow access
	elseif ($pageDetails['private'] == 0) {
		return true;	
	}
	//If user is not logged in, deny access
	elseif(!isUserLoggedIn()) 
	{
		header("Location: login.php");
		return false;
	}
	else {
		//Retrieve list of permission levels with access to page
		$stmt = $mysqli->prepare("SELECT
			permission_id
			FROM ".$db_table_prefix."permission_page_matches
			WHERE page_id = ?
			");
		$stmt->bind_param("i", $pageDetails['id']);	
		$stmt->execute();
		$stmt->bind_result($permission);
		while ($stmt->fetch()){
			$pagePermissions[] = $permission;
		}
		$stmt->close();
		//Check if user's permission levels allow access to page
		if ($loggedInUser->checkPermission($pagePermissions)){ 
			return true;
		}
		//Grant access if master user
		elseif ($loggedInUser->user_id == $master_account){
			return true;
		}
		else {
			header("Location: account.php");
			return false;	
		}
	}
}

//Retrieve information for all properties
function fetchAllProperties()
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		propID,
		Address,
		Bedrooms,
		SittingRooms,
		BathRooms,
		DateAdded,
		DistanceFromCampus,
		Town,
		Comments,
		img1,
		img2,
		img3,
		img4,
		img5,
		Status,
		costPM,
		availableFrom
		FROM ".$db_table_prefix."properties");
	$stmt->execute();
	$stmt->bind_result($propID, $Address, $Bedrooms, $SittingRooms, $BathRooms, $DateAdded, $DistanceFromCampus, $Town, $Comments, $img1, $img2, $img3,$img4,$img5,$Status,$costPM,$availableFrom);
	
	while ($stmt->fetch()){
		$row[] = array('propID' => $propID, 'Address' => $Address, 'Bedrooms' => $Bedrooms, 'SittingRooms' => $SittingRooms, 'BathRooms' => $BathRooms, 'DateAdded' => $DateAdded, 'DistanceFromCampus' => $DistanceFromCampus, 'Town' => $Town, 'Comments' => $Comments, 'img1' => $img1, 'img2' => $img2, 'img3' => $img3,'img4' => $img4,'img5' => $img5,'Status' => $Status,'costPM' => $costPM,'availableFrom'=>$availableFrom);
	}
	$stmt->close();
	return ($row);
}

//Delete a defined array of properties
function deleteProperties($properties) {
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."properties 
		WHERE propID = ?");
	$stmt2 = $mysqli->prepare("DELETE FROM ".$db_table_prefix."rooms 
		WHERE propID = ?");
	foreach($properties as $id){
		$stmt->bind_param("i", $id);
		$stmt->execute();
		$stmt2->bind_param("i", $id);
		$stmt2->execute();
		$i++;
	}
	$stmt->close();
	$stmt2->close();
	return $i;
}

//Retrieve information for all announcements
function fetchAllNews()
{
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		title,
		announcement,
		postedby,
		active,
		datestamp,
		url
		FROM ".$db_table_prefix."announcements");
	$stmt->execute();
	$stmt->bind_result($id, $title, $announcement, $postedby, $active, $datestamp, $url);
	
	while ($stmt->fetch()){
		$row[] = array('id' => $id, 'title' => $title, 'announcement' => $announcement, 'postedby' => $postedby, 'active' => $active, 'datestamp' => $datestamp, 'url' => $url);
	}
	$stmt->close();
	return ($row);
}

//fetch member details
function fetchMembers($nation=NULL)
{
	$condition="";
	if($nation!=NULL){
		$condition="WHERE m.country='".$nation."' ORDER BY m.firstName ASC";
	}
 	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
	    m.id,
	    u.id,
		u.isAdmin,
		m.firstName,
		m.lastName,
		m.salutation,
		m.address,
		m.surburb,
		m.city,
		l.localityName,
		c.name,
		m.sex,
		m.DOB,
		m.assembly,
		m.email,
		m.phone,
		m.addedBy,
		m.modifiedDate,
		m.image,
		m.tags
		FROM ".$db_table_prefix."members m
		LEFT JOIN ".$db_table_prefix."countries c on m.country=c.code 
		LEFT JOIN ".$db_table_prefix."localities l ON m.locality=l.id
		LEFT JOIN ".$db_table_prefix."users u ON u.memberId=m.id
		".$condition."");
	    $stmt->execute();
	
	    $stmt->bind_result($id,$userId,$isAdmin, $firstName, $lastName, $salutation, $address, $surburb, $city,
	    $locality, $country, $sex, $DOB,$assembly, $email,$phone,$addedBy,$modifiedDate,$image,$tags);
	 while ($stmt->fetch()){
	 	$fname="'".$firstName."'";
	 	$route="routie('/disciple/".$id."')";
	 	$actions='<a  onclick="'.$route.';" class="btn btn-info btn-xs"><i class="icon-pencil"></i></a><a onclick="deleteMember('.$id.')" class="btn btn-danger btn-xs"><i class="icon-trash"></i></a>';
		$row[] = array('id' => $id,'userId'=>$userId,'isAdmin'=>$isAdmin, 'firstName' => $firstName, 'lastName' => $lastName, 'salutation' => $salutation, 'address' => $address,
		'surburb' => $surburb, 'city' => $city, 'locality' => $locality, 'country' => $country, 'sex' => $sex, 
		'DOB' => $DOB,'assembly' => $assembly,'email' => $email,'phone' => $phone,'addedBy' => $addedBy, 'modifiedDate' => $modifiedDate, 'image' => $image,'tags'=>$tags,'actions'=>$actions);
	 }
	$stmt->close();
	return ($row);
}

//fetch most recently added members
function fetchRecentMembers($nation=NULL)
{
	$condition="";
	if($nation!=NULL){
		$condition="WHERE m.country='".$nation."'";
	}
 	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
	    m.id,
		m.firstName,
		m.lastName,
		m.salutation,
		m.address,
		m.surburb,
		m.city,
		l.localityName,
		c.name,
		m.sex,
		m.DOB,
		m.assembly,
		m.email,
		m.phone,
		m.dateAdded,
		day(m.dateAdded) as day,
		Monthname(m.dateAdded) as month,
		m.addedBy,
		concat(mm.firstName,', ',mm.lastName) as userName,
		m.modifiedDate,
		m.image,
		m.tags	
		
		FROM ".$db_table_prefix."members m
		LEFT JOIN ".$db_table_prefix."countries c on m.country=c.code 
		LEFT JOIN ".$db_table_prefix."localities l ON m.locality=l.id
		LEFT JOIN ".$db_table_prefix."members mm ON mm.id=m.addedBy
		".$condition."
		ORDER BY m.id desc
		LIMIT 3");
	    $stmt->execute();
	
	    $stmt->bind_result($id, $firstName, $lastName, $salutation, $address, $surburb, $city,
	    $locality, $country, $sex, $DOB,$assembly, $email,$phone,$date,$day,$month,$addedBy,$userName,$modifiedDate,$image,$tags);
	 while ($stmt->fetch()){
	    $dateAdded=defineTime($month,$day,strtotime($date));
		$row[] = array('id' => $id, 'firstName' => $firstName, 'lastName' => $lastName, 'salutation' => $salutation, 'address' => $address,
		'surburb' => $surburb, 'city' => $city, 'locality' => $locality, 'country' => $country, 'sex' => $sex, 
		'DOB' => $DOB,'assembly' => $assembly,'email' => $email,'phone' => $phone,'dateAdded'=>$dateAdded,'addedBy' => $addedBy,'userName'=>$userName, 'modifiedDate' => $modifiedDate, 'image' => $image,'tags'=>$tags);
	 }
	$stmt->close();
	return ($row);
}

//fetch member names
function fetchMemberNames($name=NULL)
{
	$condition="";
	if($name!=NULL){
		$name=strtolower(trim($name));
		$condition="WHERE firstName like '%".$name."%'";
	}
 	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
	    m.id,
		concat(lastName,' ',firstName) as name
		FROM ".$db_table_prefix."members m
		LEFT JOIN ".$db_table_prefix."countries c on m.country=c.code 
		".$condition."
		ORDER BY lastName asc
		");
	    $stmt->execute();
	
	    $stmt->bind_result($id, $name);
	 while ($stmt->fetch()){
	 
		$row[] = array('id' => $id, 'name' => $name);
	 }
	$stmt->close();
	return ($row);
}



//fetch temporary member details
function fetchTempMembers($userId)
{
 	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
	    m.id,
		firstName,
		lastName,
		salutation,
		address,
		surburb,
		city,
		locality,
		c.name,
		sex,
		DOB,
		assembly,
		email,
		phone,
		addedBy,
		m.modifiedDate,
		image,
		tags	
		FROM ".$db_table_prefix."memberspreview m
		LEFT JOIN ".$db_table_prefix."countries c on m.country=c.code 
		LEFT JOIN ".$db_table_prefix."localities l ON m.locality=l.id
		WHERE m.addedBy=?");
		$stmt->bind_param('i',$userId);
	    $stmt->execute();
		$stmt->store_result();
		$check = $stmt->num_rows;
		if($check != 0){
			$stmt->bind_result($id, $firstName, $lastName, $salutation, $address, $surburb, $city,
			$locality, $country, $sex, $DOB,$assembly, $email,$phone,$addedBy,$modifiedDate,$image,$tags);
			while ($stmt->fetch()){
		 
			$row[] = array('id' => $id, 'firstName' => $firstName, 'lastName' => $lastName, 'salutation' => $salutation, 'address' => $address,
			'surburb' => $surburb, 'city' => $city, 'locality' => $locality, 'country' => $country, 'sex' => $sex, 
			'DOB' => $DOB,'assembly' => $assembly,'email' => $email,'phone' => $phone,'addedBy' => $addedBy, 'modifiedDate' => $modifiedDate, 'image' => $image,'tags'=>$tags);
			 }
			$stmt->close();
			return ($row);
		}
		else{echo "no data";}
}




//fetch member details
function fetchMemberDetails($id=NULL)
{
 if($id!=NULL) {
		$column = "m.id";
		$data = $id;
	}
	$row=array();
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
	    m.id,
	    u.id,
	    u.isAdmin,
		m.firstName,
		m.lastName,
		m.salutation,
		m.address,
		m.surburb,
		m.city,
		m.locality,
		m.country,
		m.sex,
		m.DOB,
		m.assembly,
		m.email,
		m.phone,
		m.addedBy,
		m.modifiedDate,
		m.image,
		m.tags,
		m.familyID,
		m.familyRole,
		m.discipleMaker,
		u.visits
		FROM ".$db_table_prefix."members m 
		LEFT JOIN ".$db_table_prefix."users u
		ON m.id=u.memberId
		WHERE
		$column = ?
		LIMIT 1");
		$stmt->bind_param("i", $data);
	    $stmt->execute();
	
	    $stmt->bind_result($id,$userId,$isAdmin, $firstName, $lastName, $salutation, $address, $surburb, $city,
	    $locality, $country, $sex, $DOB,$assembly, $email,$phone,$addedBy,$modifiedDate,$image,$tags,$familyId,$familyRole,$discipleMaker, $visits);
	 while ($stmt->fetch()){
	 
		$row = array('id' => $id,'userId'=>$userId,'isAdmin'=>$isAdmin, 'firstName' => $firstName, 'lastName' => $lastName, 'salutation' => $salutation, 'address' => $address,
		'surburb' => $surburb, 'city' => $city, 'locality' => $locality, 'country' => $country, 'sex' => $sex, 
		'DOB' => $DOB,'assembly' => $assembly,'email' => $email,'phone' => $phone,'addedBy' => $addedBy, 'modifiedDate' => $modifiedDate, 'image' => $image,'tags'=>$tags,'familyId'=>$familyId,'familyRole'=>$familyRole,'discipleMaker'=>$discipleMaker, 'visits'=>$visits);
	 }
	$stmt->close();
	return ($row);
}


 function insertChatMessage($chatUserID,$chatText){
		$req=$mysqli->prepare("INSERT INTO gantt_chat(chatUserID,ChatText) values(:chatUserID, :chatText)");
		$req->execute(array(
		'chatUserID'=>$chatUserID,
		'chatText'=>$chatText
		));	
	}
	
 function displayMessages(){
		$chatReq=$mysqli->prepare("SELECT * FROM gantt_chat ORDER BY chatID DESC");
		$chatReq->execute();
		
		while($DataChat=$ChatReq->fetch()){
			$userReq=$bdd->prepare("SELECT * FROM gantt_users WHERE UserID=:UserID");
			$userReq->execute(array(
				'userID'=>$DataChat["chatUserID"]
			));
			$DataUser=$userReq['chatUserID'];
			?>
            <span class="UserNames"><?php echo $DataUser['userName'] ;?></span><br/>
             <span class="chatMessages"><?php echo $DataChat['chatText'];?></span>
            <?php	
		}
	}
	
 function countTasksToday(){
	 global $mysqli;
		$query="SELECT  gl.text, DATE_ADD( gl.start_date, INTERVAL (gl.duration-1) DAY) as due_date,gl.progress FROM gantt_tasks gl 
WHERE CURDATE()=DATE_ADD( gl.start_date, INTERVAL (gl.duration-1) DAY)";
		
		
		if ($result = $mysqli->query($query)) {
			$rowcount=mysqli_num_rows($result);
			return $rowcount;
		}
 }
 
 
 //this section deals with functions for formatting dates

function formatDate($date){
$h1 = "2";// Hour for time zone goes here e.g. +7 or -4, just remove the + or -
$hm = $h1 * 60; 
$ms = $hm * 60;
$time=time()+($ms); 

$h=date("H",$date);
$m=date("m",$date);
$today=date("d",$time);
$chatday=date("d",$date);
$returnDate='xx';

$dateDiff=$today-$chatday;



if($dateDiff==0){
	$returnDate="Today"." ".$h.":".$m;
	
}
else{
	if($dateDiff<8){
		$returnDate=date("l H:m",$date);
	}
	else{
		$returnDate=date("Y-m-d H:m",$date);
	}
	
}

 
 return $returnDate;
}


/*----------------------------------------*/
/* functions relating to KPAs and tasks   */
/*----------------------------------------*/


function getRecentTasks($userId){
	global $mysqli,$db_table_prefix,$row; 
	$stmt = $mysqli->prepare("SELECT
				taskId,
				taskTitle,
				lastUpdated,
				UNIX_TIMESTAMP(lastUpdated) AS orderDate
			FROM
				tasks
			WHERE
				userId = ".$userId." AND
				lastUpdated != '0000-00-00 00:00:00'
			ORDER BY
				orderDate DESC
			LIMIT 5");
	$stmt->execute();
	$stmt->bind_result($taskId, $taskTitle,$LastUpdated,$orderDate);
	
	while ($stmt->fetch()){
		$row[] = array('taskId' => $taskId, 'taskTitle' => $taskTitle, 'lastUpdated' => $LastUpdated);
	}
	$stmt->close();
	return ($row);
	
	}


/*------------------------------------------------------------------------*/
/*				FUNCTIONS RELATING TO EVENTS
/*------------------------------------------------------------------------*/

function getTopEvents($nation=null){
	global $mysqli,$db_table_prefix; 
	if($nation!=null){
		$condition="WHERE nation='".$nation."' ";
	}else{
		$condition="WHERE nation='GLB'";
	}
	$time=strtotime(date("Y-m-d"));
	$stmt = $mysqli->prepare("SELECT
				eventId,
				eventTitle,
				eventDesc,
				startDate,
				LEFT(Monthname(startDate),3) as month,
				DAY(startDate) as day,
				endDate,
				eventColor,
				allday,
				image,
				lastUpdated,
				UNIX_TIMESTAMP(startDate) AS orderDate
			FROM			
				".$db_table_prefix."events
			".$condition."
			ORDER BY
				orderDate DESC
			LIMIT 3");
	$stmt->execute();		
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check != 0){

	$stmt->bind_result($eventId, $eventTitle,$eventDesc,$startDate,$month,$day,$endDate,$eventColor,$allday,$image, $LastUpdated,$orderDate);
	
	while ($stmt->fetch()){
		$eventDesc=substr($eventDesc,0,120)."...";
		$row[] = array('eventId' => $eventId, 'eventTitle' => $eventTitle,'eventDesc'=>$eventDesc,
		'startDate'=>$startDate,'month'=>$month,'day'=>$day,'endDate'=>$endDate,'eventColor'=>$eventColor,
		'allday'=>$allday,'image'=>$image, 'lastUpdated' => $LastUpdated);
	}
	$stmt->close();
	return ($row);
	}else{
	  echo 'no events';
	}
	
}

//event registrations
function getEventRegistrations($id=null){
	global $mysqli,$db_table_prefix; 
	$time=strtotime(date("Y-m-d"));
	$stmt = $mysqli->prepare("SELECT
				e.id,
				CONCAT(m.firstName,' ',m.lastName) as name,
				m.name,
				m.localityName,
				e.eventId,
				e.memberId,
				e.arrivalDate,
				e.departureDate,
				e.registeredBy,
				e.remark,
				e.dateRegistered,
				m.city,
				e.transportMode,
				e.entryPort,
				e.pickUpEntry,
				e.dietRestrictions,
				e.dietRestSpec,
				e.medicalForm,
				m.image,
				m.email,
				m.address,
				m.phone
			FROM			
				".$db_table_prefix."event_registration e INNER JOIN 
				(SELECT x.id,l.id as locId,l.localityName,x.firstname,x.lastName,c.name,x.city,x.image,x.address,x.email,x.phone FROM ".$db_table_prefix."members x 
				LEFT JOIN ".$db_table_prefix."localities l ON x.locality=l.id
				LEFT JOIN ".$db_table_prefix."countries c on x.country=c.code
				) m
				ON e.memberId=m.id
				
			WHERE e.eventId=?
			ORDER BY
				name ASC");
	$stmt->bind_param("i", $id);
	$stmt->execute();		
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check != 0){
		$stmt->bind_result($id,
		$name,
		$nation,
		$locality,
		$eventId, 
		$memberId,
		$arrivalDate,
		$departureDate,
		$registeredBy,
		$remark,
		$dateRegistered,
		$city,
		$transportMode,
		$entryPort,
		$pickUpEntry,
		$dietRestrictions,
		$dietRestSpec,
		$medicalForm,
		$image,
		$email,
		$address,
		$phone
	);
	
		while ($stmt->fetch()){
		$row[] = array( 'city'=>$city,
		'id'=>$id,
		'name'=>'<a onclick="showMemberCard('.$memberId.')" >'.$name.'</a>',
		'nation'=>$nation,
		'locality'=>$locality, 
		'eventId' => $eventId, 
		'memberId' => $memberId,
		'registeredBy'=>$registeredBy,
		'remark'=>$remark,
		'dateRegistered'=>$dateRegistered,
		'arrivalDate'=>$arrivalDate,
		'departureDate'=>$departureDate,
		'transMode'=>$transportMode,
		'entryPort'=>$entryPort,
		'pickUpEntry'=>$pickUpEntry,
		'dietRestrictions'=>$dietRestrictions,
		'dietRestSpec'=>$dietRestSpec,
		'medicalForm'=>$medicalForm,
		'image'=>$image,
		'email'=>$email,
		'address'=>$address,
		'phone'=>$phone,
		'actions'=>'<a onclick="eventDeregister('.$id.')" class="btn btn-danger btn-xs "><i class="icon-trash"></i></a>');
		}
	$stmt->close();
	return ($row);
	}
	
}


//fetch all events
function fetchEvents($nation=null,$id=null){
	global $mysqli,$db_table_prefix;

	$condition='';
	// Returning array
	if($nation!=null){
		$data=$nation;
	}
	if($id=="GLB"){
		$condition="OR nation='GLB'";
	}
    $events = array(); 
	$stmt = $mysqli->prepare("SELECT
				eventId,
				eventTitle as title,
				DATE_FORMAT(startDate, '%Y-%m-%d') as start,
				DATE_FORMAT(endDate, '%Y-%m-%d') as end,
				allday,
				eventColor,
				image,
				lastUpdated,
				eventDesc,
				nation,
				UNIX_TIMESTAMP(endDate) AS orderDate
			FROM
				".$db_table_prefix."events
			WHERE nation=? ".$condition."
			ORDER BY end desc
			");
	$stmt->bind_param("s", $data);
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check != 0){
	$stmt->bind_result($eventId, $eventTitle,$startDate,$endDate,$allday,$eventColor,$image, $LastUpdated,$eventDesc,$nation,$orderDate);
	$form="'eventAddForm'";
	while ($stmt->fetch()){
		$route="routie('/admin/event/$eventId')";
		$action=utf8_encode('<a onclick="eventEdit('.$eventId.');show('.$form.')" class="btn btn-primary btn-xs"><i class="icon-pencil"></i></a><a onclick="deleteEvent('.$eventId.')" class="btn btn-danger btn-xs "><i class="icon-trash"></i></a>');
		$row = array('eventId' => $eventId, 'title' => $eventTitle,'linkedTitle'=>'<a onclick="'.$route.';">'.$eventTitle.'</a>', 'start'=>$startDate,'end'=>$endDate,'allday'=>$allday,'color'=>$eventColor,'image'=>$image,'lastUpdated' => $LastUpdated, 'eventDesc'=>$eventDesc,'nation'=>$nation, 'orderDate'=>$orderDate,'action'=>$action);
		array_push($events, $row);
	}
	$stmt->close();
	return $events;
	}else{
		return array('eventId'=>0,'title'=>'no local events', 'status'=>'no events','eventDesc'=>'No Local Events');
	}
	
}

//fetch single event
function fetchEvent($id=null){
	global $mysqli,$db_table_prefix;
	// Returning array
	
    //$events = array(); 
	$stmt = $mysqli->prepare("SELECT
				eventId,
				eventTitle as title,
				DATE_FORMAT(startDate, '%Y-%m-%d') as start,
				DATE_FORMAT(endDate, '%Y-%m-%d') as end,
				allday,
				eventColor,
				image,
				lastUpdated,
				eventDesc,
				nation,
				eventLeader,
				UNIX_TIMESTAMP(lastUpdated) AS orderDate
			FROM
				".$db_table_prefix."events
			WHERE eventId=? 
			");
	$stmt->bind_param("i", $id);
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check != 0){
	$stmt->bind_result($eventId, $eventTitle,$startDate,$endDate,$allday,$eventColor,$image, $LastUpdated,$eventDesc,$nation,$eventLeader,$orderDate);
	
	while ($stmt->fetch()){
		$eventReg=countRegistered($eventId);
		$eventLocalities=countEventLocalities($eventId);
		$eventNations=countEventNations($eventId);
		$row = array('eventId' => $eventId, 'eventLeader'=>$eventLeader,'title' => $eventTitle,'linkedTitle'=>'<a onclick="openPage(20,'.$eventId.')">'.$eventTitle.'</a>', 'start'=>$startDate,'end'=>$endDate,'allday'=>$allday,'color'=>$eventColor,'image'=>$image,'lastUpdated' => $LastUpdated, 'eventDesc'=>$eventDesc,'nation'=>$nation, 'orderDate'=>$orderDate,'registrations'=>$eventReg['registrations'],'localities'=>$eventLocalities['localities'],'nations'=>$eventNations['nations'], 'actions'=>'<a onclick="" class="btn btn-primary btn-xs"><i class="icon-pencil"></i></a><a onclick="" class="btn btn-danger btn-xs "><i class="icon-trash"></i></a>');
		//array_push($events, $row);
	}
	$stmt->close();
	return $row;
	}
	
}

function countRegistered($eventId){
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT count(memberId) as attenders FROM ".$db_table_prefix."event_registration 
		WHERE eventId=?");
		$stmt->bind_param('i', $eventId);
		$result=$stmt->execute();
		$stmt->store_result();
		$check = $stmt->num_rows;
		if($check > 0){
			$stmt->bind_result($registrations);
			while ($stmt->fetch()) {
				$row=array('registrations'=>$registrations);
			}
			$stmt->close();
			return $row;

		}else{
			return 0;
		}
}

function countEventLocalities($eventId){
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT  count(localityName) FROM 
		(SELECT  l.localityName,COUNT(er.memberId) AS members 
			FROM ".$db_table_prefix."event_registration er 
			INNER JOIN ".$db_table_prefix."members m ON er.memberId=m.id
			LEFT JOIN ".$db_table_prefix."localities l on m.locality=l.id
			WHERE eventId=?
			GROUP BY l.localityName 
         ORDER BY members DESC) AS locReg 
     WHERE localityName!='undefined'
		");
		$stmt->bind_param('i', $eventId);
		$result=$stmt->execute();
		$stmt->store_result();
		$check = $stmt->num_rows;
		if($check > 0){
			$stmt->bind_result($localities);
			while ($stmt->fetch()) {
				$row=array('localities'=>$localities);
			}
			$stmt->close();
			return $row;

		}else{
			return 0;
		}
}

function countEventNations($eventId){
	global $mysqli,$db_table_prefix;
	$stmt = $mysqli->prepare("SELECT  count(nationName) FROM 
		(SELECT  n.nationName,COUNT(er.memberId) AS members 
			FROM ".$db_table_prefix."event_registration er 
			INNER JOIN ".$db_table_prefix."members m ON er.memberId=m.id
			LEFT JOIN ".$db_table_prefix."nations n on m.country=n.nationName
			WHERE eventId=?
			GROUP BY n.nationName 
         ORDER BY members DESC) AS locReg 
		");
		$stmt->bind_param('i', $eventId);
		$result=$stmt->execute();
		$stmt->store_result();
		$check = $stmt->num_rows;
		if($check > 0){
			$stmt->bind_result($nations);
			while ($stmt->fetch()) {
				$row=array('nations'=>$nations);
			}
			$stmt->close();
			return $row;

		}else{
			return $row = array('nations' => 0);
		}
}
//get data for a bar chart of members registered by locality
function getEventLocRegBar($eventId){
	global $mysqli,$db_table_prefix;
	$stmt=$mysqli->prepare("SELECT  localityName,members FROM 
		(SELECT  l.localityName,COUNT(er.memberId) AS members 
			FROM ".$db_table_prefix."event_registration er 
			INNER JOIN ".$db_table_prefix."members m ON er.memberId=m.id
			LEFT JOIN ".$db_table_prefix."localities l on m.locality=l.id
			WHERE eventId=?
			GROUP BY l.localityName 
			ORDER BY members DESC
			LIMIT 10) AS locReg 
		ORDER BY localityName ASC
 		
		");
		$stmt->bind_param('i', $eventId);
		$result=$stmt->execute();
		$stmt->store_result();
		$check = $stmt->num_rows;
		if($check > 0){
			$stmt->bind_result($locality,$members);
			while ($stmt->fetch()) {
				if(is_null($locality)){$locality='undefined';}
				$row[]=array('locality'=>$locality,'members'=>$members);
			}
			$stmt->close();
			return $row;

		}
}

function getEventGenderLocRegBar($eventId){
	global $mysqli,$db_table_prefix;
	$stmt=$mysqli->prepare("SELECT lm.id,lm.localityName,Male,Female FROM 
			(SELECT l.id,l.localityName,COUNT(er.memberId) AS Male 
			FROM cmfi_event_registration er 
			INNER JOIN cmfi_members m 
			ON er.memberId=m.id 
			LEFT JOIN cmfi_localities l 
			on m.locality=l.id 
			WHERE eventId=? and sex='M' 
			GROUP BY l.localityName ) AS lm 
		left join 
			(SELECT l.id,l.localityName,COUNT(er.memberId) AS Female 
			FROM cmfi_event_registration er 
			INNER JOIN cmfi_members m 
			ON er.memberId=m.id 
			LEFT JOIN cmfi_localities l 
			on m.locality=l.id 
			WHERE eventId=? and sex='F' 
			GROUP BY l.localityName) as lf 
		on lm.id=lf.id
		ORDER BY female DESC
		LIMIT 10
		");
		$stmt->bind_param('ii', $eventId,$eventId);
		$result=$stmt->execute();
		$stmt->store_result();
		$check = $stmt->num_rows;
		if($check > 0){
			$stmt->bind_result($id,$locality,$male,$female);
			while ($stmt->fetch()) {
				if(is_null($locality)){$locality='undefined';}
				$row[]=array('locality'=>$locality,'male'=>$male,'female'=>$female);
			}
			$stmt->close();
			return $row;

		}
}

//event nation registrations bar
function getEventGenderNationBar($eventId){
	global $mysqli,$db_table_prefix;
	$stmt=$mysqli->prepare("SELECT lm.code,lm.name,Male,Female FROM 
			(SELECT l.code,l.name,COUNT(er.memberId) AS Male 
			FROM cmfi_event_registration er 
			INNER JOIN cmfi_members m 
			ON er.memberId=m.id 
			LEFT JOIN cmfi_countries l 
			on m.country=l.code 
			WHERE eventId=? and sex='M' 
			GROUP BY l.name,l.code ) AS lm 
		left join 
			(SELECT l.code,l.name,COUNT(er.memberId) AS Female 
			FROM cmfi_event_registration er 
			INNER JOIN cmfi_members m 
			ON er.memberId=m.id 
			LEFT JOIN cmfi_countries l 
			on m.country=l.code
			WHERE eventId=? and sex='F' 
			GROUP BY l.name,l.code) as lf 
		on lm.code=lf.code
		ORDER BY female DESC
		LIMIT 20
		");
		$stmt->bind_param('ii', $eventId,$eventId);
		$result=$stmt->execute();
		$stmt->store_result();
		$check = $stmt->num_rows;
		if($check > 0){
			$stmt->bind_result($id,$country,$male,$female);
			while ($stmt->fetch()) {
				if(is_null($country)){$country='undefined';}
				if(is_null($female)){$female=0;}
				if(is_null($male)){$male=0;}
				$row[]=array('country'=>$country,'male'=>$male,'female'=>$female);
			}
			$stmt->close();
			return $row;

		}
}
function getProvinceBar($nation){
	global $mysqli,$db_table_prefix;
	$stmt=$mysqli->prepare("select t2.provinceName,db_disciples,disciples 
		FROM 
			(SELECT count(m.id) as db_disciples,provinceName
			FROM ".$db_table_prefix."members m 
			INNER JOIN ".$db_table_prefix."localities l
			on m.locality=l.id
			LEFT JOIN ".$db_table_prefix."provinces p 
			ON l.province=p.id
			WHERE m.country=?
			GROUP BY provinceName
			ORDER BY provinceName ASC) as t1
		RIGHT JOIN 
			(SELECT provinceName,disciples 
			FROM ".$db_table_prefix."provinces 
			WHERE nation=?) as t2
		ON t1.provinceName=t2.provinceName
		");
		$stmt->bind_param('ss', $nation,$nation);
		$result=$stmt->execute();
		$stmt->store_result();
		$check = $stmt->num_rows;
		if($check > 0){
			$stmt->bind_result($province,$db_disciples,$disciples);
			while ($stmt->fetch()) {
				if($db_disciples==null){$db_disciples=0;}
				if($disciples==null){$disciples=0;}
				$row[]=array('province'=>$province,'db_disciples'=>$db_disciples,'disciples'=>$disciples);
			}
			$stmt->close();
			return $row;
		}
}


function getMemberRegSparkline(){
	global $mysqli,$db_table_prefix;
	$stmt=$mysqli->prepare("SELECT dateAdded,count(id) as members FROM ".$db_table_prefix."members 
		GROUP BY dateAdded
		ORDER by dateAdded desc
		LIMIT 30
		");
		$result=$stmt->execute();
		$stmt->store_result();
		$check = $stmt->num_rows;
		if($check > 0){
			$stmt->bind_result($date,$records);
			while ($stmt->fetch()) {
				$row[]=array('date'=>$date,'records'=>$records);
			}
			$stmt->close();
			return $row;

		}
}


function getKPAs($userId){
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT
				catId,
				catName,
				UNIX_TIMESTAMP(catDate) AS orderDate
			FROM
				categories
			WHERE
				userId = ".$userId." AND
				isActive = 1
			ORDER BY
				orderDate DESC");
    $check = $stmt->num_rows();
	if($check != 0){
	$stmt->execute();
	$stmt->bind_result($catId, $catName, $orderDate);
	
	while ($stmt->fetch()){
		$row[] = array('catId' => $catId, 'catName' => $catName);
	}
	$stmt->close();
	return ($row);
	
	}else{
	  echo '';
	}
	
}
	
	//function to calculate the percentage complete of a parent task
	function percentComplete($task){
		global $mysqli,$db_table_prefix,$row; 
		$query="SELECT sum(taskPercent) as percentage, count(taskPercent) as num FROM tasks where parent=".$task;
		$result=mysqli_query($mysqli,$query) ;
		$row = mysqli_fetch_assoc($result);
		if($row["num"]>0){
		return round( $row["percentage"]/$row["num"],0,1);}
		else{ return "0";}
		
	}
	
		//function to calculate the percentage complete of a Goal
	function percentGoalComplete($goal){
		global $mysqli,$db_table_prefix,$row; 
		$query="SELECT sum(taskPercent) as percentage, count(taskPercent) as num FROM tasks where catId=".$goal;
		$result=mysqli_query($mysqli,$query) ;
		$row = mysqli_fetch_assoc($result);
		if($row["num"]>0){
		return round( $row["percentage"]/$row["num"],0,1);}
		else{ return "0";}
		
	}
	
	//function to calculate percentage progress for a user
function upercentComplete($user){
		global $mysqli,$db_table_prefix,$row; 
		$query="SELECT sum(taskPercent) as percentage, count(taskPercent) as num FROM tasks where userId=".$user;
		$result=mysqli_query($mysqli,$query) ;
		$row = mysqli_fetch_assoc($result);
		if($row["num"]>0){
		return round( $row["percentage"]/$row["num"],0,1);}
		else{ return "0";}
		
	}

	//function to insert a new item into the logs table
	//Create a permission level in DB
function addLog($userId,$logtype,$desc ) {
	global $mysqli,$db_table_prefix; 
	$time=date("Y-m-d G:i:s");
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."logs (
		userId,
		logType,
		description,
		date
		)
		VALUES (
		?,
		?,
		?,
		?
		)");
	$stmt->bind_param("isss", 
						$userId,
						$logtype,
						$desc,
						$time
						);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;
}

//update task percentages for tasks that have nested subtasks
function updateParentPercentage($userId){
global $mysqli;
$query="SELECT parent,(sum(taskPercent)/count(taskPercent)) as taskPerc FROM tasks WHERE parent>0 and userId=".$userId." GROUP BY parent";
$res=$mysqli->query($query);
if(mysqli_num_rows($res)>0){

while($row=mysqli_fetch_array($res)){
$taskPerc=round($row['taskPerc'],2);
$stmt = $mysqli->prepare("
								UPDATE tasks set taskPercent=?
								WHERE
									taskId=?
								LIMIT 1		
			");
			$stmt->bind_param('ss',
								$taskPerc,
								$row['parent']
			);
			$stmt->execute();
			$stmt->close();		

		}

	}
}

//function to send email when an event is created
function emailEvent($team,$author,$eventTitle,$startDate,$endDate,$description)
			{
			global $mysqli;
				$q="select memberID from gantt_team_members where teamID=".$team;
				$res=$mysqli->query($q);
				while($row=mysqli_fetch_array($res)){
					$id=$row['memberID'];
					//get userName
					$u='select userFirst,email from gantt_users where id='.$id;
					$r=$mysqli->query($u);
					$userdetails=mysqli_fetch_assoc($r);
					$userName=$userdetails['userFirst'];
					
					$mail = new questMail();
					
					
					//Setup our custom hooks
					$hooks = array(
						"searchStrs" => array("#AUTHOR#","#USERNAME#","#EVENT-TITLE#","#START-DATE#","#END-DATE#","#DESCRIPTION#"),
						"subjectStrs" => array($author,$userName,$eventTitle,$startDate,$endDate,$description)
						);
					
					if(!$mail->newTemplateMsg("new-event.txt",$hooks))
					{
						$errors[] = lang("MAIL_TEMPLATE_BUILD_ERROR");
					}
					else
					{
						if(!$mail->sendMail($userdetails["email"],"New Event Scheduled"))
						{
							$errors[] = lang("MAIL_ERROR");
						}
						
					}
				}
			}

//function to create template for user appraisal ratings for a user			
function createUserRatings($userId){
	global $mysqli; //echo " UserID=".$userId."<br>";
	$uQuery="select * from gantt_users";
	$userRes=$mysqli->query($uQuery);
	while($urow=mysqli_fetch_array($userRes)){ //iterate through all users
		$company=$urow['companyId'];
		$rQuery="select * from gantt_rating_domains where companyId=".$company;
		$ratingRes=$mysqli->query($rQuery);
		
		while($row=mysqli_fetch_array($ratingRes)){
		$rDomain=$row['rdomainId'];
		$rActive=1;
		$friendId=$urow['id'];//echo $friendId;
		$rating=0;
		$date=date("Y-m-d h:i:s");
		//add new row only if it doesnt exist at all
		//echo checkRatingEntry($userId,$rDomain,$friendId);
		if(checkRatingEntry($userId,$rDomain,$friendId)==0){
		
			$stmt = $mysqli->prepare("INSERT INTO 
										gantt_ratings(
										userId,
										friendId,
										userRating,
										ratingDate,
										companyId,
										rActive,
										rDomain
										)
										VALUES(
										 ?,
										 ?,
										 ?,
										 ?,
										 ?,
										 ?,
										 ?)"
			);
			$stmt->bind_param('sssssss',
								$userId,
								$friendId,
								$rating,
								$date,
								$company,
								$rActive,
								$rDomain
			);
			$stmt->execute();
			$stmt->close();
			}
		}
	}
}

function checkRatingEntry($uId,$domain,$fId){
	global $mysqli;
	$qry="select * from gantt_ratings where userId=".$uId." and friendId=".$fId." and rDomain=".$domain;
	$res=$mysqli->query($qry);
	if(mysqli_num_rows($res)>0){$result=1;}
	else{$result=0;}
	
	return $result;
}

//count members in a nation
function countMembers($nationID){
		$row=array();
		global $mysqli,$emailActivation,$websiteUrl,$db_table_prefix;

		//count members in control
		$stmt = $mysqli->prepare("SELECT 
				disciples,
				leaders,
				localities,
				housechurches,
				provinces,
				date,
				code,
				prayer_rooms
				FROM
					".$db_table_prefix."nation_stats
				WHERE
					code=?
				order by date desc
				limit 1
				");
			$stmt->bind_param("s", $nationID);
			$stmt->execute();
			$stmt->bind_result($members,$leaders,$localities,$housechurches,$provinces,$date,$code,$prayer_rooms);
			while ($stmt->fetch()){
				if($members==null){$members=0;}
				$row = array('members' => $members,
							'leaders'=>$leaders,
							'localities'=>$localities,
							'housechurches'=>$housechurches);
			}
		$stmt->close();
		return $row;
	}


//get latest total number of disciples in country
function countLatestMembers($nationID){
		global $mysqli,$emailActivation,$websiteUrl,$db_table_prefix;
		$stmt = $mysqli->prepare("
					SELECT members FROM vw_nationstats
					WHERE
					nation=?
					LIMIT 1
				
				");
			$stmt->bind_param("s", $nationID);
			$stmt->execute();
			$stmt->bind_result($members);
			while ($stmt->fetch()){
				$row = array('members' => $members);
			}
		$stmt->close();
		return ($row);
	}
//count localities in a nation
function countLocalities($nationID){
		global $mysqli,$emailActivation,$websiteUrl,$db_table_prefix;
		$stmt = $mysqli->prepare("SELECT 
				count(id)
				FROM
					".$db_table_prefix."localities
				WHERE
					country=?
				");
			$stmt->bind_param("s", $nationID);
			$stmt->execute();
			$stmt->bind_result($localities);
			while ($stmt->fetch()){
				$row = array('localities' => $localities);
			}
		$stmt->close();
		return ($row);
	}

//function to write world map data to json file.
function writeMapData() {
	global $mysqli,$emailActivation,$websiteUrl,$db_table_prefix;   

    $res = mysqli_query($mysqli,"select c.code, m.members FROM cmfi_countries c  left join (
	select country,count(id) members from cmfi_members group by country) m
	on c.code=m.country");
	$myFile = "js/maps/gdp-data.js";
	$fh = fopen($myFile, 'w') or die("can't open file");
	$rows = mysqli_num_rows($res);
	$i = 1;
	fwrite($fh,"var gdpData = {");
	while( $rs2 = $res->fetch_array(MYSQLI_ASSOC)) {
		$members=$rs2["members"];
		$code=$rs2["code"];
		if($i==$rows){$comma="\n";} else{$comma=",\n";}
		if($members==null){$members=0;}
		else{$line = '"'.$rs2["code"].'":'.$members.$comma;
		fwrite($fh, $line);
		$i++;}
	}
	fwrite($fh,"};");
	fclose($fh);

}


//function to get mySQL database size
function getDBSize(){
	global $mysqli,$emailActivation,$websiteUrl,$db_table_prefix; 
	mysqli_select_db($mysqli,"wcdb");  
	$q = mysqli_query($mysqli,"SHOW TABLE STATUS");  
	$size = 0;  
	while($row = mysqli_fetch_array($q)) {  
		$size += $row["Data_length"] + $row["Index_length"];  
	}
	$decimals = 2;  
	$mbytes = number_format($size/(1024),$decimals); //for MB number_format($size/(1024*1024),$decimals)
	return $mbytes;
}

//write gauge value for database size
function updateGauge(){
	$myFile = "js/gauge/gauge_demo.js";
	$fh = fopen($myFile, 'w') or die("can't open file");
	$info="
	var opts = {
    lines: 12, // The number of lines to draw
    angle: 0, // The length of each line
    lineWidth: 0.4, // The line thickness
    pointer: {
        length: 0.75, // The radius of the inner circle
        strokeWidth: 0.042, // The rotation offset
        color: '#1D212A' // Fill color
    },
    limitMax: 'false', // If true, the pointer will not go past the end of the gauge
    colorStart: '#1ABC9C', // Colors
    colorStop: '#1ABC9C', // just experiment with them
    strokeColor: '#F0F3F3', // to see which ones work best for you
    generateGradient: true
	};
	var target = document.getElementById('foo'); // your canvas element
	var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
	gauge.maxValue = 5120; // set max gauge value
	gauge.animationSpeed = 32; // set animation speed (32 is default value)
	gauge.set(".ceil(getDBSize())."); // set actual value
	gauge.setTextField(document.getElementById('gauge-text'));
	
	";
	fwrite($fh,$info);
	fclose($fh);
}

//write gauge value for database size
function updateProfileGauge($ctrl_disciples,$db_disciples){
	try{
	$myFile = "js/gauge/nation_gauge.js";
	$fh = fopen($myFile, 'w') or die("can't open file");
	$info="
	var opts = {
    lines: 12, // The number of lines to draw
    angle: 0, // The length of each line
    lineWidth: 0.4, // The line thickness
    pointer: {
        length: 0.75, // The radius of the inner circle
        strokeWidth: 0.042, // The rotation offset
        color: '#1D212A' // Fill color
    },
    limitMax: 'false', // If true, the pointer will not go past the end of the gauge
    colorStart: '#1ABC9C', // Colors
    colorStop: '#1ABC9C', // just experiment with them
    strokeColor: '#F0F3F3', // to see which ones work best for you
    generateGradient: true
	};
	var target = document.getElementById('foo'); // your canvas element
	var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
	gauge.maxValue = ".$ctrl_disciples."; // set max gauge value
	gauge.animationSpeed = 32; // set animation speed (32 is default value)
	gauge.set(".$db_disciples."); // set actual value
	gauge.setTextField(document.getElementById('gauge-text'));
	
	";
	fwrite($fh,$info);
	fclose($fh);
	return 1;
}catch(Exception $e){
	return 0;
}

}

function defineTime($month,$day,$date){
	$h1 = "2";// Hour for time zone goes here e.g. +7 or -4, just remove the + or -
	$hm = $h1 * 60; 
	$ms = $hm * 60;
	$time=time()+($ms); 

	$h=date("H",$date);
	$m=date("m",$date);
	$today=date("d",$time);
	$chatday=date("d",$date);
	$returnDate='xx';

	$dateDiff=$today-$chatday;



	$signInTime=date('Y-m-d G:i:s',$date);
	$year=Date('Y');
	$dayName = array('Sun','Mon','Tue','Wed','Thur','Fri','Sat' );
	$daydiff=date('d',$time)-date('d',$date);
	$monthdiff=date('m',$time)-date('m',$date);
	$yeardiff=date('Y',$time)-date('Y',$date);
	$res='';
	if($yeardiff==0){
		if($monthdiff==0){
			if($daydiff==0){
				//show time only
				$res=date('G:i',$date);
			}elseif($daydiff>0 && $daydiff<6){
				//get day abbr
				$res=$dayName[$day+1]." ".date('G:i',$date);

			}elseif($daydiff>=6 && $daydiff<14){
				$res='Last Week';
			}
		}else{
			$res=$month.' '.Date('d',$date);
		}
	}else{
		$res=date('Y',$time)-date('Y',$date);
	}
	

	return $res;
}


//fetch recent activities
//fetch logs 
function getLogs($nation=null){
	if($nation==null){
		$condition="WHERE logtype!='signIn'";
	}
	else{
		$condition="WHERE LEFT(name,1)='".$nation."'";
	}
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
			l.userId,
			u.memberId,
			l.logtype,
			l.description,
			l.date,
			LEFT(Monthname(l.date),3) as month,
			weekday(l.date) as day,
			concat(hour(l.date),':',minute(l.date)) as hour,
			CONCAT(userFirst, ' ', userLast) as user
		FROM ".$db_table_prefix."logs l
		LEFT JOIN ".$db_table_prefix."users u 
		ON u.id=l.userId ".$condition."
		ORDER BY
				l.date DESC 
		LIMIT 3");
			
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($userId,$memberId,$logtype, $description,$date,$month,$day,$hour,$user);
	$icon="clock";
	while ($stmt->fetch()){
		if($logtype=='new event'||$logtype=='update event'){$icon='calendar';}
		elseif($logtype=='new record'){$icon='clock';}
		$time=defineTime($month,$day,strtotime($date));
		$row[] = array('icon'=>$icon,'id'=>$userId,'memberId'=>$memberId,'title' => $logtype, 'description' => $description,'date'=>$date,'user'=>$user,'month'=>$month,'day'=>$day,'time'=>$time);
	}
	$stmt->close();
	return ($row);
	}else{
		return 0;
	}
}


//Function to fetch data pertaining to nations
//fetch nations 
function getNations($nation=null){
	if($nation==null){
		$condition="";
	}
	else{
		$condition="WHERE LEFT(name,1)='".$nation."'";
	}
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
			nationID,
			code,
			name,
			region,
			continent,
			CONCAT(firstName, ', ', LastName) as Leader, 
			m.id,
			CONCAT(m.address,', ',m.surburb,', ',m.city) as address,
			m.phone, 
			m.email,
			m.image
		FROM ".$db_table_prefix."nations n
		LEFT JOIN ".$db_table_prefix."countries c 
		ON n.nationName=c.code
		LEFT JOIN ".$db_table_prefix."members m 
		ON m.id=n.nationLeader ".$condition."
		ORDER BY
				c.name ASC");
			
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($nationID,$code, $name,$region,$continent,$leader,$leaderId,$address,$phone,$email,$image);
	$members=0;$localities=0;$housechurches=0;
	while ($stmt->fetch()){
		$code_="'".$code."'";
		$nationName="'".$name."'";
		$housechurches=0;$localities=0;$members=0;$leaders=0;
		$count=countMembers($code);
		if(isset($count['members'])){$members=$count['members'];}
		if(isset($count['localities'])){$localities=$count['localities'];}
		if(isset($count['housechurches'])){$housechurches=$count['housechurches'];}
		if(isset($count['leaders'])){$leaders=$count['leaders'];}
		$route="routie('/disciple/".$leaderId."')";
		$lnkleader='<a onClick="'.$route.'">'.$leader.'</a>';

		if($image=="" && $leaderId>0){$image='user.png';}
		$image_='<a class="user-profile" onClick="openPage(23,'.$leaderId.')"><img src="images/members/'.$image.'" alt="" ></a>';
		$route="routie('/$code');";
		$actions='<a onclick="'.$route.'" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-pencil"></i></a><a onclick="deleteNation('.$nationID.','.$nationName.')"  class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></a>';
		$row[] = array('id'=>$nationID,'code' => $code, 'name' => $name,'region'=>$region,'continent'=>$continent,'members'=>$members,'localities'=>$localities,'housechurches'=>$housechurches,'leader'=>$leader,
		'leaderLinked'=>$lnkleader,'address'=>$address,'phone'=>$phone,'email'=>$email,'image'=>$image_);
	}
	$stmt->close();
	return ($row);
	}else{
		return 0;
	}
}

function getNationDisciples($nation=null){
	if($nation==null){
		$condition="";
	}
	else{
		$condition="WHERE LEFT(name,1)='".$nation."'";
	}
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
			code,name
		FROM ".$db_table_prefix."countries 
		");
			
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($code, $name);
	while ($stmt->fetch()){
		$code_="'".$code."'";
		$nationName="'".$name."'";
		

	}
	$stmt->close();
	return ($row);
	}else{
		return 0;
	}
}


function getTopNations(){

	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
			code,name
		FROM ".$db_table_prefix."countries 
		");
			
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($code, $name);

	while ($stmt->fetch()){
		$code_=$code;
		$m=countMembers($code);
		if(isset($m['members'])){
			$members=countMembers($code)['members'];
		}else{
			$q="SELECT id FROM ".$db_table_prefix."members WHERE country like '".$code."'";
			$r=mysqli_query($mysqli, $q);
			$members=mysqli_num_rows($r);
		}
		
		if($members>0){
			$m=number_format($members);
			$row[]= array('members'=>$members,'m'=>$m,'code' => $code, 'name'=>$name);
		}
	}
	$stmt->close();
	rsort($row);
	$res=array_slice($row,0,6,true);
	return ($res);
	}else{
		return 0;
	}
}

function getNationsDash(){

	global $mysqli,$db_table_prefix; 
	$row=array();
	$stmt = $mysqli->prepare("SELECT 
			code,name
		FROM ".$db_table_prefix."countries 
		");
			
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($code, $name);

	$myFile = "js/maps/gdp-data1.js";
	$fh = fopen($myFile, 'w') or die("can't open file");
	$i = 1;
	fwrite($fh,"var gdpData = {");

	while ($stmt->fetch()){
		$q="SELECT id FROM ".$db_table_prefix."members WHERE country like '".$code."'";
			$r=mysqli_query($mysqli, $q);
			$members=mysqli_num_rows($r);
		$code_=$code;
		$m=countMembers($code);
		if(isset($m['members'])&&$m['members']>$members){
			//$x=$m['members'];
			//if($memmbers<=intval($x)){$members=$x;}
			$members=$m['members'];
		}//else{
		//	$members=0;
		//}
		//if($members==null){$members=0;}
		$row[]= array('code' =>$code , 'members'=>$members);

		if($i==$check){$comma="\n";} else{$comma=",\n";}
		//if($members==null){$members=0;}
		$line = '"'.$code.'":'.$members.$comma;
		fwrite($fh, $line);
		$i++;
	

	}
	$stmt->close();
	fwrite($fh,"};");
	fclose($fh);
	return $row;
	}else{
		return 0;
	}
}

//fetch nations 
function getNationDetails($nation=null){
		$condition="WHERE LEFT(name,1)='".$nation."'";
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
			nationID,
			code,
			name,
			region,
			continent,
			CONCAT(firstName, ', ', LastName) as Leader,
			nationLeader, 
			CONCAT(m.address,', ',m.surburb,', ',m.city) as address,
			m.phone, 
			m.email,
			nationDate,
			m.image
		FROM ".$db_table_prefix."nations n
		LEFT JOIN ".$db_table_prefix."countries c 
		ON n.nationName=c.code
		LEFT JOIN ".$db_table_prefix."members m 
		ON m.id=n.nationLeader 
		WHERE code=?
		");
	$stmt->bind_param('s',$nation);		
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($nationID,$code, $name,$region,$continent,$leader,$leaderId,$address,$phone,$email,$date,$image);
	
	while ($stmt->fetch()){
		$row = array('id'=>$nationID,'code' => $code, 'name' => $name,'region'=>$region,'continent'=>$continent,
		'leader'=>$leader,'leaderId'=>$leaderId, 'address'=>$address,'phone'=>$phone,'email'=>$email,'date'=>$date,'image'=>$image);
	}
	$stmt->close();
	return ($row);
	}else{
		return null;
	}
}

//get total statistics for a nation
function getNationTotals($code){
		$disciples=getTotalMembers($code);
		$localities=countLocalities($code)['localities'];
		$housechurches=countHouseChurches($code);
		$ctrlDisciples=getctrlTotalDisciples($code);
		$provinces=getTotalProvinces($code);
		//$x=updateProfileGauge($ctrlDisciples,$disciples);
		return $stats = array('disciples' =>$disciples,'localities'=>$localities,'housechurches'=>$housechurches,'provinces'=>$provinces,'ctrlDisciples'=>$ctrlDisciples );
}

function getGlobalTotals(){
	global $mysqli,$db_table_prefix;
			//total members
			$disciples=getTotalMembers();
			//Count localities
			$localities=getTotalLocalities();
			//count nations
			$q2 = "SELECT * FROM ".$db_table_prefix."nations";
			$r2 = mysqli_query($mysqli, $q2);
			$nations=mysqli_num_rows($r2);
			
			//count events
			$q3="SELECT * FROM ".$db_table_prefix."events WHERE nation='GLB' and endDate >now()";
			$r3=mysqli_query($mysqli, $q3);
			$events=mysqli_num_rows($r3);

			//count house churches
			$q4="SELECT * FROM ".$db_table_prefix."housechurches";
			$r4=mysqli_query($mysqli, $q4);
			$housechurches=mysqli_num_rows($r4);

			//count leaders
			$q5="SELECT * FROM ".$db_table_prefix."members WHERE tags!='' ";
			$r5=mysqli_query($mysqli, $q5);
			$leaders=mysqli_num_rows($r5);

			//get database size
			$dbSize=getDBSize();


			$m=0;$lo=0;$le=0;$hc=0;

			$stmt = $mysqli->prepare("SELECT 
						code,name
					FROM ".$db_table_prefix."countries 
					");
						
				$stmt->execute();
				$stmt->store_result();
				$check = $stmt->num_rows;
				if($check !=0){
					$stmt->bind_result($code, $name);
					while ($stmt->fetch()){
						$count=countMembers($code);
						if(isset($count['members'])){$m=$m+$count['members'];}
						if(isset($count['members'])){$lo=$lo+$count['localities'];}
						if(isset($count['members'])){$le=$le+$count['leaders'];}
						if(isset($count['members'])){$hc=$hc+$count['housechurches'];}
						$row = array('disciples' =>$m,'localities'=>$lo,'housechurches'=>$hc,'nations'=>$nations,'events'=>$events, 'leaders'=>$le,'dbsize'=>$dbSize );
					}
			$stmt->close();
			return ($row);
			}else{
				return 0;
			}
}

//get regions
function getRegions(){
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		id,
		code,
		name	
	FROM ".$db_table_prefix."regions 
	ORDER BY name asc");
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($id,$code,$name);
	
	while ($stmt->fetch()){
		$row[] = array('id'=>$id,'code'=>$code,'name'=>$name);
	}
	$stmt->close();
	return ($row);
	}else{
		return 0;
	}
}

//fetch nation code 
function getNationCode($name=null){
	 if($name!=NULL) {
		$column = "name";
		$data=$name;
	}
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
			code,
			name
			
		FROM ".$db_table_prefix."countries c INNER JOIN 
		".$db_table_prefix."nations n
		ON c.code=n.nationName
		WHERE
		$column=? 
		LIMIT 1
		");
	$stmt->bind_param("s", $data);		
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($code,$name);
	while ($stmt->fetch()){
		$row = array('code' => $code,'name'=>$name);
	}
	$stmt->close();
	return ($row);
	}else{
		return $row = array('code' => null,'name'=>$data);
	}
}

//fetch details of a nation 
function getNation($id=null){
	 if($id!=NULL) {
		$column1 = "nationID";
		$column2 = "code";
		$data1 = $id;
		$data2=$id;
	}
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
			nationID,
			code,
			name,
			region,
			continent,
			CONCAT(firstName, ', ', LastName) as Leader, 
			CONCAT(m.address,', ',m.surburb,', ',m.city) as address,
			m.phone, 
			m.email,
			m.image,
			nationDate,
			nationLeader
		FROM ".$db_table_prefix."nations n
		LEFT JOIN ".$db_table_prefix."countries c 
		ON n.nationName=c.code
		LEFT JOIN ".$db_table_prefix."members m 
		ON m.id=n.nationLeader
		WHERE
		$column1=? OR $column2=?
		LIMIT 1
		");
	$stmt->bind_param("is", $data1,$data2);		
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($nationID,$code, $name,$region,$continent,$leader,$address,$phone,$email,$image,$nationDate,$nationLeader);
	while ($stmt->fetch()){
		$row = array('id'=>$nationID,'code' => $code, 'name' => $name,'region'=>$region,'continent'=>$continent,
		'leader'=>$leader,'address'=>$address,'phone'=>$phone,'email'=>$email,'image'=>$image,'nationDate'=>$nationDate,'nationLeader'=>$nationLeader);
	}
	$stmt->close();
	return ($row);
	}else{
		return 0;
	}
}




//fetch report for a nation
function getReports($id=null){
	$condition="";
	if($id!=null){$condition="WHERE r.province=".$id;}
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT
			r.id ,
			r.province,
			r.date,
			r.members,
			r.housechurches,
			r.localities,
			r.leaders,
			r.churches,
			r.user,
			r.notes,
			p.provinceName,
			p.nation
			
		FROM ".$db_table_prefix."nationalreports r 
		LEFT JOIN ".$db_table_prefix."provinces p 
		ON r.province=p.id ".$condition);
   	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($id,$provinceId, $date,$members,$hsech,$loc,$lead,$chch,$user,$notes,$name,$nation);
	
	while ($stmt->fetch()){
		$action='<a  onclick="statsEdit('.$id.');" class="btn btn-success btn-xs"><i class="icon-pencil"></i></a><a onclick="deleteReport('.$id.')" class="btn btn-danger btn-xs"><i class="icon-trash"></i></a>';
		$row[] = array('id' => $id,'provinceId'=>$provinceId, 'date'=>$date,'members'=>$members,'housechurches'=>$hsech,'localities'=>$loc,
		'leaders'=>$lead,'churches'=>$chch,'user'=>$user,'notes'=>$notes,'name' => $name,'nation'=>$nation,'action'=>$action);
	}
	$stmt->close();
	return ($row);
	}else{
		return 0;
	}
	
}
//fetch provinces for a nationID
function getProvinces($nationID){
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
			p.id,
			provinceName,
			m.id,
			m.firstName,
			m.lastName ,
			p.tags,
			p.disciples
		FROM ".$db_table_prefix."provinces p
		LEFT JOIN ".$db_table_prefix."members m 
		ON p.leader=m.id
		WHERE nation=? 
		order by provinceName 
				 ASC");
   
	$stmt->bind_param("s", $nationID);
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($id, $provinceName,$memberId,$firstName,$lastName,$tags,$disciples);
	$form="'provinceAddForm'";
	while ($stmt->fetch()){
		$row[] = array('id' => $id,'provinceName'=>$provinceName, 'name' => '<a onclick="openPage(24,'.$id.')" >'.$provinceName.'</a>', 'memberId'=>$memberId, 'firstName'=>$firstName,'lastName'=>$lastName,'tags'=>$tags, 'leaderName'=>'<a onclick="openPage(23,'.$memberId.')" cursor="pointer">'.$firstName.' '.$lastName.'</a>','actions'=>'<a  onclick="provinceEdit('.$id.'); show('.$form.')" class="btn btn-primary btn-xs"><i class="icon-pencil"></i></a>
			<a onclick="deleteProvince('.$id.')" class="btn btn-danger btn-xs"><i class="icon-trash"></i></a>','disciples'=>$disciples);
	}
	$stmt->close();
	return ($row);
	}else{
		return 0;
	}
	
}

//fetch single  province for using id
function getProvince($id){
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
			p.id,
			provinceName,
			m.id,
			m.firstName,
			m.lastName ,
			p.tags,
			p.disciples
		FROM ".$db_table_prefix."provinces p
		LEFT JOIN ".$db_table_prefix."members m 
		ON p.leader=m.id
		WHERE p.id=? 
		LIMIT 1
				");
   
	$stmt->bind_param("i", $id);
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($id, $provinceName,$memberId,$firstName,$lastName,$tags,$disciples);
	while ($stmt->fetch()){
		$row = array('id' => $id, 'name' => $provinceName, 'memberId'=>$memberId, 'firstName'=>$firstName,'lastName'=>$lastName,'tags'=>$tags, 'leaderName'=>'<a onclick="openPage(26,'.$memberId.')" cursor="pointer">'.$firstName.' '.$lastName.'</a>','disciples'=>$disciples);
	}
	$stmt->close();
	return $row;
	}else{
		return 0;
	}
	
}
	
//fetch localities for a province
function getLocalities($provinceID,$nationID){
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
			id,
			localityName 
		FROM ".$db_table_prefix."localities 
		WHERE province=? 
		and country=?
		order by localityName 
				 ASC");
   
	$stmt->bind_param("is", $provinceID,$nationID);
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($id, $localityName);
	
	while ($stmt->fetch()){
		$row[] = array('id' => $id, 'localityName' => $localityName);
	}
	$stmt->close();
	return ($row);
	}else{
		return 0;
	}
	
}

//fetch localities for a province
function getAllLocalities($nationID){
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
			id,
			localityName 
		FROM ".$db_table_prefix."localities 
		WHERE country=?
		order by localityName 
				 ASC");
   
	$stmt->bind_param("s",$nationID);
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($id, $localityName);
	
	while ($stmt->fetch()){
		$row[] = array('id' => $id, 'localityName' => $localityName);
	}
	$stmt->close();
	return ($row);
	}else{
		return 0;
	}
	
}

//fetch house churches
function fetchHouseChurches($nationID){
	global $mysqli,$db_table_prefix; 
	$leaders="";
	$stmt = $mysqli->prepare("SELECT 
			h.id,
			f.familyName,
			concat(f.address,', ',f.surburb) as address,
			f.city,
			h.leaderid
			 
		FROM ".$db_table_prefix."housechurches h
		INNER JOIN ".$db_table_prefix."families f
		ON h.familyid=f.id
		WHERE nation=?
		order by f.city 
				 ASC");
   
	$stmt->bind_param("s",$nationID);
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($id, $familyName,$adress,$city,$leaderId);

	
	$form="'hcAddForm'";
	while ($stmt->fetch()){
			//split the leaderId array
			$leaderArray = preg_split('/\,/', $leaderId);
			//-- just output
			foreach ($leaderArray as $item) {
				$x=intval($item);
				$person=fetchPersonName($x);
				$leaders=$leaders.'<a onclick="openPage(23,'.$x.')" cursor="pointer">'.$person["firstName"].' 
				'.$person["lastName"].'</a> ,';
			}
		$row[] = array('id' => $id, 'familyName' => $familyName, 'address'=>$adress,'city'=>$city,
			'leaderId'=>$leaderId,'leaderName'=>$leaders,
			'actions'=>'<a  onclick="houseChurchEdit('.$id.');show('.$form.')" class="btn btn-primary btn-xs">
			<i class="icon-pencil"></i></a>
			<a onclick="deleteHouseChurch('.$id.')" class="btn btn-danger btn-xs"><i class="icon-trash"></i></a>');
		$leaders="";
	}
	$stmt->close();
	return ($row);
	}else{
		return 0;
	}
	
}

//fetch single house churche
function getHouseChurch($id){
	global $mysqli,$db_table_prefix; 
	$leaders="";
	$stmt = $mysqli->prepare("SELECT 
			h.id,
			f.familyName,
			h.familyid,
			concat(f.address,', ',f.surburb) as address,
			f.city,
			h.datePlanted,
			h.locality,
			h.leaderid
			 
		FROM ".$db_table_prefix."housechurches h
		INNER JOIN ".$db_table_prefix."families f
		ON h.familyid=f.id
		WHERE h.id=?
		LIMIT 1");
   
	$stmt->bind_param("s",$id);
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($id, $familyName,$familyid,$adress,$city,$startDate,$locality,$leaderId);

	while ($stmt->fetch()){
			//split the leaderId array
			$leaderArray = preg_split('/\,/', $leaderId);
			//-- just output
			foreach ($leaderArray as $item) {
				$x=intval($item);
				$person=fetchPersonName($x);
				$leaders=$leaders.'<a onclick="openPage(23,'.$x.')" cursor="pointer">'.$person["firstName"].' '.$person["lastName"].'</a> ,';
			}
		$row = array('id' => $id, 'familyName' => $familyName,'familyid'=>$familyid, 'address'=>$adress,'city'=>$city,'startDate'=>$startDate,'locality'=>$locality, 'leaderId'=>$leaderId,'leaders'=>$leaderArray);
		$leaders="";
	}
	$stmt->close();
	return ($row);
	}else{
		return 0;
	}
	
}

//fetch name of a person given the member Id
 function fetchPersonName($id=NULL){
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
			id,
			firstName,
			lastName 
		FROM ".$db_table_prefix."members 
		WHERE id=?
		LIMIT 1
		");
   
	$stmt->bind_param("i",$id);
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	$stmt->bind_result($id, $firstname,$lastName);
	
	while ($stmt->fetch()){
		$row = array('id' => $id, 'firstName' => $firstname,'lastName'=>$lastName);
	}
	$stmt->close();
	return;
	
}

//fetch localities for a province
function fetchLocality($id=null){
	global $mysqli,$db_table_prefix; $leaders="";
	$stmt = $mysqli->prepare("
	SELECT 
		l.id,
		c.name as nation, 
		l.localityName,
		p.provinceName,
		datePlanted,
		m.id,
		p.id 
	FROM ".$db_table_prefix."localities l 
	LEFT JOIN ".$db_table_prefix."countries c ON l.country=c.code
	LEFT JOIN ".$db_table_prefix."provinces p ON l.province=p.id
	LEFT JOIN ".$db_table_prefix."members m ON l.leader=m.id
	WHERE l.id=?
	LIMIT 1"
	);
	$stmt->bind_param("i",$id);
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($id,$nation, $localityName,$provinceName,$datePlanted,$leaderId,$provinceId);
	$form="'addLocalityForm'";
	while ($stmt->fetch()){
		$leaderArray = preg_split('/\,/', $leaderId);
			//-- just output
			foreach ($leaderArray as $item) {
				$x=intval($item);
				$person=fetchPersonName($x);
				$leaders=$leaders.'<a onclick="openPage(23,'.$x.')" cursor="pointer">'.$person["firstName"].' '.$person["lastName"].'</a> ,';
			}
		$row = array('id' => $id, 'nation'=>$nation,'localityName' => $localityName,'provinceName'=>$provinceName,'datePlanted'=>$datePlanted, 'leaderId'=>$leaderId,'leaders'=>$leaderArray, 'provinceId'=>$provinceId);
	}
	$stmt->close();
	return ($row);
	}else{
		return 0;
	}
	
}

//fetch localities for a province
function fetchLocalities($nationID=null){
	global $mysqli,$db_table_prefix; 
	if ($nationID==null){
		$stmt=$mysqli->prepare("SELECT l.id,c.name as nation, l.localityName,p.provinceName,datePlanted,concat(m.firstname,', ',m.lastName) as locality_leader,m.id 
		FROM ".$db_table_prefix."localities l 
		LEFT JOIN ".$db_table_prefix."countries c ON l.country=c.code
		LEFT JOIN ".$db_table_prefix."provinces p ON l.province=p.id
		LEFT JOIN ".$db_table_prefix."members m ON l.leader=m.id
	");
	}
	else{
	$stmt = $mysqli->prepare("
	SELECT l.id,c.name as nation, l.localityName,p.provinceName,datePlanted,concat(m.firstname,', ',m.lastName) as locality_leader, m.id 
	FROM ".$db_table_prefix."localities l 
	LEFT JOIN ".$db_table_prefix."countries c ON l.country=c.code
	LEFT JOIN ".$db_table_prefix."provinces p ON l.province=p.id
	LEFT JOIN ".$db_table_prefix."members m ON l.leader=m.id
	WHERE c.code=?
	order by localityName 
				 ASC"
	);
	$stmt->bind_param("s",$nationID);
	}
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($id,$nation, $localityName,$provinceName,$datePlanted,$locality_leader,$leaderId);
	$form="'localityAddForm'";
	while ($stmt->fetch()){
		$row[] = array('id' => $id, 'nation'=>$nation,'localityName' => $localityName,'provinceName'=>$provinceName,'datePlanted'=>$datePlanted, 'locality_leader'=>$locality_leader, 'leaderId'=>$leaderId, 
			'actions'=>'<a  onclick="localityEdit('.$id.');show('.$form.')" class="btn btn-primary btn-xs"><i class="icon-pencil"></i></a>
			<a onclick="deleteLocality('.$id.')" class="btn btn-danger btn-xs"><i class="icon-trash"></i></a>');
	}
	$stmt->close();
	return ($row);
	}else{
		return 0;
	}
	
}
//fetch all countries 
function getCountries(){
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
			code,
			name 
		FROM ".$db_table_prefix."countries 
		order by name 
				 ASC");
   	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($code, $name);
	
	while ($stmt->fetch()){
		$row[] = array('code' => $code, 'name' => $name);
	}
	$stmt->close();
	return ($row);
	}else{
		return 0;
	}
	
}

//fetch all countries  with provinces
function getCountryWithProvince(){
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
			code,
			name 
		FROM ".$db_table_prefix."countries c
		INNER JOIN ".$db_table_prefix."provinces p
		ON c.code=p.nation
		GROUP BY code
		order by name 
				 ASC");
   	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($code, $name);
	
	while ($stmt->fetch()){
		$row[] = array('code' => $code, 'name' => $name);
	}
	$stmt->close();
	return ($row);
	}else{
		return 0;
	}
	
}


//functions that return statistics

//get percent change in members
function getChangesMembers($country)
	{
		global $mysqli,$db_table_prefix;
		if ($country=="")
		{
			$thisYr=date('Y'); $lastYr=date('Y'-1);
			// Open members & Count
			$q1 = "SELECT count(*) as n FROM ".$db_table_prefix."members where dateAdded like '".$thisYr."%'";
			$q2 = "SELECT count(*) as n FROM ".$db_table_prefix."members";
			$r1 = mysqli_query($mysqli, $q1);
			$r2 = mysqli_query($mysqli, $q2);
			$thisYrMembers = mysqli_fetch_assoc($r1);
			$totalMembers = mysqli_fetch_assoc($r2);
			$Change=($thisYrMembers["n"]/($totalMembers["n"]-$thisYrMembers["n"]))*100;
			
			
		}else{
			$thisYr=date('Y'); $lastYr=date('Y')-1;
			// Open members & Count
			$q1 = "SELECT count(*) FROM ".$db_table_prefix."members where country='".$country."' and dateAdded like '".$thisYr."%'";
			$q2 = "SELECT count(*) FROM ".$db_table_prefix."members where country='".$country."' and dateAdded like '".$lastYr."%'";
			$r1 = mysqli_query($mysqli, $q1);
			$r2 = mysqli_query($mysqli, $q2);
			$thisYrNum = mysqli_fetch_assoc($r1);
			$lastYrNum = mysqli_fetch_assoc($r2);
			$Change=$thisYrNum[0]-$lastYrNum[0];
			
		}
		return $Change;
	}
//get total members as per reports
function getMembers($country=null)
{
	global $mysqli,$db_table_prefix;
	// Open members & Count
			$stmt = $mysqli->prepare("select members from vw_nationstats
			WHERE nation=?
			LIMIT 1");
			$stmt->bind_param('s', $country);
			$stmt->execute();
			$stmt->store_result();
			$check = $stmt->num_rows;
	if($check !=0){
		$stmt->bind_result($total);
		
		while ($stmt->fetch()){
			$result =  $total;
		}
		$stmt->close();
		return ($result);
	}else{
		return 0;
	}
}

//get total members
function getTotalMembers($country=null)
	{
		global $mysqli,$db_table_prefix;
		if ($country=="")
		{
			// Open members & Count
			$q1 = "SELECT * FROM ".$db_table_prefix."members";
			$r1 = mysqli_query($mysqli, $q1);
			$totMembers = mysqli_num_rows($r1);
			
		}else{
			// Open members & Count
			$q1 = "SELECT * FROM ".$db_table_prefix."members WHERE country='".$country."'";
			$r1 = mysqli_query($mysqli, $q1);
			$totMembers = mysqli_num_rows($r1);
			
		}
		return $totMembers;
	}

//get total members
function getTotalHouseChurches($country=null)
	{
		global $mysqli,$db_table_prefix;
		if ($country=="")
		{
			// Open members & Count
			$q1 = "SELECT * FROM ".$db_table_prefix."housechurches";
			$r1 = mysqli_query($mysqli, $q1);
			$total = mysqli_num_rows($r1);
			
		}else{
			// Open members & Count
			$stmt = $mysqli->prepare("SELECT count(h.id) as total FROM ".$db_table_prefix."housechurches h
			INNER JOIN ".$db_table_prefix."families f
			ON h.familyid=f.id
			WHERE f.nation=?");
			$stmt->bind_param('s', $country);
			$stmt->execute();
			$stmt->store_result();
			$check = $stmt->num_rows;
	if($check !=0){
		$stmt->bind_result($total);
		
		while ($stmt->fetch()){
			$result =  $total;
		}
		$stmt->close();
		return ($result);
	}else{
		return 0;
	}
}
}
		
	
//get total localities in a country	
	function getTotalLocalities($country=null)
	{
		global $mysqli,$db_table_prefix;
		if ($country=="")
		{
			// Open localities & Count
			$q1 = "SELECT * FROM ".$db_table_prefix."localities";
			$r1 = mysqli_query($mysqli, $q1);
			$totLocalities = mysqli_num_rows($r1);	
		}
		else{
				// Open localities & Count
			$q1 = "SELECT * FROM ".$db_table_prefix."localities WHERE country='".$country."'";
			$r1 = mysqli_query($mysqli, $q1);
			$totLocalities = mysqli_num_rows($r1);
		}
		return $totLocalities;
	}
//get total number of provinces in a country
function getTotalProvinces($country)
	{
		global $mysqli,$db_table_prefix;
		if ($country=="")
		{
			// Open localities & Count
			$q1 = "SELECT * FROM ".$db_table_prefix."provinces";
			$r1 = mysqli_query($mysqli, $q1);
			$totLocalities = mysqli_num_rows($r1);	
		}
		else{
				// Open localities & Count
			$q1 = "SELECT * FROM ".$db_table_prefix."provinces WHERE nation='".$country."'";
			$r1 = mysqli_query($mysqli, $q1);
			$totLocalities = mysqli_num_rows($r1);
		}
		return $totLocalities;
	}

//Update Site Settings
function updateSetting($id,$name,$value)
	{
		global $mysqli,$db_table_prefix;
		$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."configuration 
		SET  
			name = ?, 
			value=? 
		WHERE id=? 
		LIMIT 1");
		$stmt->bind_param('ssi', $name,$value,$id);
		$stmt->execute();
		$stmt->close();
		return 1;
	}

// get settings from database
function getSettings(){
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
			id,
			name,
			value 
		FROM ".$db_table_prefix."configuration
		order by name 
				 ASC");
   	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($id,$name, $value);
	
	while ($stmt->fetch()){
		$row[] = array('id'=>$id,'name' => $name, 'value' => $value);
	}
	$stmt->close();
	return ($row);
	}else{
		return 0;
	}
	
}

// get pages from database
function getPages(){
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
			id,
			page,
			private,
			pageId,
			active 
		FROM ".$db_table_prefix."pages
		order by page 
				 ASC");
   	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($id,$page,$private,$pageId,$active);
	
	while ($stmt->fetch()){
		$row[] = array('id'=>$id,'page' => $page,'private'=>$private,'pageId'=>$pageId,'active'=>$active,'actions'=>'');
	}
	$stmt->close();
	return ($row);
	}else{
		return 0;
	}
	
}

//Clear temp table
function ClearTemp($userId) {
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."memberspreview WHERE addedBy=".$userId);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;
}

//return list of tables in DB
function showTables(){
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SHOW TABLES ");
   	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	if($check !=0){
	$stmt->bind_result($table);
	
	while ($stmt->fetch()){
		$row[] = array('table' => $table);
	}
	$stmt->close();
	return ($row);
	}else{
		return 0;
	}
	
	
}


/* backup the db OR just a table */
/* backup the whole db by default ('*') OR a single table ('tableName') */
function backup_tables($compression,$tables = '*') {
	global $mysqli;
	$conn=$mysqli;
	$BACKUP_PATH="../controllers/backup/";
	//get all of the tables
	if($tables == '*') {
		$tables = array();
		$result = mysqli_query($conn,'SHOW TABLES');
		while ($row = mysqli_fetch_array($result, MYSQLI_NUM)) {
			$tables[] = $row[0];
		}
	} else {
		$tables = is_array($tables) ? $tables : explode(',',$tables);
	}

	//cycle through data
	$return = "";
	foreach($tables as $table) {
		$result = mysqli_query($conn,'SELECT * FROM '.$table);
		$num_fields = mysqli_num_fields($result);

		$return.= 'DROP TABLE IF EXISTS '.$table.';';
		$row2 = mysqli_fetch_row(mysqli_query($conn,'SHOW CREATE TABLE '.$table));
		$return.= "\n\n".$row2[1].";\n\n";

		$return.= 'INSERT INTO '.$table." (";
		$cols = mysqli_query($conn,"SHOW COLUMNS FROM ".$table);
		$count = 0;
		while ($rows = mysqli_fetch_array($cols, MYSQLI_NUM)) {
			$return.= $rows[0];
			$count++;
			if ($count < mysqli_num_rows($cols)) {
				$return.= ",";
			}
		}
		$return.= ")".' VALUES';
		for ($i = 0; $i < $num_fields; $i++) {
			$count = 0;
			while($row = mysqli_fetch_row($result)) {
				$return.= "\n\t(";
				for($j=0; $j<$num_fields; $j++) {
					$row[$j] = addslashes($row[$j]);
					//$row[$j] = preg_replace("\n","\\n",$row[$j]);
					if (isset($row[$j])) {
						$return.= '"'.$row[$j].'"' ;
					} else {
						$return.= '""';
					}
					if ($j<($num_fields-1)) {
						$return.= ',';
					}
				}
				$count++;
				if ($count < mysqli_num_rows($result)) {
					$return.= "),";
				} else {
				$return.= ");";
				}
			}
		}
		$return.="\n\n\n";
	}
	//save file
	if ($compression==1) {
		$zp = gzopen($BACKUP_PATH . 'db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql.gz', "w9");
		gzwrite($zp, $return);
		gzclose($zp);
		return "success";
	} else {
		$handle = fopen($BACKUP_PATH . 'db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql','w+');
		fwrite($handle,$return);
		fclose($handle);
		return "success";
	}
}

?>

