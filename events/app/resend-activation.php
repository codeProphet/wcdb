<?php
/*
UserCake Version: 2.0.2
http://usercake.com
*/

require_once("models/config.php");
if (!securePage($_SERVER['PHP_SELF'])){die();}

//Forms posted
if(!empty($_POST) && $emailActivation)
{
	$email = $_POST["email"];
	$username = $_POST["username"];
	
	//Perform some validation
	//Feel free to edit / change as required
	if(trim($email) == "")
	{
		$errors[] = lang("ACCOUNT_SPECIFY_EMAIL");
	}
	//Check to ensure email is in the correct format / in the db
	else if(!isValidEmail($email) || !emailExists($email))
	{
		$errors[] = lang("ACCOUNT_INVALID_EMAIL");
	}
	
	if(trim($username) == "")
	{
		$errors[] =  lang("ACCOUNT_SPECIFY_USERNAME");
	}
	else if(!usernameExists($username))
	{
		$errors[] = lang("ACCOUNT_INVALID_USERNAME");
	}
	
	if(count($errors) == 0)
	{
		//Check that the username / email are associated to the same account
		if(!emailUsernameLinked($email,$username))
		{
			$errors[] = lang("ACCOUNT_USER_OR_EMAIL_INVALID");
		}
		else
		{
			$userdetails = fetchUserDetails($username);
			
			//See if the user's account is activation
			if($userdetails["active"]==1)
			{
				$errors[] = lang("ACCOUNT_ALREADY_ACTIVE");
			}
			else
			{
				if ($resend_activation_threshold == 0) {
					$hours_diff = 0;
				}
				else {
					$last_request = $userdetails["last_activation_request"];
					$hours_diff = round((time()-$last_request) / (3600*$resend_activation_threshold),0);
				}
				
				if($resend_activation_threshold!=0 && $hours_diff <= $resend_activation_threshold)
				{
					$errors[] = lang("ACCOUNT_LINK_ALREADY_SENT",array($resend_activation_threshold));
				}
				else
				{
					//For security create a new activation url;
					$new_activation_token = generateActivationToken();
					
					if(!updateLastActivationRequest($new_activation_token,$username,$email))
					{
						$errors[] = lang("SQL_ERROR");
					}
					else
					{
						$mail = new userCakeMail();
						
						$activation_url = $websiteUrl."activate-account.php?token=".$new_activation_token;
						
						//Setup our custom hooks
						$hooks = array(
							"searchStrs" => array("#ACTIVATION-URL","#USERNAME#"),
							"subjectStrs" => array($activation_url,$userdetails["display_name"])
							);
						
						if(!$mail->newTemplateMsg("resend-activation.txt",$hooks))
						{
							$errors[] = lang("MAIL_TEMPLATE_BUILD_ERROR");
						}
						else
						{
							if(!$mail->sendMail($userdetails["email"],"Activate your ".$websiteName." Account"))
							{
								$errors[] = lang("MAIL_ERROR");
							}
							else
							{
								//Success, user details have been updated in the db now mail this information out.
								$successes[] = lang("ACCOUNT_NEW_ACTIVATION_SENT");
							}
						}
					}
				}
			}
		}
	}
}

//Prevent the user visiting the logged in page if he/she is already logged in
if(isUserLoggedIn()) { header("Location: account.php"); die(); }

require_once("models/header3.php");
?>
<body class="home page page-id-18 page-template-default boxed-layout wpb-js-composer js-comp-ver-3.7.4 vc_responsive metro" style="background-image: url(images/paper.png);">
	<div id="follow" class="bg-gray opacity" >
   <p style="color:#fff;margin:2px;"><a href='#' >Inquire Now</a></p>
  </div>
  <script>
   $(document).ready(function() {
    var current_top = parseInt($('#follow').css('top'));
    $('#follow').fadeIn(3000);
    $(window).scroll(function() {
	  var top =  $(window).scrollTop();
	  $('#follow').css('top', top + current_top);
	});
   });
  </script>
	<div class="wrapper">

<?php
require_once("models/top.html");

?>


<div id="masthead" style="background-image:url(images/h0.jpg)" class="">  
      <div class="container">
          <div class="row">
            <div class="col-sm-6 pull-left">
              <h1 class="text-center" ><small style="color:#fff;font-weight:bolder">Dever Student Properties<small></h1>
              <p class="lead text-center" style="color:#fff;">The largest property agent, we help students find the right accomodation</p>
              <p class="lead text-center" style="color:#fff;">Are you looking for a flat share or house share?</p>
			  <p class="lead text-center" style="color:#fff;">You have come to the right place !</p>


            </div>
			<br/>
            <div class="col-sm-6 pull-right">
                <!-- START login panel -->
	       
			
           </div>
      </div><!-- /cont -->
    </div>
	</div>
		 
<div class="container" style="margin-top:0px;">
	    
		 
		  <div class="col-sm-6 column-right">
					<!--<h1><img style='height:250px;' src='images/bg-old.jpg'/></h1>-->
                    <div style="text-align: left;"><strong><br />
DevaStudentProperties &#8211; Student property and rental Software.</strong></div><br />
<p>Welcome to the simple student property website for Deva Student Properties. Here students find refreshing accomodation.<br /> This is one of the UK's leading property portals, helping you to find property for to rent and make smarter decisions when renting homes in the UK. <br />
By registering to the site you get access to our regular email updates and alerts. Stay in touch with us and get the best accomodation close to university campus!
</p>
 
  </div>
            
         <div class="col-sm-6 column-right" style="margin-top:30px;">
		<!--<div id='left-nav'>";
<?php /*<?php 
include("left-nav.php");
?>*/?>

</div>-->
		 
		    <!--/span-->
		 
         <!-- START login panel -->
	        <div class="dialog">
              <div class="panel panel-basic">
                    <p class="panel-heading no-collapse"> <span class="add-on"><i class="icon-unlocked"></i></span>Resend activation</p>
                <div class="panel-body" style="margin-top:-40px;color:#000;">
                
		          <div>
				       <?php echo resultBlock($errors,$successes); ?></div>
                       <?php 
                           if(!$emailActivation)
                              { 
                                  echo lang("FEATURE_DISABLED");
                              }
                               else
                                  {
								    echo "<form name='resendActivation' action='".$_SERVER['PHP_SELF']."' method='post'>"; 

					   ?>
                       <br/>
                                 <label for="username">Username:</label>
							   <div class="input-control text">
                                 <input type='text' name='username' />
                               </div>
							   
                               <div class="input-control text">    
                                 <label for="email">Email:</label>
                                 <input type='text' name='email' />
                               </div>
							   
                               <div>
                                  <label>&nbsp;</label>
                                  <input type='submit' value='Submit' class='submit' />
                               </div>
                             </form>


                                <div style="margin-top:20px;">
						                <a href="forgot-password.php"><button class="btn btn-primary pull-left ribbed-cyan" id="createFlatWindow" style="margin-right:10px;">
				             		 <i class="icon-unlocked"></i>
				             		 Forgot your password? Reset</button></a>
                              
		                </div></form>
						<?php
															}
															?>
                        <div class="clearfix"></div>
            
                </div>
				
              </div>
			</div>
	
	
	     </div><!-- cols -->
         

<?php
require_once("models/bottom.html");

?>

</div>
<div id='bottom'></div>
<?php include ('models/footer.php'); ?>
		
		
</body>
</html>


