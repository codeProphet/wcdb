<?php
if (isset($_FILES["file"])) {
	$memberID=$_REQUEST["memberID"];
    $tmpFile = $_FILES["file"]["tmp_name"];
    $fileName = "images/members/".$memberID.".jpg"; // determine secure name for uploaded file

    list($width, $height) = getimagesize($tmpFile);
    // check if the file is really an image
    if ($width == null && $height == null) {
        header("Location: index.php");
        return;
    }
    // resize if necessary
    if ($width >= 400 && $height >= 400) {
        $image = new Imagick($tmpFile);
        $image->thumbnailImage(400, 400);
        $image->writeImage($fileName);
    }
    else {
        move_uploaded_file($tmpFile, $fileName);
    }
}