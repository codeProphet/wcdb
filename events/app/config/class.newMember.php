<?php
/*
DSP Version: 1.0.0
*/


class newMember 
{
	public $firstName = "";
	public $lastName="";
	public $phone = "";
	public $email="";
	public $locality="";
	public $country="";
	public $surburb ="";
	public $city = "";
	public $sex = "M";
	public $DOB = "0000-00-00";
	public $image="user.png";
	public $tags="";
	public $id="";
	public $memberid=0;
	public $address="";
	public $addedBy=1;
	public $houseChurch=0;
	
	function __construct($firstName_,$lastName_,$phone_,$email_,$sex_,$locality_,$country_,$DOB_,$tags_,$address_,$city_,$surburb_,$image_,$addedBy_,$houseChurch_)
	{
		//assign property values
		$this->firstName=$firstName_;
		$this->lastName=$lastName_;
		$this->surburb=$surburb_;
		$this->phone=$phone_;
		$this->email=$email_;
		$this->sex=$sex_;
		$this->locality=$locality_;
		$this->country = $country_;
		$this->DOB=$DOB_;
		$this->tags=$tags_;
		$this->address=$address_;
		$this->city=$city_;
		$this->image=$image_;
		$this->addedBy=$addedBy_;
		$this->houseChurch=$houseChurch_;
		
	}
	
//Functions that interact with members table
//------------------------------------------------------------------------------

//Add new member

	public function Add() {
	global $mysqli,$db_table_prefix; 
	$time=date("Y-m-d");
	try{
		$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."members (
			firstName,
			lastName,
			address,
			surburb,
			city,
			phone,
			email,
			sex,
			locality,
			country,
			DOB,
			tags,
			image,
			dateAdded,
			addedBy,
			assembly,
			modifiedDate
			)
			VALUES (
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?
			)");
		$stmt->bind_param("ssssssssssssssiis", 
							$this->firstName,
							$this->lastName,
							$this->address,
							$this->surburb,
							$this->city,
							$this->phone,
							$this->email,
							$this->sex,
							$this->locality,
							$this->country,
							$this->DOB,
							$this->tags,
							$this->image,
							$time,
							$this->addedBy,
							$this->houseChurch,
							$time
							);
		
		$stmt->execute();
		$inserted_id = $mysqli->insert_id;
		$stmt->close();	
		$result=array('status'=>1,'id'=>$inserted_id, 'msg'=>"New Member ".$this->firstName." ".$this->lastName." added successfully");
	}catch(Exception $e){
		$result=array('status'=>0,'msg'=>$e->getMessage());
	}
	return $result;
}



//Update member details
function Update()
	{
		global $mysqli,$db_table_prefix;
		$time=date("Y-m-d");
		$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."members 
		SET  
			firstName = ?, 
			lastName=? ,
			phone=? ,
			email=? ,
			address=?,
			surburb=?,
			sex=? ,
			locality=? ,
			country=?,
			DOB=?,
			tags=?,
			assembly=?
			modifiedDate=?
			
		WHERE id=? 
		LIMIT 1");
		$stmt->bind_param('sssssssisssisi', 
						$this->firstName,
						$this->lastName,
						$this->phone,
						$this->email,
						$this->address,
						$this->surburb,
						$this->sex,
						$this->locality,
						$this->country,
						$this->DOB,
						$this->tags,
						$this->houseChurch,
						$time,
						$this->id);
		$stmt->execute();
		$stmt->close();
		return 1;
	}

public	function nameExists()
{
	global $mysqli,$db_table_prefix;
		$stmt = $mysqli->prepare("SELECT firstName,lastName FROM ".$db_table_prefix."members 
		WHERE lastName=? AND firstName=?");
		$stmt->bind_param('ss', $this->lastName,$this->firstName);
		$stmt->execute();
		$stmt->store_result();
		$check = $stmt->num_rows;
		$stmt->close();
		if($check > 0){
			return 1;
		}else{
			return 0;
		}
}


}



?>