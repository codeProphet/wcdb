<?php
/*
WCDB Version: 1.0.0
*/

class province{
	public $id=0;
	public $country = "";
	public $provinceName="UNKOWN";
	public $disciples=0;
	public $leader=0;

	public function Delete()
	{
		global $mysqli,$db_table_prefix;
		$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."provinces 
		WHERE id=? ");
		$stmt->bind_param('i', 	$this->id);
		$stmt->execute();
		$stmt->close();
		return 1;
	}

 function Update() 
	{
	global $mysqli,$db_table_prefix; 
	$time=date("Y-m-d h:i:s");
	$stmt = $mysqli->prepare("UPDATE  ".$db_table_prefix."provinces 
		SET
			provinceName=?,
			leader=?,
			nation=?,
			disciples=?,
			modifiedDate=?
		WHERE id=?
		LIMIT 1
		");
		/* Execute the statement */
		$stmt->bind_param("sisisi",$this->provinceName, $this->leader, $this->country,$this->disciples, $time,$this->id);
		try{
			$stmt->execute();
			$stmt->close();
			$res=array('status'=>1,'text'=>'Suucessfully updated province');
		}catch(Exception $e){
			$res=array('status'=>0,'text'=>$e);
		}
		
	return $res;
}

}

class newProvince 
{
	public $country = "";
	private $provinceName="UNKOWN";
	public $leader=0;
	public $disciples=0;
	public $id=0;
	

	
	function __construct($provinceName_,$leader_,$country_,$disciples)
	{
		//assign values
		$this->country = $country_;
		$this->provinceName=$provinceName_;
		$this->leader=$leader_;
		$this->disciples=$disciples;
		
	}
	
	
//Functions that interact with province data
//------------------------------------------------------------------------------

//Add new properties
 function Add() 
	{
	global $mysqli,$db_table_prefix; 
	$i = 0;
	$time=date("Y-m-d h:i:s");
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."provinces (
		provinceName,
		leader,
		nation,
		disciples,
		modifiedDate
		)
		VALUES (
		?,
		?,
		?,
		?,
		?
		)");
		/* Execute the statement */
		$stmt->bind_param("sisis",$this->provinceName, $this->leader, $this->country,$this->disciples, $time);
		
		$stmt->execute();
		$stmt->close();
	$i++;
	return $i;
}

//Add new properties

 function provinceExists()
{
	global $mysqli,$db_table_prefix;
		$stmt = $mysqli->prepare("SELECT provinceName FROM ".$db_table_prefix."provinces 
		WHERE provinceName=? and nation=?");
		$stmt->bind_param('ss', $this->provinceName,$this->country);
		$stmt->execute();
		$stmt->store_result();
		$check = $stmt->num_rows;
		$stmt->close();
		if($check != 0){
			return 1;
		}else{
			return 0;
		}
}


}



?>