<?php
/*
wcdb Version: 1.0.0

*/
class users
{
	public $userId=0;

	public function checkAdmin()
	{
		global $mysqli,$db_table_prefix;
		$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."users
			SET 
			 isAdmin=1^isAdmin
			WHERE
			id = ?");
		$stmt->bind_param("i", $this->userId);
		$result=$stmt->execute();
		$stmt->close();	
		return $result;
	}
	
}

?>