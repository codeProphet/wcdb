<?php

require_once("../config-small.php");
//User must activate their account first
		global $mysqli,$emailActivation,$websiteUrl,$db_table_prefix,$result;
	
		$email=$_POST['email'];
		$firstname=$_POST['firstname'];
		$lastname=$_POST['lastname'];
		$eventTitle=$_POST['eventTitle'];
		$eventVenue=$_POST['eventVenue'];
		$startDate=$_POST['startDate'];
		$endDate=$_POST['endDate'];
		$token=$_POST['token'];
		$site_url="https://wcdb.cmfi-yla.club/events";

		 $errText="";
		 $sText="";
		 $result= array("id"=>"0","text"=>$errText.$sText);
		

try{
		
		$mail = new wcdbMail();
		
		//Build the activation message
		//$activation_message = lang("ACCOUNT_ACTIVATION_MESSAGE",array($websiteUrl,$this->activation_token));
		
		//Define more if you want to build larger structures
		$hooks = array(
		"searchStrs" => array("#SITE_URL","#TOKEN","#FIRSTNAME","#EMAIL","#LASTNAME","#EVENTTITLE","#STARTDATE","#ENDDATE",
			"#VENUE"),
		"subjectStrs" => array($site_url,$token,$firstname,$email,$lastname,$eventTitle,$startDate,$endDate,$eventVenue));
		
		/* Build the template - Optional, you can just use the sendMail function 
		Instead to pass a message. */
		
		if(!$mail->newTemplateMsg("eventRegistration.html",$hooks))
		{
			//$this->mail_failure = true;
			$errText=$errText." Failed to create mail message.";
		}
		else
		{
			//Send the mail. Specify users email here and subject. 
			//SendMail can have a third parementer for message if you do not wish to build a template.
			
			
			if($mail->sendMail($email,"Event Registration Alert")!="true")
			{
				$errText=$errText." Failed to send mail";
				
			}
			else{
			$success = lang("EVENT_REGISTRATION_COMPLETE_TYPE2");
			$sText=$sText." Email sent successfully ";
			 //  echo "Email sent successfully ";
		}
		}
		
	  $result= array("id"=>"1","text"=>$errText.$sText);
}catch(Exception $e){
	  $result= array("id"=>"0","text"=>$errText.$sText."  ".$e.getMessage());
}

echo  json_encode($result);
?>