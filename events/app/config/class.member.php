<?php
/*
WCDB Version: 1.0.0
*/


class member 
{
	public $firstName = "";
	public $lastName="";
	public $familyName="";
	public $familyId=0;
	public $familyRole="";
	public $phone = "";
	public $email="";
	public $locality="";
	public $country="ZW";
	public $surburb ="";
	public $city = "";
	public $sex = "M";
	public $DOB = "0000-00-00";
	public $image="user.png";
	public $tags="";
	public $id=0;
	public $address="";
	public $userId=0;
	public $discipleMaker=0;
	
	
	
//Functions that interact with members table
//------------------------------------------------------------------------------

//Add new member

	public function Add() {
	global $mysqli,$db_table_prefix; 
	$time=date("Y-m-d");
	if(nameExists($this->firstName,$this->lastName)==0){
	try{
		$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."members (
			firstName,
			lastName,
			phone,
			email,
			sex,
			address,
			surburb,
			city,
			locality,
			country,
			DOB,
			dateAdded,
			addedBy,
			modifiedDate
			)
			VALUES (
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?,
			?
			)");
		$stmt->bind_param("ssssssssssssis", 
							$this->firstName,
							$this->lastName,
							$this->phone,
							$this->email,
							$this->sex,
							$this->address,
							$this->surburb,
							$this->city,
							$this->locality,
							$this->country,
							$this->DOB,
							$time,
							$this->userId,
							$time
							);
		$stmt->execute();
		$stmt->close();	
		$result=array('status'=>1,'msg'=>'successfully added new disciple <strong>'.$this->firstName.' '.$this->lastName.'</strong>');
	}catch(Exception $e){
		$result=array('status'=>0,'msg'=>$e.getMessage());
	}
	return $result;
	}
	else{
		return array('status'=>0,'msg'=>'Name Exists');
	}
}

//create new family
public function newFamily(){
	global $mysqli,$db_table_prefix; 
	$time=date("Y-m-d");

	//check if family exists already
	if(familyExists($this->lastName,$this->address)=="false"){
		$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."families (
			familyName,
			address,
			surburb,
			city,
			nation,
			modifiedDate
			)
			VALUES (
			?,
			?,
			?,
			?,
			?,
			?
			)");
		$stmt->bind_param("ssssss", 
							$this->lastName,
							$this->address,
							$this->surburb,
							$this->city,
							$this->country,
							$time
							);
		$stmt->execute();
		$result=$mysqli->insert_id;
		$stmt->close();	
	}else{
		$result=0;
	}
	return $result;
}



//Add new member ro temp table

	public function AddTemp() {
	global $mysqli,$db_table_prefix; 
	$time=date("Y-m-d");
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."memberspreview (
		firstName,
		lastName,
		phone,
		email,
		sex,
		address,
		surburb,
		city,
		locality,
		country,
		DOB,
		addedBy,
		modifiedDate
		)
		VALUES (
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?
		)");
	$stmt->bind_param("sssssssssssis", 
						$this->firstName,
						$this->lastName,
						$this->phone,
						$this->email,
						$this->sex,
						$this->address,
						$this->surburb,
						$this->city,
						$this->locality,
						$this->country,
						$this->DOB,
						$this->userId,
						$time
						);
	$result = $stmt->execute();
	$stmt->close();	
	return $result;
}

//Update member details
function Update()
	{
		global $mysqli,$db_table_prefix;
		$time=date("Y-m-d");
		try{
		$stmt = $mysqli->prepare("UPDATE ".$db_table_prefix."members 
		SET  
			firstName = ?, 
			lastName=? ,
			familyID=?,
			phone=? ,
			email=? ,
			address=?,
			surburb=?,
			city=?,
			sex=? ,
			locality=? ,
			country=?,
			DOB=?,
			tags=?,
			familyRole=?,
			discipleMaker=?,
			modifiedDate=?
			
		WHERE id=? 
		LIMIT 1");
		$stmt->bind_param('ssissssssissssisi', 
						$this->firstName,
						$this->lastName,
						$this->familyId,
						$this->phone,
						$this->email,
						$this->address,
						$this->surburb,
						$this->city,
						$this->sex,
						$this->locality,
						$this->country,
						$this->DOB,
						$this->tags,
						$this->familyRole,
						$this->discipleMaker,
						$time,
						$this->id);
		$stmt->execute();
		$stmt->close();
		$result=array('status'=>1,'msg'=>'successfully saved member details for '.$this->firstName.' '.$this->lastName);
	}catch(Exception $e){
		$result= array('status' =>0 ,'msg'=>$e->getMessage() );
	}
	return $result;
	}
	
	
//Delete member 
public function Delete()
	{
		global $mysqli,$db_table_prefix;
		$stmt = $mysqli->prepare("DELETE FROM ".$db_table_prefix."members 
		WHERE id=? ");
		$stmt->bind_param('i', 	$this->id);
		$stmt->execute();
		$stmt->close();
		return 1;
	}


}


function nameExists($fn,$ln)
{
	global $mysqli,$db_table_prefix;
		$stmt = $mysqli->prepare("SELECT firstName,lastName FROM ".$db_table_prefix."members 
		WHERE lastName=? and firstName=?");
		$stmt->bind_param('ss', $ln,$fn);
		$stmt->execute();
		$stmt->store_result();
		$check = $stmt->num_rows;
		$stmt->close();
		if($check != 0){
			return 1;
		}else{
			return 0;
		}
}

function familyExists($lastName,$address)
{
	global $mysqli,$db_table_prefix;
 	$stmt = $mysqli->prepare("SELECT * FROM ".$db_table_prefix."families WHERE familyName=? AND address=?");
 	$stmt->bind_param('ss', $lastName,$address);
	$stmt->execute();
	$stmt->store_result();
	$check = $stmt->num_rows;
	$stmt->close();
		if($check != 0){
			$result="true";
		}else{
			$result="false";
		}
	return $result;
 }
?>