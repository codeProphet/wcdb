routie({
      '/register/:id': function(id) {
          $('#contentDiv').load('views/register.html').show();
          $('#btnRegister').hide();
      },
      '/events': function() {//show landing page
          $('#contentDiv').load('views/events.html').show();
      },
      '/rvc': function() {//show landing page
          $('#contentDiv').load('views/radio.html').show();
      },
      '/': function() {//show landing page
          $('#contentDiv').load('views/landing.html').show();
      },
       '/event/:eventId': function(eventId) {//show landing page
          $('#contentDiv').load('views/eventProfile.html').show();
          console.log(eventId);
      },
      '/admin': function() {//show landing page
          $('#contentDiv').load('views/events_admin.html').show();
      },
      '/admin/event/:eventId': function(eventId) {//show landing page
          $('#contentDiv').load('views/events_admin_profile.html').show();
      }
  });

  var sresponse="Success";
  var fresponse="Something's wrong";
   function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete?");
      if (x)
          return true;
      else
        return false;
    }

  function monthName(monthNumber) {
      var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
      return month[monthNumber];
    }

  function getImage(img){
    var image='event-default.jpg';
    if(img!=null){
      image=img;
    }
    return image;
  }

  function show(div){
    var icon=div+"_icon";
    var x=document.getElementById(div);
    if(x.style.display == 'none'){
      x.style.display = 'block';
      changeIcon(icon);
    }
  }

  function showHideForm(div){
    var icon=div+"_icon";
    changeIcon(icon);
    var x=document.getElementById(div);
    if(x.style.display == 'block'){
      x.style.display = 'none';
    }else{
      x.style.display = 'block'; 
    }
  }

function changeIcon(icon){
  var x = document.getElementById(icon);
  if(x){
    if (x.classList.contains("glyphicon-plus-sign")==true) {
        x.classList.remove("glyphicon-plus-sign");
        x.classList.remove("green");
        x.classList.add("glyphicon-remove-sign");
        x.classList.add("red");
    } else {  x.classList.remove("glyphicon-remove-sign");
      x.classList.remove("red");
        x.classList.add("glyphicon-plus-sign");
        x.classList.add("green");
        
    }
}
}

  //save member using event manager
    function createNewMember(eventId_){ //alert("now in");
          var country_=$("#country option:selected").val();
          var nationality_=$("#nationality option:selected").val();
          var gender_=$("#gender option:selected").val();
          var preferredAcc_=$("#preferredAcc option:selected").val();
          var room_=$("#room option:selected").val();
          var babySitting_=$("#needBabySitting option:selected").val();
          var transportMeans_=$("#meansoftransport option:selected").val();
          var transporttoKoume_=$("#transporToKoume option:selected").val();
          var accommodation_=$("#accommodation option:selected").val();
          var arrivaldate_=$("#arrivaldate").val();
          var depaturedate_=$("#depaturedate").val();
          var time=Date.now();
          var childrenNames_="";
          var dietRestrictions_=$("#dietRestrictions option:selected").val();
          var entryPort_=$("#pickupport option:selected").val();
          var dietRestrictionsSpec_="";
          var transDetails_="";
          var accDetails_="";
          var firstname_=regForm.firstname.value;
          var lastname_=regForm.lastname.value;
          var email_=regForm.email.value;
          if(regForm.children=="Yes"){
            childrenNames_=regForm.elements["childrennames"].value;
          }
          if(dietRestrictions_=="Yes"){
            dietRestrictionsSpec_=regForm.elements["specifydietRestrictions"].value;
          }
          if(regForm.transporToKoume=="Yes"){
            transDetails_=regForm.elements["transportDetails"].value;
          }
          if(regForm.accommodation=="Yes"){
            accDetails_=regForm.elements["accomodationDetails"].value;
          }

          

          function uploadMedicalForm(id){
              //var filepath=document.getElementById('regForm').medicalform.value;
              //var medFile= filepath.split(/[\\\/]/).pop();
              //var imageFile= "user.png";  //research on how to get the filename from the input
              var data = new FormData();
                jQuery.each(jQuery('#medicalform')[0].files, function(i, file) {
                    data.append('medicalform-'+i, file);
                });
                data.append('memberId',id);
                console.log(data);
            /*if(medFile=='')
              {
              alert("Please select medical form file first");
              }
              else{*/
              $.ajax({
                url: "controllers/fileUpload.php", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: function(data)   // A function to be called if request succeeds
                {
                  successNote(sresponse,'medical form uploaded sussessfully! ')
                   },
                error: function(data)
                {
                  errorNote(fresponse,'medical form not uploaded successfully!')
                }
                });
             // }
          }
          function registerMember(id){
           

            $.post("../app/api/api.php/records/cmfi_event_registration", //Required URL of the page on server
                    { // Data Sending With Request To Server
                      eventId:eventId_,
                      memberId:id,
                      children:regForm.children.value,
                      needBabySitting:babySitting_,
                      childrenNames:childrenNames_,
                      transportMode:transportMeans_,
                      entryPort:entryPort_,
                      arrivalDate:arrivaldate_,
                      departureDate:depaturedate_,
                      arrangedTransport:transporttoKoume_,
                      dietRestrictions:dietRestrictions_,
                      dietRestSpec:dietRestrictionsSpec_,
                      accommodation:accommodation_,
                      transDetails:transDetails_,
                      accDetails: accDetails_,
                      preferredAcc:preferredAcc_,
                      remark:'Web',
                      accType:room_
                    },
                    function(id,status){ 
                      console.log("event reg id: "+id);
                      if(id){
                        successNote(sresponse,'<p> you have been registered sussessfully!</p>');
                            //send email to registrant after successful registration
                            // $.getJSON("../app/api/funcs.php?fn=fetchEvents&n="+eventId_,
                           // console.log("before:"+firstName_);
                        $.get("../app/api/api.php/records/cmfi_events?filter=eventId,eq,"+eventId_,
                    function(data){
                      var event=data.records[0];
                   //   console.log("after:"+firstName_);
                              $.post("../app/config/sendEmail.php",
                              {
                                  firstname:firstname_,
                                  lastname:lastname_,
                                  email:email_,
                                  eventTitle:event.eventTitle,
                                  eventVenue:event.eventVenue,
                                  startDate:event.startDate,
                                  endDate:event.endDate
                              },

                           function(response){ // Required Callback Function
                            
                             var res=response;
                             console.log(res);
                           
                            if(res.id==1){successNote(sresponse,res.text);
                            }else{
                              errorNote(fresponse,res.text);
                            }
                            })
                          });

                        }else{
                          errorNote(fresponse,'registration failed try again!');
                        }
                    });
            routie('/event/'+eventId_);
          }
          function updateMember(id){
            var data={ // Data Sending With Request To Server
                              firstName:regForm.firstname.value,
                              lastName:regForm.lastname.value,
                              email:regForm.email.value,
                              city:regForm.city.value,
                              phone:regForm.phone.value,
                              profession:regForm.profession.value,
                              sex:gender_,
                              country:country_,
                              language:regForm.language.value,
                              nationality:nationality_,
                              DOB:regForm.birthday.value
                            }
            $.ajax({
              type:'PUT',
              url:"../app/api/api.php/records/cmfi_members/"+id, 
              contentType:'application/json',
              data:JSON.stringify(data),
            }).done(function(){
              console.log('saved member details successfully');
            }).fail(function(msg){
              console.log('Failed to save member details: '+msg)
            });
                  
          }


            $.get("../app/api/api.php/records/cmfi_members?filter=email,eq,"+regForm.email.value,
              function(data,status){
                var data2=data.records;
                 //var id=data2[0];
                 var id=data2[0].eventId;
                 console.log(id);
                if(id&& regForm.email.value!=''){
                  //this member is registered
                    updateMember(id);
                    registerMember(id);
                    uploadMedicalForm(id);

                }else{
                      //not registered registered
                      $.post("../app/api/api.php/records/cmfi_members", //Required URL of the page on server
                            { // Data Sending With Request To Server
                              firstName:regForm.firstname.value,
                              lastName:regForm.lastname.value,
                              email:regForm.email.value,
                              city:regForm.city.value,
                              phone:regForm.phone.value,
                              profession:regForm.profession.value,
                              sex:gender_,
                              country:country_,
                              language:regForm.language.value,
                              nationality:nationality_,
                              DOB:regForm.birthday.value
                            },
                            function(memberId,status){ 
                              console.log("member id: "+memberId);
                              //use the id to create event registration
                              registerMember(memberId);
                              uploadMedicalForm(id);
                      });
                }

            });
          
        }
 function uploadEventImage(id){
              //var filepath=document.getElementById('regForm').medicalform.value;
              //var medFile= filepath.split(/[\\\/]/).pop();
              //var imageFile= "user.png";  //research on how to get the filename from the input
              var data = new FormData();
                jQuery.each(jQuery('#image')[0].files, function(i, file) {
                    data.append('image-'+i, file);
                });
                data.append('eventId',id);
                console.log(data);
            /*if(medFile=='')
              {
              alert("Please select medical form file first");
              }
              else{*/
              $.ajax({
                url: "controllers/eventImageUpload.php", // Url to which the request is send
                type: "POST",             // Type of request to be send, called as method
                data: data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false,        // To send DOMDocument or non processed data file it is set to false
                success: function(data)   // A function to be called if request succeeds
                {
                  successNote(sresponse,'event image uploaded sussessfully! ')
                   },
                error: function(data)
                {
                  errorNote(fresponse,'event image did not upload successfully!')
                }
                });
             // }
          }

function updateEvent(id){
              var start_=addEventForm.reservation.value.substring(0,10);
              var end_=addEventForm.reservation.value.substring(12,10);

    var data={
        title:addEventForm.eventTitle.value,
        eventDesc:addEventForm.eventDesc.value,
        eventVenue:addEventForm.eventVenue.value,
        eventColor:addEventForm.eventColor.value,
        start:start_,
        end:end_,
        nation:addEventForm.eventRegion.value,
        regFee:addEventForm.regFee.value,
        criteria:addEventForm.criteria.value,
        coordinator:addEventForm.eventLeader.value

    }

     
    $.ajax({
      url:"../app/api/api.php/records/cmfi_events/"+id,
      type:"PUT",
      contentType:'application/json',
      data:JSON.stringify(data),
    })

    .done(function(){
              console.log('saved event details successfully');
              uploadEventImage(id);
    })

    .fail(function(msg){
      console.log('Failed to save event details: '+msg.responseText)
      console.log(msg);
    });


}

function saveEvent(id) {
              var title =$('#eventTitle').val();
              var eventDesc=$('#eventDesc').val();
              var start=addEventForm.reservation.value.substring(0,10);
              var end=addEventForm.reservation.value.substring(12,10);
              var eventColor=$('#eventColor').val();
              var eventId=$('#eventId').val(); //alert(eventId);
              var nation= $('#eventRegion option:selected').val();
              var regFee=$('#regFee').val();
              var eventVenue=$('#eventVenue').val();
              var image=$('#image').val();
              var criteria=$('#criteria').val();
              var coodinator=$('#eventLeader').val();
              uploadEventImage(id);

              
              $.ajax({
                url: '../app/api/post.php',
                data: 'type=fullUpdate&title='+title+'&eventId='+eventId+'&start='+start+'&end='+end+'&eventColor='+eventColor+'&eventDesc='+eventDesc+'&nation='+nation+'&eventVenue='+eventVenue+'&criteria='+criteria+'&regFee='+regFee+'&image='+image+'&coodinator='+eventLeader,
                type: 'POST',
                dataType: 'json',
                success: function(response)
                {successNote(sresponse,'event saved sussessfully!')
                  //if(response.status == 'success')
                  //$('#calendar').fullCalendar('refetchEvents');
                  //initialise event form
                  uploadEventImage(id);
                  $("#addEventForm")[0].reset(); //clear form
                  $('#globalEventsTable').DataTable().ajax.reload(); //refresh events table
                },
                error: function(e){
                  errorNote('Error processing your request: '+e.responseText);
                }
              });
              

  }

  function eventEdit(id){
          var form="'addLocalityForm'";
          var container="'localityAddForm'";
          $.getJSON("../app/api/api.php/records/cmfi_events?filter=eventId,eq,"+id, function(data) {
              var res=data.records[0];
              var options={
                startDate: res.startDate.substring(0,10),
                endDate: res.endDate.substring(0,10),
                minDate: '2010-01-01',
                maxDate: '2065-12-31',
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                format: 'YYYY-MM-DD',
              }
               $('#reservation').daterangepicker(options);
                addEventForm.eventTitle.value=res.eventTitle;
                addEventForm.eventDesc.value=res.eventDesc;
                addEventForm.reservation.value=res.startDate.substring(0,10)+' - '+res.endDate.substring(0,10);
                addEventForm.eventColor.value=res.eventColor;
                $('#eventColor').trigger('change');
                addEventForm.eventRegion.value=res.eventRegion;
                $('#eventRegion').trigger('change');
                $('#eventTitle').html('<h2>Edit Event <small>select leader</small></h2><div class="clearfix"></div>');
                $('#eventSubmit').html('<button  onclick="cancel('+form+','+container+')" type="submit" class="btn btn-default" name="submit" ><i class="icon-remove"></i> Cancel</button><button  onclick="saveEvent('+id+')" type="submit" class="btn btn-success" name="submit" ><i class="icon-check"></i>  Save</button>');
                //alert(response.name);
            });

        }
function deleteEvent(id,title){// alert('now in');
    (new PNotify({
                  title: 'Confirmation Needed',
                  text: 'Are you sure you want to delete this event?',
                  icon: 'glyphicon glyphicon-question-sign',
                  hide: false,
                  confirm: {
                    confirm: true
                  },
                  buttons: {
                    closer: false,
                    sticker: false
                  },
                  history: {
                    history: false
                  },
                  addclass: 'stack-modal',
                  stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
               })).get().on('pnotify.confirm', function(){
      
        if (ConfirmDelete()){
          $.ajax({
            url:'api/api.php/records/cmfi_events/'+id,
            type:'DELETE',
            success: function(response,status){
            if(response==1){successNote(sresponse,'Event deleted sussessfully!')
            }else{
              errorNote(fresponse,'Event not deleted!')
            }

            $('#globalEventsTable').DataTable().ajax.reload(); //refresh provinces table
                
              }
            })
        }  
      });
    }

    function Lang(lang){
      var lang = sessionStorage.setItem("lang"); 
      switch (lang){
        case 'en':
          //assign form elements to english language

          break;
        case 'fr':
          //assign form elements to french

          break;


      }
    }





    //----------------------------------notifications------------------------------------------------//
    //success nofication
    function successNote(title_,text_){new PNotify({
          title: title_,
          text: text_,
          type: 'success'
        });
    }

    //failure or Error notifications
    function errorNote(title_,text_){new PNotify({
          title: title_,
          text: text_,
          type: 'error'
        });
    }