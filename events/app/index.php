<?php

	// Check if install.php is present
	if(is_dir('install')) {
		header('Location: install/install.php');
	} else {
		
		
		require_once("config/config.php");

		//language settings
		$langx = $settings['language']['value'];  //get default language 
		
		if(isset($_GET['lang'])){ 
			$langx = $_GET['lang']; //update language as defined by user
		} 
		
		$langpath="config/languages/".$langx.".php";
		require_once($langpath); //load language file

		
		if (!securePage($_SERVER['PHP_SELF'])){die();}
		
		//Prevent the user visiting the log in page if he/she is already logged in
		if(!isUserLoggedIn()) { header("Location: login.php"); die(); }
		if(isUserLoggedIn() && !isset($_GET['page']) )
			{	
					// inactive in seconds
					/*$inactive = 10;
					if( !isset($_SESSION['timeout']) )
					$_SESSION['timeout'] = time() + $inactive; 

					$session_life = time() - $_SESSION['timeout'];

					if($session_life > $inactive)
					{  session_destroy(); header("Location:index.php");     }

					$_SESSION['timeout']=time();*/

					//initialise page variables
					$userId=$loggedInUser->user_id;
					$userEmail=$loggedInUser->email;
					$alertBox="";
					$headerIncTitle="Home";
					$image=$loggedInUser->image;
					$page='Home';
					$menu="";
					$loggedInUser->lang=$langpath;
					if($image==''){$image='user1.png';} 
					//writeMapData();
					getNationsDash();
					updateGauge();
					userOnline($loggedInUser->user_id); //set user online time
					//$mail =new wcdbMail();
					// try {
					// 	$mail->sendMail('tapiwa@dataage.solutions','test','you are logged in');
					// }catch(Exception $e){
					// 	echo "Error:".$e->getMessage();
					// }

				

   				//set the page header
					include('includes/head.php');
					//--load Javascript custom functions --
					echo '
					<script src="js/funcs.js"></script>';
					include 'templates/events.wcdb'; //load events plugin
					include 'templates/profile.wcdb'; //load profile plugin
					include 'templates/members.wcdb'; //load top users plugin
					include 'templates/recentActions.wcdb'; //load top recent actions plugin
					include 'templates/onlineUsers.wcdb'; //load top recent actions plugin
					include 'templates/family.wcdb'; //load family template
?>


<body  class="nav-md pace-done container body" style="background: #F7F7F7;">
	
  <!--div   class="container body">


    <div class="main_container"-->

	<!-- side bar -->
	
	
	<!-- Top Navigation -->
	<?php include("includes/top_nav.php"); ?>
	
	<!-- page content -->
	<div class="body" >
	<div class="pl-4 pr-4">
     <div  id="contentDiv" class="right_col" role="main" >
		
	 </div> <!--/Content Div-->
	</div>
	</div>
	<!--/div>
	</div-->
  <!-- /page content -->
  <!-- footer content -->
  <!--div id="footer"></div-->
  <!-- /footer content -->
  
<script type="text/javascript">
$(function() {
	var userId='<?php echo $userId;?>';
	var nation='<?php echo $loggedInUser->nation;?>';
	var memberId='<?php echo $loggedInUser->memberId;?>';

    sessionStorage.setItem("nation", nation);
    sessionStorage.setItem("userId",userId);
    sessionStorage.setItem("memberId",memberId);
	var value='<?php echo $langx;?>';
	
		//$("#contentDiv").load("views/globalDash.php?lang="+value);
	

}); //function
</script>

<?php
include('includes/footer.php');
		}
	}
?>

