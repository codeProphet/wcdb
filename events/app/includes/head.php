<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicon -->
  <link href="images/CMFI-ONLINE-new-logo1_fav.png" rel="shortcut icon" type="image/vnd.microsoft.icon">

  <title>World Conquest</title>

  <!-- Bootstrap core CSS -->

  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="fonts/simple-line-icons.css" rel="stylesheet" >
  <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="css/animate.min.css" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="css/custom.php" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.3.css" />
  <link href="css/icheck/flat/green.css" rel="stylesheet" />
   <!-- switchery -->
  <link rel="stylesheet" href="css/switchery/switchery.min.css" />
  <link href="css/floatexamples.css" rel="stylesheet"  />

 
  <!-- PNotify -->
   <link href="css/PNotify/pnotify.custom.min.css" rel="stylesheet">
 
  <link href="css/jquery-ui.min.css" rel="stylesheet">
  
  <link href="js/datatables/jquery.dataTables.min.css" rel="stylesheet"  />
  <link href="js/datatables/buttons.bootstrap.min.css" rel="stylesheet"  />
  <link href="js/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet"  />
  <link href="js/datatables/responsive.bootstrap.min.css" rel="stylesheet"  />
  <link href="js/datatables/scroller.bootstrap.min.css" rel="stylesheet"  />
     <!-- select2 -->
  <link href="css/select/select2.min.css" rel="stylesheet">
   <!-- calendar --> 
  <link href="css/calendar/fullcalendar.css" rel="stylesheet">
  <link href="css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
  <!-- colorpicker -->
  <link href="css/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">
  
  <!--script type="text/javascript" src="js/vue.js"></script>
    <script type="text/javascript" src="js/axios.js"></script-->
  <script src="js/jquery.min.js"></script>
 <script src="js/jquery-ui.min.js"></script>
  <script src="js/nprogress.js"></script>
<script src="js/masonry.pkgd.min.js"></script>
<script src="js/routie.min.js"></script>
<script src="js/pages.js"></script>


  <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>
