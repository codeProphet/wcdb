<?php
 /* $image=$loggedInUser->image;
  $a=new alert;
  $a->id=$loggedInUser->user_id;
  $n=$a->number();
  $alerts=fetchAlerts($a->id);
  */
?>  
<!-- load icheck -->
 
<script type="text/javascript" src="js/webcam/webcam.js"></script>
<script type="text/javascript">
  $(function(){
    webcam.set_api_url('data/uploadImg.php');
    webcam.set_swf_url('js/webcam/webcam.swf');
    webcam.set_quality(90);
    webcam.set_shutter_sound(true,'js/webcam/shutter.mp3');
    $('#camera').html(webcam.get_html(320,320));
});
 </script>
 <script language="JavaScript">
    webcam.set_hook( 'onComplete', 'my_completion_handler' );
    
    function take_snapshot() {
      // take snapshot and upload to server
      document.getElementById('upload_results').innerHTML = '<h1>Uploading...</h1>';
      webcam.snap();
    }
    
    function my_completion_handler(msg) {
     
      $("#profileAvatar").html('<img id="avatarImg" class="img-responsive avatar-view" src="images/members/'+msg+'" alt="Avatar" title="Change the avatar" ><input type="hidden" id="imagefile" name="imagefile" value="'+msg+'">');
      $("#imgFileName").html(msg);
      $('#webcamImageModal').modal('hide');
        return false;
    }
  </script>

<script>
  function imgDisplay(){
    var img=$("#imagefile").val();
    alert(img);
  }


</script>


  <script src="js/icheck/icheck.min.js"></script>
  <script src="js/custom-sm.js"></script>
  <!-- ------------------------- -->
  <style>
/* member avatar update form   */

  a.boxclose{
    float:right;
    margin-top:-10px;
    margin-right:-10px;
    cursor:pointer;
    color: #fff;
    border: 1px solid #AEAEAE;
    border-radius: 10px;
    background: red;
    font-size: 11px;
    font-weight: bold;
    display: inline-block;
    line-height: 0px;
    padding: 6px 3px;       
}

.avatarUploadForm{
  display:none;
  border:2px dashed #d2d2d2;
  background-color:#f7f7f7; 
  padding:2px;
}

.boxclose:before {
    content: "×";
}

</style>
   <!-- top navigation -->
      <div class="top_nav" >

        <div class="nav_menu">
          <nav class="" role="navigation">
            <!--div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div-->
            <div class="nav toggle" style="display: block;">
              <a id="logo" href=".">
                <img src="images/CMFI-logo1.png" alt="CMFI World Conquest Database" width="160">
              </a>
            </div>
            <div id="back" class="nav toggle" style="display:none;">
              <a href="javascript:;" data-toggle="dropdown" aria-expanded="false">
                  <i class="fa fa-chevron-left"></i>
                </a>
            </div>
			
			
            <ul class="nav navbar-nav navbar-right">
        <li>
          
        </li>     
        <li>
                    <div id="loaderImg"><img src="images/loading1.gif" width="30px"></div>
                  </li> 
              
			 <li class="pull-left" style="max-width:300px;padding-top:10px;">
              
            </li>

              <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" 
                aria-expanded="false">
                  <img src="images/members/<?php echo $image;?>" alt="" onerror="if (this.src != 'images/members/user.png') this.src = 'images/members/user.png';"><?php echo $loggedInUser->fullName;?>
                  <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu pull-right">
                  <li><a onclick="openPage(14,<?php echo $loggedInUser->memberId;?>)"><i class="icon-user pull-right"></i>  <?php echo lang("PROFILE");?></a>
                  </li>
                 
                  <li>
                    <a href="javascript:;"><i class="icon-envelope pull-right"></i> Feedback</a>
                  </li>
				   <li>
                    <a onclick="openPage(15)"><i class="icon-envelope-letter pull-right"></i> Inbox</a>
                  </li>
                  <li><a href="logout.php"><i class="icon-logout pull-right"></i> Log out</a>
                  </li>

                </ul>
              </li>
                  
              <li role="presentation" class="dropdown">
                <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                  <i class="icon-bell"></i>
                  <span class="badge bg-red"><?php ?></span>
                </a>
                <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
				        <li>
                    <div class="text-center">
                      <a href="inbox.html">
                        <strong>No new alerts - View read alerts?</strong>
                        <i class="fa fa-angle-right"></i>
                      </a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Language Switch -->
			        <li role="presentation" class="dropdown" data-toggle="tooltip" data-placement="bottom" title="Change site language">
                <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                  <i class="icon-directions"></i>
                  
                </a>
                <form id="lang" method="get" action=""></form>
                <ul id="menu2" class="dropdown-menu dropdown-usermenu pull-right" role="menu">
                  
                  <li>
                    <a id="en" name="en" value="en" onclick="">
                      <span>English </span>                     
                    </a>
                  </li>    

                  <li>
                    <a id="fr" name="fr" value="fr" onclick="">
                      <span>French </span> 
                    </a>
                  </li>
              
                </ul>
              
            </li>
              <!-- Admin -->
            <?php if($loggedInUser->isAdmin==1){echo '
            <li role="presentation" class="dropdown"  data-toggle="tooltip" data-placement="bottom" title="Admin">
                <a  class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                  <i class="icon-settings"></i>  <span class=" fa fa-angle-down"></span>
                  
                </a>
                <ul class="dropdown-menu dropdown-usermenu pull-right" role="menu">
                  <li>
                    <a onclick="routie(`/admin/settings`)"><i class="icon-settings pull-right"></i>
                      <span>'.Lang("SETTINGS").'</span>
                    </a>
                  </li>
                  <li>
                    <a onclick="routie(`/admin/users`)"><i class="icon-users pull-right"></i> Users</a>                    
                    </a>
                  </li>    
                  <li>
                    <a onclick="routie(`/admin/events`)"><i class="icon-calendar pull-right"></i> Events</a>                    
                    </a>
                  </li> 
                  <li>
                    <a onclick="routie(`/admin/backup`)"><i class="icon-docs pull-right"></i>
                      <span> DB Backup</span> 
                    </a>
                  </li>
              
                </ul>
              </li>';
            }?>

             <!-- Tools -->
            <li role="presentation" class="dropdown" data-toggle="tooltip" data-placement="bottom" title="System tools">
                <a  class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                  <i class="icon-wrench"></i>
                  
                </a>
                <ul class="dropdown-menu dropdown-usermenu pull-right" role="menu">
                  
                  <li>
                    <a onclick="routie('/tools/excel_import')"><i class="icon-arrow-down pull-right"></i> Import List from excel</a>                    
                    </a>
                  </li>    

                  <!--li>
                    <a onclick="">
                      <span>Database Backup</span> 
                    </a>
                  </li-->
              
                </ul>
              </li>
            <!-- Nations -->
            <li role="presentation" class="dropdown" data-toggle="tooltip" data-placement="bottom" title="Nations">
                
                <a onclick="routie('/nations')" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                  <i class="icon-globe"></i>
                  
                </a>
              </li>
            <!-- Home -->
            <li id="nationHome" role="presentation" class="dropdown" data-toggle="tooltip" data-placement="bottom" title="Home">
                <a href="." >
                  <i class="icon-home"></i>
                  
                </a>
              </li>
            </ul>
          </nav>
        </div>

      </div>
      <!-- /top navigation -->


      <!-- Add New Member -->  
        <div id="newMember" class="modal fade newMember" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-md">
                    <div class="modal-content">

                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Add New Member</h4>
                      </div>
                      <div class="modal-body">
                        <div class="x_panel">
            
                    <div class="x_content">
                      <br />
                      <form id="addMemberFormx" class="form-horizontal form-label-left input_mask" action="" method="post">

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" name="firstName" autocomplete="firstName" id="firstName" placeholder="First Name">
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control" id="lastName" name="lastName" autocomplete="lastName" placeholder="Last Name">
                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="email" autocomplete="email" name="email" placeholder="Email">
                        <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" autocomplete="phone">
                        <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                        <input id="dateOfBirth" name="DOB" class="date-picker form-control col-md-7 col-xs-12" type="text" >
                        </div>
                        
                         
                          <script type="text/javascript">
                      $(document).ready(function() {
                        $('#dateOfBirth').datepicker({
                          singleDatePicker: true,
                          dateFormat:"yy-mm-dd",
                          changeYear:true,
                          changeMonth:true,
                          calender_style: "picker_4"
                        }, function(start, end, label) {
                          console.log(start.toISOString(), end.toISOString(), label);
                        });
                      });
                    </script>

                      </div>
                      <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender  <span class="required">*</span></label>
                        
                        <div class="col-md-9 col-sm-9 col-xs-12">
                        <p>
                          M:
                          <input type="radio" class="flat" name="gender" id="genderM" value="M" checked="" required /> F:
                          <input type="radio" class="flat" name="gender" id="genderF" value="F" />
                        </p>
                        </div>
                      </div>

                      <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Country</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input id="countryx" name="countryx" class="form-control col-md-7 col-xs-12" type="text" value="" disabled="true" >
                        
                        </div>
                      
                      </div>
                     <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">City</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input id="cityy" name="cityy" class="form-control col-md-7 col-xs-12" type="text" value="" >
                        
                        </div>
                      </div>
                        <input type="hidden"  name="leaderId" >
                      </form>             
                    </div>
                    </div>
                      </div>
            
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button  onclick="addShortMember()" type="submit" class="btn btn-primary" name="submit" value="AddMember" data-dismiss="modal">Submit</button>
                      </div>
            
                    </div>
                  </div>
                </div>
                <script>
                   $(function() {
                     $("#localitiez").select2({ //create locality leaders drop down
                        maximumSelectionLength: 1,
                        placeholder: "Max 1",
                        allowClear: true
                      });
                     
                  });

                </script>
<!--/New Member -->

  <!-- Add new Region -->     
        
<div id="newRegion" class="modal fade newRegion" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-md">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Add New Region</h4>
              </div>
              <div class="modal-body">
                <div class="x_panel">
            
                  <div class="x_content">
                    <br />
                    <form id="addRegionForm" class="form-horizontal form-label-left input_mask" action="" method="post">

                  <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Region Code">
                          <input type="text" class="form-control has-feedback-left" id="regionCode" name="regionCode" placeholder="Region Code" required>
                          <span class="icon-key form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Region Name">
                          <input type="text" class="form-control has-feedback-left" id="regionName" name="regionName" placeholder="Region Name">
                          <span class="icon-pointer form-control-feedback left" aria-hidden="true"></span>
                        </div>
                    
                  </form>
                  </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                  <button onclick="saveRegion()" id="submit" class="btn btn-primary" value="AddNation" data-dismiss="modal">Submit</button>
                </div>
        
              </div>
            </div>
          </div>
        

  <!--- disciple edit modal --->      
<div id="discipleModal" class="modal fade discipleModal" tabindex="-1" role="dialog" 
aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" ><div class="lblHeading">Disciple Details</div></h4>
              </div>
              <div class="modal-body">
                   
                    <div class="clearfix"></div>
                    <div id="newdiscipleFormx" class="well" style="display: block">
                    <div class="row">
                 <div class="col-md-3 col-sm-12 col-xs-12">
                        <div id="uploadForm" class="avatarUploadForm">
                            <a onclick="showHide('uploadForm')" class="boxclose" type="button"></a>
                            <form id="uploadimagex" name="uploadimagex" action="" method="post" enctype="multipart/form-data">
                              <input class="form-control" id="image_file" name="image_file" type="file">
                              <input type="hidden" name="memberId">
                            </form>
                            <!--button class="btn btn-default" data-dismiss="modal" type="button">Close</button--><div style="margin:10px"></div>
                            <div class="text-center">
                            <button class="btn btn-primary btn-sm " onclick="saveImageFromFile('uploadimagex')" type="submit" name="submit" value="Upload">Upload Image</button>
                            </div>
                            <!--button class="btn btn-default btn-sm" data-toggle="modal" data-target=".imgUpload"><i class="glyphicon glyphicon-camera"></i> <?php echo lang("WEBCAM");?></button-->
                            <!--button class="btn btn-default btn-sm" onclick="imgDisplay()"> img file</button-->
                        </div>
                             <div class="profile_img">
                            
                              <div id="avatar">
                              <!-- Current avatar -->
                              <div id="uploadimagex_avatar"  data-toggle="tooltip" data-placement="bottom" title="Change the avatar">
                              <img  onclick="showHide('uploadForm')" class="img-responsive avatar-view" src="images/members/user.png" alt="Avatar" 
                              onerror="if (this.src != 'images/members/user.png') this.src = 'images/members/user.png';"></div>
                                            
                              <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                              </div>
                              
                               <!--div class="col-md-3 col-sm-12 col-xs-12 form-group has-feedback" >
                                <input id="imgFileName" value="" type="text" class="form-control has-feedback-left" >
                                <span class="icon-camera form-control-feedback left" aria-hidden="true"></span>
                              </div-->
                            </div>
                             <div class="divider"></div>
                               <button  onclick="makeUser()" id="submit" class="btn btn-success btn-sm"
                                value="submit"> <i class="fa fa-plus"></i> Make A User</button>
                    </div>

                      <div class="col-md-9 col-sm-12 col-xs-12">
                      <form id="discipleFormx" class="form-horizontal form-label-left input_mask" action="" method="POST">
                       <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input name="firstNamex" id="firstNamex"  type="text" class="form-control has-feedback-left"  placeholder="First Name" required="required">
                        <span class="icon-user form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control" name="lastNamex" id="lastNamex" placeholder="Last Name">
                        <span class="icon-users form-control-feedback right" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input name="emailx" id="emailx"  type="text" class="form-control has-feedback-left"  placeholder="Email" >
                        <span class="icon-envelope form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control" name="phonex" id="phonex" placeholder="Phone">
                        <span class="icon-screen-smartphone form-control-feedback right" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input name="dobx" id="dobx" type="datepicker" class="form-control has-feedback-left"  placeholder="DOB">
                        <span class="icon-calendar form-control-feedback left" aria-hidden="true"></span>
                      </div>
                       <script type="text/javascript">
                      $(document).ready(function() {
                        $('#dobx').datepicker({
                          singleDatePicker: true,
                          dateFormat:"yy-mm-dd",
                          yearRange: "1930:2065",
                          changeYear:true,
                          changeMonth:true,
                          calender_style: "picker_4"
                        }, function(start, end, label) {
                          console.log(start.toISOString(), end.toISOString(), label);
                        });
                      });
                    </script>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control" name="cityx" id="cityx" placeholder="City">
                        <span class="icon-map form-control-feedback right" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback-left">
                         <select id="genderx" name="genderx" class="form-control  has-feedback-left">
                            <option>Choose Gender</option>
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                          </select>
                        <span class="icon-symbol-female form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <select  class="form-control has-feedback-right localities" name="localityx" 
                        id="localityx">
                      </select>
                        <span class="icon-pin form-control-feedback right" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                         <select id="familyRolex" name="familyRolex" class="form-control has-feedback-left">
                            <option value="0">Choose Family Role</option>
                            <option value="1">Family head</option>
                            <option value="2">Spouse</option>
                            <option value="3">Child</option>
                           </select>
                        <span class="icon-user form-control-feedback left" aria-hidden="true"></span>
                      </div>

                       <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-right" name="addressx" 
                        id="addressx" placeholder="Address">
                        <span class="icon-pointer form-control-feedback right" aria-hidden="true"></span>
                      </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" name="proffessionx" 
                        id="proffessionx" placeholder="Proffession">
                        <span class="icon-pin form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-right" name="surbubx" 
                        id="surbubx" placeholder="Surbub">
                        <span class="icon-home form-control-feedback right" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                         <select  id="rolex" name="rolex" class="form-control has-feedback-left roleCombo">
                           </select>
                        <span class="icon-wrench form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                         <select  id="discipleMakerx" name="discipleMakerx" class="form-control has-feedback dm">
                            <option value="0">Choose Disciple Maker</option>
                           </select>
                        <span class="icon-user form-control-feedback right" aria-hidden="true"></span>
                      </div>

                         
                         <div class="col-md-12 col-sm-9 col-xs-12 control-group" data-toggle="tooltip" 
                        data-placement="top" placeholder="TAGS">
                        <input name="tagsx" id="tagsx" type="text" class="tags form-control" />
                        <div id="suggestions-container" style="position: relative; float: left; width: 250px;
                         margin: 10px;"></div>
                      </div>
                         
                      <input type="hidden"  name="id" >
                      <input type="hidden"  name="familyID" id="familyID">
                      <input type="hidden"  name="code" id="code">
                    </div>
                  </div>
                  </form>
                  <div class="divider"></div>
                  <div id ="btnDivv" class="text-right">
                      <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                      <button onclick="updateDisciple()" class="btn btn-primary  btn-sm" value="submit" data-dismiss="modal"><i class="fa fa-check"></i> Save </button> 
                    </div>
                  </div>

              </div>
            </div>
        </div>

     
  </div>

<div id="newDiscipleModal" class="modal fade newDiscipleModal" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
               <h4 class="modal-title" ><div class="lblHeading">New Disciple </div></h4>
              </div>
              <div class="modal-body">
                   
                    <div class="clearfix"></div>
                    <div id="newDiscipleFormx" class="well" style="display: block">
                    <div class="row">
                    <div class="col-md-3 col-sm-12 col-xs-12">
                    <div id="uploadForm2" class="avatarUploadForm">
                            <a onclick="showHide('uploadForm2')" class="boxclose" type="button"></a>
                            <form id="uploadimage2" name="uploadimage2" action="" method="post" enctype="multipart/form-data">
                              <input class="form-control" id="image_file" name="image_file" type="file">
                              
                            </form>
                            <!--button class="btn btn-default" data-dismiss="modal" type="button">Close</button--><div style="margin:10px"></div>
                            <div class="text-center">
                            <button class="btn btn-primary btn-sm " onclick="saveImageFromFile('uploadimage2')" type="submit" name="submit" value="Upload">Upload Image</button>
                            </div>
                            <!--button class="btn btn-default btn-sm" data-toggle="modal" data-target=".imgUpload"><i class="glyphicon glyphicon-camera"></i> <?php echo lang("WEBCAM");?></button-->
                            <!--button class="btn btn-default btn-sm" onclick="imgDisplay()"> img file</button-->
                        </div>
                             <div class="profile_img">
                            
                              <div id="avatar">
                              <!-- Current avatar -->
                              <div id="uploadimage2_avatar"  data-toggle="tooltip" data-placement="bottom" title="Change the avatar">
                              <img  onclick="showHide('uploadForm2')" class="img-responsive avatar-view" src="images/members/user.png" alt="Avatar" 
                              onerror="if (this.src != 'images/members/user.png') this.src = 'images/members/user.png';"></div>
                                            
                              <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                              </div>
                              
                               <!--div class="col-md-3 col-sm-12 col-xs-12 form-group has-feedback" >
                                <input id="imgFileName" value="" type="text" class="form-control has-feedback-left" >
                                <span class="icon-camera form-control-feedback left" aria-hidden="true"></span>
                              </div-->
                            </div>
      
                        </div>
                      <div class="col-md-9 col-sm-12 col-xs-12">
                      <form id="newDiscipleFormy" class="form-horizontal form-label-left input_mask" action="" method="POST">
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input name="firstName"  type="text" class="form-control has-feedback-left"  placeholder="First Name" required="required">
                        <span class="icon-user form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control" name="lastName"  placeholder="Last Name">
                        <span class="icon-users form-control-feedback right" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                         <select  name="familyName" class="form-control has-feedback-left familyCombo">
                           </select>
                        <span class="icon-pin form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input name="email"  type="text" class="form-control has-feedback"  placeholder="Email" >
                        <span class="icon-envelope form-control-feedback right" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                         <select  name="familyRole" class="form-control has-feedback-left">
                            <option value="0">Choose Family Role</option>
                            <option value="1">Family head</option>
                            <option value="2">Spouse</option>
                            <option value="3">Child</option>
                           </select>
                        <span class="icon-user form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control" name="phone" placeholder="Phone">
                        <span class="icon-screen-smartphone form-control-feedback right" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input id="dob" name="dob"type="text" class="form-control has-feedback-left dob" 
                         placeholder="0000-00-00">
                        <span class="icon-calendar form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      <!-- <input type="text" class="form-control" id="birthday" name="birthday" 
                             placeholder="0000-00-00" required="required"> -->
                       <script type="text/javascript">
                        $('#dob').datepicker({
                         changeMonth: true,
                          changeYear: true,
                          dateFormat: 'yy-mm-dd'

                      });
                    </script>

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control" name="address"  placeholder="Address">
                        <span class="icon-map form-control-feedback right" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback-left">
                         <select name="gender" class="form-control  has-feedback-left">
                            <option value="M">Choose Gender</option>
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                          </select>
                        <span class="icon-symbol-female form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-right" name="surburb" 
                         placeholder="Surburb">
                        <span class="icon-pin form-control-feedback right" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                         <select  name="role" class="form-control has-feedback-left roleCombo">
                           </select>
                        <span class="icon-wrench form-control-feedback left" aria-hidden="true"></span>
                      </div>

                       <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-right" name="city" 
                         placeholder="City">
                        <span class="icon-pointer form-control-feedback right" aria-hidden="true"></span>
                      </div>
                      
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                         <select  name="locality" class="form-control has-feedback-left localities">
                           </select>
                        <span class="icon-pin form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      
                     
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                         <select  name="discipleMaker" class="form-control has-feedback dm">
                            <option value="0">Choose Disciple Maker</option>
                           </select>
                        <span class="icon-user form-control-feedback right" aria-hidden="true"></span>
                      </div>
                       <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" name="profession" 
                         placeholder="Profession">
                        <span class="icon-pin form-control-feedback left" aria-hidden="true"></span>
                      </div>

                    </br></br>
                         
                         <div class="col-md-12 col-sm-9 col-xs-12 control-group" data-toggle="tooltip" 
                        data-placement="top" placeholder="TAGS">
                        <input id="tagsy" name="tags" type="text" class="tags form-control" value="" />
                        <div id="suggestions-container" style="position: relative; float: left; width: 250px;
                         margin: 10px;"></div>
                      </div>
                         
                      <input type="hidden"  name="id" >
                      <input type="hidden"  name="familyID" id="familyID">
                      <input type="hidden"  name="code" id="code">
                    </div>
                  </div>
                   <div class="divider"></div>
                  <div id ="btnDivv" class="text-right">
                      <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                      <button type="submit" onclick="saveNewDisciple('newDiscipleFormy','uploadimage2')" 
                      class="btn btn-primary  btn-sm" value="submit" data-dismiss="modal">
                      <i class="fa fa-check"></i> Save </button> 
                    </div>
                  
                  </form>
                  </div>

              </div>
            </div>
        </div>
  </div>
<script type="text/javascript">
  //$(function() {
  // $("#newDiscipleFormy").validate({
  //   rules: {
  //     firstName: {
  //       required: true,
  //       minlength: 3
  //     },
  //     action: "required"
  //   },
  //    rules: {
  //     lastName: {
  //       required: true,
  //       minlength: 3
  //     },
  //     action: "required"
  //   },
  //   messages: {
  //     firstName: {
  //       required: "Please enter your First Name",
  //       minlength: "Your name must be at least 3 characters"
//       },
//       action: "Please Enter Your First Name"
//     },
//       messages: {
//       lastName: {
//         required: "Please enter your Last Name",
//         minlength: "Your Last Name must be at least 3 characters"
//       },
//       action: "Please Enter Your Last Name"
//     }
//   });
// });
</script>
<script type="text/javascript">


  function updateDisciple(){ 
  var formName_="discipleFormx";
  var locality_=$("select[name=locality] option:selected").val(); 
  var gender_=$("select[name=gender] option:selected").val();
  var dm=$("select[name=discipleMaker] option:selected").val();
  var role_=$("select[name=role] option:selected").val();
  var firstName= discipleFormx.firstNamex.value;
  var lastName = discipleFormx.lastNamex.value;
  var email = discipleFormx.emailx.value;
  var address = discipleFormx.addressx.value;
  var surburb = discipleFormx.surbubx.value;
  var city = discipleFormx.cityx.value;
  var phone = discipleFormx.phonex.value;
  var tags = discipleFormx.tagsx_;
  var dob  = discipleFormx.dobx.value;
  var profession = discipleFormx.proffessionx.value;
  var id = discipleFormx.id.value;
    
   console.log(id);
  $.ajax({
     url:'api/api.php/records/cmfi_members/'+id,
    // url:"api/api.php/records/cmfi_members/"+id, //Required URL of the page on server
    type: 'PUT',
    data:{ // Data Sending With Request To Server
        firstName:firstName,
        lastName:lastName,
        locality:locality_,
        email:email,
        address:address,
        surburb:surburb,
        city:city,
        phone:phone,
        discipleMaker:dm,
        tags:tags,
        sex:gender_,
        DOB:dob,
        role:role_,
        profession:profession
        
      },
        success: function(data)   // A function to be called if request succeeds
        {
            successNote(sresponse,'Disciple updated successfully!');
           $('#datatable-disciples').DataTable().ajax.reload(); //refresh member table
            
        },
        error: function(err){
          errorNote(fresponse,'Failed to update Disciple Record! '+err.responseJSON.message);
        }
        
    // success: function(response,status){ // Required Callback Function
    //  if(response==1){successNote(sresponse,'Disciple updated successfully')
    //  }else{
    //    errorNote(fresponse,'Failed to update Disciple Record' +err.responseJSON.message)
      
    //  }
    // }
    });
}
</script>

 <!-- tags functionality -->
  <script src="js/tags/jquery.tagsinput.min.js"></script>
 
 <!-- input tags -->
  


  

  <script>
    function onAddTag(tag) {
      alert("Added a tag: " + tag);
    }

    function onRemoveTag(tag) {
      alert("Removed a tag: " + tag);
    }

    function onChangeTag(input, tag) {
      alert("Changed a tag: " + tag);
    }

     
  </script>
  
  <script> 
    $(function() {
      

      $('#tagsy').tagsInput({
        width: 'auto'
      });
       $('#tags').tagsInput({
        width: 'auto'
      });
       
     

     
     initialiseRolesCombo();
  });
      
     

      
</script>

