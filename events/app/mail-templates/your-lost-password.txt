Hello #USERNAME#<br/><br/>

We have set up a temporary password for your account at #WEBSITENAME#.
<br/>
Please login at #WEBSITEURL#login.php as soon as possible and change this password to something you will remember.
<br/>
Your Password: #GENERATED-PASS#
<br/>
-Regards
<br/><br/>

World Conquest DB team