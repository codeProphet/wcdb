Hello #USERNAME#<br/><br/>

We have received a new activation request for your account. Please follow the link below to activate.
<br/>
If you did not request this e-mail, please disregard this message.
<br/>
#ACTIVATION-URL
<br/>
-Regards
<br/><br/>
World Conquest DB Team