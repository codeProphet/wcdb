<?php
include_once'../config-small.php';
$tables=showTables(); 
$thelist="";
?>  
  <!-- Custom styling plus plugins -->
  <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Backup Database</h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Backup Wizard <small>backup database to .sql file</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
					<label  class="control-label col-md-3 col-sm-3 col-xs-12" for="exampleInputFile"></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
						 <h2 class="StepTitle">Backup Options</h2>
						  <p>
							specify the name of the backup file.
						  </p>
						  </div>
						 <form id="backupForm" action=""  method="post" role="form" class="form-horizontal form-label-left">
							<div class="form-group">
							 <label class="control-label col-md-3 col-sm-3 col-xs-12">Table Name</label>
							  <div class="col-md-6 col-sm-9 col-xs-12">
								<select id="tables" name="tables" class="form-control">
								  <option value="*">Whole Database</option>
								  <?php 
									foreach ($tables as $t) 
										{
											echo '<option value="'.$t["table"].'">'.$t["table"].'</option>';
										}
								  ?>
								</select>
							  </div>
							 </div>
							<div class="form-group">
							 <label class="control-label col-md-3 col-sm-3 col-xs-12">Compress</label>
							  <div class="col-md-6 col-sm-9 col-xs-12">
								<input type="checkbox" name="compressx" id="compressx" value="1" class="flat" />
							  </div>
							</div>
						</form>
					<div class="form-group">
						<label  class="control-label col-md-3 col-sm-3 col-xs-12" for="import"></label>
						<div class="col-md-6 col-sm-9 col-xs-12">
							<button onclick="backupDB()" type="submit" class="btn btn-default" name="Import" value="Import"><i class="glyphicon glyphicon-download"></i> Run Backup</button>
							
						</div>
					</div>
                </div>
              </div>
            </div>
			<div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel" >
			   <div class="x_title">
                  <h2>Backup Files <small>list of back-ups,click to download </small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#"><i class="glyphicon glyphicon-trash"></i> Delete All</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
				<div class="x_content" id="results" style="height:150px;overflow-y: show;">
					<?php
					  if ($handle = opendir('../data/backup/')) {
						while (false !== ($file = readdir($handle))) {
						  if ($file != "." && $file != "..") {
							$thelist .= '<li><a href="data/backup/'.$file.'"><i class="fa fa-file-archive-o"></i>'.$file.'</a></li>';
						  }
						}
						closedir($handle);
					  }
					?>
					<ul class="list-unstyled project_files"><?php echo $thelist; ?></ul>
				</div>
			  </div>
			</div> <!--Results panel-->
          </div>
        </div>
      </div>
      <!-- /page content -->
<script type="text/javascript">
   $(document).ready(function() {
      $("#loaderImg").hide(); //hide loader image
    });
</script>