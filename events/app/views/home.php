<?php
$userId=$loggedInUser->user_id;
$userEmail=$loggedInUser->email;
$alertBox="";
$headerIncTitle="Home";
$image=$loggedInUser->image;
$page='Home';
$menu="";
$x="";
$stats=new memberStats($x);
if($image==''){$image='user1.png';} 
writeMapData();
updateGauge();

// Add New Nation
	if (isset($_POST['submit']) && $_POST['submit'] == 'AddNation') {
		$nationDate = $mysqli->real_escape_string($_POST['nationDate']);
		$nationName = $mysqli->real_escape_string($_POST['nationName']);
		$nation=new nation($nationDate,$nationName);
		if($nation->Add()=="success") { 
		$alertBox='<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <i class="fa fa-check-circle" style="font-size:28px;"></i> New nation <strong>'.$nationName.'</strong> added successfully! 
                </div>';
		}
		else{
			$alertBox='<div class="alert alert-error alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong>Error</strong> new data not saved.
                </div>';
		}
				
				
		
	}
	
// Delete Nation
	if (isset($_POST['submit']) && $_POST['submit'] == 'deleteNation') {
		$nationId = $mysqli->real_escape_string($_POST['nationId']);
		$nationName = $mysqli->real_escape_string($_POST['nationName']);
		
		// Delete the nation
		$stmt = $mysqli->prepare("DELETE FROM cmfi_nations WHERE nationID = ?");
		$stmt->bind_param('i', $nationId);
		
		if($stmt->execute()) { 
		$alertBox='<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <i class="fa fa-check-circle" style="font-size:28px;"></i> Nation <strong>'.$nationName.'</strong> deleted successfully!
                </div>';
		}
		else{
			$alertBox='<div class="alert alert-error alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <i class="fa fa-delete-circle" style="font-size:28px;"></i><strong> Error</strong> new data not saved.
                </div>';
		}
		$stmt->close();

		/*Delete all related Provinces
		$stmt = $mysqli->prepare("DELETE FROM cmfi_provinces WHERE nation = ?");
		$stmt->bind_param('s', $nationId);
		$stmt->execute();
		$stmt->close();*/
    }			
// Add new member
	if (isset($_POST['submit']) && $_POST['submit'] == 'AddMemberAddMember') {
		$firstName = $mysqli->real_escape_string($_POST['firstName']);
		$lastName = $mysqli->real_escape_string($_POST['lastName']);
		$email = $mysqli->real_escape_string($_POST['email']);
		$phone = $mysqli->real_escape_string($_POST['phone']);
		$sex = $mysqli->real_escape_string($_POST['gender']);
		$country = $mysqli->real_escape_string($_POST['country']);
		$locality = $mysqli->real_escape_string($_POST['locality']);
		$DOB = $mysqli->real_escape_string($_POST['DOB']);
		$member=new member($firstName,$lastName,$phone,$email,$sex,$locality,$country,$DOB);
		if($member->Add()=="success") { 
		$alertBox='<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <i class="fa fa-check-circle" style="font-size:28px;"></i> New member <strong>'.$firstName.' '.$lastName.'</strong> added successfully!
                </div>';
		}
		else{
			$alertBox='<div class="alert alert-error alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <i class="fa fa-delete-circle" style="font-size:28px;"></i><strong> Error</strong> new data not saved.
                </div>';
		}
	}	


//Add New Report
if (isset($_POST['submit']) && $_POST['submit'] == 'AddReport') {
		$members = $mysqli->real_escape_string($_POST['members']);
		$localities = $mysqli->real_escape_string($_POST['localities']);
		$houseChurches = $mysqli->real_escape_string($_POST['houseChurches']);
		$leaders = $mysqli->real_escape_string($_POST['leaders']);
		$notes = $mysqli->real_escape_string($_POST['notes']);
		$country = $mysqli->real_escape_string($_POST['nation']);
		$date = $mysqli->real_escape_string($_POST['reportDate']);
		$report=new report($members,$localities,$houseChurches,$leaders,$notes,$country,$date,$userId);
		if($report->Add()=="success") { 
		$alertBox='<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <i class="fa fa-check-circle" style="font-size:28px;"></i> New Report for <strong>'.$country.'</strong> added successfully!
                </div>';
		}
		else{
			$alertBox='<div class="alert alert-error alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <i class="fa fa-delete-circle" style="font-size:28px;"></i><strong> Error</strong> new data not saved.
                </div>';
		}
	}	


//Add New Province
if (isset($_POST['submit']) && $_POST['submit'] == 'AddProvince') {
		$provinceName = $mysqli->real_escape_string($_POST['provinceName']);
		$country = $mysqli->real_escape_string($_POST['nation']);
		$leader=0;
		$province=new province($provinceName,$leader,$country);
		if($province->Add()!=0) { 
		$alertBox='<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <i class="fa fa-check-circle" style="font-size:28px;"></i> New Province <strong>'.$provinceName.'</strong> added successfully!
                </div>';
		}
		else{
			$alertBox='<div class="alert alert-error alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <i class="fa fa-delete-circle" style="font-size:28px;"></i><strong> Error</strong> new data not saved.
                </div>';
		}
	}
//Add New Locality
if (isset($_POST['submit']) && $_POST['submit'] == 'AddLocality') {
		$localityName = $mysqli->real_escape_string($_POST['localityName']);
		$datePlanted = $mysqli->real_escape_string($_POST['datePlanted']);
		$province = $mysqli->real_escape_string($_POST['province']);
		$country = $mysqli->real_escape_string($_POST['nation']);
		$leader=0;
		$locality=new locality($localityName,$province,$datePlanted,$leader,$country);
		if($locality->Add()!=0) { 
		$alertBox='<div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <i class="fa fa-check-circle" style="font-size:28px;"></i> New locality <strong>'.$locality.'</strong> added successfully!
                </div>';
		}
		else{
			$alertBox='<div class="alert alert-error alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <i class="fa fa-delete-circle" style="font-size:28px;"></i><strong> Error</strong> new data not saved.
                </div>';
		}
	}
include('includes/head.php');
	
?>

<script>
$(document).on('change', 'select#countryName', function(){

            var value = $('select#countryName option:selected').val();
		$("#localities").load("includes/fetchLocalities.php?nationID="+value)

});
</script>
<script>

	function openPage(p){
		
		if(p==1){
		var page="backhome";
		}
		else if(p==2){ var page="calendar";	}
		else if(p==3){ var page="localities";	}
		else if(p==4){ var page="nations";	}
		else if(p==5){ var page="members";	}
		else if(p==6){ var page="reportsx";	}
		else if(p==7){ var page="profile";	}
		else if(p==8){ var page="siteSettings";	}
		else if(p==9){ var page="users";	}
		
		var getPage="pages/"+page+".php";
		$("#contentDiv").load(getPage).show();
	}
	function nationProfile(id,name){
			//alert (id);
			$("#contentDiv").load("pages/nationDash.php?nation="+id+"&nationName="+name)
											
												return false;
		 }
	
	function showUserProfile(id){
			//alert (id);
			$("#contentDiv").load("pages/viewProfile.php?userId="+id)
											
												return false;
		 }
  
	function loadProvinces(id){
			//alert (id);
			$("#provinces").load("includes/fetchProvinces.php?nationID="+id)
											
												return false;
		}
	function loadLocalities(id){
			//alert (id);
			$("#contentDiv").load("pages/localities.php?nationID="+id)
											
												return false;
		 }
		 
	function viewMemberTab(id,name){
		var tabID="tab_content_"+id;
		$("#homeTab").removeClass("active");
		$("#homeTab").after('<li role="presentation" class=""><a href="#'+tabID+'" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="true">'+name+'</a></li>');
		$("#tab_content1").after( '<div role="tabpanel" class="tab-pane fade active in" id="'+tabID+'" aria-labelledby="home-tab"></div>')
		$("#"+tabID).load("pages/memberProfile.php?memberID="+id);
	}	 
	function closeTab(tabId){
		$('a[href$="'+tabId+'"]').parent().remove();
		var index = $('#memberTabs a[href="#tab_content1"]').parent().index();
		$( "#memberTabs" ).tabs({ active: index });
	}

</script>


<body class="nav-md">

  <div class="container body">


    <div class="main_container">

	<?php include("includes/sidebar.php"); ?>
	<?php include("includes/top_nav.php"); ?>
	
	

      <!-- page content -->
      <div id="contentDiv" class="right_col" role="main">
		<div><?php echo $alertBox; ?></div>
        <!-- top tiles -->
        <div class="row tile_count">
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count dashTab" onclick="openPage(5)">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-users"></i> Total Members</span>
              <div class="count"><?php echo $stats->total ;?></div>
              <span class="count_bottom"><i class="green">4% </i> From last year</span>
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count dashTab" onclick="openPage(4)">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-globe"></i> Nations</span>
              <div class="count green"><?php echo $stats->nations ;?></div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> From last year</span>
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count dashTab">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-users"></i> Reports</span>
              <div class="count"><?php echo $stats->adults ;?></div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last year</span>
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count dashTab">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-institution"></i> Localities</span>
              <div class="count"><?php echo $stats->localities ;?></div>
              <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>12% </i> From last year</span>
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count dashTab">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-home"></i> House Churches</span>
              <div class="count"><?php echo $stats->houseChurches ;?></div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last year</span>
            </div>
          </div>
          <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count dashTab">
            <div class="left"></div>
            <div class="right">
              <span class="count_top"><i class="fa fa-male"></i> Leaders</span>
              <div class="count"><?php echo $stats->total ;?></div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last year</span>
            </div>
          </div>

        </div>
        <!-- /top tiles -->

        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
          
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Members location <small>geo-presentation</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="dashboard-widget-content">
                      <div class="col-md-3 hidden-small">
                        <h2 class="line_30"><?php echo $stats->total.' members from '.$stats->nations.' nations';?></h2>

                        <table class="countries_list">
                          <tbody>
							  <?php $str="select count(id) as members, c.name, c.code from cmfi_members m inner join cmfi_countries c on m.country=c.code group by country order by members desc LIMIT 5" ;
									$result = mysqli_query($mysqli, $str);
									while( $rs = $result->fetch_array(MYSQLI_ASSOC)) {
							  ?>
                            <tr>
                              <td><a style="cursor:pointer;" onclick="nationProfile('<?php echo $rs["code"]; ?>','<?php echo $rs["name"];?>')"><?php echo $rs["name"];?></a></td>
                              <td class="fs15 fw700 text-right"><?php echo $rs["members"];?></td>
                            </tr>
								<?php }?>
                           
                          </tbody>
                        </table>
                      </div>
                      <div id="world-map-gdp" class="col-md-9 col-sm-12 col-xs-12" style="height:300px;"></div>
                    </div>
                  </div>
                </div>
        </div>
        <br />
		</div>
		
        <div class="row">


          <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="x_panel tile fixed_height_320 overflow_hidden">
                      <div class="x_title">
                        <h2>Top Profiles</h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#">Settings 1</a>
                              </li>
                              <li><a href="#">Settings 2</a>
                              </li>
                            </ul>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
					  <div class="x_content">
                      <ul class="list-unstyled top_profiles scroll-view">
                        <li class="media event">
                          <a class="pull-left border-aero profile_thumb">
                            <i class="fa fa-user aero"></i>
                          </a>
                          <div class="media-body">
                            <a class="title" href="#">Ms. Mary Jane</a>
                            <p><strong>$2300. </strong> Agent Avarage Sales </p>
                            <p> <small>12 Sales Today</small>
                            </p>
                          </div>
                        </li>
                        <li class="media event">
                          <a class="pull-left border-green profile_thumb">
                            <i class="fa fa-user green"></i>
                          </a>
                          <div class="media-body">
                            <a class="title" href="#">Ms. Mary Jane</a>
                            <p><strong>$2300. </strong> Agent Avarage Sales </p>
                            <p> <small>12 Sales Today</small>
                            </p>
                          </div>
                        </li>
                        <li class="media event">
                          <a class="pull-left border-blue profile_thumb">
                            <i class="fa fa-user blue"></i>
                          </a>
                          <div class="media-body">
                            <a class="title" href="#">Ms. Mary Jane</a>
                            <p><strong>$2300. </strong> Agent Avarage Sales </p>
                            <p> <small>12 Sales Today</small>
                            </p>
                          </div>
                        </li>
                        
                       
                      </ul>
					  </div>
                    </div>
          </div>

          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="x_panel tile fixed_height_320 overflow_hidden">
              <div class="x_title">
                <h2>Top Dates</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
             <article class="media event">
                    <a class="pull-left date">
                      <p class="month">April</p>
                      <p class="day">23</p>
                    </a>
                    <div class="media-body">
                      <a class="title" href="#">Item One Title</a>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                  </article>
                  <article class="media event">
                    <a class="pull-left date">
                      <p class="month">April</p>
                      <p class="day">23</p>
                    </a>
                    <div class="media-body">
                      <a class="title" href="#">Item Two Title</a>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                  </article>
                  <article class="media event">
                    <a class="pull-left date">
                      <p class="month">April</p>
                      <p class="day">23</p>
                    </a>
                    <div class="media-body">
                      <a class="title" href="#">Item Two Title</a>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                  </article>
                 
              </div>
            </div>
          </div>


          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="x_panel tile fixed_height_320">
              <div class="x_title">
                <h2>Quick Actions</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Settings 1</a>
                      </li>
                      <li><a href="#">Settings 2</a>
                      </li>
                    </ul>
                  </li>
                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <div class="dashboard-widget-content">
                  <ul class="quick-list">
                    <li><i class="fa fa-calendar-o"></i><a href="#">Open Calendar</a>
                    </li>
                    <li><i class="fa fa-envelope-o"></i><a href="#">Send Report</a>
                    </li>
                    <li><i class="fa fa-home"></i><a href="#">Add Member</a> </li>
                    <li><i class="fa fa-globe"></i><a href="#">Add Nation</a>
                    </li>
                    <li><i class="fa fa-users"></i><a href="#">View members</a> </li>
                    <li><i class="fa fa-user"></i><a href="#">My Profile</a>
                    </li>
                    <li><i class="fa fa-area-chart"></i><a href="#">Logout</a>
                    </li>
                  </ul>

                  <div class="sidebar-widget">
                    <h4>Database Growth</h4>
                    <canvas width="150" height="80" id="foo" class="" style="width: 160px; height: 100px;"></canvas>
                    <div class="goal-wrapper">
                      <span class="gauge-value pull-left">MB</span>
                      <span id="gauge-text" class="gauge-value pull-left"><?php echo getDBSize();?></span>
                      <span id="goal-text" class="goal-value pull-right">1,024MB</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>


    
       
          </div>
        </div>
      </div>
      <!-- /page content -->

<?php
include('includes/footer.php');
?>

