<?php
 include "../templates/nations_tiled_links.wcdb";
 include "../templates/nations_tiled.wcdb";

?>

        <div class="">
          <div class="page-title">
            <div class="title_left">
			
              <h3>
                    <a href="." type="button"><i class="fa fa-arrow-circle-left"></i></a>  Manage Nations
                    <small>
                        details of all nations registered
                    </small>
                </h3>
            </div>

            <div class="title_right">
              <!--div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                </div>
              </div-->
			  <div class="col-md-7 col-sm-7 col-xs-12 form-group pull-right">
			  <div class="btn-group">
					<a onclick="openPage(4)" class="btn btn-default btn-sm" ><i class="glyphicon glyphicon-list"></i></a>
					<a onclick="openPage(12)" class="btn btn-default btn-sm" ><i class="glyphicon glyphicon-refresh"></i></a>
                    <button class="btn btn-success btn-sm" onclick="openPage(22)"><i class="glyphicon glyphicon-plus"></i> New Nation</button>
				</div>
			  </div>
            </div>
          </div>
          <div class="clearfix"></div>

         <div class="row">
            <div class="col-md-12">
              <div class="x_panel">
                <div class="x_content">

                  <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center;" id="paginationDiv"></div>
                    <div class="clearfix"></div>
					<div id="nationsDisplay"></div>


                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
              </div>
            </div>
            <!-- /page content -->

<script type="text/javascript">

$(function() {

	var alphabet={"letter":["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]};
	var linksTemplate = $('#linkstpl').html();
	var linksHtml=Mustache.render(linksTemplate,alphabet);
	$('#paginationDiv').html(linksHtml);
	
	$.getJSON('api/funcs.php?fn=getNationsL&letter=A', function(data) {
		var template = $('#nationtpl').html();
		var html = Mustache.to_html(template, data);
		$('#nationsDisplay').html(html);
	}); //getFirst Nations letter A
	
	
		
	}); //function

 $("#loaderImg").hide(); //hide loader image
</script>