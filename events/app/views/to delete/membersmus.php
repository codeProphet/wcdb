
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>
                   <a href="." type="button"><i class="fa fa-arrow-circle-left"></i></a> Manage Members
                    <small>
                        
                    </small>
                </h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="row">


		  <div class="col-md-12">
			<div class="x_panel">
			  <div class="x_content">
			  

							
			   <div id="memberTabs" class="" role="tabpanel" data-example-id="togglable-tabs">
				<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
				  <li id="homeTab" role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Home</a>
				  </li>
				  <!--li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Profile</a>
				  </li>
				  <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Profile</a>
				  </li-->
				</ul>
				<div id="myTabContent" class="tab-content">
					
					<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab" tabindex='1'>
							  
					  <div class="col-md-12 col-sm-12 col-xs-12">
                        <p class="text-muted font-13 m-b-30">
                          
                        </p>
                           <a href='#' id="add_more" class="btn btn-primary"><span class="glyphicon glyphicon-plus"> Add Member</span></a>
							<span id="spinner" class="glyphicon glyphicon-refresh"></span>
							<a href="#" class='record_count'>Records <span class="badge"></span></a>
							<span id="found" class="label label-info"></span>
							<table id="members_table" class='table table-striped table-bordered'>
							  <thead>
								<tr>
								  <th>FirstName</th>
								  <th>LastName</th>
								  <th>Adress</th>
								  <th>Surburb</th>
								  <th>Locality</th>
								  <th>City</th>
								  <th>Country</th>
								  <th>Sex</th>
								  <th>Phone</th>
								  <th>E-mail</th>
								  <th>Actions</th>
								</tr>
							  </thead>
							  <tbody>
							  </tbody>
							</table>
							<div id="summary"><div>
						  </div>
						</div>

						<script id="template" type="text/html">
						 {{#members}}
							<tr id="member{{id}}">
							  <td><a onclick="viewMemberTab({{id}},'{{firstName}}')" style="cursor:pointer;">{{firstName}}</a></td>
							  <td>{{lastName}}</td>
							  <td>{{address}}</td>
							  <td>{{surburb}}</td>
							  <td>{{locality}}</td>
							  <td>{{city}}</td>
							  <td id="country{{id}}">{{country}}</td>
							  <td>{{sex}}</td>
							  <td>{{phone}}</td>
							  <td>{{email}}</td>
							  <td><a id="edit{{id}}" onclick="editMember({{id}})" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-pencil"></i></a>
							  <a  id="delete{{id}}" onclick="deleteMember({{id}})" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove"></i></a></td>
							</tr>
						{{/members}}
						
	
							
						  </div>
						  {{#members}}
						  <div role="tabpanel" class="tab-pane fade" id="tab_content{{id}}" aria-labelledby="profile-tab"></div>
						  {{/members}}
						</script>
						</div>
					  </div>

					

                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /page content -->
 <script>
 //var data=members;
$(document).ready(function() {
var options = {
  view: view,                    //View function to render rows.
  data_url: 'config/funcs.php?fn=fetchmembers',  //Data fetching url
  stream_after: 2,               //Start streaming after 2 secs
  fetch_data_limit: 500,         //Streaming data in batch of 500
}
 
var data=getData();

var template = Mustache.compile($.trim($("#template").html()));
 
var view = function(record, index){
  return template({record: record, index: index});
};

function getData(){
$.ajax({                                      
 type: 'post',
 url: 'config/funcs.php?fn=fetchmembers',                  
 data: "fn=fetchmembers",                                       
 dataType: 'json',                     
 success: function(data){     
  if(data){
	  
	 return data
   
  }else{
   alert("oops nothing happened :(");
  }        
 } 
}); }
    
});

</script>    

