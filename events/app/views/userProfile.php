<?php 
//get global settings
require_once("../config-small.php"); //loads the system configuration settings
session_start();
$memberID=$_REQUEST['id'];
$member=fetchMemberDetails($memberID);
$familyId=$member['familyId'];
$code=$member['country'];
$tabID="tab_content".$memberID;
$lang_path="../".$_SESSION["wcdbUser"]->lang;
require_once("$lang_path");
$familyMembers=getFamily($familyId);
$families=getFamilies($code);
//get nations and countries
$nations=getNations();
$countries=getCountries();

?>
  <!-- icheck -->
  <script src="js/icheck/icheck.min.js"></script>
  <script src="js/custom-sm.js"></script>
<style>
/* member avatar update form   */

  a.boxclose{
    float:right;
    margin-top:-10px;
    margin-right:-10px;
    cursor:pointer;
    color: #fff;
    border: 1px solid #AEAEAE;
    border-radius: 10px;
    background: red;
    font-size: 11px;
    font-weight: bold;
    display: inline-block;
    line-height: 0px;
    padding: 6px 3px;       
}

.avatarUploadForm{
  display:none;
  border:2px dashed #d2d2d2;
  background-color:#f7f7f7; 
  padding:2px;
}

.boxclose:before {
    content: "×";
}

/*Family tree*/
* {margin: 0; padding: 0;}

.tree ul {
  padding-top: 20px; position: relative;
  
  transition: all 0.5s;
  -webkit-transition: all 0.5s;
  -moz-transition: all 0.5s;
}

.tree li {
  float: left; text-align: center;
  list-style-type: none;
  position: relative;
  padding: 20px 5px 0 5px;
  
  transition: all 0.5s;
  -webkit-transition: all 0.5s;
  -moz-transition: all 0.5s;
}

/*We will use ::before and ::after to draw the connectors*/

.tree li::before, .tree li::after{
  content: '';
  position: absolute; top: 0; right: 50%;
  border-top: 1px solid #ccc;
  width: 50%; height: 20px;
}
.tree li::after{
  right: auto; left: 50%;
  border-left: 1px solid #ccc;
}

/*We need to remove left-right connectors from elements without 
any siblings*/
.tree li:only-child::after, .tree li:only-child::before {
  display: none;
}

/*Remove space from the top of single children*/
.tree li:only-child{ padding-top: 0;}

/*Remove left connector from first child and 
right connector from last child*/
.tree li:first-child::before, .tree li:last-child::after{
  border: 0 none;
}
/*Adding back the vertical connector to the last nodes*/
.tree li:last-child::before{
  border-right: 1px solid #ccc;
  border-radius: 0 5px 0 0;
  -webkit-border-radius: 0 5px 0 0;
  -moz-border-radius: 0 5px 0 0;
}
.tree li:first-child::after{
  border-radius: 5px 0 0 0;
  -webkit-border-radius: 5px 0 0 0;
  -moz-border-radius: 5px 0 0 0;
}

/*Time to add downward connectors from parents*/
.tree ul ul::before{
  content: '';
  position: absolute; top: 0; left: 50%;
  border-left: 1px solid #ccc;
  width: 0; height: 20px;
}

.tree li a{
  border: 1px solid #ccc;
  padding: 5px 10px;
  text-decoration: none;
  color: #666;
  font-family: arial, verdana, tahoma;
  font-size: 11px;
  display: inline-block;
  
  border-radius: 5px;
  -webkit-border-radius: 5px;
  -moz-border-radius: 5px;
  
  transition: all 0.5s;
  -webkit-transition: all 0.5s;
  -moz-transition: all 0.5s;
}

/*Time for some hover effects*/
/*We will apply the hover effect the the lineage of the element also*/
.tree li a:hover, .tree li a:hover+ul li a {
  background: #c8e4f8; color: #000; border: 1px solid #94a0b4;
}
/*Connector styles on hover*/
.tree li a:hover+ul li::after, 
.tree li a:hover+ul li::before, 
.tree li a:hover+ul::before, 
.tree li a:hover+ul ul::before{
  border-color:  #94a0b4;
}

</style>
<script type="text/javascript">
   //load calendar
      $("#calendar-container").load("views/calendar.php?n=<?php echo $code;?>").show();
</script>
   
    <!--div class="x_title">
    <h2><a onclick="openPage(21)" type="button"><i   class="icon-arrow-left purple"></i></a> User Profile</h2>
    <ul class="nav navbar-right panel_toolbox">
      <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
      <ul class="dropdown-menu" role="menu">
        <li><a href="#"><i class="icon-printer"></i> Print</a>
        </li>
        <li><a href="#"><i class="icon-shield"></i> Deactivate</a>
        </li>
         <li><a href="#"  data-toggle="modal" data-target=".confirmUser"><i class="icon-user-follow" ></i> Make User</a>
        </li>
        <li><a onclick="deleteMember(<?php echo $member['id']; ?>)"><i class="icon-trash"></i> Delete</a>
        </li>
      </ul>
      </li>
      <li><a onclick="closeTab('<?php echo $tabID;?>')" class="close-link"><i class="fa fa-close"></i></a>
      </li>
    </ul>
    <div class="clearfix"></div>
    </div-->
    
   
     
    
      
      <div class="col-xs-10">
                    <!-- Tab panes -->
              <div class="tab-content">
                <div class="tab-pane active" id="home-r">
                  <div class="col-md-3 col-sm-12 col-xs-12">
                    <div id="uploadForm" class="avatarUploadForm">
                      <a onclick="showHide('uploadForm')" class="boxclose" type="button"></a>
                      <form id="uploadimage" action="" method="post" enctype="multipart/form-data">
                        <input id="id" name="memberId" value="<?php echo $member["id"]; ?>" type="hidden">
                        <input class="form-control" id="image_file" name="image_file" type="file">
                      </form>
                      <!--button class="btn btn-default" data-dismiss="modal" type="button">Close</button--><div style="margin:10px"></div>
                      <button class="btn btn-primary btn-sm" onclick="saveImageFromFile('uploadimage')" type="submit" name="submit" value="Upload">Upload</button>
                    </div>
                       <div class="profile_img">
                      
                        <!-- end of image cropping -->
                        <div id="avatar">
                        <!-- Current avatar -->
                        <div id="profileAvatar"  data-toggle="tooltip" data-placement="bottom" title="Change the avatar">
                        <img onclick="showHide('uploadForm')" class="img-responsive avatar-view" src="images/members/<?php echo $member["image"];?>" alt="Avatar" 
                        onerror="if (this.src != 'images/members/user.png') this.src = 'images/members/user.png';"></div>
                                      
                        <!-- Loading state -->
                        <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                        </div>
                        <!-- end of image cropping -->

                      </div>
                    <div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center;" >
                      
                      <h3><?php echo $member["firstName"]." ".$member["lastName"];?></h3><br>
                      <span class="badge"><?php if(!is_null($member["userId"])){echo "System User";} ?></span>
                      <span class="badge"><?php if($member["isAdmin"]==1){echo "Admin";} ?></span><br><br>
                     <div class="flex">
                          <ul class="list-inline count2">
                            <li>
                              <h3><?php echo $member["visits"];?></h3>
                              <span>Visits</span>
                            </li>
                            <li>
                              <h4><?php echo date('m.d.y',$_SESSION["wcdbUser"]->lastSignIn);?></h4>
                              <span>Last seen</span>
                            </li>
                            <li>
                              <h3><?php echo getRecords($memberID);?></h3>
                              <span>Records</span>
                            </li>
                          </ul>
                        </div>

                      </div>
                    </div>
        <div class="col-md-9 col-sm-12 col-xs-12">
            <form id="saveUser" class="form-horizontal form-label-left input_mask" action="" method="post">
            <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="First Name(s)">
              <input type="text" class="form-control has-feedback-left" id="firstNamex"  placeholder="First Name" value="<?php echo $member["firstName"]; ?>" required>
              <span class="icon-user form-control-feedback left" aria-hidden="true"></span>
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Surname">
              <input type="text" class="form-control has-feedback-left" id="lastNamex" value="<?php echo $member["lastName"]; ?>" placeholder="Last Name" required>
              <span class="icon-users form-control-feedback left" aria-hidden="true"></span>

            </div>
            

            <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Email">
              <input type="text" class="form-control has-feedback-left" id="emailx" value="<?php echo $member["email"]; ?>" name="email" placeholder="Email" required>
              <span class="icon-envelope form-control-feedback left" aria-hidden="true"></span>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Address">
              <input type="text" class="form-control has-feedback-left" id="addressx" value="<?php echo $member["address"]; ?>" placeholder="Address">
              <span class="icon-pointer form-control-feedback left" aria-hidden="true"></span>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Surburb">
              <input type="text" class="form-control has-feedback-left" id="surburbx" value="<?php echo $member["surburb"]; ?>" placeholder="Surburb">
              <span class="icon-home form-control-feedback left" aria-hidden="true"></span>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="City">
              <input type="text" class="form-control has-feedback-left" id="cityx" value="<?php echo $member["city"]; ?>" placeholder="City">
              <span class="icon-map form-control-feedback left" aria-hidden="true"></span>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback">
              <select id="countryNamez" name="countryNamez" class="form-control has-feedback-left">
                <option value="">Choose Country</option>
               <?php 
                foreach ($nations as $n) 
                  {
                    echo '<option  value="'.$n["code"].'">'.$n["name"].'</option>';
                  }
                ?>
              </select>
             <span class="icon-globe form-control-feedback left" aria-hidden="true"></span>
                
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Locality">
                
              <select id="localitiesz" name="localitiesz" class="form-control has-feedback-left">
                <option>Choose Locality</option>
               </select>
            
              <span class="icon-pin form-control-feedback left" aria-hidden="true"></span>
            </div>

            <!--function to set the selected country-->
            <?php echo"<script>
                $( 'select#countryNamez' ).val( '".$code."' );
                $('select#countryNamez').trigger('change');
            </script>"; ?>  
            <script>
              $(document).on('change', 'select#countryNamez', function(){
                  var value = $('select#countryNamez option:selected').val();
                  $("select#localitiesz").load("includes/fetchLocalities.php?nationID="+value);
              })
            
            </script>
            
            <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Phone Number">
              <input type="text" class="form-control has-feedback-left" id="phonex" value="<?php echo $member["phone"]; ?> "data-validate-length-range="8,20" placeholder="Phone">
              <span class="icon-screen-smartphone form-control-feedback left" aria-hidden="true"></span>
            </div>
            
            <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Date of Birth">
              <input id="birthdayx" id="DOB" class="date-picker form-control has-feedback-left birthdayx" value="<?php echo $member["DOB"]; ?>" type="text" required>
               <span class="icon-calendar form-control-feedback left" aria-hidden="true" ></span>
            </div>
                <script>
                      $( function() {
                        $( ".birthdayx" ).datepicker({
                            dateFormat: "yyyy-mm-dd",
                            ShowAnim:"clip",                 
                            changeMonth: true,
                            changeYear: true
                        });
                      } );
                  </script>
            
            <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback">
             <label class="control-label col-md-3 col-sm-3 col-xs-12"><span class="required">*</span>Gender:</label>
              
               <div class="col-md-9 col-sm-9 col-xs-12">
               <p>
                M:
                <input type="radio" class="flat" name="gender" id="genderM" value="M" <?php if( $member["sex"]=="M"){echo 'checked=""';}?> required /> F:
                <input type="radio" class="flat" name="gender" id="genderF" value="F"  <?php if( $member["sex"]=="F"){echo 'checked=""';}?>/>
              </p>
              </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 control-group" data-toggle="tooltip" data-placement="top" title="Tags">
              
              <input id="tags_" type="text" class="tags form-control" value="<?php echo $member["tags"];?>" />
              <div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
            
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12 product_price">
               <h4>Change your password here</h4>
              <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Password">
                  <input type="password" class="form-control has-feedback-left" id="passwordx" > 
                  <span class="icon-key form-control-feedback left" aria-hidden="true"></span>
                  <p style="font-style: italic;font-size: 10px;">leave unchanged if you do not intend to change password</p>
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Confirm Password">
                  <input type="password" class="form-control has-feedback-left" id="confirmPasswordx">
                  <span class="icon-key form-control-feedback left" aria-hidden="true"></span>
                  <span id="message"></span>
                </div>

                <script>
                  $('#passwordx, #confirmPasswordx').on('keyup', function () {
                      if ($('#passwordx').val() == $('#confirmPasswordx').val()) {
                        $('#confirmPasswordx').css('border-color', 'green');
                        $('#passwordx').css('border-color', 'green');
                      } else 
                        $('#confirmPasswordx').css('border-color', 'red');
                    });
                </script>
              </div>
            </div>
             <input id="id2" name="memberId2" value="<?php echo $member["id"]; ?>" type="hidden">
            </form> 
           </div>
         </div>
       
          <div class="tab-pane" id="profile-r">
                  <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Family Name">
                      <select id="familyNamex" name="familyNamex" class="form-control has-feedback-left">
                        <option value="0">Add new family</option>
                       <?php 
                        foreach ($families as $family) 
                          {
                            echo '<option  value='.$family["id"].'>'.$family["familyName"].'</option>';
                          }
                        ?>
                      </select>
                      <?php echo"<script>
                            $( 'select#familyNamex' ).val( '".$member['familyId']."' );
                            $('select#familyNamex').trigger('change');
                        </script>"; ?>  
                    <span class="icon-users form-control-feedback left" aria-hidden="true"></span>
                       
                  </div>
                    <!-- family role Selector -->
                    <div class="col-md-6 col-sm-12 col-xs-12 form-group has-feedback" data-toggle="tooltip" data-placement="top" title="Family Role">  
                      <select id="familyRolex" name="familyRolex" class="form-control has-feedback-left" >
                        <option value=0>Choose role</option>
                        <option value=1>Family head</option>
                        <option value=2>Spouse</option>
                        <option value=3>Child</option>
                       </select>
                      <span class="icon-home form-control-feedback left" aria-hidden="true"></span>
                     <?php echo"<script>
                        $( 'select#familyRolex' ).val( '".$member['familyRole']."' );
                        $('select#familyRolex').trigger('change');
                        </script>"; ?>  
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12" style="background-color: #F2F2F2;height: 250px">
                       <div class="tree">
                        <ul>
                        <?php 
                        if(!empty($familyMembers)){
                          foreach ($familyMembers as $m) 
                            {
                              if($m['familyRole']==1){
                                echo '<li>
                                <a href="#">'.$m["firstName"].'</a><ul>';

                              }elseif($m['familyRole']==2){
                                echo '<li>
                                <a href="#">'.$m["firstName"].'</a>';
                                echo '<ul>';
                              }else{

                                 echo '<li>
                                <a href="#">'.$m["firstName"].'</a></li>';
                              }


                              }
                          }
                              
                          ?>
                          </ul>
                          </ul>
                              </li>
                            </li>
                        </ul>
                       </div>
                     </div>
                   </div>
             


                      <div class="tab-pane" id="Calendar-r">
                        <div id="calendar-container" class="col-md-12 col-sm-12 col-xs-12"></div>
                      </div>
                      <div class="tab-pane" id="settings-r">Coming Soon....</div>
                    </div>
               </div>  

          <div class="col-xs-2">
                    <!-- required for floating -->
                    <!-- Nav tabs -->
              <ul class="nav nav-tabs tabs-right">
                <li class="active"><a href="#home-r" data-toggle="tab">General Info</a>
                </li>
                <li>
                  <a href="#profile-r" data-toggle="tab">Family <span class="badge bg-red">hot</span></a>
                </li>
                <li><a href="#Calendar-r" data-toggle="tab">Calendar</a>
                </li>
                <!--li><a href="#settings-r" data-toggle="tab">Testimony</a>
                </li-->
              </ul>
          </div>
        
     
         <div class="x_footer pull-right" style="margin-top:10px;margin-right:5px;">
            <input id="memberID" type="hidden" value="<?php echo $member['id']; ?>" />
              <button type="button" onclick="closeTab('<?php echo $tabID;?>')" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="submit" onclick="saveUserProfile()" class="btn btn-primary" name="submit" >Save</button>
        </div>
      
     




  
   <!-- tags -->
  <script src="js/tags/jquery.tagsinput.min.js"></script>
   <!-- input tags -->
   

  
  <script>
    function onAddTag(tag) {
      alert("Added a tag: " + tag);
    }

    function onRemoveTag(tag) {
      alert("Removed a tag: " + tag);
    }

    function onChangeTag(input, tag) {
      alert("Changed a tag: " + tag);
    }

     
  </script>
   <script> 
    $(function() {
      $("#loaderImg").hide(); //hide loader image
      $('#tags_').tagsInput({
        width: 'auto'
      });

      

    });
  </script>
 

  <!-- /input tags -->
  
