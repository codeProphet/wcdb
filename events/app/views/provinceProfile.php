<?php
require_once("../config-small.php"); //loads the system configuration settings
session_start();
$user=$_SESSION["wcdbUser"];
$id=$_REQUEST["id"];  
$province=getProvince($id);
$n=$province["nation"];

?>

<script src="js/JsBarcode-master/dist/JsBarcode.all.js"></script>
        
          
              <div class="x_panel">
               
                <div class="x_content">
                    <!-- start of user-activity-graph -->
                    <!--div id="graph_bar" style="width:100%; height:280px;"></div-->
                    <!-- end of user-activity-graph -->

                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true"><i class="icon-list"></i> Province Details</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false"><i class="icon-users"></i> Localities</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false"><i class="icon-calendar"></i> House Churches</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab3" data-toggle="tab" aria-expanded="false"><i class="icon-camera"></i> Disciples</a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content5" role="tab" id="profile-tab4" data-toggle="tab" aria-expanded="false"><i class="icon-wrench"></i> Settings</a>
                        </li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                            <div class="row">
                              <div class="col-md-12">
                                <div class="x_panel">
                                  <div class="x_title">
                                    <h2 class="eventTitle"></h2>
                                    
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="x_content">
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                      <ul class="stats-overview">
                                        <li>
                                          <span class="name"> Registered Attenders </span>
                                          <span class="value text-success" id="registrations"> 2300 </span>
                                        </li>
                                        <li>
                                          <span class="name"> Nations </span>
                                          <span class="value text-success" id="nations"> 2000 </span>
                                        </li>
                                        <li class="hidden-phone">
                                          <span class="name"> Localities </span>
                                          <span class="value text-success" id="localities"> 20 </span>
                                        </li>
                                      </ul>
                                      <!--br />
                                      <div id="mainb" style="height:350px;"></div>
                                      <br>
                                      <div class="clearfix"></div-->
                                      <div class="row">
                                        
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                          <div class="x_panel">
                                            <div class="x_title">
                                              <h2>Top Registrations <small>by locality</small></h2>
                                              <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                                <li class="dropdown">
                                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                  <ul class="dropdown-menu" role="menu">
                                                    <li><a href="#">Localities Report</a>
                                                    </li>
                                                    <li><a href="#">Nations Report</a>
                                                    </li>
                                                  </ul>
                                                </li>
                                                <!--li><a class="close-link"><i class="fa fa-close"></i></a>
                                                </li-->
                                              </ul>
                                              <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content">
                                              <canvas id="mycanvas"></canvas>
                                            </div>
                                          </div>
                                        </div>
          
                                      
                                      </div>


                                    </div>

                                    <!-- start project-detail sidebar -->
                                    <div class="col-md-3 col-sm-3 col-xs-12">

                                      <section class="panel">

                                        <div class="x_title">
                                          <h2>Event Description</h2>
                                          <div class="clearfix"></div>
                                        </div>
                                        <div class="panel-body">
                                          <div class="green"><h3><i class="fa fa-flag"></i> <span class="eventTitle"> World Conquest Event</span></h3></div>

                                          <p class="eventDescription">Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terr.</p>
                                          <br />

                                          <div class="project_detail">

                                            <p class="title">Event Country</p>
                                            <p class="eventCountry">Deveint Inc</p>
                                            <p class="title">Event Coordinator</p>
                                            <p class="eventLeader">Tony Chicken</p>
                                          </div>

                                          <br />
                                          <h5>Event files</h5>
                                          <ul class="list-unstyled project_files">
                                            <li><a href=""><i class="fa fa-file-word-o"></i> Prayer_topics.docx</a>
                                            </li>
                                      
                                          </ul>
                                          <br />

                                          <div class="text-center mtop20">
                                            <a href="#" class="btn btn-sm btn-primary">Add files</a>
                                          </div>
                                        </div>

                                      </section>

                                    </div>
                                    <!-- end project-detail sidebar -->

                                  </div>
                                </div>
                              </div>
                            </div>

                        </div> <!-- Cose Tab Panel -->
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                          <div class="col-md-9 col-sm-9 col-xs-12">
                            <div class="well">
                            <form id="eventRegForm">
                              <input id="eventId" name="eventId" value="<?php echo $id; ?>" type="hidden" />
                              </form>
                           <div class="form-group">
                              <label class="control-label col-md-2 col-sm-2 col-xs-12">Select Member(s)</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="membersDropDown" class="select2_multiple form-control leaders" multiple="multiple" style="width:100%;">
                                  <option value="select2"></option>
                                </select>
                              </div>
                           
                              <button id="btnRegister" onclick="eventRegister()" class="btn btn-sm btn-success"><i class="icon-user-following"></i> Register</button>
                               <span><a href="#"  data-toggle="modal" data-target=".newMember" id="newRegionBtn" class="btn btn-primary btn-sm"><i class="icon-magic-wand"></i> New Member</a></span>
                            </div>
                           </div>  
                          <!-- start member registration table -->
                          <table id="eventsRegTable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>Name</th>
                              <th>Nation</th>
                              <th>Locality</th>
                              <th>Date Registered</th>
                              <th>Actions</th>
                            </tr>
                          </thead>
                        </table>
                          <!-- end member registration table -->
                        </div>

                                  <div class="col-md-3 col-sm-3 col-xs-12 profile_right">


                    <div class="profile_img">

                      <!-- end of image cropping -->
                      <div id="crop-avatar">
                        <!-- Current avatar -->
                        <div id="profileAvatar"  data-toggle="tooltip" data-placement="bottom" title="Change the avatar">
                         <img onclick="showHide('uploadForm')" class="img-responsive avatar-view" src="images/members/<?php echo $member["image"];?>" alt="Avatar" 
                          onerror="if (this.src != 'images/members/user.png') this.src = 'images/members/user.png';"></div>

                       

                        <!-- Loading state -->
                        <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                      </div>
                      <!-- end of image cropping -->

                    </div>
                    <div id="idCard" style="font-family: expertsans; font-weight: 500;">
                    <h3 id="memberName" style="margin-top: 10px;margin-bottom: 10px;font-size: 24px;line-height: 1.1;color:#674172;"></h3>

                    <ul class="list-unstyled user_data" style="list-style: none;padding-left: 0;display: block; margin-block-start:lem;">
                      <li><i class="icon-globe user-profile-icon"></i><span id="memberCountry"> </span>
                      </li>

                      <li>
                        <i class="icon-envelope-letter"></i> <span id="memberEmail"></span>
                      </li>
                      <li>
                        <i class="icon-screen-smartphone"></i> <span id="memberPhone"></span>
                      </li>
                      <li>
                        <i class="icon-direction"></i> <span id="memberAddress"></span>
                      </li>
                      <li>
                        <span id="memberGender"></span>
                      </li>
                      <li>
                        <img id="barcode" height="70px"/>
                      </li>
                    </ul>
                  </div>
                    <!--a class="btn btn-success"><i class="fa fa-edit m-right-xs"></i>Edit Profile</a-->
                    <div class="clearfix"></div>

                    <!-- start tags -->
                    <h4><i class="icon-tag" ></i> Tags</h4>
                    <div id="tags"></div>
                   
                    <br/>
                    <!-- end of tags -->
                    <div class="panel-footer">
                      <span> <a class="btn btn-primary btn-sm" type="button" href="javascript:;" id="print"><i class="icon-printer"></i> print ID</a><a class="btn btn-info btn-sm" type="button"><i class="icon-note"></i> Edit Info</a></span>
                    </div>

                  </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                           <p>Attendance...</p>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
                           <p>Gallery...</p>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="profile-tab">
                           <p>Settings...</p>
                        </div>
                      </div>
                    </div>
                  </div>
                
                </div>
              </div>
            </div>
       
      
	  
  
 <!-- image cropping -->
  <script src="js/cropping/cropper.min.js"></script>
  <script src="js/cropping/main.js"></script>

  <!-- daterangepicker -->
  <script type="text/javascript" src="js/moment/moment.min.js"></script>
  <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>

</script>
<!-- select2 -->
  <script>
    $(document).ready(function() {
     
      //$(".select2_group").select2({});
      $("#membersDropDown").select2({ //create house church leaders drop down
        maximumSelectionLength: 4,
        placeholder: "Max 4",
        allowClear: true
      });

      var n='<?php echo $n;?>';
      //initialise combo for localities
      initialiseLoclaitiesCombo(n)
    });
  </script>

  <script>
    var id=<?php echo $id;?>;
    $(document).ready(function() {
        //provinces table
        var nation = '<?php echo $id; ?>';
        $('#eventsRegTable').DataTable({
          "ajax": {
        "url": "api/funcs.php?fn=fetchEventRegistrations&id="+id,
        "dataSrc": ""
        },
        "columns": [
                  { "data": "name" },
                  { "data": "nation" },
                  { "data": "locality" },
                  { "data": "dateRegistered" },
                  { "data": "actions" }
                ],
          dom: "Bfrtip",
                buttons: [{
                  extend: "copy",
                  className: "btn-sm"
                }, {
                  extend: "csv",
                  className: "btn-sm"
                }, {
                  extend: "excel",
                  className: "btn-sm"
                }, {
                  extend: "pdf",
                  className: "btn-sm"
                }, {
                  extend: "print",
                  className: "btn-sm"
                }
        ]
        });

      });

</script>
  <script type="text/javascript">
    $("#loaderImg").hide(); //hide loader image
    var id=<?php echo $id;?>;
    $.getJSON("api/funcs.php?fn=fetchEvent&id="+id, function(response) {
            $( '.eventTitle' ).html(response.title);
            $('#registrations').html(response.registrations);
            $('#localities').html(response.localities);
            $('#nations').html(response.nations);
            $('#startDate').html(response.start);
            $('#endDate').html(response.end);
            $('.eventDescription').html(response.eventDesc);
            $('.eventCountry').html(response.nation);
            $('#countryx').val(response.nation);
            //$('#mainb').html('<img src="images/events/churchEvent.jpg" width="670px" height="350px" />');
            
            //alert(response.name);
        });

    //populate leaders drop down
      var opt3 = $(".leaders");
          $.getJSON("api/funcs.php?fn=fetchmembers&n=<?php echo $n;?>", function(response) {
               $.each(response, function() {
               opt3.append($("<option />").val(this.id).text(this.firstName+' '+this.lastName));
           });
        });
    
  </script>

  
  <script type="text/javascript">
    $(document).ready(function(){
    $.ajax({
    url: "api/funcs.php?fn=fetchEventLocRegBar&id=<?php echo $id;?>",
    method: "GET",
    success: function(data) {
      console.log(data);
      var locality = [];
      var male = [];
      var female = [];
      var jsonArr = jQuery.parseJSON(data);

      for(var i in jsonArr) {
        locality.push(jsonArr[i].locality);
        male.push(jsonArr[i].male);
        female.push(jsonArr[i].female);
      }
      console.log(locality);

      var chartdata = {
        labels: locality,
        datasets : [
          {
            label: 'Male Members Registered',
            backgroundColor: 'rgba(200, 200, 200, 0.75)',
            borderColor: 'rgba(200, 200, 200, 0.75)',
            hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
            hoverBorderColor: 'rgba(200, 200, 200, 1)',
            data: male
          },
          {
            label: 'Female Members Registered',
            backgroundColor: 'rgba(200, 200, 200, 0.75)',
            borderColor: 'rgba(200, 200, 200, 0.75)',
            hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
            hoverBorderColor: 'rgba(200, 200, 200, 1)',
            data: female
          }
        ]
      };
     Chart.defaults.global.legend = {
      enabled: false
    };
    // Bar chart
    var ctx = document.getElementById("mycanvas");
    var mycanvas = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: locality,
        datasets: [{
          label: 'male members registered',
          backgroundColor: "#26B99A",
          data: male
        },
        {
          label: 'female members registered',
          backgroundColor: "#03586A",
          data: female
        }
        ]
      },

      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
    },
    error: function(data) {
      console.log(data);
    }
    });
      });
  </script>
<script>
   $(document).ready(function(){
     //populate localities drop down
      var x = $("#localitiez");
          $.getJSON("api/funcs.php?fn=fetchLocalities&n=<?php echo $n;?>", function(response) {
               $.each(response, function() {
               x.append($("<option />").val(this.id).text(this.localityName));
           });
        });
    //function to display member card on event registration ref eventProfile  page

  });

  function showMemberCard(id){
  var form="uploadForm";
  var temp = new Array();
  
  $.getJSON("api/funcs.php?fn=fetchMemberDetails&id="+id, function(response) {
        temp = response.tags.split(",");
        //data = $.parseJSON(temp);
        $('#tags').html(' '); //clear previous tags
      $.each(temp, function(i, item) {
          $('#tags').append('<span class="label label-primary" >'+ item +'</span> ');
      });

            $( '#memberName').html(response.firstName+' '+response.lastName);
            $('#memberEmail').html(response.email);
            $('#memberPhone').html(response.phone);
            if(response.sex=="M"){$('#memberGender').html("<i class='icon-symbol-male'></i> Male");}
            else{$('#memberGender').html("<i class='icon-symbol-female'></i> Female");}
            //$('#memberGender').html(response.sex);
            $('#memberAddress').html(response.address+', '+response.surburb+', '+response.city);
            $('#memberCountry').html(response.nation);
            if(response.image==""){$('#profileAvatar').html('<img onclick="showHide('+form+')" class="img-responsive avatar-view" src="images/members/user.png" />'); }
            else{$('#profileAvatar').html('<img onclick="showHide('+form+')" class="img-responsive avatar-view" src="images/members/'+response.image+'"  alt="Avatar" />');
            }
            
            var name=response.firstName+' '+response.lastName;

            JsBarcode("#barcode", name, {
              displayValue:false,
              //fontSize:24,
              lineColor: "#0cc"
            });
            //alert(response.name);
        });
}



function nWin() {
  var w = window.open();
  var html = $("#idCard").html();

    $(w.document.body).html(html);
    w.print();
}

$(function() {
    $("a#print").click(nWin);
});
</script>
 